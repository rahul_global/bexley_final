<?php
include_once('dbopen.php');


if(isset($_REQUEST['ASCorder']))
{
$order = $_REQUEST['ASCorder'];
}
?>
<html>
<head>
<title>bexleysnap</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<?php 
include_once('topscript.php');
?>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
<nav id="nav" role="navigation">
<div class="container">
  
  <div id="navbar" class="navbar-collapse collapse">
    
  <ul class="nav navbar-nav">
      <?php 
      $select_pages = "SELECT * FROM {$tbname}_cmspages WHERE _PID = 0 AND _Topdisplay = 0 AND _Status = 'Active' ORDER BY _Order";
      
	$res_pages = mysqli_query($con, $select_pages);
      
	while ($row_pages = mysqli_fetch_assoc($res_pages)) 
	{
      ?>
        <li class="">
          <?php 
             $id = $row_pages['_ID']; 
             $sql_removespan = mysqli_query($con,"SELECT * FROM ".$tbname."_cmspages WHERE _PID = '".$id."'");
             $row = mysqli_num_rows($sql_removespan);
             if($row > 0 )
             { 
                ?>
                <a href="<?php echo $row_pages['_Url'] ?>?id=<?php echo encrypt($row_pages['_ID'],$encrypt); ?>"  class="dropdown-toggle " data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false"><?php echo $row_pages['_Title'];?><span class="caret"></span></a>
                <?php 
             }else
             { 
                ?> 
                <a href="<?php echo $row_pages['_Url'] ?>?id=<?php echo encrypt($row_pages['_ID'],$encrypt); ?>"  class="dropdown-toggle " aria-haspopup="true" aria-expanded="false"><?php echo $row_pages['_Title'];?></a>
                <?php    
             } 
     	$pid = $row_pages['_ID'];
		
			$child_pages = mysqli_query($con, "SELECT * FROM {$tbname}_cmspages WHERE _PID = '".$pid."' AND _Status= 'Active' ORDER BY _Title" );
			$row = mysqli_num_rows($child_pages);
			if($row > 0)
			{
				?>
				<ul class="dropdown-menu">
				<?php
				while($row_child_pages = mysqli_fetch_assoc($child_pages))
				{
					?>
							<li><a href="<?php echo $row_child_pages['_Url'] ?>?id=<?php echo encrypt($row_child_pages['_ID'],$encrypt); ?>"><?php echo $row_child_pages['_Title']; ?></a></li>
					<?php
				}	
				?>
				</ul>
				<?php					
			}
			
		?>
	  </li>

      <?php
      }
      ?>
      <li><a href="contact.php">Contact Us</a></li>
      </ul>
  </div>
 
</div>
</nav>

	<div id="js-main-content" class="main-content container">
		<section class="section section-content">
			<div class="col-md-12">
				<a href="home.php">Home</a> | Contact
			</div>
			<br>
			<br>
			

			<div class="col-md-12">
				<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2485.409211972728!2d0.1692683033914011!3d51.46900260942142!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sBexley+SNAP+%09Normandy+Children&#39;s+Centre+%09Fairford+Avenue+%09Barnehurst+%09DA7+6QP!5e0!3m2!1sen!2sin!4v1516791895968" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				<br>
				<br>
				<hr>

				<h2><strong>Where</strong></h2>
				<pre style="background-color: white !important">
	Bexley SNAP
	Normandy Children's Centre
	Fairford Avenue
	Barnehurst
	DA7 6QP
				</pre>

				<hr>
				<h2><strong>How</strong></h2>
				<pre style="background-color: white !important">
	<b>Telehpone</b>
	01322 334192

	<b>Email</b>
	<a href="mailto:admin@bexleysnap.org.uk">admin@bexleysnap.org.uk</a>
				</pre>

				<hr>
				<h2>Enquire</h2>
				<br>
				<div class="col-md-12">
					<form method="POST" action="contactaction.php">
						<div class="form-group col-md-12">
							<label for="Contact name">Contact name&nbsp;<span style="color: red">*</span></label>
							<input type="text" class="form-control" name="cntctname" placeholder="Enter Contact name" required>
						</div>
						<div class="form-group col-md-12">
							<label for="Email">Email&nbsp;<span style="color: red">*</span></label>
							<input type="email" class="form-control" name="email" placeholder="Enter Email" required>
						</div>
						<div class="form-group col-md-12">
							<label for="Phone">Phone</label>
							<input type="tel" class="form-control" name="phonr" placeholder="Enter Phone number">
						</div>
						<div class="form-group col-md-12">
							<label for="Enquiry">Enquiry&nbsp;<span style="color: red">*</span></label>
							<textarea name="enquiry" class="form-control" cols="15" rows="15" required></textarea>
						</div>
						<div class="form-group col-md-12">
							<input type="hidden" name="e_action" value="<?php echo encrypt('Enquiry',$encrypt); ?>">
							<input type="submit" class="btn contact_submit" name="submit" value="Submit">
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
<br>
<br>
<br>
<br>

<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script type='text/javascript' src='js/owl.carousel.js'></script>
<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    .contact_submit:hover {
    color: #fff !important;
    background-color: #f7941e !important;;
    border-color: #f7941e !important;
}
    </style>
<style>
    #owl-demo .item{
        display: block;
        cursor: pointer;
        background: #ffd800;
        padding: 30px 0px;
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
    }
    #owl-demo .item:hover{
      background: #F2CD00;
    }
    </style>

<script>
    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel({
        navigation : false,
        singleItem : true,
        transitionStyle : "fade",
		autoPlay : 5000,
		stopOnHover : false
      });

      //Select Transtion Type
      $("#transitionType").change(function(){
        var newValue = $(this).val();

        //TransitionTypes is owlCarousel inner method.
        owl.data("owlCarousel").transitionTypes(newValue);

        //After change type go to next slide
        owl.trigger("owl.next");
      });
    });
    </script> 
<script>
$(document).ready(function(){
    $(".list-accordion-home li").hover(function(){
		
        $(this).toggleClass("active");
    });
	
	 $("#ss").focusin(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','-90px');
    });
	 $("#ss").focusout(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','0px');
    });
	
	 $("#inputEmail").focusin(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','-110px');
    });
	 $("#inputEmail").focusout(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','0px');
    });
	
	/*-------------dropdown navigation---------------*/
	
	$("#nav .primary-nav li").each(function(){
		$(this).click(function(){
			
			var cls = $(this).children("a").siblings("ul").hasClass("is-hidden");
			
			if(cls == true){
				hideall($(this));
				

			$(this).children("a").siblings("ul").removeClass("is-hidden");
			
			if($(this).children("a").children().hasClass("icon-chevron-down"))
			{
				$(this).children("a").children().removeClass("icon-chevron-down");
				$(this).children("a").children().addClass("icon-chevron-up");
			}
			else
			{
				$(this).children("a").children().removeClass("icon-chevron-up");
				$(this).children("a").children().addClass("icon-chevron-down");
			}
			}else{
				$(this).children("a").siblings("ul").addClass("is-hidden");
				$(this).children("a").children().removeClass("icon-chevron-up");
								$(this).children("a").children().addClass("icon-chevron-down");
			
			}
			
			
		});
		
	});
	
	
	
	function hideall(test){
		
		$("#nav .primary-nav li").each(function() {
			if($(this).children("a").children().hasClass("icon-chevron-up"))
			{
				$(this).children("a").children().removeClass("icon-chevron-up");
				$(this).children("a").children().addClass("icon-chevron-down");
			}
			
			$(this).children("ul").addClass("is-hidden");
		});
		
	}
	
	
	/*-------------dropdown navigation---------------*/
	$("#js-nav-toggle").click(function(){
		
        $(this).parents("#header").next().children().children().toggleClass("is-visible");
		
    });
	
$("#nav .primary-nav li a").click(function(){
		
        $(this).siblings("").toggleClass("is-hidden3");
		$(this).children().toggleClass("icon-chevron-up");
    });
	
	$("#nav .primary-nav li ul li:first-child").click(function(){
	$(this).parent().removeClass("is-hidden3");
	
    });
	
	$("#js-search-toggle").click(function(){
		
	$(this).parent().siblings("#js-form-search").slideToggle();
	
    });
	

  $('.video-placeholder').on('click', function(ev) {
	$(this).fadeOut('fast'); 
 
    $("#video")[0].src += "?autoplay=1";
    ev.preventDefault();
	
  
  });

	
});
</script>
</body>
</html>