<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

$eaction = decrypt($_REQUEST['e_action'],$encrypt);
//echo $eaction;exit;

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

if($eaction != 'view' && $eaction != 'edit')
{
	header('location:login.php');
	exit;
}

$btntext 	= "Add";
$bread 		= "Add Children";
$title 		= $sitename." : Add Children";


$carer_qry = "SELECT * FROM ".$tbname."_clientmaster WHERE _Status = '1' ORDER BY _Lastname";
$carer_run = mysqli_query($con,$carer_qry);
$carer_num = mysqli_num_rows($carer_run);

$school_qry = "SELECT * FROM ".$tbname."_schools";
$school_run = mysqli_query($con,$school_qry);
$school_num = mysqli_num_rows($school_run);

if(isset($_GET['e_action']))
{
	$action 	= decrypt($_GET['e_action'],$encrypt);

	if($action == 'edit' || $action == 'view')
	{
		$title 		= $sitename." : Edit Children";
		$id 		= decrypt($_GET['id'],$encrypt);
		$pupil_qry 	= "select * FROM ".$tbname."_pupilmaster where _ID='".$id."'";
		$run_pupil 	= mysqli_query($con,$pupil_qry);
		$num_pupil 	= mysqli_num_rows($run_pupil);
		
		if($num_pupil >	0)
		{
			$fetch_pupil = mysqli_fetch_assoc($run_pupil);
			$bread 		= "Edit Children";
			$btntext 	= 'Update';
			$profile_id = $fetch_pupil['_Profileid'];

            $sqlclient = "select * from ".$tbname."_clipup_rel where _PupilID = '".$id."'";
            $rsclient = mysqli_fetch_assoc(mysqli_query($con,$sqlclient));
            $clientid = $rsclient['_ClientID'];

            $sqlcate = "select * from ".$tbname."_pupcate_rel where _PupilID = '".$id."'";
            $fetchcate = mysqli_fetch_assoc(mysqli_query($con,$sqlcate));
            $categoryid = $fetchcate['_CategoryID'];
		}
		else
		{
			header("location:allpupil.php");
			exit;
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		<?php
		if($action == 'view')
		{ ?>
			 <script type="text/javascript">
			    $(document).ready(function(){
			        $("#edit_pupil_frm :input").prop("disabled", true);
			    });
			</script>
		<?php
		}
		?>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-2">
					<?php include 'user_menu.php'; ?>
				</div>
				<div class="col-md-10">
					<?php if(isset($result) && $result == 'success'){ ?>
	                        <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
	                <?php } else if(isset($result) && $result == 'failed') {?> 
							<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button> <strong>Error!</strong> Error Occurred.</div>
					<?php } ?>
	                <div id="response_msg" class=""></div>
	                <form class="form-horizontal" role="form" name="edit_pupil_frm" id="edit_pupil_frm" method="post" action="pupilaction.php">
	                    <h3 class="heading_a"><span class="heading_text">General info</span></h3>
						<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
						<input type="hidden" name="pupil_id" id="pupil_id" value="<?php echo isset($_GET['id'])?$_GET['id']:''; ?>">
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Family</label>
	                        <div class="col-sm-9">
	                            <select name="carer" id="carer" class="form-control" disabled>
									<option value="">---Select Family---</option>
								<?php 
								if($carer_num > 0)
								{
									while($carer_fetch = mysqli_fetch_assoc($carer_run))
									{
								?>
										<option value="<?php echo $carer_fetch['_ID'];?>" <?php echo (isset($clientid) && $clientid == $carer_fetch['_ID']) ? "SELECTED='SELECTED'" : '';?>><?php echo $carer_fetch['_Lastname']; ?></option>
								<?php 
									}
								}
								?>
								</select>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Child Surname</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtclname" name="txtclname" value="<?php echo isset($fetch_pupil['_LastName'])?$fetch_pupil['_LastName']:'';?>" required>
	                        </div>
							<div class="col-sm-1">
								<span class="required_field">*</span>
							</div>
	                    </div>
	                    <div class="form-group">
	                        <label for="txtfname" class="col-sm-2 control-label">Child First Name</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtcfname" name="txtcfname" value="<?php echo isset($fetch_pupil['_FirstName'])?$fetch_pupil['_FirstName']:'';?>" required>
	                        </div>
							<div class="col-sm-1">
								<span class="required_field">*</span>
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Family Surname<br>(if different)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtfsurname" name="txtfsurname" value="<?php echo isset($fetch_pupil['_FamilySurname'])?$fetch_pupil['_FamilySurname']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Date of Birth</label>
	                        <div class="col-sm-3">
	                            <input type="text" class="form-control" id="txtdob" name="txtdob" value="<?php echo isset($fetch_pupil['_DOB'])?date('d/m/Y',strtotime($fetch_pupil['_DOB'])):'';?>" PLACEHOLDER="dd/mm/yyyy">
	                        </div>
							<label for="txtlname" class="col-sm-2 control-label">Child Ethnicity</label>
	                        <div class="col-sm-4">
								<select class="form-control" id="txtethnicity" name="txtethnicity">
									<option value="">--- Select Ethnicity ---</option>
									<option value="White:  White British" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'White:  White British') ? 'SELECTED="SELECTED"':''; ?>>White:  White British</option>
									<option value="White:  Irish" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'White:  Irish') ? 'SELECTED="SELECTED"':''; ?>>White:  Irish</option>
									<option value="White:  Other" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'White:  Other') ? 'SELECTED="SELECTED"':''; ?>>White:  Other</option>
									<option value="Mixed:  White and Black Caribbean" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Mixed:  White and Black Caribbean') ? 'SELECTED="SELECTED"':''; ?>>Mixed:  White and Black Caribbean</option>
									<option value="Mixed:  White and Black African" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Mixed:  White and Black African') ? 'SELECTED="SELECTED"':''; ?>>Mixed:  White and Black African</option>
									<option value="Mixed:  White and Asian" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Mixed:  White and Asian') ? 'SELECTED="SELECTED"':''; ?>>Mixed:  White and Asian</option>
									<option value="Mixed: Other" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Mixed: Other') ? 'SELECTED="SELECTED"':''; ?>>Mixed: Other</option>
									<option value="Asian or Asian British:  India" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Asian or Asian British:  India') ? 'SELECTED="SELECTED"':''; ?>>Asian or Asian British:  India</option>
									<option value="Asian or Asian British:  Pakistani" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Asian or Asian British:  Pakistani') ? 'SELECTED="SELECTED"':''; ?>>Asian or Asian British:  Pakistani</option>
									<option value="Asian or Asian British:  Bangladeshi" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Asian or Asian British:  Bangladeshi') ? 'SELECTED="SELECTED"':''; ?>>Asian or Asian British:  Bangladeshi</option>
									<option value="Asian or Asian British:  Other" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Asian or Asian British:  Other') ? 'SELECTED="SELECTED"':''; ?>>Asian or Asian British:  Other</option>
									<option value="Black or Black British:  Caribbean" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Black or Black British:  Caribbean') ? 'SELECTED="SELECTED"':''; ?>>Black or Black British:  Caribbean</option>
									<option value="Black or Black British:  African" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Black or Black British:  African') ? 'SELECTED="SELECTED"':''; ?>>Black or Black British:  African</option>
									<option value="Black or Black British:  Other" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Black or Black British:  Other') ? 'SELECTED="SELECTED"':''; ?>>Black or Black British:  Other</option>
									<option value="Chinese" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Chinese') ? 'SELECTED="SELECTED"':''; ?>>Chinese</option>
									<option value="Other Ethnic Group" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Other Ethnic Group') ? 'SELECTED="SELECTED"':''; ?>>Other Ethnic Group</option>
									<option value="Not disclosed/unknown" <?php echo (isset($fetch_pupil['_Ethnicity']) && $fetch_pupil['_Ethnicity'] == 'Not disclosed/unknown') ? 'SELECTED="SELECTED"':''; ?>>Not disclosed/unknown</option>

								</select>
							</div>
							<div class="col-sm-1">
							</div>
	                    </div>
						
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Gender</label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtgender" value="Male" <?php echo isset($fetch_pupil['_Gender']) && $fetch_pupil['_Gender'] == 'Male'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Male
								&nbsp; &nbsp;
								<input type="radio" class="" name="txtgender" value="Female" <?php echo isset($fetch_pupil['_Gender']) && $fetch_pupil['_Gender'] == 'Female'?"checked='checked'":'';?>> Female
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">School Name</label>
	                        <div class="col-sm-9">
								<select class="form-control" id="txtschool" name="txtschool">
									<option value="">--- Select School ---</option>
								<?php
								if($school_num > 0)
								{
									while($school_fetch = mysqli_fetch_assoc($school_run))
									{
									?>
										<option value="<?php echo isset($school_fetch['_ID']);?>" <?php echo (isset($fetch_pupil['_School']) && $fetch_pupil['_School'] == $school_fetch['_ID']) ? 'SELECTED="SELECTED"':''; ?>><?php echo $school_fetch['_Name'];?></option>
									<?php
									}
								}
								?>
								</select>
	                           
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Daily or Residential</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtresidential" name="txtresidential" value="<?php echo isset($fetch_pupil['_Residential'])?$fetch_pupil['_Residential']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Parent or Carer<br>(Please state)</label>
	                        <div class="col-sm-9">
	                            <?php /* ?><input type="text" class="form-control" id="txtparent" name="txtparent" value="<?php echo isset($fetch_pupil['_Carer'])?$fetch_pupil['_Carer']:'';?>"><?php */ ?>
								<input type="radio" class="" name="txtparent" value="Parent" <?php echo isset($fetch_pupil['_Carer']) && $fetch_pupil['_Carer'] == 'Parent'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Parent
								&nbsp; &nbsp;
								<input type="radio" class="" name="txtparent" value="Carer" <?php echo isset($fetch_pupil['_Carer']) && $fetch_pupil['_Carer'] == 'Carer'?"checked='checked'":'';?>> Carer
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="txttype" class="col-sm-2 control-label">Club Profile Id</label>
	                        <div class="col-sm-9">
	                            <select class="form-control" id="profile_id" name="profile_id">
	                                <option value="0">Select Club Profile Id</option>
	                                <option <?php if($profile_id == 1) { echo 'selected';} ?> value="1">Red</option>
	                                <option <?php if($profile_id == 2) { echo 'selected';} ?> value="2">Blue</option>
	                                <option <?php if($profile_id == 3) { echo 'selected';} ?> value="3">Green</option>
	                            </select>
	                        </div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Title</label>
	                        <div class="col-sm-9">
	                            <select class="form-control" id="txttitle" name="txttitle">
									<option value="mr" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'mr')?"selected='selected'":'';?>>Mr</option>
									<option value="mrs" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'mrs')?"selected='selected'":'';?>>Mrs</option>
									<option value="ms" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'ms')?"selected='selected'":'';?>>Ms</option>
									<option value="miss" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'miss')?"selected='selected'":'';?>>Miss</option>
									<option value="mrmrs" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'mrmrs')?"selected='selected'":'';?>>Mr & Mrs</option>
									<option value="Dr" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'Dr')?"selected='selected'":'';?>>Dr</option>
									<option value="Revd" <?php echo (isset($fetch_pupil['_CarerTitle']) && $fetch_pupil['_CarerTitle'] == 'Revd')?"selected='selected'":'';?>>Revd</option>
								</select>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
	                    <div class="form-group">
	                        <label for="txtfname" class="col-sm-2 control-label">First Name</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtfname" name="txtfname" value="<?php echo isset($fetch_pupil['_CarerFirstName'])?$fetch_pupil['_CarerFirstName']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtlname" class="col-sm-2 control-label">Surname</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtlname" name="txtlname" value="<?php echo isset($fetch_pupil['_CarerLastName'])?$fetch_pupil['_CarerLastName']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Telephone</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtphone" name="txtphone" value="<?php echo isset($fetch_pupil['_CarerPhone'])?$fetch_pupil['_CarerPhone']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Mobile</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtmobile" name="txtmobile" value="<?php echo isset($fetch_pupil['_CarerMobile'])?$fetch_pupil['_CarerMobile']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">Email</label>
	                        <div class="col-sm-9">
	                            <input type="email" class="form-control" id="txtemail" name="txtemail" value="<?php echo isset($fetch_pupil['_Email'])?$fetch_pupil['_Email']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Alternative Emergency Contact Name </label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtemergname" name="txtemergname" value="<?php echo isset($fetch_pupil['_EmerContactName'])?$fetch_pupil['_EmerContactName']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Alternative Emergency Contact Number </label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtemergnum" name="txtemergnum" value="<?php echo isset($fetch_pupil['_EmerContactNumber'])?$fetch_pupil['_EmerContactNumber']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I  am a looked after child or have needs and risks subject to a Safeguarding plan, risk assessment</label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtriscassess" value="yes" <?php echo isset($fetch_pupil['_RiscAssessment']) && $fetch_pupil['_RiscAssessment'] == 'yes'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Yes
								<br>
								<input type="radio" class="" name="txtriscassess" value="no" <?php echo isset($fetch_pupil['_RiscAssessment']) && $fetch_pupil['_RiscAssessment'] == 'no'?"checked='checked'":'';?>> No
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I  am registered with the Disabled Children’s Service</label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtcservice" value="yes" <?php echo isset($fetch_pupil['_DisableService']) && $fetch_pupil['_DisableService'] == 'yes'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Yes
								<br>
								<input type="radio" class="" name="txtcservice" value="no" <?php echo isset($fetch_pupil['_DisableService']) && $fetch_pupil['_DisableService'] == 'no'?"checked='checked'":'';?>> No
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Social Worker Name </label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtcsworkername" name="txtcsworkername" value="<?php echo isset($fetch_pupil['_DSSocialName'])?$fetch_pupil['_DSSocialName']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Contact Number </label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtcssocialnum" name="txtcssocialnum" value="<?php echo isset($fetch_pupil['_DSSocialContact'])?$fetch_pupil['_DSSocialContact']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I have a statement of Special Educational Needs or Education, Health and Care plan </label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtstatement" value="yes" <?php echo isset($fetch_pupil['_Statement']) && $fetch_pupil['_Statement'] == 'yes'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Yes
								<br>
								<input type="radio" class="" name="txtstatement" value="no" <?php echo isset($fetch_pupil['_Statement']) && $fetch_pupil['_Statement'] == 'no'?"checked='checked'":'';?>> No
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Number of recommended hours  </label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtnumhr" name="txtnumhr" value="<?php echo isset($fetch_pupil['_RecHours'])?$fetch_pupil['_RecHours']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I receive a personal budget or Direct Payments from London Borough of Bexley</label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtborough" value="yes" <?php echo isset($fetch_pupil['_ReceiveBudget']) && $fetch_pupil['_ReceiveBudget'] == 'yes'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Yes
								<br>
								<input type="radio" class="" name="txtborough" value="no" <?php echo isset($fetch_pupil['_ReceiveBudget']) && $fetch_pupil['_ReceiveBudget'] == 'no'?"checked='checked'":'';?>> No
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I like to be called (please use other names by which your child likes to be called)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtnicname" name="txtnicname" value="<?php echo isset($fetch_pupil['_NicName'])?$fetch_pupil['_NicName']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">My diagnosis is (please list all)</label>
	                        <div class="col-sm-9">
	                            <!-- <input type="text" class="form-control" id="txtdiagnosis" name="txtdiagnosis" value="<?php echo isset($fetch_pupil['_MyDiagnosis'])?$fetch_pupil['_MyDiagnosis']:'';?>"> -->
	                            <select class="form-control" id="txtdiagnosis" name="txtdiagnosis">
									<option value="">--- Select Diagnosis ---</option>
									<option value="ADHD" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'ADHD') ? 'SELECTED="SELECTED"':''; ?>>ADHD</option>
									<option value="Achondroplasia " <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Achondroplasia ') ? 'SELECTED="SELECTED"':''; ?>>Achondroplasia </option>
									<option value="Arthogryposis" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Arthogryposis') ? 'SELECTED="SELECTED"':''; ?>>Arthogryposis</option>
									<option value="Arthritus" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Arthritus') ? 'SELECTED="SELECTED"':''; ?>>Arthritus</option>
									<option value="Aspergers" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Aspergers') ? 'SELECTED="SELECTED"':''; ?>>Aspergers</option>
									<option value="Autism" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Autism') ? 'SELECTED="SELECTED"':''; ?>>Autism</option>
									<option value="Bilateral Anophthalmia" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Bilateral Anophthalmia') ? 'SELECTED="SELECTED"':''; ?>>Bilateral Anophthalmia</option>
									<option value="Cerebral Palsy" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Cerebral Palsy') ? 'SELECTED="SELECTED"':''; ?>>Cerebral Palsy</option>
									<option value="Challenging Behaviour" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Challenging Behaviour') ? 'SELECTED="SELECTED"':''; ?>>Challenging Behaviour</option>
									<option value="Chrones Disease" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Chrones Disease') ? 'SELECTED="SELECTED"':''; ?>>Chrones Disease</option>
									<option value="Complex Needs" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Complex Needs') ? 'SELECTED="SELECTED"':''; ?>>Complex Needs</option>
									<option value="Downs Syndrome" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Downs Syndrome') ? 'SELECTED="SELECTED"':''; ?>>Downs Syndrome</option>
									<option value="Dyspraxia/Developmental Coordination Difficulties" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Dyspraxia/Developmental Coordination Difficulties') ? 'SELECTED="SELECTED"':''; ?>>Dyspraxia/Developmental Coordination Difficulties</option>
									<option value="Dyshasia" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Dyshasia') ? 'SELECTED="SELECTED"':''; ?>>Dyshasia</option>
									<option value="Emotional Behavioural Difficulties" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Emotional Behavioural Difficulties') ? 'SELECTED="SELECTED"':''; ?>>Emotional Behavioural Difficulties</option>
									<option value="Dyslexia/Specific Learning Difficulties" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Dyslexia/Specific Learning Difficulties') ? 'SELECTED="SELECTED"':''; ?>>Dyslexia/Specific Learning Difficulties</option>
									<option value="Epilepsy" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Epilepsy') ? 'SELECTED="SELECTED"':''; ?>>Epilepsy</option>
									<option value="Hearing Impairment" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Hearing Impairment') ? 'SELECTED="SELECTED"':''; ?>>Hearing Impairment</option>
									<option value="Hypermobility" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Hypermobility') ? 'SELECTED="SELECTED"':''; ?>>Hypermobility</option>
									<option value="Global Development Delay" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Global Development Delay') ? 'SELECTED="SELECTED"':''; ?>>Global Development Delay</option>
									<option value="Microcephaly" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Microcephaly') ? 'SELECTED="SELECTED"':''; ?>>Microcephaly</option>
									<option value="Mitochondrial Disorder" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Mitochondrial Disorder') ? 'SELECTED="SELECTED"':''; ?>>Mitochondrial Disorder</option>
									<option value="Muscular Dystrophy" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Muscular Dystrophy') ? 'SELECTED="SELECTED"':''; ?>>Muscular Dystrophy</option>
									<option value="Learning Difficulties" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Learning Difficulties') ? 'SELECTED="SELECTED"':''; ?>>Learning Difficulties</option>
									<option value="Obsessive Compulsive Disorder" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Obsessive Compulsive Disorder') ? 'SELECTED="SELECTED"':''; ?>>Obsessive Compulsive Disorder</option>
									<option value="Oppositional Defiant Disorder" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Oppositional Defiant Disorder') ? 'SELECTED="SELECTED"':''; ?>>Oppositional Defiant Disorder</option>
									<option value="Other (please state)" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Other (please state)') ? 'SELECTED="SELECTED"':''; ?>>Other (please state)</option>
									<option value="Physical Impairment" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Physical Impairment') ? 'SELECTED="SELECTED"':''; ?>>Physical Impairment</option>
									<option value="Rare Syndromes" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Rare Syndromes') ? 'SELECTED="SELECTED"':''; ?>>Rare Syndromes</option>
									<option value="Sensory Impairment" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Sensory Impairment') ? 'SELECTED="SELECTED"':''; ?>>Sensory Impairment</option>
									<option value="Speech & Language" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Speech & Language') ? 'SELECTED="SELECTED"':''; ?>>Speech & Language</option>
									<option value="Undiagnosed" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Undiagnosed') ? 'SELECTED="SELECTED"':''; ?>>Undiagnosed</option>
									<option value="Visual Impairment" <?php echo (isset($fetch_pupil['_MyDiagnosis']) && $fetch_pupil['_MyDiagnosis'] == 'Visual Impairment') ? 'SELECTED="SELECTED"':''; ?>>Visual Impairment</option>


								</select>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I communicate by (e.g. verbal, PECS / symbols)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtcommunicateby" name="txtcommunicateby" value="<?php echo isset($fetch_pupil['_CommunicateBy'])?$fetch_pupil['_CommunicateBy']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">My mobility is (include if your child is able to swim)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtmobility" name="txtmobility" value="<?php echo isset($fetch_pupil['_MyMobility'])?$fetch_pupil['_MyMobility']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I have Medical needs (including tube feeding)</label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtmedicalneeds" value="yes" <?php echo isset($fetch_pupil['_MedicalNeeds']) && $fetch_pupil['_MedicalNeeds'] == 'yes'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Yes
								<br>
								<input type="radio" class="" name="txtmedicalneeds" value="no" <?php echo isset($fetch_pupil['_MedicalNeeds']) && $fetch_pupil['_MedicalNeeds'] == 'no'?"checked='checked'":'';?>> No
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Please indicate if your child needs regular medication and for what –</label>
	                        <div class="col-sm-9">
								 <input type="text" class="form-control" name="txtmedicalindicate" id="txtmedicalindicate" value="<?php echo isset($fetch_pupil['_MedicationFor'])?$fetch_pupil['_MedicationFor']:'';?>"><br>you will need to complete a ‘green’ Medication Consent Form if they require medication whilst attending a leisure scheme. <br>
	All medications must be in their original packaging with the child/young person’s name clearly marked on them.
	                        </div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I have Personal Care needs </label>
	                        <div class="col-sm-9">
	                            <input type="radio" class="" name="txtpersonalneeds" value="yes" <?php echo isset($fetch_pupil['_PersonalCareNeed']) && $fetch_pupil['_PersonalCareNeed'] == 'yes'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Yes
								<br>
								<input type="radio" class="" name="txtpersonalneeds" value="no" <?php echo isset($fetch_pupil['_PersonalCareNeed']) && $fetch_pupil['_PersonalCareNeed'] == 'no'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> No
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Please indicate if your child needs regular support with personal care and to what level –</label>
	                        <div class="col-sm-9">
								 <input type="text" class="form-control" name="txtpersonalindicate" id="txtpersonalindicate" value="<?php echo isset($fetch_pupil['_PersonalCareFor'])?$fetch_pupil['_PersonalCareFor']:'';?>"><br>You will need to provide appropriate changes of clothes, nappies, pads, wipes. Without these items you may be contact to collect your child if we cannot meet their personal care needs
	                        </div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">This means I may do (please indicate particular behaviours of your child)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildbehave" name="txtchildbehave" value="<?php echo isset($fetch_pupil['_Behaviour'])?$fetch_pupil['_Behaviour']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">When I do this, it means I am anxious (please indicate signs your child displays when getting anxious or distressed)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildanxious" name="txtchildanxious" value="<?php echo isset($fetch_pupil['_WhenAnxious'])?$fetch_pupil['_WhenAnxious']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">When I do this, it means I am happy (please indicate behaviour displayed)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildhappy" name="txtchildhappy" value="<?php echo isset($fetch_pupil['_WhenHappy'])?$fetch_pupil['_WhenHappy']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I like to (please indicate activities your child/young person likes to do)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildactivity" name="txtchildactivity" value="<?php echo isset($fetch_pupil['_LikedActivities'])?$fetch_pupil['_LikedActivities']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I do not like to (please indicate activities your child/young person does not like to do)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchilddislike" name="txtchilddislike" value="<?php echo isset($fetch_pupil['_DislikedActivities'])?$fetch_pupil['_DislikedActivities']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I like to be calmed by (please indicate how we can calm your child if distressed or angry)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildcalm" name="txtchildcalm" value="<?php echo isset($fetch_pupil['_CalmBy'])?$fetch_pupil['_CalmBy']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I like to eat/drink (please indicate which food/drink)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildfood" name="txtchildfood" value="<?php echo isset($fetch_pupil['_LikedFood'])?$fetch_pupil['_LikedFood']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I do not like / I cannot have to eat/drink (please indicate which food /drink)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchilddislikefood" name="txtchilddislikefood" value="<?php echo isset($fetch_pupil['_DislikedFood'])?$fetch_pupil['_DislikedFood']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I am allergic to (Please include food allergies)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildallergic" name="txtchildallergic" value="<?php echo isset($fetch_pupil['_Allergic'])?$fetch_pupil['_Allergic']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Other things you should know about me</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildabout" name="txtchildabout" value="<?php echo isset($fetch_pupil['_AboutMe'])?$fetch_pupil['_AboutMe']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">GPs Contact Details (GP Address and contact number to be used in an emergency)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildgpcontact" name="txtchildgpcontact" value="<?php echo isset($fetch_pupil['_GPContact'])?$fetch_pupil['_GPContact']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Hospital Consultant (Name of Hospital and contact number to be used in an emergency)</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtchildhospital" name="txtchildhospital" value="<?php echo isset($fetch_pupil['_HospitalConsultant'])?$fetch_pupil['_HospitalConsultant']:'';?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Permission for outings with SNAP local parks,  shops facilities, community</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildpermition" name="txtchildpermition" value="yes" <?php echo isset($fetch_pupil['_OutingPermitted']) && $fetch_pupil['_OutingPermitted'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Take photos / video for external/publicity website flyers, advertising and presentations</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildpublicity" name="txtchildpublicity" value="yes" <?php echo isset($fetch_pupil['_AdvertisePermitted']) && $fetch_pupil['_AdvertisePermitted'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Take a photo to add to  completed passport</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildtakepic" name="txtchildtakepic" value="yes" <?php echo isset($fetch_pupil['_CompletedPassport']) && $fetch_pupil['_CompletedPassport'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Apply sun cream as required</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildsuncream" name="txtchildsuncream" value="yes" <?php echo isset($fetch_pupil['_SunCream']) && $fetch_pupil['_SunCream'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Emergency escort in ambulance</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildescort" name="txtchildescort" value="yes" <?php echo isset($fetch_pupil['_EmergencyEscort']) && $fetch_pupil['_EmergencyEscort'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">Give Emergency 1st Aid</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                            	<input type="checkbox" class="form-control" id="txtchildaid" name="txtchildaid" value="yes" <?php echo isset($fetch_pupil['_EmergencyAid']) && $fetch_pupil['_EmergencyAid'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">I am able to swim</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildswim" name="txtchildswim" value="yes" <?php echo isset($fetch_pupil['_AbleSwim']) && $fetch_pupil['_AbleSwim'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtphone" class="col-sm-2 control-label">happy to get in a swimming pool</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtchildgetswim" name="txtchildgetswim" value="yes" <?php echo isset($fetch_pupil['_GetPool']) && $fetch_pupil['_GetPool'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
					    
	                    <h3 class="heading_a"><span class="heading_text">Services, Signposting and Goal setting</span></h3>
						

	                    <div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">what service would you like for you or your child</label>
	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="txtextraser" name="txtextraser" value="<?php echo $fetch_pupil['_Extraservice']; ?>">
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">I receive a personal Budget from LBB</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtbudget" name="txtbudget" value="yes" <?php echo isset($fetch_pupil['_ReceiveBudgetLBB']) && $fetch_pupil['_ReceiveBudgetLBB'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">I receive Disability Living Allowance</label>
	                        <div class="col-sm-9 checkbox">
	                        	<label>
	                        		<input type="checkbox" class="form-control" id="txtallowance" name="txtallowance" value="yes" <?php echo isset($fetch_pupil['_DisabilityAllowance']) && $fetch_pupil['_DisabilityAllowance'] == 'yes'?"checked='checked'":'';?>>
	                        	</label>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">Why I need or want to access SNAP services</label>
	                        <div class="col-sm-9">
	                            <!--<input type="text" class="form-control" id="txtsnap" name="txtsnap" value="<?php echo isset($fetch_pupil['_WhyAccessSNAP'])?$fetch_pupil['_WhyAccessSNAP']:'';?>">-->
								<textarea name="txtsnap" class="form-control" id="txtsnap" row="5"><?php echo isset($fetch_pupil['_WhyAccessSNAP'])?$fetch_pupil['_WhyAccessSNAP']:'';?></textarea>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">Positive things I want to achieve as a result of attending</label>
	                        <div class="col-sm-9">
	                            <!--<input type="text" class="form-control" id="txtpositive" name="txtpositive" value="<?php echo isset($fetch_pupil['_WantAchieve'])?$fetch_pupil['_WantAchieve']:'';?>">-->
								<textarea name="txtpositive" class="form-control" id="txtpositive" row="5"><?php echo isset($fetch_pupil['_WantAchieve'])?$fetch_pupil['_WantAchieve']:'';?></textarea>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>
						<div class="form-group">
	                        <label for="txtemail" class="col-sm-2 control-label">Any other information (referred services)</label>
	                        <div class="col-sm-9">
	                            <!--<input type="text" class="form-control" id="txtinfo" name="txtinfo" value="<?php echo isset($fetch_pupil['_OtherInfo'])?$fetch_pupil['_OtherInfo']:'';?>">-->
								<textarea name="txtinfo" class="form-control" id="txtinfo" row="5"><?php echo isset($fetch_pupil['_OtherInfo'])?$fetch_pupil['_OtherInfo']:'';?></textarea>
	                        </div>
							<div class="col-sm-1">
							</div>
	                    </div>

	                    <hr/>
	                    <div class="form-group">
	                        <div class="col-sm-10 col-sm-offset-2">

	                        <?php
							if($action == 'view')
							{ ?>
								 <a href="view_children.php" class="btn btn-default">Back</a>
							<?php
							}
							else
							{
							?>
								<input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
	                            <button class="btn-default btn" id="btncancle" onclick='window.location ="view_children.php";return false;'>Cancel</button>
	                        <?php
							}
							?>

	                            
	                        </div>
	                    </div>
	                </form>
	            </div>
			</div>


		</div>
	</div>
	
	<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>

<script type="text/javascript">
		$(function() {
			var date = Date();
			$('.time').timepicker({ 'timeFormat': 'H:i' });
			$('#txtdob').datepicker({endDate: date, format: 'dd/mm/yyyy' });
		})
</script>
<script>
	function fn_print(id){
		//var divToPrint = document.getElementById('edit_pupil_frm');
		var popupWin = window.open('print_childrendtl.php?id='+id, '', 'width=1024,height=768');
		//popupWin.print();
		//popupWin.close();
	}
</script>