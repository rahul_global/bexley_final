<?php
include_once('dbopen.php');

if(isset($_REQUEST['ASCorder']))
{
$order = $_REQUEST['ASCorder'];
}
?>
<nav id="nav" role="navigation">
<div class="container">
  
  <div id="navbar" class="navbar-collapse collapse">
    
  <ul class="nav navbar-nav">
      <?php 
      $select_pages = "SELECT * FROM {$tbname}_cmspages WHERE _PID = 0 AND _Topdisplay = 0 AND _Status = 'Active' AND IS_Club = '0' ORDER BY _Order";
      
  $res_pages = mysqli_query($con, $select_pages);
      
  while ($row_pages = mysqli_fetch_assoc($res_pages)) 
  {
      ?>
        <li class="dropdown men ">
          <?php 
             $id = $row_pages['_ID']; 
             $sql_removespan = mysqli_query($con,"SELECT * FROM ".$tbname."_cmspages WHERE _PID = '".$id."'  ORDER BY _Order ");
             $row = mysqli_num_rows($sql_removespan);
             if($row > 0 )
             { 
                
                ?>

                <a href="<?php echo $row_pages['_Url'] ?>?id=<?php echo encrypt($row_pages['_ID'],$encrypt); ?>" aria-haspopup="true" aria-expanded="false"><?php echo $row_pages['_Title'];?><span class="caret"></span></a>
                <?php 
             }else
             { 
                if($row_pages['_Title'] == 'Home')
                {
                  ?>
                  <li><a href="index.php">Home</a></li>
                  <?php
                }else
                {
                    ?> 
                  <a href="<?php echo $row_pages['_Url'] ?>?id=<?php echo encrypt($row_pages['_ID'],$encrypt); ?>"  class="dropdown-toggle " aria-haspopup="true" aria-expanded="false"><?php echo $row_pages['_Title'];?></a>
                  <?php
                }
                    
             } 
      $pid = $row_pages['_ID'];
    
      $child_pages = mysqli_query($con, "SELECT * FROM {$tbname}_cmspages WHERE _PID = '".$pid."' AND _Status= 'Active'  ORDER BY _Order" );
      $row = mysqli_num_rows($child_pages);
      if($row > 0)
      {
        ?>
        <ul class="dropdown-menu">
        <?php
        while($row_child_pages = mysqli_fetch_assoc($child_pages))
        {
          ?>
              <li class="dropdown-submenu men " >
                <?php 
                 $third_lvl_pid = $row_child_pages['_ID'];
                 $sql_2ndlvl_removespan = mysqli_query($con, "SELECT * FROM ".$tbname."_cmspages WHERE _PID = '".$third_lvl_pid."' AND _Status= 'Active'  ORDER BY _Order"); 
                 $row_2nd_lvl = mysqli_num_rows($sql_2ndlvl_removespan);
                 
                 if($row_2nd_lvl > 0)
                 {

                    ?>
                    <a  class="dropdown-toggle" href="<?php echo $row_child_pages['_Url'] ?>?id=<?php echo encrypt($row_child_pages['_ID'],$encrypt); ?>"  ><?php echo $row_child_pages['_Title']; ?><span class="caret"></span></a>
                   
                        <?php
                          $third_lvl_pages = mysqli_query($con,"SELECT * FROM {$tbname}_cmspages WHERE _PID = '".$third_lvl_pid."' AND _Status= 'Active'  ORDER BY _Order");
                          ?>
                        <ul class="dropdown-menu">
                          <?php
                          while($third_lvl_cms = mysqli_fetch_assoc($third_lvl_pages))
                          {
                              ?><li>
                                  <a href="<?php echo $third_lvl_cms['_Url'] ?>?id=<?php echo encrypt($third_lvl_cms['_ID'],$encrypt); ?>"><?php echo $third_lvl_cms['_Title']; ?></a>
                                </li>
                              <?php    
                          }
                        ?>
                        </ul>
                    <?php
                 }else
                 {
                    ?>
                      <a href="<?php echo $row_child_pages['_Url'] ?>?id=<?php echo encrypt($row_child_pages['_ID'],$encrypt); ?>"><?php echo $row_child_pages['_Title']; ?></a>
                    <?php
                 }
                ?>
              </li>
          <?php
        } 
        ?>
        </ul>
        <?php         
      }
      
    ?>
    </li>

      <?php
      }
      ?>
      <li><a href="contact.php">Contact Us</a></li>
	  <li><a target="_blank" href="https://uk.virginmoneygiving.com/charity-web/charity/finalCharityHomepage.action?charityId=1001927">Donate Now</a></li>
	  

	  
	  
	  
      <!-- <li class="active dropdown"><a href="#"  class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Who We are <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="http://techcitywebdesign.co.uk/Bexley/inner.php">About Snap</a></li>
          <li><a href="#">Meet our team</a></li>
          <li><a href="#">Get involved</a></li>
        </ul>
      </li> -->
  <!--<li class=" dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">What's going on <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Clubs</a></li>
          <li><a href="#">Family Support</a></li>
          <li><a href="#">Special Events</a></li>
          <li><a href="#">Gallery</a></li>
        </ul>
      </li>
  <li><a href="#">Fundraising & Giving</a></li>
  <li><a href="#">Bookings</a> </li>
  <li><a href="inner.php">FAQ</a></li>
      <!--<li><a href="#">What we do</a> </li>
      
      
      <li><a href="#">Team Bexley Snap</a> </li>
      
      <li><a href="#">Information Guidance</a></li>-->
  
    </ul>
  </div>
  <!--<ul id="js-primary-nav" class="primary-nav">
    <li class="menu-8 first has-children"> <a class="js-mega-toggle" href="#" title="Who we are">Who&thinsp;we&thinsp;are<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
  
  
      <ul class="is-hidden columns-6">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="Who we are">Who we are <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="About">About</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="About"><span class="icon icon-chevron-left" aria-hidden="true"></span> About</a></li>
            <li class="see-all"><a href="#" title="Who we are">Who we are</a></li>
            <li><a href="#" title="Mission, goals &#038; values">Mission, goals &#038; values</a></li>
            <li><a href="#" title="Our story">Our story</a></li>
            <li><a href="#" title="Challenging perspectives">Challenging perspectives</a></li>
          </ul>
        </li>
        <li><a href="#" title="Trustees">Trustees</a></li>
        <li><a href="#" title="The Ambassadors">The Ambassadors</a></li>
        <li><a href="#" title="Senior Management">Senior Management</a></li>
        <li><a href="#" title="The families">The families</a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Business Club">Business Club</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Business Club"><span class="icon icon-chevron-left" aria-hidden="true"></span> Business Club</a></li>
            <li class="see-all"><a href="#" title="Who we are">Who we are</a></li>
            <li><a href="#" title="Business club members">Business club members</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="menu-10 has-children"> <a class="js-mega-toggle" href="#" title="What we do">What&thinsp;we&thinsp;do<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
      <ul class="is-hidden columns-6">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="What we do">What we do <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Preschool 2 – 5 years">Preschool 2 – 5 years</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Preschool 2 – 5 years"><span class="icon icon-chevron-left" aria-hidden="true"></span> Preschool 2 – 5 years</a></li>
            <li class="see-all"><a href="#" title="What we do">What we do</a></li>
            <li><a href="#" title="Farnham">Farnham</a></li>
            <li><a href="#" title="Guildford">Guildford</a></li>
          </ul>
        </li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Playschemes 4 – 12 years">Playschemes 4 – 12 years</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Playschemes 4 – 12 years"><span class="icon icon-chevron-left" aria-hidden="true"></span> Playschemes 4 – 12 years</a></li>
            <li class="see-all"><a href="#" title="What we do">What we do</a></li>
            <li><a href="#" title="Alton">Alton</a></li>
            <li><a href="#" title="Basingstoke">Basingstoke</a></li>
            <li><a href="#" title="Eastleigh">Eastleigh</a></li>
            <li><a href="#" title="Epsom">Epsom</a></li>
            <li><a href="#" title="Farnham">Farnham</a></li>
            <li><a href="#" title="Godstone">Godstone</a></li>
            <li><a href="#" title="Guildford">Guildford</a></li>
            <li><a href="#" title="Leatherhead">Leatherhead</a></li>
            <li><a href="#" title="Family Days">Family Days</a></li>
          </ul>
        </li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Youthschemes 13 – 18 years">Youthschemes 13 – 18 years</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Youthschemes 13 – 18 years"><span class="icon icon-chevron-left" aria-hidden="true"></span> Youthschemes 13 – 18 years</a></li>
            <li class="see-all"><a href="#" title="What we do">What we do</a></li>
            <li><a href="#" title="Andover">Andover</a></li>
            <li><a href="#" title="Basingstoke">Basingstoke</a></li>
            <li><a href="#" title="Bookham">Bookham</a></li>
            <li><a href="#" title="Eastleigh">Eastleigh</a></li>
            <li><a href="#" title="Epsom">Epsom</a></li>
            <li><a href="#" title="Farnham">Farnham</a></li>
            <li><a href="#" title="Guildford">Guildford</a></li>
            <li><a href="#" title="Merstham">Merstham</a></li>
            <li><a href="#" title="Petersfield">Petersfield</a></li>
            <li><a href="#" title="Reading">Reading</a></li>
            <li><a href="#" title="Activity Overnighters <br> 13 – 17 years">Activity Overnighters <br>
              13 – 17 years</a></li>
          </ul>
        </li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Young Adult Schemes 18 – 25 years">Young Adult Schemes 18 – 25 years</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Young Adult Schemes 18 – 25 years"><span class="icon icon-chevron-left" aria-hidden="true"></span> Young Adult Schemes 18 – 25 years</a></li>
            <li class="see-all"><a href="#" title="What we do">What we do</a></li>
            <li><a href="#" title="Basingstoke">Basingstoke</a></li>
            <li><a href="#" title="Epsom">Epsom</a></li>
            <li><a href="#" title="Farnham">Farnham</a></li>
            <li><a href="#" title="Guildford">Guildford</a></li>
          </ul>
        </li>
        <li><a href="#" title="Hire our centres">Hire our centres</a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Swim">Swim</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="" title="Swim"><span class="icon icon-chevron-left" aria-hidden="true"></span> Swim</a></li>
            <li class="see-all"><a href="#" title="What we do">What we do</a></li>
            <li><a href="#" title="Fun Swim">Fun Swim</a></li>
            <li><a href="#" title="Swim club">Swim club</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="menu-12 has-children"> <a class="js-mega-toggle" href="#" title="Fundraising &#038; Giving">Fundraising&thinsp;&#038;&thinsp;Giving<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
      <ul class="is-hidden columns-3">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="Fundraising &#038; Giving">Fundraising &#038; Giving <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Communities <br>getting involved">Communities <br>
          getting involved</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="" title="Communities <br>getting involved"><span class="icon icon-chevron-left" aria-hidden="true"></span> Communities <br>
              getting involved</a></li>
            <li class="see-all"><a href="" title="Fundraising &#038; Giving">Fundraising &#038; Giving</a></li>
            <li><a href="#" title="Inspiration &#038; Ideas">Inspiration &#038; Ideas</a></li>
            <li><a href="#" title="Organise an event">Organise an event</a></li>
            <li><a href="#" title="Schools, Colleges <br>&#038; Universities">Schools, Colleges <br>
              &#038; Universities</a></li>
            <li><a href="#" title="Societies, Clubs &#038; Community groups">Societies, Clubs &#038; Community groups</a></li>
          </ul>
        </li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Company support">Company support</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Company support"><span class="icon icon-chevron-left" aria-hidden="true"></span> Company support</a></li>
            <li class="see-all"><a href="#" title="Fundraising &#038; Giving">Fundraising &#038; Giving</a></li>
            <li><a href="#" title="Ways to support">Ways to support</a></li>
          </ul>
        </li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Ways to give">Ways to give</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="" title="Ways to give"><span class="icon icon-chevron-left" aria-hidden="true"></span> Ways to give</a></li>
            <li class="see-all"><a href="#" title="Fundraising &#038; Giving">Fundraising &#038; Giving</a></li>
            <li><a href="#" title="Make a donation">Make a donation</a></li>
            <li><a href="#" title="Regular Gift">Regular Gift</a></li>
            <li><a href="#" title="Gift in Wills &#038; Legacies">Gift in Wills &#038; Legacies</a></li>
            <li><a href="#" title="Occasions">Occasions</a></li>
            <li><a href="#" title="Other ways to give">Other ways to give</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="menu-14 has-children"> <a class="js-mega-toggle" href="#" title="What's going on">What's&thinsp;going&thinsp;on<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
      <ul class="is-hidden columns-4">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="What's going on">What's going on <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li><a href="/whats-going-on/?type=events" title="Events">Events</a></li>
        <li><a href="#" title="Newsroom">Newsroom</a></li>
        <li><a href="/whats-going-on/?type=gallery" title="Gallery">Gallery</a></li>
        <li><a href="#" title="Partnerships">Partnerships</a></li>
      </ul>
    </li>
    <li class="menu-16 has-children"> <a class="js-mega-toggle" href="#" title="Team Bexley Snap">Team&thinsp;Bexley Snap<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
      <ul class="is-hidden columns-3">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="Team Bexley Snap">Team Bexley Snap <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li><a href="#" title="Paid Vacancies">Paid Vacancies</a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Volunteering">Volunteering</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Volunteering"><span class="icon icon-chevron-left" aria-hidden="true"></span> Volunteering</a></li>
            <li class="see-all"><a href="#" title="Team Bexley Snap">Team Bexley Snap</a></li>
            <li><a href="#" title="Volunteer vacancies">Volunteer vacancies</a></li>
            <li><a href="#" title="Meet our volunteers">Meet our volunteers</a></li>
          </ul>
        </li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Useful information">Useful information</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Useful information"><span class="icon icon-chevron-left" aria-hidden="true"></span> Useful information</a></li>
            <li class="see-all"><a href="#" title="Team Bexley Snap">Team Bexley Snap</a></li>
            <li><a href="#" title="Book on to the Rotas">Book on to the Rotas</a></li>
            <li><a href="#" title="Staff Training">Staff Training</a></li>
            <li><a href="#" title="Staff Downloads">Staff Downloads</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="menu-18 has-children"> <a class="js-mega-toggle" href="#" title="Bookings">Bookings<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
      <ul class="is-hidden columns-5">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="Bookings">Bookings <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="The booking process">The booking process</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="The booking process"><span class="icon icon-chevron-left" aria-hidden="true"></span> The booking process</a></li>
            <li class="see-all"><a href="#" title="Bookings">Bookings</a></li>
            <li><a href="#" title="How to register &#038; book">How to register &#038; book</a></li>
            <li><a href="#" title="When to book">When to book</a></li>
            <li><a href="#" title="After you book">After you book</a></li>
            <li><a href="#" title="Cancellations">Cancellations</a></li>
          </ul>
        </li>
        <li><a href="#" title="First visits">First visits</a></li>
        <li class="has-children"> <a class="js-mega-toggle" href="#" title="Prices &#038; payments">Prices payments</a>
          <ul class="is-hidden">
            <li class="go-back"><a href="#" title="Prices &#038; payments"><span class="icon icon-chevron-left" aria-hidden="true"></span> Prices &#038; payments</a></li>
            <li class="see-all"><a href="#" title="Bookings">Bookings</a></li>
            <li><a href="#" title="Pay an Invoice">Pay an Invoice</a></li>
            <li><a href="#" title="Concessions policy">Concessions policy</a></li>
          </ul>
        </li>
        <li><a href="#" title="Transitioning">Transitioning</a></li>
        <li><a href="booking.php" title="Book now" target="_blank">Book now</a></li>
      </ul>
    </li>
    <li class="menu-20 last has-children"> <a class="js-mega-toggle" href="#" title="Information &#038; Guidance">Information&thinsp;Guidance<span class="icon icon-chevron-down" aria-hidden="true"></span></a>
      <ul class="is-hidden columns-3">
        <li class="go-back"><a title="Menu">Menu</a></li>
        <li class="see-all"><a href="#" title="Information &#038; Guidance">Information &#038; Guidance <span class="icon icon-chevron-right" aria-hidden="true"></span></a></li>
        <li><a href="#" title="FAQs">FAQs</a></li>
        <li><a href="#" title="Useful information">Useful information</a></li>
        <li><a href="#" title="Bexley Snap reports">Bexley Snap reports</a></li>
      </ul>
    </li>
  </ul>-->
</div>
</nav>
<style type="text/css">
ul.nav li.dropdown:hover > ul.dropdown-menu {
  display: block;
  margin-top:0px;
}

.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}
.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>