<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

$id = $_SESSION['frontuserid'];

$query  = "SELECT * FROM ".$tbname."_clientmaster WHERE _ID = ".$id;
$run 	= mysqli_query($con , $query);
$fetch  = mysqli_fetch_assoc($run);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
		  <script type="text/javascript">
			function validate(){
				var err="";
				var name=/^[0-9a-zA-Z]+$/;
				var mail=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.]{2,6})+\.([A-Za-z]{2,6})$/;
				/*var day = document.getElementsByName('txtdays[]');*/
				
				if(document.frmprofile.txtemail.value==''){
				  err += "Please Select Email.\n"
				}
				if(document.frmprofile.txtpassword.value==''){
				  err += "Please Enter Password.\n"
				}
				if(document.frmprofile.txtphone.value==''){
				  err += "Please Enter Phone.\n"
				}
				if(document.frmprofile.txtaddress1.value==''){
				  err += "Please Enter Address.\n"
				}
				if(document.frmprofile.txtcity.value==''){
				  err += "Please Enter City.\n"
				}
				if(document.frmprofile.txtstate.value==''){
				  err += "Please Enter State.\n"
				}
				if(document.frmprofile.txtcountry.value==''){
				  err += "Please Enter Country.\n"
				}
				if(document.frmprofile.txtzip.value==''){
				  err += "Please Enter Postcode.\n"
				}

				if(err=="")
				{
					return true;
				}
				else
				{
					alert(err);
					return false;
				}
			}
			
		   function isNumberKey(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode;
				if (charCode != 46 && charCode > 31
				&& (charCode < 48 || charCode > 57))
					return false;

				return true;
				}
			</script>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-4">
					<ul class="menu">
						<li ><a href="booking.php">Make a Booking</a></li>
						<li><a href="#">View Bookings</a></li>
						<li><a href="profile.php">Profile</a></li>
					</ul>
				</div>
				<div class="col-md-8">
					<h2 class="text-center">Profile</h2>
					<?php 
						if($result != ''){ 
							if ($result == 'failed'){
					?>
								<div class="alert alert-danger">Error Occurred</div>
					<?php 
							} else if($result == 'success'){
					?>
								<div class="alert alert-success">Profile Updated Successfully</div>
					<?php		
							} else if($result == 'already'){
					?>
								<div class="alert alert-danger">Email Already Exists</div>
					<?php		
							}
						} 
					 ?>
					<form name="frmprofile" id="frmprofile" method="post" action="profileaction.php" onsubmit="return validate();">
						<input type="hidden" name="id" id="id" value="<?php echo encrypt($id, $encrypt);?>">
						<div class="form-group">
						  <label for="txtpupil">Email:</label>
						  <input type="email" name="txtemail" id="txtemail" value="<?php echo $fetch['_Email'];?>" class="form-control" onchange="chk_email(this.value);" required> 
						  <span class="error"></span>
						</div>
						<div class="form-group">
						  <label for="txttype">Password:</label>
						  <input type="password" name="txtpassword" id="txtpassword" value="<?php echo decrypt($fetch['_Password'] , $encrypt);?>" class="form-control" required> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">Phone:</label>
						  <input type="text" name="txtphone" id="txtphone" value="<?php echo $fetch['_Phone'];?>" class="form-control" required> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">Address 1:</label>
						  <input type="text" name="txtaddress1" id="txtaddress1" value="<?php echo $fetch['_Address1'];?>" class="form-control" required> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">Address 2:</label>
						  <input type="text" name="txtaddress2" id="txtaddress2" value="<?php echo $fetch['_Address2'];?>" class="form-control"> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">City:</label>
						  <input type="text" name="txtcity" id="txtcity" value="<?php echo $fetch['_City'];?>" class="form-control" required> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">State:</label>
						  <input type="text" name="txtstate" id="txtstate" value="<?php echo $fetch['_State'];?>" class="form-control" required> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">Country:</label>
						  <input type="text" name="txtcountry" id="txtcountry" value="<?php echo $fetch['_Country'];?>" class="form-control" required> 
						</div>
						
						<div class="form-group">
						  <label for="txtfreq">Post Code:</label>
						  <input type="text" name="txtzip" id="txtzip" value="<?php echo $fetch['_Postcode'];?>" class="form-control" required> 
						</div>

						<button type="submit" class="btn btn-primary" style="margin-bottom:30px">Update</button>
					</form>
				</div>
			</div>	
		
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>
<script type="text/javascript">
	function chk_email(emailid){
		var mailid = emailid;
		
		$.ajax({
			url : 'ajax/check_email.php',
			type : 'POST',
			data : {'email':mailid},
			dataType : 'json',
			success : function(e){
				if(e.result == 'exists'){
					$(".error").text('Emailid Already Exists');
				} else {
					$(".error").text('');
				}
			}
		});
		return false;
	}
</script>