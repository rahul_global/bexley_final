<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

$id = $_SESSION['frontuserid'];
/*echo $id;
exit;*/
$query  = "SELECT * FROM ".$tbname."_clientmaster WHERE _ID = ".$id;
$run 	= mysqli_query($con , $query);
$fetch  = mysqli_fetch_assoc($run);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-2">
					<ul class="menu">
						<li ><a href="booking.php">Make a Booking</a></li>
						<li><a href="viewbooking.php">View Bookings</a></li>
						<li><a href="profile.php">Profile</a></li>
					</ul>

				</div>
				<div class="col-md-10">
					<?php
						$cnt = 0;
						$booking = "SELECT ww_bookingtype._Type as clubname, ww_booked_lessons. * , ww_pupilmaster._FirstName, ww_pupilmaster._LastName FROM  `ww_booked_lessons` JOIN  `ww_pupilmaster` ON ww_booked_lessons._PupilID = ww_pupilmaster._ID JOIN `ww_timeslot` ON ww_booked_lessons._ClubID = ww_timeslot._ID JOIN  `ww_bookingtype` ON ww_bookingtype._ID = ww_timeslot._Bookingtype WHERE ww_booked_lessons._ClubID !=  '' AND _ClientID =  '".$_SESSION['frontuserid']."'";
						
							$bookingdetail = mysqli_query($con,$booking);
							$ordernumrows = mysqli_num_rows($bookingdetail);	
							
					?>
					<table class="table">
						<thead>
							  <tr>
							  	<th>Sr no.</th>
								<th>Booking id</th>
								<th>Child</th>
								<th>Club</th>
								<th>Date</th>
								<th>From Time</th>
								<th>To Time</th>
								<th>Payment</th>	
							  </tr>
						</thead>
						<tbody>
							<?php
								if($ordernumrows > 0)
								{
										while($bookingresult = mysqli_fetch_assoc($bookingdetail))
										{
											$cnt++;
											?>
											<tr>
												<td><?php echo $cnt; ?></td>
												<td><?php echo $bookingresult['_ID']; ?></td>
												<td><?php echo $bookingresult['_FirstName'].' '.$bookingresult['_LastName']; ?></td>
												<td><?php echo $bookingresult['clubname']; ?></td>
												<td><?php echo $bookingresult['_Date']; ?></td>
												<td><?php echo $bookingresult['_Fromtime']; ?></td>
												<td><?php echo $bookingresult['_Totime']; ?></td>
												<?php
													if($bookingresult['_PaymentID'] == NULL || $bookingresult['_PaymentID'] == '')
													{
														?>
															<td style="color: red;"><?php echo 'Pending...'; ?></td>
														<?php
													}else
													{
														?>
															<td  style="color: green;"><?php echo 'Confirmed'; ?></td>
														<?php
													}
												?>
																					
											</tr>
											<?php
										}
									
								}
								?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>