<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$title = $sitename." : Booked Resource";
if(isset($_REQUEST['done'])){

$done = decrypt($_REQUEST['done'],$encrypt);

}


?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
                $(function() {
                    // footable
                    yukon_datatables.p_plugins_tables_datatable();
                })
        </script>
        <style type="text/css">
        .pending{
            color: #f6b738 !important;
            font-weight: bolder;
        }
        .activex{
            color: #87be4a !important;
            font-weight: bolder;
        }
        .inactive{
            color: #d83b4b !important;
            font-weight: bolder;
        }
        </style>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li>Resource Booking</li>
                </ul>
            </nav>

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <?php 
                        if($done == "11"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Resource Booking Data Updated Successfully.
                            </div>
                    <?php   }
                        if($done == "00"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Resource Booking Data Added Successfully.
                            </div>
                    <?php   }
                        if($done == "22"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Resource Booking Data Deleted Successfully.
                            </div>
                    <?php   }
                            if($done == "2"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Resource Booking Data Not deleted Successfully.
                                    </div>
                    <?php   }
                            if($done == "1"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Resource Booking Data Not Updated Successfully.
                                    </div>
                            <?php   }
                            if($done == "01"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Resource Booking Data Not Added Successfully.
                                    </div>
                            <?php   }
                        ?>   
                 
                                           
                    </div>
                    <!-- <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <a href="edit-client.php?e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Add New Client</a>
                    </div>
                        
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-bordered table-striped " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Resource</th>
                                        <th>Resource Type</th>
                                        <th>Parent / Carer Name</th>
                                        <th>Payment Made</th>
                                        <th>Purchase Date</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php/*
                                           $sel_client = "select rb.*,concat(cm._Firstname,' ',cm._Lastname) as clientname,r._Title as resourcename,r._Type,rt._Type as resourcetype from ".$tbname."_resourcebooking as rb left join ".$tbname."_clientmaster as cm on rb._ClientID = cm._ID left join ".$tbname."_resources as r on rb._ResourceID = r._ID left join ".$tbname."_resourcetypes as rt on r._Type = rt._Id  ";
                                            $rst_client = mysqli_Query($con,$sel_client);
                                            while ($row = mysqli_fetch_assoc($rst_client)){
                                           
                                                ?>
                                                <tr >
                                                    <td><?php echo $row['resourcename']; ?></td>
                                                    <td><?php echo $row['resourcetype']; ?></td>
                                                    <td><?php echo $row['clientname']; ?></td>
                                                    <td><?php echo $row['_Getpayment']; ?></td>
                                                    <td><?php echo date("d M Y", strtotime($row['_Created'])); ?></td>
                                                    <td align="center"><a href="viewbooking.php?id=<?php echo encrypt($row['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('view',$encrypt); ?>" class="btn btn-primary">View Booking</a></td>
                                                <!--  <td><a href="clientaction.php?id=<?php echo encrypt($row['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary">Delete</a></td> -->
                                                </tr>


                                        <?php   }
*/
                                        ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main content -->
            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

        
        
        
    </body>
</html>
