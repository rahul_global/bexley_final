<?php 
include_once('db/dbopen.php');

/*echo '<pre>';
print_r($_SESSION);
exit;*/
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}




$btntext 	= "Add";
$bread 		= "Add CMS Page";
$title 		= $sitename." : Add CMS Page";
$tutor_subject = array();

if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

if(isset($_GET['e_action']))
{
	$action 	= decrypt($_GET['e_action'],$encrypt);
    
	if($action == 'edit')
	{
		$title 		= $sitename." : Edit CMS Page";
		$id 		= decrypt($_GET['id'],$encrypt);
		$query	 	= "select _ID, _PID, _Order, _Title , _Content, _Content2 ,_Image , _Status,_Url, _Topdisplay, _Footerdisplay, IS_Club FROM ".$tbname."_cmspages where _ID='".$id."'";
		$run 		= mysqli_query($con,$query);
		$num	 	= mysqli_num_rows($run);
		if($num >	0)
		{
			$fetch	 = mysqli_fetch_assoc($run);
			
			$bread 		= "Edit CMS Page";
			$btntext 	= 'Update';
		}
		else
		{
			header("location:allcmspages.php");
			exit;
		}
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<script type="text/javascript">
            $(function() {
                // wysiwg editor
                yukon_wysiwg.p_forms_extended_elements();
            })
        </script>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allcmspages.php">CMS Pages</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($result) && $result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                                    <?php } else if(isset($result) && $result == 'failed') {?> 
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
									<?php }else if(isset($result) && $result == 'fillall') {?> 
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Fill All Require Fields.</div>
									<?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" method="post" action="cmspageaction.php" name="edit_btype_frm" id="edit_btype_frm" enctype="multipart/form-data">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
										
										<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
										<input type="hidden" name="cmspage_id" id="cmspage_id" value="<?php echo isset($_GET['id'])?$_GET['id']:''; ?>">
                                        <div class="form-group">
                                            <label for="txttitle" class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txttitle" name="txttitle" value="<?php echo isset($fetch['_Title'])?$fetch['_Title']:'';?>" PLACEHOLDER="Title" required <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'readonly'; } ?>>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txturl" class="col-sm-2 control-label">Redirect Url</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txturl" name="txturl" value="<?php echo isset($fetch['_Url'])?$fetch['_Url']:'';?>" PLACEHOLDER="Url" required  <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'readonly'; } ?>>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>
                                        <?php /* ?>
                                        <div class="col-lg-12">
                                            <ul>
                                                <?php
                                                $parentsql1 = "SELECT * FROM ".$tbname."_cmspages Where _PID = 0 ORDER BY _Title ";
                                                $result1 = mysqli_query($con,$parentsql1);
                                                while($row11 = mysqli_fetch_assoc($result1)){
                                                ?>
                                                <li><?php echo $row11['_Title']; ?>
                                                    <ul>
                                                    <?php 
                                                    $childsql2 = "SELECT * from ".$tbname."_cmspages where _PID=".$row11["_ID"]." ORDER BY _Title";
                                                    $result12 = mysqli_query($con,$childsql2);
                                                    while($row3231 = mysqli_fetch_assoc($result12)){
                                                    ?>
                                                    <li><?php echo $row3231['_Title']; ?>
                                                        <ul>
                                                        <?php 
                                                        $childsql23 = "SELECT * from ".$tbname."_cmspages where _PID=".$row3231["_ID"]." ORDER BY _Title";
                                                        $result1sds2 = mysqli_query($con,$childsql23);
                                                        while($row3ds231 = mysqli_fetch_assoc($result1sds2)){
                                                        ?>
                                                        <li><?php echo $row3ds231['_Title']; ?></li>
                                                        <?php
                                                        }
                                                        ?>   
                                                        </ul>
                                                    </li>
                                                    <?php
                                                    }
                                                    ?>   
                                                    </ul>
                                                </li>
                                                <?php 
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                        <?php */ ?>
                                        <div class="form-group">
                                            <label for="txturl" class="col-sm-2 control-label">Select Parent Page</label>
                                            <div class="col-sm-9">
                                               <?php
                                                

                                                $parentsql = "SELECT * FROM ".$tbname."_cmspages Where _PID = 0 ORDER BY _Title ";                           
                                                if(isset($addpid) && $addpid != '')
                                                {
                                                    $selected = $addpid;
                                                }else
                                                {
                                                    $selected = $pid;
                                                }                                
                                                $result = mysqli_query($con,$parentsql);
                                                if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                                                {
                                                    print "<select name='parentpage' class='form-control'  ".$disable.">";    
                                                }else
                                                {
                                                    print "<select name='parentpage' class='form-control'>";
                                                }
                                                print "<option value=\"\">--SELECT--</option>";
                                                while($row = mysqli_fetch_assoc($result)){
                                                $childsql = "SELECT * from ".$tbname."_cmspages where _PID=".$row["_ID"]." ORDER BY _Title";
                                                $result2 = mysqli_query($con,$childsql);
                                                $sel1 = ($selected == $row['_ID']) ? 'selected' : '-'; 
                                                print "<option value=\"".$row["_ID"]."\" ".$sel1." >--".$row["_Title"]."";
                                                    while($row2 = mysqli_fetch_assoc($result2)){
                                                    $subchildsql = "SELECT * from ".$tbname."_cmspages where _PID=".$row2["_ID"]." ORDER BY _Title";
                                                    $subresult2 = mysqli_query($con,$subchildsql);    
                                                    $sel2 = ($selected == $row2['_ID']) ? 'selected' : '-'; 
                                                    if($row2["_PID"]==1)
                                                    {
                                                                                 
                                                        print "<option value=\"".$row2["_ID"]."\" ".$sel2.">----".$rs["_TypeName"]."</option>\n";
                                                        
                                                    }
                                                    else
                                                    {
                                                    print "<option value=\"".$row2["_ID"]."\" ".$sel2.">----".$row2["_Title"]."</option>\n";
                                                    }
                                                    while($row3 = mysqli_fetch_assoc($subresult2)){
                                                        $sel3 = ($selected == $row3['_ID']) ? "selected='selected'" : ""; 
                                                        print "<option value=\"".$row3["_ID"]."\"".$sel3." >------".$row3["_Title"]."</option>\n";
                                                    }

                                                        
                                                    }
                                                print "</option>\n";
                                                }
                                                print "</select>";
                                                ?>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>

										<div class="form-group">
                                            <label for="txtcontent" class="col-sm-2 control-label">Content</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control ckeditor" id="wysiwg_editor" name="wysiwg_editor" required   <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>><?php echo isset($fetch['_Content'])?$fetch['_Content']:'';?></textarea>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>


                                        <div class="form-group">
                                            <label for="txtcontent" class="col-sm-2 control-label">Content 2</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control ckeditor" id="wysiwg_editor" name="wysiwg_editor2"  <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>><?php echo isset($fetch['_Content2'])?$fetch['_Content2']:'';?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtcontent" class="col-sm-2 control-label">Image</label>
                                            <div class="col-sm-9">
                                                <?php

                                                    if($fetch['_Image'] != '' && file_exists($_SERVER['DOCUMENT_ROOT']."/bexley_new1/images/".$fetch['_Image']))
                                                    {
                                                        if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                                                        {
                                                            ?>
                                                                <img src="<?php echo $PicPath.$fetch['_Image']; ?>" class="form-control" style="height: 250px;width: 250px;">
                                                            <?php
                                                        }else
                                                        {
                                                            ?>
                                                               <input type="hidden" name="img" class="form-control" value="<?php echo $fetch['_Image']; ?>">
                                                        <img src="<?php echo "../images/".$fetch['_Image']; ?>" class="form-control" style="height: 250px;width: 250px;"><a href="2ndlvlcmsaction.php?id=<?php echo encrypt($fetch['_ID'],$encrypt);?>&pid=<?php echo encrypt($addpid,$encrypt); ?>&e_action=<?php echo encrypt('delete_img',$encrypt); ?>" >Delete</a>
                                                            <?php
                                                        }
                                                        ?>
                                                        
                                                        <?php   
                                                    }else
                                                    {
                                                        ?>
                                                            <input type="file" name="img" class="form-control" <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>>
                                                        <?php
                                                    }
                                                ?>
                                                
                                            </div>
                                        </div>

										<div class="form-group">
                                            <label for="txtstatus" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-9">
                                                <?php
                                                    if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                                                    {
                                                        ?>
                                                            <select class="form-control" id="txtstatus" name="txtstatus" required <?php echo $disable; ?>>
                                                        <?php        
                                                    }else
                                                    {
                                                        ?>
                                                            <select class="form-control" id="txtstatus" name="txtstatus" required>
                                                        <?php
                                                    }
                                                ?>
													<option value="">Select Status</option>
													<option value="Active" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == 'Active')?"selected='Selected'":'';?>>Active</option>
													<option value="Inactive" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == 'Inactive')?"selected='Selected'":'';?>>Inactive</option>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtstatus" class="col-sm-2 control-label">Menu Display</label>
                                            <div class="col-sm-9">
                                                <input type="checkbox" name="txtheader" value="0" <?php echo (isset($fetch['_Topdisplay']) && $fetch['_Topdisplay'] == '0')?" checked='checked'":'';?>  <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>>&nbsp;<span>Header</span>

                                                <input type="checkbox" name="txtfooter" value="0" <?php echo (isset($fetch['_Footerdisplay']) && $fetch['_Footerdisplay'] == '1')?" checked='checked'":'';?> <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>>&nbsp;<span>Footer</span>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="txtstatus" class="col-sm-2 control-label">Club Page</label>
                                            <div class="col-sm-9">
                                                <input type="checkbox" name="txtclub" value="1" <?php echo (isset($fetch['IS_Club']) && $fetch['IS_Club'] == '1')?" checked='checked'":'';?>  <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>>&nbsp;<span>Club page</span>

                                                <input type="checkbox" name="txtnotclub" value="0" <?php echo (isset($fetch['IS_Club']) && $fetch['IS_Club'] == '0')?" checked='checked'":'';?> <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>>&nbsp;<span>Page</span>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>

                                        
                                        <div class="form-group">
                                            <label for="txturl" class="col-sm-2 control-label">Menu Page Order</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtpageorder" name="txtpageorder" value="<?php echo $fetch['_Order'] ?>" <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?> >
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>
                                        <?php
                                            if(isset($_SESSION['usertype']) && $_SESSION['usertype'] == 'admin')
                                            {
                                                ?>
                                                    <hr/>
                                                    <div class="form-group">
                                                        <div class="col-sm-10 col-sm-offset-2">
                                                            <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                            <button class="btn-default btn" id="btncancle" onclick='window.location = "allcmspages.php";return false;'>Cancel</button> 
                                                        </div>
                                                    </div>
                                                <?php 
                                            }else
                                            {
                                                ?>
                                                    <hr/>
                                                    <div class="form-group">
                                                        <div class="col-sm-10 col-sm-offset-2">
                                                            
                                                            <button class="btn-default btn" id="btncancle" onclick='window.location = "allcmspages.php";return false;'>Cancel</button> 
                                                        </div>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                        
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>        
    </body>
</html>
