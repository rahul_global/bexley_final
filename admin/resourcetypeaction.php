<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$type = $_REQUEST['txttype'];


if($action == "add"){
		

		$str = "INSERT INTO " . $tbname . "_resourcetypes (";
     
     	$str .= " _Type) VALUES(";
     
        if ($type != "")
		 	$str = $str . "'" . replaceSpecialChar($type) . "'";
		else
		  	$str = $str . "null";
        
          $str = $str . ") ";

          //echo $str;exit;

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Added New Resource Type - ".replaceSpecialChar($type));
		//log create end
		header("location:resourcetype.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:resourcetype.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_resourcetypes SET ";
	    
	    if ($type != "")
			$str = $str . "_Type = '" . replaceSpecialChar($type) . "' ";
		else
	        $str = $str . "_Type = null ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Resource Type - ".replaceSpecialChar($type));
		//log create end
		header("location:resourcetype.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:resourcetype.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){
	//log create start
		$rtype_qry  = "SELECT _Type FROM ".$tbname."_resourcetypes WHERE _ID = ".$id;
		$run_rtype = mysqli_query($con, $rtype_qry);
		$fetch_rtype = mysqli_fetch_assoc($run_rtype);
		$type = $fetch_rtype['_Type'];
	//log create end
	$delclient = "delete from ".$tbname."_resourcetypes where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Resource Type - ".$type);
		//log create end
		header("location:resourcetype.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:resourcetype.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>