<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);

if($action == 'edit'){

$title = $sitename." : Edit File";
$id = decrypt($_REQUEST['id'],$encrypt);
$sel_ser = "select * from ".$tbname."_filemanager where _ID='".$id."'";
$rst_ser = mysqli_query($con,$sel_ser);
$row = mysqli_fetch_assoc($rst_ser);
$name = $row['_Name'];
$category = $row['_Category'];
$txtfile = $row['_File'];
$status = $row['_Status'];
$bread = "Edit File";
$btntext = 'Update';
$AdminProductPicPath = 'files/';

}else{
    $title = $sitename." : Add New File";
    $bread = "Add New File";
    $btntext = 'Add';
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="filemanager.php">File Manager</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="filemanageraction.php" method="post" enctype="multipart/form-data">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
                                        
                                        <div class="form-group">
                                            <label for="txtname" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtname" name="txtname" value="<?php echo $name;?>" PLACEHOLDER="Name" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtname" class="col-sm-2 control-label">Category</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtcategory" name="txtcategory" value="<?php echo $category;?>" PLACEHOLDER="Category" required>   
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtname" class="col-sm-2 control-label">File</label>
                                            <div class="col-sm-3">
                                                <?php
                                                $ext = pathinfo($txtfile, PATHINFO_EXTENSION);
                                                if( $ext == 'gif' || $ext == 'png' || $ext == 'jpg' )
                                                {?>
                                                <img class="img-responsive" src="<?php echo $AdminProductPicPath.$txtfile;?>">
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                    <a href="<?php echo $AdminProductPicPath.$txtfile;?>"><?php echo $txtfile;?></a>
                                                <?php
                                                }
                                                ?>
                                                <input style="margin-top: 10px;" type="file" class="form-control" id="txtfile" name="txtfile" PLACEHOLDER="File" >
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label for="txtname" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-9">
												<select name="status" id="status" class="form-control" required>
													<option value="">--- Select Status ---</option>
                                                    <option value="1" <?php echo (isset($status) && $status == '1') ? "selected='selected'" : '';?>>Active</option>
                                                    <option value="0" <?php echo (isset($status) && $status == '0') ? "selected='selected'" : '';?>>Inactive</option>
												</select>
                                            </div>
											<div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>
                                      
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                        <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "filemanager.php";
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
