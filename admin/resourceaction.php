<?php 
include_once('db/dbopen.php');
include 'classresizeimage.php';
/*echo "<pre>";
print_r($_REQUEST);exit;*/
session_start();
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$orgtype = $_REQUEST['type'];
$type = $_REQUEST['txttype'];
$title = $_REQUEST['txttitle'];
$description = $_REQUEST['txtdesc'];
$price = $_REQUEST['txtprice'];
$Adminresoucepath = "uploads/resources/";


if($action == "add"){
		
		if ($_FILES["txtimg"]["size"] > 0) {
                if ($_FILES["txtimg"]["error"] > 0)
                    echo "Return Code: " . $_FILES["txtimg"]["error"] . "<br />";
                else {
                    $splitfilename1 = strtolower($_FILES["txtimg"]["name"]);
                    $exts1 = explode(".", $splitfilename1);
                    $n1 = count($exts1) - 1;
                    $exts1 = $exts1[$n1];
                    $datetime1 = date("YmdHis") . genRandomString(4);
                    $Thumbnail1 = "";
                    $Thumbnail1 = $datetime1 . "." . $exts1;

                    if (file_exists($AdminProductPicPath1 . $Thumbnail1))
                        echo " already exists. ";
                    else {
//Thumbnail
                        $myfile1 = new UploadedImage;
                        $myfile1->file = $_FILES["txtimg"];
                        $myfile1->maxwidth = 335;
                        $myfile1->maxheight = 200;
                        $myfile1->relscale = false;
                        $myfile1->newfile = $datetime1;
                        $myfile1->maxsize = 10485760000;
                        $myfile1->pictype = "Thumbnail";
                        $myfile1->transfer = "Copy";
                        $myfile1->uploaddir = $Adminresoucepath;
                        if (!$myfile1->upload_file()) {
                            echo $myfile1->ErrorMsg;
                            exit();
                        }
                        move_uploaded_file($_FILES["txtimg"]["tmp_name"],$Adminresoucepath . $Thumbnail1);
                    }
                }
            } else {
                $Thumbnail1 = $_REQUEST['txtimg'];                
            }



		$str = "INSERT INTO " . $tbname . "_resources (";
     
     	$str .= " _Type,_Title,_Image,_Description,_Price) VALUES(";
     
        if ($type != "")
		 	$str = $str . "'" . replaceSpecialChar($type) . "', ";
		else
		  	$str = $str . "null, ";

		if ($title != "")
		 	$str = $str . "'" . replaceSpecialChar($title) . "', ";
		else
		  	$str = $str . "null, ";

		if ($Thumbnail1 != "")
		 	$str = $str . "'" . $Thumbnail1 . "', ";
		else
		  	$str = $str . "null, ";

		if ($description != "")
		 	$str = $str . "'" . replaceSpecialChar($description) . "', ";
		else
		  	$str = $str . "null, ";

		if ($price != "")
		 	$str = $str . "'" . replaceSpecialChar($price) . "' ";
		else
		  	$str = $str . "null ";
        
        $str = $str . ") ";

        //echo $str;exit;

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$rtype_qry = "SELECT _Type FROM ".$tbname."_resourcetypes WHERE _ID = ".$type;
			$run_rtype = mysqli_query($con , $rtype_qry);
			$fetch_rtype = mysqli_fetch_assoc($run_rtype);
			$resourcetype = $fetch_rtype['_Type'];
			
			$create_log = auditlog($msg = "Added New Resource ".replaceSpecialChar($title)." Into Resource Type ".$resourcetype);
		//log create end
		header("location:resource.php?done=".encrypt("00",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;
	}else{
		header("location:resource.php?done=".encrypt("01",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;	
	}

}
if($action == "edit"){

		if ($_FILES["txtimg"]["size"] > 0) {
                if ($_FILES["txtimg"]["error"] > 0)
                    echo "Return Code: " . $_FILES["txtimg"]["error"] . "<br />";
                else {
                    $splitfilename1 = strtolower($_FILES["txtimg"]["name"]);
                    $exts1 = explode(".", $splitfilename1);
                    $n1 = count($exts1) - 1;
                    $exts1 = $exts1[$n1];
                    $datetime1 = date("YmdHis") . genRandomString(4);
                    $Thumbnail1 = "";
                    $Thumbnail1 = $datetime1 . "." . $exts1;

                    if (file_exists($AdminProductPicPath1 . $Thumbnail1))
                        echo " already exists. ";
                    else {
//Thumbnail
                        $myfile1 = new UploadedImage;
                        $myfile1->file = $_FILES["txtimg"];
                        $myfile1->maxwidth = 335;
                        $myfile1->maxheight = 200;
                        $myfile1->relscale = false;
                        $myfile1->newfile = $datetime1;
                        $myfile1->maxsize = 10485760000;
                        $myfile1->pictype = "Thumbnail";
                        $myfile1->transfer = "Copy";
                        $myfile1->uploaddir = $Adminresoucepath;
                        if (!$myfile1->upload_file()) {
                            echo $myfile1->ErrorMsg;
                            exit();
                        }
                        move_uploaded_file($_FILES["txtimg"]["tmp_name"],$Adminresoucepath . $Thumbnail1);
                    }
                }
            } else {
                $Thumbnail1 = $_REQUEST['txtimg'];                
            }




		$str = "UPDATE " . $tbname . "_resources SET ";
	    
	    if ($type != "")
			$str = $str . "_Type = '" . replaceSpecialChar($type) . "', ";
		else
	        $str = $str . "_Type = null, ";

	    if ($title != "")
			$str = $str . "_Title = '" . replaceSpecialChar($title) . "', ";
		else
	        $str = $str . "_Title = null, ";

	    if ($Thumbnail1 != "")
			$str = $str . "_Image = '" . replaceSpecialChar($Thumbnail1) . "', ";
		else
	        $str = $str . "_Image = null, ";

	    if ($description != "")
			$str = $str . "_Description = '" . replaceSpecialChar($description) . "', ";
		else
	        $str = $str . "_Description = null, ";

	    if ($price != "")
			$str = $str . "_Price = '" . $price . "' ";
		else
	        $str = $str . "_Price = null ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$rtype_qry = "SELECT _Type FROM ".$tbname."_resourcetypes WHERE _ID = ".$type;
			$run_rtype = mysqli_query($con , $rtype_qry);
			$fetch_rtype = mysqli_fetch_assoc($run_rtype);
			$resourcetype = $fetch_rtype['_Type'];
			
			$create_log = auditlog($msg = "Updated Resource ".replaceSpecialChar($title)." From  Resource Type ".$resourcetype);
		//log create end
		header("location:resource.php?done=".encrypt("11",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;
	}else{
		header("location:resource.php?done=".encrypt("1",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){
	//log create start
		$resource_qry = "SELECT * FROM ".$tbname."_resources WHERE _ID = ".$id;
		$run_resource = mysqli_query($con , $resource_qry);
		$fetch_resource = mysqli_fetch_assoc($run_resource);
		$type = $fetch_resource['_Type'];
		$title = $fetch_resource['_Title'];
		
		$rtype_qry = "SELECT _Type FROM ".$tbname."_blogtype WHERE _ID = ".$type;
		$run_rtype = mysqli_query($con , $rtype_qry);
		$fetch_rtype = mysqli_fetch_assoc($run_rtype);
		$resourcetype = $fetch_rtype['_Type'];

	//log create end
	
	$delclient = "delete from ".$tbname."_resources where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Resource ".$title." From Resource Type ".$resourcetype);
		//log create end
		header("location:resource.php?done=".encrypt("22",$encrypt)."&type=".$orgtype);
		exit;
	}else{
		header("location:resource.php?done=".encrypt("2",$encrypt)."&type=".$orgtype);
		exit;	
	}

}
if($action == "deletepic"){

	//echo $orgtype;exit;
	$selimg = "select _Image from ".$tbname."_resources where _ID='".$id."'";
	$rowimg = mysqli_fetch_assoc(mysqli_query($con,$selimg));
	$background = $rowimg['_Image'];

	
	if (file_exists($Adminresoucepath . $background)) {
	    unlink($Adminresoucepath . $background);
		$delclient = "update ".$tbname."_resources set _Image = '' where _ID = '".$id."'";
		$rstdel = mysqli_query($con,$delclient);
	}


	if($rstdel){
		
		header("location:edit-resource.php?type=".$orgtype."&id=".encrypt($id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
		exit;
	}else{
		header("location:edit-resource.php?type=".$orgtype."&id=".encrypt($id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
		exit;	
	}

}
?>