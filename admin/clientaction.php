<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}

$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
//$fname = $_REQUEST['txtfname'];
$lname = $_REQUEST['txtlname'];
$email = $_REQUEST['txtemail'];
$phone = $_REQUEST['txtphone'];
$address1 = $_REQUEST['txtadd1'];
$address2 = $_REQUEST['txtadd2'];
$city = $_REQUEST['txtcity'];
$state = $_REQUEST['txtstate'];
$country = $_REQUEST['txtcountry'];
$postcode = $_REQUEST['txtzip'];
$status = $_REQUEST['txtstatus']; 	//0 for pending, 1 for active, 2 for inactive
$carer = $_REQUEST['txtguardian'];
$carer1	=	$_REQUEST['carer1'];
$carer2	=	$_REQUEST['carer2'];
$carer3	=	$_REQUEST['carer3'];
$carer4	=	$_REQUEST['carer4'];

$created = date("Y-m-d h:i:s");


$password = encrypt($_REQUEST['txtpassword'],$encrypt);


if($action == "add"){

	$pupils = $_REQUEST['txtpupils'];
			

		$str = "INSERT INTO " . $tbname . "_clientmaster (";
     
     	$str .= " _Lastname,_Email,_Carer1,_Carer2,_Carer3,_Carer4,_Address1,_Address2,_City,_State,_Country,_Postcode,_Phone,_Password,_Created,_Guardian,_Status) VALUES(";
     
        /*if ($fname != "")
            $str = $str . "'" . replaceSpecialChar($fname) . "', ";
        else
            $str = $str . "null, ";*/

          if ($lname != "")
              $str = $str . "'" . replaceSpecialChar($lname) . "', ";
          else
              $str = $str . "null, ";
          if ($email != "")
              $str = $str . "'" . replaceSpecialChar($email) . "', ";
          else
              $str = $str . "null, ";
		  
		  if ($carer1 != "")
              $str = $str . "'" . $carer1 . "', ";
          else
              $str = $str . "null, ";
		  if ($carer2 != "")
              $str = $str . "'" . $carer2 . "', ";
          else
              $str = $str . "null, ";
		  if ($carer3 != "")
              $str = $str . "'" . $carer3 . "', ";
          else
              $str = $str . "null, ";
		  if ($carer4 != "")
              $str = $str . "'" . $carer4 . "', ";
          else
              $str = $str . "null, ";
		  
          if ($address1 != "")
              $str = $str . "'" . replaceSpecialChar($address1) . "', ";
          else
              $str = $str . "null, ";

          if ($address2 != "")
              $str = $str . "'" . replaceSpecialChar($address2) . "', ";
          else
              $str = $str . "null, ";

          if ($city != "")
              $str = $str . "'" . replaceSpecialChar($city) . "', ";
          else
              $str = $str . "null, ";

          if ($state != "")
              $str = $str . "'" . replaceSpecialChar($state) . "', ";
          else
              $str = $str . "null, ";

          if ($country != "")
              $str = $str . "'" . replaceSpecialChar($country) . "', ";
          else
              $str = $str . "null, ";

          if ($postcode != "")
              $str = $str . "'" . replaceSpecialChar($postcode) . "', ";
          else
              $str = $str . "null, ";

          if ($phone != "")
              $str = $str . "'" . replaceSpecialChar($phone) . "', ";
          else
              $str = $str . "null, ";

          if ($password != "")
              $str = $str . "'" . $password . "', ";
          else
              $str = $str . "null, ";

		  if ($created != "")
              $str = $str . "'" . $created . "', ";
          else
              $str = $str . "null, ";

		  if ($carer != "")
              $str = $str . "'" . $carer . "', ";
          else
              $str = $str . "null, ";

         if ($status != "")
              $str = $str . "'" . replaceSpecialChar($status) . "' ";
          else
              $str = $str . "null ";
        
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

		$lid = mysqli_insert_id($con);

		for ($i=0;$i<count($pupils);$i++){

			$insstu = "insert into ".$tbname."_clipup_rel (_ClientID,_PupilID) values ('".$lid."','".$pupils[$i]."')";
			mysqli_query($con,$insstu);

			//log create start
				$pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$pupils[$i];
				$run_pemail = mysqli_query($con , $pemail_query);
				$fetch_pemail = mysqli_fetch_assoc($run_pemail);
				$pupilemail = $fetch_pemail['_Email'];
				$create_log = auditlog($msg = "Assigned Children ".$pupilemail." To Family ".$email);
			//log create end
		}
		
		$assignedtutor = $_REQUEST['txttutors'];

		for ($i=0;$i<count($assignedtutor);$i++){

			$insstu = "insert into ".$tbname."_assi_tutcli_rel (_ClientID,_TutorID) values ('".$lid."','".$assignedtutor[$i]."')";
			mysqli_query($con,$insstu);
			
			//log create start
				$pemail_query = "SELECT _Email FROM ".$tbname."_tutormaster WHERE _ID = ".$assignedtutor[$i];
				$run_pemail = mysqli_query($con , $pemail_query);
				$fetch_pemail = mysqli_fetch_assoc($run_pemail);
				$pupilemail = $fetch_pemail['_Email'];
				$create_log = auditlog($msg = "Assigned Pupil ".$pupilemail." To Client ".$email);
			//log create end
			
		}

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Added New Family ".$email);
		//log create end

		header("location:allclient.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:allclient.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$selstatus = "Select _Status from ".$tbname."_clientmaster where _ID = '".$id."'";
		$rststatus = mysqli_query($con,$selstatus);
		$rsstatus = mysqli_fetch_assoc($rststatus);
		$retrivestatus = $rsstatus['_Status'];


		$str = "UPDATE " . $tbname . "_clientmaster SET ";
	    
	    /*if ($fname != "")
			$str = $str . "_Firstname = '" . replaceSpecialChar($fname) . "', ";
		else
	        $str = $str . "_Firstname = null, ";*/
	
		if ($lname != "")
	        $str = $str . "_Lastname = '" . replaceSpecialChar($lname) . "', ";
	    else
			$str = $str . "_Lastname = null, ";
		
		if ($email != "")
	        $str = $str . "_Email = '" . $email . "', ";
	    else
	        $str = $str . "_Email = null, ";

		if ($carer1 != "")
	        $str = $str . "_Carer1 = '" . $carer1 . "', ";
	    else
	        $str = $str . "_Carer1 = null, ";
		
		if ($carer2 != "")
	        $str = $str . "_Carer2 = '" . $carer2 . "', ";
	    else
	        $str = $str . "_Carer2 = null, ";
		
		if ($carer3 != "")
	        $str = $str . "_Carer3 = '" . $carer3 . "', ";
	    else
	        $str = $str . "_Carer3 = null, ";
		
		if ($carer4 != "")
	        $str = $str . "_Carer4 = '" . $carer4 . "', ";
	    else
	        $str = $str . "_Carer4 = null, ";
		
		if ($address1 != "")
	        $str = $str . "_Address1 = '" . replaceSpecialChar($address1) . "', ";
	    else
	        $str = $str . "_Address1 = null, ";

	    if ($address2 != "")
	        $str = $str . "_Address2 = '" . replaceSpecialChar($address2) . "', ";
	    else
	        $str = $str . "_Address2 = null, ";

	    if ($city != "")
	        $str = $str . "_City = '" . replaceSpecialChar($city) . "', ";
	    else
	        $str = $str . "_City = null, ";

	    if ($state != "")
	        $str = $str . "_State = '" . replaceSpecialChar($state) . "', ";
	    else
	        $str = $str . "_State = null, ";

	    if ($country != "")
	        $str = $str . "_Country = '" . replaceSpecialChar($country) . "', ";
	    else
	        $str = $str . "_Country = null, ";

	    if ($postcode != "")
	        $str = $str . "_Postcode = '" . replaceSpecialChar($postcode) . "', ";
	    else
	        $str = $str . "_Postcode = null, ";
	    
	    if ($phone != "")
	        $str = $str . "_Phone = '" . $phone . "', ";
	    else
	        $str = $str . "_Phone = null, ";

		if ($carer != "")
	        $str = $str . "_Guardian = '" . $carer . "', ";
	    else
	        $str = $str . "_Guardian = null, ";
		
	    if ($status != "")
	        $str = $str . "_Status = '" . $status . "', ";
	    else
	        $str = $str . "_Status = null, ";
	            
		if ($password != "")
	        $str = $str . "_Password = '" . $password . "' ";
	    else
	        $str = $str . "_Password = null ";


	    $str = $str . " WHERE _ID = '" . replaceSpecialChar($id) . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	// start audit Log for status
		if($retrivestatus != $status){
			
			if($status == "1"){
				$text = "Active";
			}else if($status == "2"){
				$text = "Reject";
			}else{
				$text = "Pending";
			}
			$create_log = auditlog($msg = "Changed Status of ".$email." To ".$text);
		}
	// end audit Log for status

	
	$pupils = $_REQUEST['txtpupils'];
	//print_r($_REQUEST);
	if(is_array($pupils))
	{	
		$postpup=$pupils;
	}
	else
	{
		$postpup=array();
	}

	$selpup ="select * from ".$tbname."_clipup_rel where _ClientID = '".$id."'";
	$rstpup = mysqli_query($con,$selpup);
	$retrievepup = array();
	while ($rspup = mysqli_fetch_assoc($rstpup)) {
		array_push($retrievepup,$rspup['_PupilID']);
	}

	//print_r($retrievepup);exit;
	$rstpup1 = array_diff($postpup, $retrievepup);
	$rstpup2 = array_diff($retrievepup, $postpup);

	//print_r($rstpup2);exit;
	foreach($rstpup2 as $val){
		$delsub = "delete from ".$tbname."_clipup_rel where _ClientID ='".$id."' and _PupilID = '".$val."' ";
		mysqli_query($con,$delsub);

		//log create start
			$pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$val;
			$run_pemail = mysqli_query($con , $pemail_query);
			$fetch_pemail = mysqli_fetch_assoc($run_pemail);
			$pupilemail = $fetch_pemail['_Email'];
			$create_log = auditlog($msg = "Removed Children ".$pupilemail." From Family ".$email);
		//log create end
	}
	//foreach($rstpup1 as $val1){
	foreach($rstpup1 as $val1){
		$inssub = "insert into ".$tbname."_clipup_rel (_ClientID,_PupilID) values ('".$id."','".$val1."')";
		mysqli_query($con,$inssub);

		//log create start
			$pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$val1;
			$run_pemail = mysqli_query($con , $pemail_query);
			$fetch_pemail = mysqli_fetch_assoc($run_pemail);
			$pupilemail = $fetch_pemail['_Email'];
			$create_log = auditlog($msg = "Assigned Children ".$pupilemail." To Family ".$email);
		//log create end
	}
	
	/*Assigned/Suggest Tutor*/
	$assignedtutor = $_REQUEST['txttutors'];
	if(is_array($assignedtutor))
	{	
		$posttutors = $assignedtutor;
	}
	else
	{
		$posttutors=array();
	}
	$rettutors = "select * from ".$tbname."_assi_tutcli_rel where _ClientID = '".$id."' ";
	$rstrettutors = mysqli_query($con,$rettutors);
	$retrievetutors = array();
	while ($rsretsub = mysqli_fetch_assoc($rstrettutors)) {
		array_push($retrievetutors,$rsretsub['_TutorID']);
	}
	$rsttutors1 = array_diff($posttutors, $retrievetutors);
	$rsttutors2 = array_diff($retrievetutors, $posttutors);
	/*echo "<pre>";
	print_r($rsttutors1);
	print_r($rsttutors2);exit;*/
	foreach($rsttutors2 as $val11){
		$delsub = "delete from ".$tbname."_assi_tutcli_rel where _ClientID ='".$id."' and _TutorID = '".$val11."' ";
		mysqli_query($con,$delsub);

		//log create start
			$temail_query = "SELECT _Email FROM ".$tbname."_tutormaster WHERE _ID = ".$val11;
			$run_temail = mysqli_query($con , $temail_query);
			$fetch_temail = mysqli_fetch_assoc($run_temail);
			$tutoremail = $fetch_temail['_Email'];
			$create_log = auditlog($msg = "Removed Assigned Tutor ".$tutoremail." From Client ".$email);
		//log create end
	}
	foreach($rsttutors1 as $val22){
		$inssub = "insert into ".$tbname."_assi_tutcli_rel (_ClientID,_TutorID) values ('".$id."','".$val22."')";
		mysqli_query($con,$inssub);

		//log create start
			$pemail_query = "SELECT _Email FROM ".$tbname."_tutormaster WHERE _ID = ".$val22;
			$run_temail = mysqli_query($con , $pemail_query);
			$fetch_temail = mysqli_fetch_assoc($run_temail);
			$tutoremail = $fetch_temail['_Email'];
			$create_log = auditlog($msg = "Assigned Tutor ".$tutoremail." To Client ".$email);
		//log create end
	}
	

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Parent / Carer ".$email);
		//log create end
		echo "<script>alert('Family Data Updated Successfully');</script>";
		echo "<script>window.location='allclient.php?done=".encrypt('11',$encrypt)."';</script>";
	}else{
		echo "<script>window.location='allclient.php?done=".encrypt('1',$encrypt)."';</script>";
	}

	

} 
if($action == "delete"){

	//log create start
		$cemail_query = "SELECT _Email FROM ".$tbname."_clientmaster WHERE _ID = ".$id;
		$run_cemail = mysqli_query($con , $cemail_query);
		$fetch_cemail = mysqli_fetch_assoc($run_cemail);
		$clientemail = $fetch_cemail['_Email'];
	//log create end

	$delclient = "delete from ".$tbname."_clientmaster where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
		$create_log = auditlog($msg = "Deleted Family ".$clientemail);
		//log create end
		
		header("location:allclient.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:allclient.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>