<?php
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$testsecretkey 		= $_POST['txtsecretkey'];
$testpublishablekey = $_POST['txtpublishablekey'];
$livesecretkey 		= $_POST['txtlsecretkey'];
$livepublishablekey = $_POST['txtlpublishablekey'];
$type 				= $_POST['txttype'];
$action 			= $_POST['btnsubmit'];
$current_time		= date('Y-m-d H:i:s');

if(isset($action) && $action == 'Add')
{
	$query = "INSERT INTO ".$tbname."_stripe_detail (_TestSecretKey , _TestPublishableKey , _LiveSecretKey , _LivePublishableKey , _Type , _Created) VALUES ('".$testsecretkey."','".$testpublishablekey."','".$livesecretkey."','".$livepublishablekey."','".$type."','".$current_time."')";
	if($run = mysqli_query($con , $query))
	{
		//log create start
			$create_log = auditlog($msg = "Added Stripe Account Details");
		//log create end
		header("location:setstripeaccount.php?result=".encrypt('success',$encrypt));
		exit;
	}
	else
	{
		header("location:setstripeaccount.php?result=".encrypt('failed',$encrypt));
		exit;
	}
}
elseif($action == 'Update')
{
	$query = "UPDATE ".$tbname."_stripe_detail SET _TestSecretKey = '".$testsecretkey."' , _TestPublishableKey = '".$testpublishablekey."' , _LiveSecretKey = '".$livesecretkey."' , 	_LivePublishableKey = '".$livepublishablekey."' , _Type = '".$type."' , _Created = '".$current_time."'";
	if($run = mysqli_query($con , $query))
	{
		//log create start
			$create_log = auditlog($msg = "Updated Stripe Account Details");
		//log create end
		header("location:setstripeaccount.php?result=".encrypt('success',$encrypt));
		exit;
	}
	else
	{
		header("location:setstripeaccount.php?result=".encrypt('failed',$encrypt));
		exit;
	}
}
?>