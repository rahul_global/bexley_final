<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$title = $sitename." : Children";

$id = decrypt($_REQUEST['id'],$encrypt);

$get_assigned_clubs = "SELECT * FROM  `ww_childclub_rel` WHERE _ChildID =  '".$id."'";

$sql_assigned_club = mysqli_query($con , $get_assigned_clubs);
$result_assigned_club = mysqli_fetch_assoc($sql_assigned_club);
$clubids = $result_assigned_club['_ClubID']; 
	
	//echo $result_assigned_club['clubid'].'<br>';

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script type="text/javascript">
        	$(document).ready(function(){
        		$('#cancel').click(function(){
        			$('input:checkbox').attr('checked',false);
                    window.location= "allpupil.php";
        		});
        	});
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li>Children</li>
				</ul>
            </nav>
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->
			<div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
	                        <div class="col-md-12">
	                            <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="assignclubaction.php" method="post">
									<input type="hidden" name="userid" id="userid" value="<?php echo $_REQUEST['id']; ?>">
	                                <h3 class="heading_a"><span class="heading_text">Assign CLub to Child</span></h3>
	                                <div class="form-group">
                                            <label class="col-sm-2 control-label" for="txtdate">Clubs</label>
                                            <div class="col-sm-9">
                                                <?php

                                                $categoory_array = array(
                                                    1 => "Children up to 11",
                                                    2 => "Young People up to 18",
                                                    3 => "Young Adults up to 25",
                                                    4 => "Everything Else",
                                                );

                                                foreach ($categoory_array as $key => $value) {
                                                    $get_club_cat = "SELECT * FROM ".$tbname."_bookingtype WHERE _Category_id = $key ORDER BY _Type ASC ";
                                                    $sql_get_club_cat = mysqli_query($con,$get_club_cat);
                                                ?>
                                                <h3><?php echo $value; ?></h3>
                                                <div class="row">
                                                <?php
                                                if(mysqli_num_rows($sql_get_club_cat) > 0){
                                                    while ($result_get_club_cat = mysqli_fetch_assoc($sql_get_club_cat)){
                                                ?>
                                                <div class="col-sm-6 getclub">
                                                    <?php
                                                        $cid = (explode(",",$clubids));
                                                        
                                                        if (in_array($result_get_club_cat['_ID'], $cid))
                                                          {
                                                             ?>
                                                             <input type="checkbox"  name="get_clubs[]" id="get_clubs[]" value="<?php echo $result_get_club_cat['_ID']; ?>" checked>
                                                             <?php
                                                          }
                                                        else
                                                          {
                                                            ?>
                                                             <input type="checkbox"  name="get_clubs[]" id="get_clubs[]" value="<?php echo $result_get_club_cat['_ID']; ?>" >
                                                            <?php
                                                          }
                                                    ?>
                                                    
                                                <?php echo $result_get_club_cat['_Type']; ?>
                                                </div>
                                                <?php 
                                                    }
                                                }else{
                                                    echo '<div class="col-sm-6 getclub">No club Found !!!</div>';
                                                }
                                                ?>
                                                </div>
                                                <?php
                                                }
                                                	/*$get_club = "SELECT *, 
                                                    (case 
                                                      when _Category_id = '1' then 'Children up to 11' 
                                                      when _Category_id = '2' then 'Young People up to 18'
                                                      when _Category_id = '3' then 'Young Adults up to 25'
                                                      when _Category_id = '4' then 'Everything Else'
                                                      end ) as category_name
                                                    FROM ".$tbname."_bookingtype ORDER BY _Type ASC";
                                                    
                                                	$sql_get_club = mysqli_query($con, $get_club);
                                                	while ($result_get_club = mysqli_fetch_assoc($sql_get_club)) 
                                                	{
                                                		?>
                                                			<div class="col-sm-6 getclub">
                                                                <?php
                                                                    $cid = (explode(",",$clubids));
                                                                    
                                                                    if (in_array($result_get_club['_ID'], $cid))
                                                                      {
                                                                         ?>
                                                                         <input type="checkbox"  name="get_clubs[]" id="get_clubs[]" value="<?php echo $result_get_club['_ID']; ?>" checked>
                                                                         <?php
                                                                      }
                                                                    else
                                                                      {
                                                                        ?>
                                                                         <input type="checkbox"  name="get_clubs[]" id="get_clubs[]" value="<?php echo $result_get_club['_ID']; ?>" >
                                                                        <?php
                                                                      }
                                                                ?>
                                                                
                                                            <?php echo $result_get_club['_Type']; ?>
                                                            </div>
                                                		<?php
                                                	}*/
                                                ?>

                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                    </div>
                                    <div class="col-sm-12">
	                                    <div class="col-sm-2"></div>
	                                    <div class="col-sm-10">
	                                        <div class="form-group">
		                                    	<input type="submit" name="assign_club" id="assign_club" value="Assign Club" class="btn btn-primary">
		                                	
		                                    	<input type="button" name="cancel" id="cancel" value="Cancel" class="btn btn-primary">
		                                	</div>
	                                    </div>
        							</div>
		                                    
	                            </form>
	                        </div>
                        </div>
                </div>
            </div>                	
			<!-- main content -->
        </div>
    </body>
</html>
