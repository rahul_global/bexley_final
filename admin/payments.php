<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$title = $sitename." : Payments";
if(isset($_REQUEST['done'])){

$done = decrypt($_REQUEST['done'],$encrypt);

}


?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
                $(function() {
                    // footable
                    yukon_datatables.p_plugins_tables_datatable();
                })
            function confirmdel(){

                var del=confirm("Are you sure you want to delete this item?");
                if (del==true){
                   //alert ("record deleted")
                   return true;
                }else{
                    return false;
                }
                
            }
        </script>
        <style type="text/css">
        .pending{
            color: #f6b738 !important;
            font-weight: bolder;
        }
        .activex{
            color: #87be4a !important;
            font-weight: bolder;
        }
        .inactive{
            color: #d83b4b !important;
            font-weight: bolder;
        }
        </style>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li>Payments</li>
                </ul>
            </nav>

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <?php 
                        if($done == "11"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Payment Data Updated Successfully.
                            </div>
                    <?php   }
                        if($done == "00"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Payment Data Added Successfully.
                            </div>
                    <?php   }
                        if($done == "22"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Payment Data Deleted Successfully.
                            </div>
                    <?php   }
                            if($done == "2"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Payment Data Not deleted Successfully.
                                    </div>
                    <?php   }
                            if($done == "1"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Payment Data Not Updated Successfully.
                                    </div>
                            <?php   }
                            if($done == "01"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Payment Data Not Added Successfully.
                                    </div>
                            <?php   }
                        ?>   
                 
                                           
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-bordered " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Invoice Number</th>
                                        <th>Client Name</th>
                                        <th>Client Email</th>
                                        <th>Total Amount</th>
                                        <th>Invoice Date</th>
                                        <th>Payment Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $sel_client = "select ip.*,iv.*,concat(cm._Firstname,' ',cm._Lastname) as clientname,cm._Email as clientemail from ".$tbname."_invoicepayment ip left join ".$tbname."_invoices iv on iv._ID = ip._InvoiceID left join ".$tbname."_clientmaster cm on cm._ID = iv._ClientID";
                                            $rst_client = mysqli_Query($con,$sel_client);
                                            while ($row_client = mysqli_fetch_assoc($rst_client)){
                                                
                                                ?>
                                                <tr >
                                                    <td><?php echo $row_client['_InvoiceID']; ?></td>
                                                    <td><?php echo $row_client['clientname']; ?></td>
                                                    <td><?php echo $row_client['clientemail']; ?></td>
                                                    <td><?php echo $row_client['_Amount']; ?></td>
                                                    <td><?php echo date("d-M-Y",strtotime($row_client['_Created']));  ?></td>
                                                    <td><?php echo date("d-M-Y",strtotime($row_client['_Invoicedate']));  ?></td>
                                                </tr>


                                        <?php   }

                                        ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main content -->
            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

        
        
        
    </body>
</html>
