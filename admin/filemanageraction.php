<?php 
include_once('db/dbopen.php');
include('classresizeimage.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$name = replaceSpecialChar($_REQUEST['txtname']);
$txtcategory = replaceSpecialChar($_REQUEST['txtcategory']);
$status = replaceSpecialChar($_REQUEST['status']);

$txtfile = '';
$AdminProductPicPath = 'files/';

if($action == "add"){

		if ($_FILES["txtfile"]["size"] > 0)
		{
			if ($_FILES["txtfile"]["error"] > 0)
			{
				$txtfile = '';
			}
			else
			{
				$splitfilename = strtolower($_FILES["txtfile"]["name"]); 
				$exts = pathinfo($splitfilename, PATHINFO_EXTENSION);
		
				$datetime2 = date("YmdHis") . generateNumStr(4);
				
				$txtfile = $datetime2 . "." . $exts;
							
				/*$myfile2 = new UploadedImage;
				$myfile2->file = $_FILES["txtfile"];
				$myfile2->maxwidth = 150;
				$myfile2->maxheight = 180;
				$myfile2->relscale = true;
				$myfile2->scale = $_POST['scale'];
				$myfile2->newfile = $datetime2;
				$myfile2->maxsize = 10485760000;
				$myfile2->pictype = "Thumbnail";
				$myfile2->transfer = "Copy";
				$myfile2->uploaddir = $AdminProductPicPath;
				if(!$myfile2->upload_file()){
					echo $myfile2->ErrorMsg;
					exit();
				}*/
					
				move_uploaded_file( $_FILES["txtfile"]['tmp_name'] , $AdminProductPicPath.$txtfile );	
			}
		}


		$str = "INSERT INTO " . $tbname . "_filemanager (";
     
     	$str .= " _Name , _Category, _File , _Status) VALUES(";
     
		if ($name != "")
		 	$str = $str . "'" . $name . "' ,";
		else
		  	$str = $str . "null ,";
        
		if ($txtcategory != "")
		 	$str = $str . "'" . $txtcategory . "' ,";
		else
		  	$str = $str . "null ,";

		if ($txtfile != "")
		 	$str = $str . "'" . $txtfile . "' ,";
		else
		  	$str = $str . "null ,";

		if ($status != "")
		 	$str = $str . "'" . $status . "' ";
		else
		  	$str = $str . "null ";
		
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Add New _filemanager - ".$name);
		//log create end
		header("location:filemanager.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:filemanager.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}

if($action == "edit"){

		if ($_FILES["txtfile"]["size"] > 0)
		{
			if ($_FILES["txtfile"]["error"] > 0)
			{
				$txtfile = '';
			}
			else
			{
				$splitfilename = strtolower($_FILES["txtfile"]["name"]); 
				$exts = pathinfo($splitfilename, PATHINFO_EXTENSION);
		
				$datetime2 = date("YmdHis") . generateNumStr(4);
				
				$txtfile = $datetime2 . "." . $exts;
							
				/*$myfile2 = new UploadedImage;
				$myfile2->file = $_FILES["txtfile"];
				$myfile2->maxwidth = 150;
				$myfile2->maxheight = 180;
				$myfile2->relscale = true;
				$myfile2->scale = $_POST['scale'];
				$myfile2->newfile = $datetime2;
				$myfile2->maxsize = 10485760000;
				$myfile2->pictype = "Thumbnail";
				$myfile2->transfer = "Copy";
				$myfile2->uploaddir = $AdminProductPicPath;
				if(!$myfile2->upload_file()){
					echo $myfile2->ErrorMsg;
					exit();
				}*/
				
				$qry = "select _File from " . $tbname . "_filemanager WHERE _ID = '" . $id . "'";
				$result = mysqli_query($con,$qry);
				$r = mysqli_fetch_assoc($result); 

				$fl = $r['_File'];
				if(file_exists($AdminProductPicPath.$fl))
				{
					unlink($AdminProductPicPath.$fl);
				}

				move_uploaded_file( $_FILES["txtfile"]['tmp_name'] , $AdminProductPicPath.$txtfile );	
			}
		}


		$str = "UPDATE " . $tbname . "_filemanager SET ";
	    
	    if ($name != "")
			$str = $str . "_Name = '" . $name . "', ";
		else
	        $str = $str . "_Name = null ,";
	
		if ($txtcategory != "")
			$str = $str . "_Category = '" . $txtcategory . "', ";
		else
	        $str = $str . "_Category = null ,";

	    if ($txtfile != "")
			$str = $str . "_File = '" . $txtfile . "', ";
	

	    if ($status != "")
			$str = $str . "_Status = '" . $status . "' ";
		else
	        $str = $str . "_Status = null ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Filemanager - ".$name);
		//log create end
		header("location:filemanager.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:filemanager.php?done=".encrypt("1",$encrypt));
		exit;	
	}
}

if($action == "delete"){
	//log create start
		$scl_qry  = "SELECT _Name FROM ".$tbname."_filemanager WHERE _ID = ".$id;
		$run_scl = mysqli_query($con, $scl_qry);
		$fetch_scl = mysqli_fetch_assoc($run_scl);
		$service = $fetch_scl['_Name'];
	//log create end

	$qry = "select _File from " . $tbname . "_filemanager WHERE _ID = '" . $id . "'";
	$result = mysqli_query($con,$qry);
	$r = mysqli_fetch_assoc($result); 

	$fl = $r['_File'];
	if(file_exists($AdminProductPicPath.$fl))
	{
		unlink($AdminProductPicPath.$fl);
	}

	$delclient = "delete from ".$tbname."_filemanager where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Filemanager - ".$service);
		//log create end
		header("location:filemanager.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:filemanager.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>