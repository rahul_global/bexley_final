<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}

$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$type = $_REQUEST['txttype'];
$cap = $_REQUEST['txtcap'];
$cat = $_REQUEST['txtcat'];
$active = $_REQUEST['txtactive'];
$notes = $_REQUEST['txtnotes'];
$price = $_REQUEST['txtprice'];
$profile_id = $_REQUEST['profile_id'];
$page_id = $_REQUEST['txtpage_id'];
$price2 = $_REQUEST['txtprice2'];
$txtdays = $_REQUEST['txtdays'];
$txttime = $_REQUEST['txttime'];
$txtlocation = $_REQUEST['txtlocation'];
$txtstuff = $_REQUEST['txtstuff'];
$txtemergency = $_REQUEST['txtemergency'];

$content 	= str_replace('&quot;','"',$_POST['wysiwg_editor2']);
$intro_content 	= str_replace('&quot;','"',$_POST['intro_content']);
$banner_img   			= $_POST['banner_image'];

if(empty($_FILES["banner_image"]['name'])){
	$imagefilename  = $banner_img ? $banner_img : null;
}else{
	$imagefilename  = rand().basename($_FILES["banner_image"]["name"]);
}



$targetfolder 	= $_SERVER['DOCUMENT_ROOT']."/Bexley/admin/banners/".$imagefilename;

if(isset($_FILES['banner_image'])){
	move_uploaded_file($_FILES["banner_image"]["tmp_name"], $targetfolder);
}
//echo $action;

if($action == "add"){

		$str = "INSERT INTO " . $tbname . "_bookingtype (";
     
     	$str .= " _Type, _capacity, _Intro_Content, _Content, _Days, _Time, _Location, _Stuff, _Emergency, _BannerImage, _Category_id, _Profileid, _PageID,_active, _Price, _Price2, _Notes) VALUES(";
     
		if ($type != "")
		 	$str = $str . "'" . replaceSpecialChar($type) . "', ";
		else
		  	$str = $str . "null, ";
		if ($cap != "")
		 	$str = $str . "'" . replaceSpecialChar($cap) . "', ";
		else
		  	$str = $str . "null, ";

		if ($intro_content != "")
		 	$str = $str . "'" . replaceSpecialChar($intro_content) . "', ";
		else
		  	$str = $str . "null, ";

		if ($content != "")
		 	$str = $str . "'" . replaceSpecialChar($content) . "', ";
		else
		  	$str = $str . "null, ";

		if ($txtdays != "")
		 	$str = $str . "'" . $txtdays . "', ";
		else
		  	$str = $str . "null, ";

		if ($txttime != "")
		 	$str = $str . "'" . $txttime . "', ";
		else
		  	$str = $str . "null, ";

		if ($txtlocation != "")
		 	$str = $str . "'" . $txtlocation . "', ";
		else
		  	$str = $str . "null, ";

		if ($txtstuff != "")
		 	$str = $str . "'" . $txtstuff . "', ";
		else
		  	$str = $str . "null, ";

		if ($txtemergency != "")
		 	$str = $str . "'" . $txtemergency . "', ";
		else
		  	$str = $str . "null, ";

		if ($imagefilename != "")
		 	$str = $str . "'" . $imagefilename . "', ";
		else
		  	$str = $str . "null, ";

		if ($cat != "")
		 	$str = $str . "'" . $cat . "', ";
		else
		  	$str = $str . "0, ";

		if ($profile_id != "")
		 	$str = $str . "'" . $profile_id . "', ";
		else
		  	$str = $str . "0, ";

		if ($page_id != "")
		 	$str = $str . "'" . $page_id . "', ";
		else
		  	$str = $str . "0, ";

		if ($active != "")
		 	$str = $str . "'" . replaceSpecialChar($active) . "', ";
		else
		  	$str = $str . "0, ";

		if ($price != "")
		 	$str = $str . "" . $price . ", ";
		else
		  	$str = $str . "0, ";

		if ($price2 != "")
		 	$str = $str . "" . $price2 . ", ";
		else
		  	$str = $str . "0, ";

		if ($notes != "")
		 	$str = $str . "'" . replaceSpecialChar($notes) . "' ";
		else
		  	$str = $str . "null ";
        
          $str = $str . ") ";

         //echo $str;exit;
		//$rstins = mysqli_query($con,$str);
		
		//echo $str;exit;
		//echo "nic";
		$rstins = mysqli_query($con,$str);
		//exit;

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Add New Booking Type - ".replaceSpecialChar($type));
		//log create end
		header("location:bookingtypes.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:bookingtypes.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_bookingtype SET ";
	    
	    if ($type != "")
			$str = $str . "_Type = '" . replaceSpecialChar($type) . "', ";
		else
	        $str = $str . "_Type = null, ";
		if ($cap != "")
			$str = $str . "_capacity = '" . replaceSpecialChar($cap) . "', ";
		else
	        $str = $str . "_capacity = null, ";

	    if ($intro_content != "")
			$str = $str . "_Intro_Content = '" . replaceSpecialChar($intro_content) . "', ";
		else
	        $str = $str . "_Intro_Content = null, ";

	    if ($content != "")
			$str = $str . "_Content = '" . replaceSpecialChar($content) . "', ";
		else
	        $str = $str . "_Content = null, ";

	    if ($imagefilename != "")
			$str = $str . "_BannerImage = '" . $imagefilename . "', ";
		else
	        $str = $str . "_BannerImage = null, ";

	    if ($txtdays != "")
			$str = $str . "_Days = '" . $txtdays . "', ";
		else
	        $str = $str . "_Days = null, ";

	    if ($txttime != "")
			$str = $str . "_Time = '" . $txttime . "', ";
		else
	        $str = $str . "_Time = null, ";

	    if ($txtlocation != "")
			$str = $str . "_Location = '" . $txtlocation . "', ";
		else
	        $str = $str . "_Location = null, ";

	    if ($txtstuff != "")
			$str = $str . "_Stuff = '" . $txtstuff . "', ";
		else
	        $str = $str . "_Stuff = null, ";

	    if ($txtemergency != "")
			$str = $str . "_Emergency = '" . $txtemergency . "', ";
		else
	        $str = $str . "_Emergency = null, ";

	    if ($cat != "")
			$str = $str . "_Category_id = '" . $cat . "', ";
		else
	        $str = $str . "_Category_id = 0, ";

        if ($profile_id != "")
    		$str = $str . "_Profileid = '" . $profile_id . "', ";
    	else
            $str = $str . "_Profileid = 0, ";

        if ($page_id != "")
    		$str = $str . "_PageID = '" . $page_id . "', ";
    	else
            $str = $str . "_PageID = 0, ";
	    
		if ($active != "")
			$str = $str . "_active = '" . replaceSpecialChar($active) . "', ";
		else
	        $str = $str . "_active = 0, ";

	    if ($price != "")
			$str = $str . "_Price = " . $price . ", ";
		else
	        $str = $str . "_Price = 0, ";

	    if ($price2 != "")
			$str = $str . "_Price2 = " . $price2 . ", ";
		else
	        $str = $str . "_Price2 = 0, ";

	    if ($notes != "")
			$str = $str . "_Notes = '" . replaceSpecialChar($notes) . "' ";
		else
	        $str = $str . "_Notes = null ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		/*echo $str;exit;*/
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			//$create_log = auditlog($msg = "Updated Booking Type - ".$type);
		//log create end
		header("location:bookingtypes.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:bookingtypes.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){
	//log create start
		$scl_qry  = "SELECT _Type FROM ".$tbname."_bookingtype WHERE _ID = ".$id;
		$run_scl = mysqli_query($con, $scl_qry);
		$fetch_scl = mysqli_fetch_assoc($run_scl);
		$school = $fetch_scl['_Type'];
	//log create end

	$delclient = "delete from ".$tbname."_bookingtype where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Booking Type - ".$school);
		//log create end
		header("location:bookingtypes.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:bookingtypes.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
if($action == "delete_img")
	{
		$imgid = decrypt($_REQUEST['id'],$encrypt);
		$getimg = mysqli_query($con,"SELECT * FROM ".$tbname."_bookingtype WHERE _ID = '".$id."'");
		$imagerecord = mysqli_fetch_assoc($getimg);
		$dltimage = $_SERVER['DOCUMENT_ROOT'].'/Bexley/admin/banners/'.$imagerecord['_BannerImage'];
		//echo $dltimage;
		//echo "DELETE FROM ".$tbnamr."_cmspages WHERE _Image = '".$imagerecord['_Image']."'";
		//exit;
		if(file_exists($dltimage))
		{
			unlink($dltimage);
			$imgsqldlt = mysqli_query($con,"UPDATE ".$tbname."_bookingtype SET _BannerImage = '' WHERE _ID = '".$imgid."'");	
			echo '<script type="text/javascript">alert("Image deleted."); window.location="edit-bookingtype.php?id='.encrypt($imgid,$encrypt).'&e_action='.encrypt("edit",$encrypt).'";</script>';

		}else
		{
			echo '<script type="text/javascript">alert("Image not exists.");window.location="edit-cmspage.php?id='.encrypt($imagerecord["_ID"],$encrypt).'&e_action='.encrypt("edit",$encrypt).'";</script>';
		}
	}
?>