<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

$cid = decrypt($_GET['cid'],$encrypt);

$title = $sitename." : Children";

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<script>
			$(function() {
				// footable
				yukon_datatables.p_plugins_tables_datatable();
			})
			function confirmdel(){

				var del=confirm("Are you sure you want to delete this item?");
				if (del==true){
				   //alert ("record deleted")
				   return true;
				}else{
					return false;
				}
				
			}
        </script>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li>Children</li>
				</ul>
            </nav>
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
						<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
						<?php } ?>
						<?php if(isset($result) && $result == 'failed'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Failed!</strong> Error in assigning club.</div>
						<?php } ?>
						<div class="" style="float:right;margin:15px;">
							<a href="edit-pupil.php?e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Add Children</a>
						</div>
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
										<th>Family</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
										<th>Communication</th>
										<th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $pupil_qry = "select pm._ID, pm._Firstname ,pm._Lastname,cl._ClientID,cl._PupilID,cm._Lastname as clientname  FROM ".$tbname."_pupilmaster as pm left join ".$tbname."_clipup_rel as cl on cl._PupilID = pm._ID left join ".$tbname."_clientmaster as cm on cm._ID = cl._ClientID";

                                            if($cid > 0){
                                            	$pupil_qry .= " WHERE cl._ClientID = $cid";
                                            }

                                            $pupil_qry .= "  GROUP BY pm._ID";

                                            $run_pupil = mysqli_Query($con,$pupil_qry);
											$num_pupil = mysqli_num_rows($run_pupil);
											if($num_pupil > 0)
											{
												while ($fetch_pupil = mysqli_fetch_assoc($run_pupil))
												{ 
												?>
													<tr>
														<td><?php echo ucfirst($fetch_pupil['_Firstname'])." ".ucfirst($fetch_pupil['_Lastname']); ?></td>
														<td><?php echo $fetch_pupil['clientname']; ?></td>
														<td><a href="edit-pupil.php?id=<?php echo encrypt($fetch_pupil['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>" class="btn btn-primary">Edit</a></td>
														<td><a href="pupilaction.php?id=<?php echo encrypt($fetch_pupil['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary" onclick="return confirmdel();">Delete</a></td>
														<td><a href="addpupilcom.php?id=<?php echo encrypt($fetch_pupil['_ID'],$encrypt); ?>&type=<?php echo encrypt('1',$encrypt); ?>" class="btn btn-primary">Add Communication</a></td>
														<td><a href="assignclub.php?id=<?php echo encrypt($fetch_pupil['_ID'],$encrypt); ?>&type=<?php echo encrypt('1',$encrypt); ?>" class="btn btn-primary">Manage Clubs</a></td>
													</tr>
											<?php   
												}
											}
											?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			<!-- main content -->
        </div>

        
        
        
    </body>
</html>
