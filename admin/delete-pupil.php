<?php 
include_once('db/dbopen.php');


if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

if(isset($_GET['e_action']))
{
	$action = decrypt($_GET['e_action'],$encrypt);
	$id 	= decrypt($_GET['id'],$encrypt);	
	$query 	= "UPDATE ".$tbname."_pupilmaster SET _IsDeleted = 1 WHERE _ID = $id";
	if($run = mysqli_query($con , $query))
	{
		header("location:allpupil.php?result=success");
		exit;
	}
	else
	{
		header("location:allpupil.php?result=failed");
		exit;
	}
}
else
{
	header("location:allpupil.php");
	exit;
}
?>