<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$title = $_REQUEST['txtname'];

if($action == "add"){

			

		$str = "INSERT INTO " . $tbname . "_pupcategories (";
     
     	$str .= " _Title) VALUES(";
     
        if ($title != "")
		 	$str = $str . "'" . replaceSpecialChar($title) . "' ";
		else
		  	$str = $str . "null ";
        
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Add New Categories - ".replaceSpecialChar($title));
		//log create end
		header("location:categories.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:categories.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_pupcategories SET ";
	    
	    if ($title != "")
	        $str = $str . "_Title = '" . replaceSpecialChar($title) . "' ";
	    else
	        $str = $str . "_Title = null ";

	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Categories - ".replaceSpecialChar($title));
		//log create end
		header("location:categories.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:categories.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){
	//log create start
		$level_qry  = "SELECT _Title FROM ".$tbname."_pupcategories WHERE _ID = ".$id;
		$run_level = mysqli_query($con, $level_qry);
		$fetch_level = mysqli_fetch_assoc($run_level);
		$title = $fetch_level['_Title'];
	//log create end
	$delclient = "delete from ".$tbname."_pupcategories where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Categories - ".$title);
		//log create end
		header("location:categories.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:categories.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>