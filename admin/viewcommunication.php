<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$id = decrypt($_REQUEST['id'],$encrypt);

$comm_qry = "SELECT a.* , b._Name as `_Comtype` FROM ".$tbname."_communications a LEFT JOIN ".$tbname."_communication_type b ON b._ID = a._Commtype WHERE a._ID = ".$id;
$run_comm = mysqli_Query($con,$comm_qry);
$fetch_comm = mysqli_fetch_assoc($run_comm);

$usertype = $fetch_comm['_Usertype'];
$tablename = $usertype == 0 ? 'clientmaster' : 'pupilmaster';

$nameqry = "SELECT * FROM ".$tbname."_".$tablename." WHERE _ID = ".$fetch_comm['_UserID'];
$runname = mysqli_query($con,$nameqry);
$fetchname = mysqli_fetch_assoc($runname);

$firstname = $usertype == 0 ? $fetchname['_Firstname'] : $fetchname['_FirstName'] ;
$lastname = $usertype == 0 ? $fetchname['_Lastname'] : $fetchname['_LastName'] ;

$date = str_replace('-','/',$fetch_comm['_Date']);
$date = date('d-m-Y',strtotime($date));

$bread = "View Communication";
$title = "Winterwood : View Communication";

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
        <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/timepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="css/timepicker.css" />
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allcommunication.php">Communication</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="color: #20638F;margin: 0px;">Communication Details</h2>
                        </div>
                    </div>
                    <div class="row" style="padding:10px;">
                        <div class="col-md-6">
							
							<p><b>User Name</b></p>
							<p><?php echo $firstname.' '.$lastname; ?></p>
							
							<p><b>User Type</b></p>
							<p><?php echo $usertype == 0 ? 'Carer' : 'Children';?></p>

                            <p><b>Email</b></p>
                            <p><?php echo $fetch_comm['_Email'];?></p>  
							
							<p><b>Communication Type</b></p>
							<p><?php echo $fetch_comm['_Comtype'];?></p>
                        </div>
                        <div class="col-md-6">			

							<p><b>Person Spoken To</b></p>
							<p><?php echo $fetch_comm['_Person_spoken'];?></p>

                            <p><b>Date</b></p>
                            <p><?php echo $date;?></p>

                            <p><b>Time</b></p>
                            <p><?php echo $fetch_comm['_Time'];?></p>

							<p><b>Notes</b></p>
							<p><?php echo $fetch_comm['_Notes'];?></p>
							
						</div>
                        <div class="col-sm-12 text-center" style="margin-top: 50px; ">
                                <button class="btn-default btn" id="btncancle">Back</button>
                            </div>
                        <hr/>
                    </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "allcommunication.php";
            return false;
        });
    });
</script>
    </body>
</html>
