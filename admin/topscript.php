	
		<title><?php echo $title; ?></title>

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

        <!-- bootstrap framework -->
		<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		
        <!-- icon sets -->
        <!-- elegant icons -->
        <link href="assets/icons/elegant/style.css" rel="stylesheet" media="screen">
        <!-- elusive icons -->
        <link href="assets/icons/elusive/css/elusive-webfont.css" rel="stylesheet" media="screen">
         <link href="css/style.css" rel="stylesheet" media="screen">
            


        <!-- google webfonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <!-- main stylesheet -->
		<link href="assets/css/main.min.css" rel="stylesheet" media="screen" id="mainCss">

        <!-- moment.js (date library) -->
        <script src="assets/js/moment-with-langs.min.js"></script>
        <!-- jQuery -->
        <!--<script src="assets/js/jquery.min.js"></script>-->
		<script src="assets/js/jquery.js"></script>
		
        <!-- jQuery Cookie -->
        <script src="assets/js/jqueryCookie.min.js"></script>
        <!-- Bootstrap Framework -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- retina images -->
        <script src="assets/js/retina.min.js"></script>
        <!-- switchery -->
        <script src="assets/lib/switchery/dist/switchery.min.js"></script>
        <!-- typeahead -->
        <script src="assets/lib/typeahead/typeahead.bundle.min.js"></script>
        <!-- fastclick -->
        <script src="assets/js/fastclick.min.js"></script>
        <!-- match height -->
        <script src="assets/lib/jquery-match-height/jquery.matchHeight-min.js"></script>
        <!-- scrollbar -->
        <script src="assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- Yukon Admin functions -->
        <script src="assets/js/yukon_all.min.js"></script>

        <script src="assets/lib/ckeditor/ckeditor.js"></script>
        <script src="assets/lib/ckeditor/adapters/jquery.js"></script>

        <!-- datatable -->
        <script src="assets/lib/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="assets/lib/DataTables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
        <script src="assets/lib/DataTables/media/js/dataTables.bootstrap.js"></script>

        
		
		<!-- jquery validation  -->
		<script src="assets/js/jquery.validate.js"></script>
		<style>
		.error { color : #ff0000; }
		.required_field {color:#ff0000;position:relative;left:-25px;}
		</style>