<?php
session_start();

?>
<html>
<head>
<title>bexleysnap</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<?php 
include_once('topscript.php');
?>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
<?php include_once('menu.php');

$id = decrypt($_REQUEST['id'],$encrypt);
//echo "SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$id."'";
$sql_get_content = mysqli_query($con,"SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");
$result = mysqli_fetch_assoc($sql_get_content);
//echo "<pre>"; print_r($result);exit;
?>

<div id="js-main-content" class="main-content"> 
  <section class="section section-content">
    <div class="container">
    	<div class="row">
    		<div class="col-md-12" style="margin-left: 15px;">
    			<h1><?php echo $result['_Title']; ?></h1>
    		</div>
    		<?php 
    		$part = 0;

    		$cls = "col-md-12";
    		if(($result['_Content2'] != '' && $result['_Content2'] != null ) || ($result['_Image'] != '' &&  $result['_Image'] != null )){
    			$cls = "col-md-6";
    			$part = 1;
    		}

    		?>
		<?php
			if($part == 0){
			?>
			<div class="<?php echo $cls; ?>">
				<?php echo html_entity_decode($result['_Content']); ?>
			</div>
			<?php
			}else{
			?>
				<div class="col-md-12">
					<div class="col-md-12">
						<?php echo html_entity_decode($result['_Content']); ?>
					</div>
					<div class="col-md-6">
						<?php 
							if($result['_Image'] != '' && file_exists($_SERVER['DOCUMENT_ROOT']."/Bexley/images/".$result['_Image']))
							{
								?>
							<div class="col-md-12">
								<img src="images/<?php echo $result['_Image']; ?>" style="float:right;width: 100%;">
							</div>
								<?php		
							}
						?>
						<div class="col-md-12">
							<?php $content_main = str_replace("<img", "<img width='100%'", $result['_Content2']); ?>
							<?php echo html_entity_decode($content_main); ?>
						</div>
					</div>
				</div>
				
			<?php
			}	
		?>
    	</div>
		<?php 
			
				$get_page = mysqli_query($con , "SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".decrypt($_REQUEST['id'],$encrypt)."'");
				$pagetitle = mysqli_fetch_assoc($get_page);
				$pagettl = $pagetitle['_Title'];
				if($pagettl == 'What&#39;s going on')
				{
					?>
					<br>
				<br>
				<div class="row">
    							<div class="col-md-12">
				<div class="container">
                <ul class="list-teasers row">
                                         
                                                <li class="col-sm-6 col-md-6">
                            <a href="#" title="Read more">
                                <img width="360" height="260" src="http://localhost/bexley_new/images/img-thumb-youthschemes.jpg" class="img-force wp-post-image" alt="img-thumb-youthschemes" srcset="https://disability-challengers.org/wp-content/uploads/2016/08/img-thumb-youthschemes.jpg 360w, https://disability-challengers.org/wp-content/uploads/2016/08/img-thumb-youthschemes-300x217.jpg 300w" sizes="(max-width: 360px) 100vw, 360px">                                <div class="teaser-label">
                                    <div class="content-position-outer">
                                        <div class="content-center-inner">
                                            <h5>Children up to 11 <span class="icon icon-chevron-right" aria-hidden="true"></span></h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                                                <li class="col-sm-6 col-md-6">
                            <a href="#" title="Read more">
                                <img width="360" height="260" src="http://localhost/bexley_new/images/img-thumb-youngadultschemes.jpg" class="img-force wp-post-image" alt="img-thumb-youngadultschemes" srcset="http://localhost/bexley_new/images/img-thumb-youngadultschemes-300x217.jpg 300w" sizes="(max-width: 360px) 100vw, 360px">                                <div class="teaser-label">
                                    <div class="content-position-outer">
                                        <div class="content-center-inner">
                                            <h5>Young People up to 18 <span class="icon icon-chevron-right" aria-hidden="true"></span></h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li></ul>
						<ul class="list-teasers row">
                                                <li class="col-sm-6 col-md-6">
                            <a href="#" title="Read more">
                                <img width="360" height="260" src="http://localhost/bexley_new/img-thumb-hirecentre.jpg" class="img-force wp-post-image" alt="img-thumb-hirecentre" srcset="http://localhost/bexley_new/images/img-thumb-hirecentre.jpg 360w, http://localhost/bexley_new/images/img-thumb-hirecentre-300x217.jpg 300w" sizes="(max-width: 360px) 100vw, 360px">                                <div class="teaser-label">
                                    <div class="content-position-outer">
                                        <div class="content-center-inner">
                                            <h5>Young Adults up to 25 <span class="icon icon-chevron-right" aria-hidden="true"></span></h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                                                <li class="col-sm-6 col-md-6">
                            <a href="#" title="Read more">
                                <img width="360" height="260" src="http://localhost/bexley_new/images/swim-feature-image.jpg" class="img-force wp-post-image" alt="swim-feature-image" srcset="http://localhost/bexley_new/images/swim-feature-image.jpg 360w, http://localhost/bexley_new/images/swim-feature-image-300x217.jpg 300w" sizes="(max-width: 360px) 100vw, 360px">                                <div class="teaser-label">
                                    <div class="content-position-outer">
                                        <div class="content-center-inner">
                                            <h5>Everything Else <span class="icon icon-chevron-right" aria-hidden="true"></span></h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                                        </ul>
            </div>			</div>
			    	</div>
					<?php
				}
		?>
		<!--<div class="row">
    		<div class="col-md-12">
    			<h1>What's going on</h1>
    		</div>
    		<div class="col-md-12">
				<p>We exist to help ensure that disabled children in Bexley are supported to have the best opportunities and experiences they can to enable them to have full and fulfilling lives.</p>

				<p>We offer a range of Short Breaks Services to support this; holiday play-schemes, weekend activities, youth clubs, and more.</p>

				<p>We support families by offering signposting information,&nbsp;counselling, courses,&nbsp;and occaional family trips out.</p>

				<p>For pre-school disabled children we offer the Little Stars project (now known as Early Years)&nbsp;– a parent and child activity learning through play. The aim of this project is to encourage the development of young children and babies with complex needs and physical delay, Autism and other disabilites. They have a strong sensory element to stimulate senses, develop social skills and enhance learning.&nbsp;</p>

				<p>The CEO of SNAP sits on strategic boards and planning groups, ensuring that the needs of disabled children and families are considered in future plans for services across the borough.</p>

				<p>In addition, we organise social trips and fundraising events designed to raise money while having fun!<br>
					<br>
				Call the main Office number on 01322 334192 for further information.&nbsp;</p>
			</div>
		</div>-->
	</div>
	</section>
</div>
<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script type='text/javascript' src='js/owl.carousel.js'></script>
<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>
<style>
    #owl-demo .item{
        display: block;
        cursor: pointer;
        background: #ffd800;
        padding: 30px 0px;
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
    }
    #owl-demo .item:hover{
      background: #F2CD00;
    }
    </style>
<script>
    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel({
        navigation : false,
        singleItem : true,
        transitionStyle : "fade",
		autoPlay : 5000,
		stopOnHover : false
      });

      //Select Transtion Type
      $("#transitionType").change(function(){
        var newValue = $(this).val();

        //TransitionTypes is owlCarousel inner method.
        owl.data("owlCarousel").transitionTypes(newValue);

        //After change type go to next slide
        owl.trigger("owl.next");
      });
    });
    </script> 
<script>
$(document).ready(function(){
    $(".list-accordion-home li").hover(function(){
		
        $(this).toggleClass("active");
    });
	
	 $("#ss").focusin(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','-90px');
    });
	 $("#ss").focusout(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','0px');
    });
	
	 $("#inputEmail").focusin(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','-110px');
    });
	 $("#inputEmail").focusout(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','0px');
    });
	
	/*-------------dropdown navigation---------------*/
	
	$("#nav .primary-nav li").each(function(){
		$(this).click(function(){
			
			var cls = $(this).children("a").siblings("ul").hasClass("is-hidden");
			
			if(cls == true){
				hideall($(this));
				

			$(this).children("a").siblings("ul").removeClass("is-hidden");
			
			if($(this).children("a").children().hasClass("icon-chevron-down"))
			{
				$(this).children("a").children().removeClass("icon-chevron-down");
				$(this).children("a").children().addClass("icon-chevron-up");
			}
			else
			{
				$(this).children("a").children().removeClass("icon-chevron-up");
				$(this).children("a").children().addClass("icon-chevron-down");
			}
			}else{
				$(this).children("a").siblings("ul").addClass("is-hidden");
				$(this).children("a").children().removeClass("icon-chevron-up");
								$(this).children("a").children().addClass("icon-chevron-down");
			
			}
			
			
		});
		
	});
	
	
	
	function hideall(test){
		
		$("#nav .primary-nav li").each(function() {
			if($(this).children("a").children().hasClass("icon-chevron-up"))
			{
				$(this).children("a").children().removeClass("icon-chevron-up");
				$(this).children("a").children().addClass("icon-chevron-down");
			}
			
			$(this).children("ul").addClass("is-hidden");
		});
		
	}
	
	
	/*-------------dropdown navigation---------------*/
	$("#js-nav-toggle").click(function(){
		
        $(this).parents("#header").next().children().children().toggleClass("is-visible");
		
    });
	
$("#nav .primary-nav li a").click(function(){
		
        $(this).siblings("").toggleClass("is-hidden3");
		$(this).children().toggleClass("icon-chevron-up");
    });
	
	$("#nav .primary-nav li ul li:first-child").click(function(){
	$(this).parent().removeClass("is-hidden3");
	
    });
	
	$("#js-search-toggle").click(function(){
		
	$(this).parent().siblings("#js-form-search").slideToggle();
	
    });
	

  $('.video-placeholder').on('click', function(ev) {
	$(this).fadeOut('fast'); 
 
    $("#video")[0].src += "?autoplay=1";
    ev.preventDefault();
	
  
  });

	
});
</script>
</body>
</html>