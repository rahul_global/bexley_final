<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$id = decrypt($_REQUEST['id'],$encrypt);
$usertype = decrypt($_REQUEST['type'],$encrypt);

$selcli = "select concat(_Firstname,' ',_Lastname) as clientname,_ID from ".$tbname."_clientmaster where _ID = '".$id."'";
$rowcli = mysqli_fetch_assoc(mysqli_query($con,$selcli));
$clientname  = $rowcli['clientname'];

$bread = "<a href='edit-client.php?e_action=".encrypt('edit',$encrypt)."&id=".encrypt($id,$encrypt)."'>".$clientname."</a>";
$title = "Winterwood : Add Communication";

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
        <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="js/timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css" />
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allclient.php">All Carers</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="commaction.php" method="post">

                                        <div class="form-group">
                                            <label for="txtdate" class="col-sm-2 control-label">Date</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="dp_basic" name="txtdate" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtlname" class="col-sm-2 control-label">Communication Type</label>
                                            <div class="col-sm-9">
                                                <select name="txtcomtype" required class="form-control">
                                                    <option value="">-Select Communication Type-</option>
                                                    <?php 
                                                        $seltype = "Select * from ".$tbname."_communication_type";
                                                        $rsttype = mysqli_query($con,$seltype);
                                                        while($rowtype = mysqli_fetch_assoc($rsttype)){ ?>
                                                            <option value="<?php echo $rowtype['_ID']; ?>"><?php echo $rowtype['_Name']; ?></option>
                                                <?php   }
                                                    ?>
                                                </select>
                                                <input type="hidden" name="type" id="type" value="<?php echo encrypt($usertype,$encrypt); ?>">
                                                <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                                <input type="hidden" name="e_action" value="<?php echo encrypt('clientcom',$encrypt); ?>" />
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <label for="txtperson" class="col-sm-2 control-label">Person Spoken To</label>
                                            <div class="col-sm-9">
                                                <input type="" class="form-control" id="txtperson" name="txtperson" >
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                         <div class="form-group">
                                            <label for="txtnotes" class="col-sm-2 control-label">Notes</label>
                                            <div class="col-sm-9">
                                                 <textarea class="form-control" name="txtnotes" id="txtnotes"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txttime" class="col-sm-2 control-label">Time</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control time" name="txttime" id="txttime">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtemail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtemail" id="txtemail">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>    
                                            
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="Submit" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "allclient.php";
            return false;
        });
    });
</script>
        <script type="text/javascript">
        $(function() {
            var date = Date();
            $('.time').timepicker({ 'timeFormat': 'H:i' });
            $('#dp_basic').datepicker({startDate: date, format: 'dd/mm/yyyy' });
        })
</script>
        
        
    </body>
</html>
