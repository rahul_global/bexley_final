<?php 
require_once('db/dbopen.php');
$action = decrypt($_REQUEST['e_action'],$encrypt);



	if($action == "edit")
	{

		$cmspage_id		= decrypt($_POST['cmspage_id'],$encrypt);
		$title 			= replaceSpecialChar($_POST['txttitle']);
		$url 			= replaceSpecialChar($_POST['txturl']);
		$content		= str_replace('&quot;','"',$_POST['wysiwg_editor']);
		$content2 	= str_replace('&quot;','"',$_POST['wysiwg_editor2']);
		$img   			= $_POST['img'];
		$imagefilename  = basename($_FILES["img"]["name"]);
		$targetfolder 	= $PicPath.$imagefilename;
		$status			= replaceSpecialChar($_POST['txtstatus']);
		$parent			= $_POST['parentpage'] ? $_POST['parentpage'] : 0;

		if(isset($_POST['txtclub']))
		{
			$page_type = $_POST['txtclub'];

		}else if(isset($_POST['txtnotclub']))
		{
			$page_type = $_POST['txtnotclub'];
		
		}else
		{
			$page_type = 0;
		}
		
		$header = $_POST['txtheader'];
		$footer = $_POST['txtfooter'];
		$txtpageorder = $_POST['txtpageorder'] ? $_POST['txtpageorder'] : 0;

		if(empty($_FILES["img"]['name'])){
			$imagefilename  = null;
		}else{
			$imagefilename  = rand().basename($_FILES["img"]["name"]);
		}


		if($action != '' && $status != '' && $cmspage_id != '' && $content != '')
		{
			$check_qry = "SELECT _ID , _PID, _Title , _Content, _Content2, _Image , _Status, _Url, _Topdisplay, _Footerdisplay, IS_Club FROM ".$tbname."_cmspages WHERE _ID = $cmspage_id";
			$run_check = mysqli_query($con, $check_qry);
			$num = mysqli_num_rows($run_check);
			if($num > 0)
			{
				$fetch_data = mysqli_fetch_assoc($run_check);
				
				$qry  	= "UPDATE ".$tbname."_cmspages SET ";
				$query 	= "";
				
				if($parent != $fetch_data['_PID'])
				{
					$query .= " _PID = '$parent',";
				}

				if($title != $fetch_data['_Title'])
				{
					$query .= " _Title = '$title',";
				}
				if($content != $fetch_data['_Content'])
				{
					$query .= " _Content = '$content',";
				}
				if($content2 != $fetch_data['_Content2'])
				{
					$query .= " _Content2 = '$content2',";
				}

				if($imagefilename != '')
				{
					$query .= " _Image = '$imagefilename', ";
				}

				if($status != $fetch_data['_Status'])
				{
					$query .= " _Status = '$status',";
				}

				if($txtpageorder != $fetch_data['_Order'])
				{
					$query .= " _Order = '$txtpageorder',";
				}

				if($header != $fetch_data['_Topdisplay'])
				{
					$query .= " _Topdisplay = '$header',";
				}

				if($footer != $fetch_data['_Footerdisplay'])
				{
					$query .= " _Footerdisplay = '$footer',";
				}else
				{
					$query .= " _Footerdisplay = NULL ,"; 
				}

				if($page_type != $fetch_data['IS_Club'])
				{
					$query .= " IS_Club = '$page_type',";
				}
				
				$query = rtrim($query , ',');			
				
				if($query != '')
				{
					 $query .= " WHERE _ID = $cmspage_id"; 

					 $query = $qry.''.$query; 

					 /*echo "<pre>";
					 print_r($_REQUEST);
					 echo "</pre>";

					 echo $query;
					 exit;*/
					if($run = mysqli_query($con , $query))
					{
						if(isset($_FILES['img'])){
							move_uploaded_file($_FILES["img"]["tmp_name"], $targetfolder);
						}
						//header("location:allcmspages.php?result=".encrypt('success',$encrypt));
						echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
						exit();
					}
					else
					{
						header("location:edit-cmspage.php?result=".encrypt('failed',$encrypt)."&id=".$cmspage_id."&e_action=".encrypt('edit',$encrypt));
						exit;
					}
				}
				else
				{
					header("location:allcmspages.php?result=".encrypt('success',$encrypt));
					exit;
				}
			}
			else
			{
				header("location:edit-cmspage.php?result=".encrypt('failed',$encrypt)."&id=".$cmspage_id."&e_action=".encrypt('edit',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-cmspage.php?result=".encrypt('fillall',$encrypt)."&id=".$cmspage_id."&e_action=".encrypt('edit',$encrypt));
			exit;
		}
	}
	else if($action == "add")
	{	

		$title 			= replaceSpecialChar($_POST['txttitle']);
		$url 			= replaceSpecialChar($_POST['txturl']);
		$content 		= replaceSpecialChar($_POST['wysiwg_editor']);
		$content2 		= replaceSpecialChar($_POST['wysiwg_editor2']);
		$image 			= $_POST['img'];
		$status			= replaceSpecialChar($_POST['txtstatus']);
		$header 		= $_POST['txtheader'];
		$txtpageorder   = $_POST['txtpageorder'] ? $_POST['txtpageorder'] : 0;
		$footer 		= $_POST['txtfooter'];
		$parent			= $_POST['parentpage'] ? $_POST['parentpage'] : 0;
		
		if(isset($_POST['txtclub']))
		{

			$page_type = $_POST['txtclub'];

		}else if(isset($_POST['txtnotclub']))
		{

			$page_type = $_POST['txtnotclub'];
		
		}else
		{
		
			$page_type = 0;
		
		}
/*
		echo $page_type;
		exit;*/

		if(empty($_FILES["img"]['name'])){
			$imagefilename  = null;
		}else{
			$imagefilename  = rand().basename($_FILES["img"]["name"]);
		}
		
		$targetfolder 	= $PicPath.$imagefilename;
		
		if($action != '' && $title != '' && $content != '' && $status != '')
		{
			 $query 	= "INSERT INTO ".$tbname."_cmspages(_PID, _Title , _Content, _Content2 ,_Image ,_Status, _Url,_Topdisplay,_Footerdisplay,_Order, IS_Club) VALUES(".$parent.", '".$title."','".$content."','".$content2."','".$imagefilename."','".$status."','".$url."','".$header."','".$footer."','".$txtpageorder."','".$page_type."')"; 

			 
			 
			if($run 	= mysqli_query($con , $query))
			{	
				$lid = mysqli_insert_id($con);
				if(isset($_FILES["img"])){
					move_uploaded_file($_FILES["img"]["tmp_name"], $targetfolder);	
				}
				echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
			}
			else
			{
				header("location:edit-cmspage.php?result=".encrypt('failed',$encrypt)."&e_action=".encrypt('add',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-cmspage.php?result=".encrypt('fillall',$encrypt)."&e_action=".encrypt('add',$encrypt));
			exit;
		}


	}
	else if($action == "delete")
	{
		$id 	= decrypt($_GET['id'],$encrypt);	

		$getimgname = mysqli_query($con , "SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");

		$resultimagename = mysqli_fetch_assoc($getimgname);

		$imagename = $resultimagename['_Image'];

		$target  = $PicPath.$imagename;

		if($imagename != '')
		{
			if(file_exists($target))
			{
				unlink($target);
				$query 	= "DELETE  FROM ".$tbname."_cmspages WHERE _ID = $id";
		
				if($run = mysqli_query($con , $query))
				{
					header("location:allcmspages.php?result=".encrypt('success',$encrypt));
					exit;
				}
				else
				{
					header("location:allcmspages.php?result=".encrypt('failed',$encrypt));
					exit;
				}
			}else
			{
				$query 	= "DELETE  FROM ".$tbname."_cmspages WHERE _ID = $id";
		
				if($run = mysqli_query($con , $query))
				{
					header("location:allcmspages.php?result=".encrypt('success',$encrypt));
					exit;
				}
				else
				{
					header("location:allcmspages.php?result=".encrypt('failed',$encrypt));
					exit;
				}
			}
		}else
		{

			$query 	= "DELETE  FROM ".$tbname."_cmspages WHERE _ID = $id";
		
			if($run = mysqli_query($con , $query))
			{
				header("location:allcmspages.php?result=".encrypt('success',$encrypt));
				exit;
			}
			else
			{
				header("location:allcmspages.php?result=".encrypt('failed',$encrypt));
				exit;
			}
		}

		
	}else if($action == "delete_img")
	{
		/*echo 'sss';
		exit;*/
		$imgid = decrypt($_REQUEST['ID'],$encrypt);
		$getimg = mysqli_query($con,"SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$imgid."'");
		$imagerecord = mysqli_fetch_assoc($getimg);
		$dltimage = $_SERVER['DOCUMENT_ROOT'].'/bexley_new1/images/'.$imagerecord['_Image'];
		//echo $dltimage;
		//echo "DELETE FROM ".$tbnamr."_cmspages WHERE _Image = '".$imagerecord['_Image']."'";
		//exit;
		if(file_exists($dltimage))
		{
			unlink($dltimage);
			$imgsqldlt = mysqli_query($con,"UPDATE ".$tbname."_cmspages SET _Image = '' WHERE _Image = '".$imagerecord['_Image']."'");	
			echo '<script type="text/javascript">alert("Image deleted."); window.location="edit-cmspage.php?id='.encrypt($imagerecord["_ID"],$encrypt).'&e_action='.encrypt("edit",$encrypt).'";</script>';

		}else
		{
			echo '<script type="text/javascript">alert("Image not exists.");window.location="edit-cmspage.php?id='.encrypt($imagerecord["_ID"],$encrypt).'&e_action='.encrypt("edit",$encrypt).'";</script>';
		}
	}

?>
