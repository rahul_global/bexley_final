<?php 
require_once('db/dbopen.php');

$firstname 			= replaceSpecialChar($_POST['pro_first_name']);
$lastname 			= replaceSpecialChar($_POST['pro_last_name']);
$email 				= $_POST['pro_email'];
$username 			= replaceSpecialChar($_POST['pro_username']);
$current_password 	= $_POST['pro_current_password'];
$new_password 		= $_POST['pro_new_password'];

$userdata 			= $_SESSION['user_data'];

if($firstname != '' && $lastname != '' && $email != ''  && $username != ''  && $current_password != '')
{
	$check_qry = "SELECT _ID , _Firstname , _Lastname , _Username , _Email FROM ".$tbname."_usermaster WHERE _Email = '".$userdata['_Email']."' AND _Password = '".$current_password."' ";
	$run_check = mysqli_query($con, $check_qry);
	$num = mysqli_num_rows($run_check);
	if($num > 0)
	{
		$fetch_data = mysqli_fetch_assoc($run_check);
		
		$qry = "UPDATE ".$tbname."_usermaster SET ";
		$query='';
		if($firstname != $userdata['_Firstname'])
		{
			$query .= " _Firstname = '$firstname' ,";
		}
		if($lastname != $userdata['_Lastname'])
		{
			$query .= " _Lastname = '$lastname' ,";
		}
		if($email != $userdata['_Email'])
		{
			$query .= " _Email = '$email' ,";
		}
		if($username != $userdata['_Username'])
		{
			$query .= " _Username = '$username' ,";
		}
		
		if($new_password != '')
		{
			$query .= " _Password = '$new_password'";
		}
		$query = rtrim($query , ',');
		
		if($query != '')
		{
			$query .= " WHERE _ID = ".$fetch_data['_ID'];
			$query = $qry.''.$query;
			if($run = mysqli_query($con , $query))
			{
				$session_qry = "SELECT * FROM ".$tbname."_usermaster WHERE _ID = ".$fetch_data['_ID'];
				$run_session = mysqli_query($con , $session_qry);
				$fetch_session = mysqli_fetch_assoc($run_session);
				unset($fetch_session['_Password']);
				$_SESSION['user_data'] = $fetch_session;
				header("location:my_profile.php?result=".encrypt('success',$encrypt));
				exit;
			}
			else
			{
				header("location:my_profile.php?result=".encrypt('failed',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:my_profile.php?result=".encrypt('success',$encrypt));
			exit;
		}
	}
	else
	{
		header("location:my_profile.php?result=".encrypt('failed',$encrypt));
		exit;
	}
}
else
{
	header("location:my_profile.php?result=".encrypt('failed',$encrypt));
	exit;
}
?>