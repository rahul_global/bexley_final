<?php
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
//_Frequency 0 for daily, 1 for weekly, and 2 for monthly
$subid = array();
$pupilid = array();
$levid = array();
	$tutorid = decrypt($_GET['id'],$encrypt);
	
	$result = decrypt($_GET['result'],$encrypt);

	$query = "SELECT CONCAT(_Firstname ,' ',_Lastname) _Name FROM ".$tbname."_tutormaster WHERE _ID = $tutorid";
	$run = mysqli_query($con,$query);
	$fetch = mysqli_fetch_assoc($run);
	
	$img_path = "uploads/tutors/";

	$selstu = "select * from ".$tbname."_tut_pup_assign where _TutorID = '".$tutorid."'";
	$rststu = mysqli_query($con,$selstu);
	$studid = array();
	while ($rowstu = mysqli_fetch_assoc($rststu)) {
		array_push($studid,$rowstu['_PupilID']);
	}
	$studentid = implode(",", $studid);
	$img = (isset($fetch['_Image'])&& $fetch['_Image'] != NULL)?$fetch['_Image']:'no_image.png';
	
	$selsub = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$tutorid."' and _Approve = '1'";
	$rstsub = mysqli_query($con,$selsub);
	if(mysqli_num_rows($rstsub) > 0){
		
		while($rssub = mysqli_fetch_assoc($rstsub)){
			array_push($subid, $rssub['_SubjectID']);
		}  
		
	}
	$sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$tutorid."' and _Approve = '1'";
	$rstlev = mysqli_query($con,$sellev);
	if(mysqli_num_rows($rstlev) > 0){
		
		while($rssub = mysqli_fetch_assoc($rstlev)){
			array_push($levid, $rssub['_LevelID']);
		}  
		
	}







	$title = $sitename." : Book Lesson";
	$bread = "Book Lessons";
	$lnkbread = "<a href='edit-tutor.php?id=".$_GET['id']."&e_action=".encrypt('edit',$encrypt)."'>".$fetch['_Name']."</a>";
?>
<!DOCTYPE html>
<html>
	<head>
	<?php include 'topscript.php'; ?>
            <!-- date picker -->
	  		<link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
             <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/timepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="css/timepicker.css" />
		
	</head>
	<body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">Staff</a></li><li><?php echo $lnkbread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
							<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
							<?php }else if(isset($result) && $result == 'failed'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
							<?php }else if(isset($result) && $result == 'alreadytrial'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Trial lesson is already booked for this Pupil.</div>
							<?php
								}else if(isset($result) && $result == 'already'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Booking is Already done for this Pupil.</div>
							<?php
								}else if(isset($result) && $result == 'undefined'){ ?>
							  <div role="alert" class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Selected Staff Is Not Available At This Time.</div>
							  <?php
								}
							?>
                                    <div id="response_msg" class=""></div>
                                   <form class="form-horizontal" role="form" name="book_actual" id="book_actual" action="tutbooklesson.php" method="post">
										
                                        <h3 class="heading_a"><span class="heading_text">Book Lessons</span></h3>
                                        <div class="form-group">
											<label for="txtftime" class="col-sm-2 control-label">Name</label>
											<div class="col-sm-3">
												<h5><?php echo $fetch['_Name']; ?></h5>
											</div>
										</div>
										<div class="form-group">
                                            <label for="form-input" class="col-sm-2 control-label">Select Pupil</label>
                                            <div class="col-sm-3">
                                            	<select name="txtpupil" id="txtpupil" class="form-control" required>
                                            	<option value=""> -Select Pupil- </option>
                                                <?php 
                                                	$pupil = "Select _ID,CONCAT(_Firstname,' ',_Lastname) _Name From ".$tbname."_pupilmaster where _ID in (".$studentid.")";
                                                	$rstpupil = mysqli_query($con,$pupil);
                                                	while($rowpupil = mysqli_fetch_assoc($rstpupil)){ ?>

                                                		<option value="<?php echo $rowpupil['_ID']; ?>"><?php echo $rowpupil['_Name']; ?></option>

                                            <?php  	}
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtsubject" class="col-sm-2 control-label">Select Approved Subject</label>
                                            <div class="col-sm-3">
                                                
												<select class="form-control" name="txtsubject" required oninvalid="this.setCustomValidity('Please Select Approved Subject')">
													<option value="">-Select Approved Subject-</option>
													<?php
													$subjectid = implode(",", $subid);
													$subject_qry = "SELECT _ID , _Name FROM ".$tbname."_subjects WHERE _ID in (".$subjectid.") ";
													$run_subject = mysqli_query($con , $subject_qry);
													while($fetch_subject = mysqli_fetch_assoc($run_subject))
													{
													?>
														<option value="<?php echo $fetch_subject['_ID'];?>"><?php echo $fetch_subject['_Name'];?></option>
													<?php
													}
													?>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
	                                    <div class="form-group">
												<label for="txtlevels" class="col-sm-2 control-label">Select Approved Level&nbsp;</label>
												<div class="col-sm-3">
													<select name="txtlevels" class="form-control" required oninvalid="this.setCustomValidity('Please Select Approved Level')">
														<option value="">-Select Approved Level-</option>
														<?php
														$levelid = implode(",",$levid);
														$sellevels = "Select _ID,_Title from ".$tbname."_levels where _ID in (".$levelid.")";
														$rstlevels = mysqli_query($con,$sellevels);
														while($rowlevel = mysqli_fetch_assoc($rstlevels)){
															?>
															<option value="<?php echo $rowlevel['_ID']; ?>"><?php echo $rowlevel['_Title']; ?></option>

												<?php 	} 
														?>
													</select>
												</div>
												
											</div>
                                    <div class="row">
                                    	
										<div id="trial" class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
											
												<h3 class="heading_a"><span class="heading_text">Book Trial Lesson</span></h3>
												 <div class="form-group">
		                                            <label for="form-input" class="col-sm-3 control-label">Date</label>
		                                            <div class="col-sm-3">
		                                                <input type="text" class="form-control" id="dp_basic" name="txttrdate" required>
		                                            </div>
		                                        </div>                                        
												<div class="form-group">
													<label for="txtftime" class="col-sm-3 control-label">From Time</label>
													<div class="col-sm-3">
														<input id="timeformatExample1" type="text" class="time" name="txttrftime" required style="width: 100%;" /> 
													</div>
												</div>
												<div class="form-group">
													<label for="txtttime" class="col-sm-3 control-label">To Time</label>
													<div class="col-sm-3">
														<input name="txttrttime" id="timeformatExample2" type="text" class="time" required style="width: 100%;"/>
													</div>
												</div>
												<input type="hidden" name="tutorid" value="<?php echo encrypt($tutorid,$encrypt); ?>">
												<input type="hidden" name="e_action" value="<?php echo encrypt('book',$encrypt); ?>">
											
										</div>
										<div id="actual" class="col-md-6 col-lg-6 col-sm-12 col-xs-12" >
											<h3 class="heading_a"><span class="heading_text">Book Actual Lesson</span></h3>
											
												<div class="form-group">
		                                            <label for="form-input" class="col-sm-3 control-label">Date</label>
		                                            <div class="col-sm-3">
		                                                <input type="text" class="form-control" id="dp_basic1" name="txtacdate" required>
		                                            </div>
		                                        </div>                                        
												<div class="form-group">
													<label for="txtftime" class="col-sm-3 control-label">From Time</label>
													<div class="col-sm-3">
														<input id="timeformatExample11" type="text" class="time" name="txtacftime" required style="width: 100%;"/> 
													</div>
												</div>
												<div class="form-group">
													<label for="txtttime" class="col-sm-3 control-label">To Time</label>
													<div class="col-sm-3">
														<input name="txtacttime" id="timeformatExample21" type="text" class="time" required style="width: 100%;"/>
													</div>
												</div>												
												<div class="form-group">
		                                            <label for="form-input" class="col-sm-3 control-label">Frequency</label>
		                                            <div class="col-sm-3">
		                                            	<select name="txtfreq" id="txtfreq" class="form-control" required>
		                                            			<option value=""> -Frequency- </option>
		                                                		<option value="0">Daily</option>
		                                                		<option value="1">Weekly</option>
		                                                		<option value="2">Monthly</option>
		                                                </select>
		                                            </div>
	                                        	</div>
										</div>
																				
										<div class="form-group text-center" id="btngroup" >
                                            <div class="col-sm-10">
                                                <input type="Submit" class="btn-primary btn" value="Go" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
										
                                    </div>
								</form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "alltutor.php";
            return false;
        });
		
		$("#book_actual").submit(function(){
			var tft = $("#timeformatExample1").val();
			var ttt = $("#timeformatExample2").val();
			
			var bft = $("#timeformatExample11").val();
			var btt = $("#timeformatExample21").val();
			
			if(tft >= ttt || bft >= btt)
			{
				alert("Please Select Proper Time.");
				return false;
			}
			return true;
		});
		
        /*$("#txttype").change(function(){
        	//alert($(this).val());
        	var type = $(this).val();
        	if(type == '1'){
        		$("#trial").show();
        		$("#actual").hide();
        		$("#btngroup").show();

        	}else if(type == '2'){
        		$("#trial").hide();
	       		$("#actual").show();
	       		$("#btngroup").show();
        	}else{
        		$("#trial").hide();
        		$("#actual").hide();
        		$("#btngroup").hide();
        	}
        });

        // form submition
        $("#btnsubmit").click(function(){
        	var pupil = $("#txtpupil").val();
        	var type = $("#txttype").val();
        	if(pupil == ""){
        		alert("Please Select Pupil");
        		return false;
        	}else{
        		if(type == "1"){
        			$("#trpupil").val(pupil);
        			$("#book_trial").submit();
        		}else{
        			$("#acpupil").val(pupil);
        			$("#book_actual").submit();
        		}
        	}

        });*/
    });
</script> 
<script type="text/javascript">
		$(function() {
			var date = Date();
			$('.time').timepicker({ 'timeFormat': 'H:i' });
			$('#dp_basic').datepicker({startDate: date, format: 'dd/mm/yyyy' });
			$('#dp_basic1').datepicker({startDate: date, format: 'dd/mm/yyyy' });
		})
</script>
    </body>
</html>