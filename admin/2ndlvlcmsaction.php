<?php
require_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}



$action = decrypt($_REQUEST['e_action'],$encrypt);



if(isset($_REQUEST['e_action'])  && $action == 'Update')
{
	$action 			= decrypt($_REQUEST['e_action'],$encrypt);

	$id 				= decrypt($_REQUEST['cmspage_id'],$encrypt);

	$Hyperlink 			= $_REQUEST['Hyperlink'];

	$cmspage_id 		= decrypt($_REQUEST['cmspage_id'],$encrypt);

	$txttitle 			= addslashes($_REQUEST['txttitle']);

	$txturl 			= $_REQUEST['txturl'];

	$wysiwg_editor 		= str_replace('&quot;','"',$_POST['wysiwg_editor']);

	$wysiwg_editor2 	= str_replace('&quot;','"',$_POST['wysiwg_editor2']);

	$img   				= $_POST['img'];
	
	$imagefilename  	= basename($_FILES["img"]["name"]);
	
	$txtstatus 			= $_REQUEST['txtstatus'];	

	$parentpage 		= $_POST['parentpage'] ? $_POST['parentpage'] : 0;
	$txtpageorder 		= $_POST['txtpageorder'] ? $_POST['txtpageorder'] : 0;

	if(empty($_FILES["img"]['name'])){
		$imagefilename  = isset($_POST['img']) ? $_POST['img'] : null;
	}else{
		$imagefilename  = rand().basename($_FILES["img"]["name"]);
	}

	$targetfolder 	= $PicPath.$imagefilename;	

	$sql_update_cms_page = "UPDATE ".$tbname."_cmspages SET ";

	if($parentpage >= 0)
	{

		$sql_update_cms_page .= "_PID = '".$parentpage."' , ";

	}else
	{

		$sql_update_cms_page .= "_PID = NULL , ";

	}

	if($Hyperlink != '')
	{

		$sql_update_cms_page .= "_Hyperlink = '".$Hyperlink."' , ";

	}else
	{

		$sql_update_cms_page .= "_Hyperlink = NULL , ";

	}

	if($txttitle != '')
	{

		$sql_update_cms_page .= "_Title = '".$txttitle."' , ";

	}else
	{

		$sql_update_cms_page .= "_Title = NULL , ";

	}

	if($txturl != '')
	{

		$sql_update_cms_page .= "_Url = '".$txturl."' , ";

	}else
	{

		$sql_update_cms_page .= "_Url = NULL , ";

	}

	if($wysiwg_editor != '')
	{

		$sql_update_cms_page .= "_Content = '".$wysiwg_editor."' , ";

	}else
	{

		$sql_update_cms_page .= "_Content = NULL , ";

	}

	if($wysiwg_editor2 != '')
	{

		$sql_update_cms_page .= "_Content2 = '".$wysiwg_editor2."' , ";

	}else
	{

		$sql_update_cms_page .= "_Content2 = NULL , ";

	}

	if($imagefilename != '')
	{

		$sql_update_cms_page .= "_Image = '".$imagefilename."' , ";

	}else
	{

		$sql_update_cms_page .= "_Image = ' ' , ";

	}

	if($txtstatus != '')
	{

		$sql_update_cms_page .= "_Status = '".$txtstatus."' , ";

	}else
	{

		$sql_update_cms_page .= "_Status = NULL , ";

	}

	
	$sql_update_cms_page .= "_Topdisplay = '0' , ";


	if($txtpageorder != '')
	{
		$sql_update_cms_page .= "_Order = '".$txtpageorder."' , ";
	}

	if($txtfooter != '')
	{

		$sql_update_cms_page .= "_Footerdisplay = '".$txtfooter."' ";

	}else
	{

		$sql_update_cms_page .= "_Footerdisplay = NULL  ";

	}

	$sql_update_cms_page .= "WHERE _ID = '".$cmspage_id."'";

	$sql = mysqli_query($con,$sql_update_cms_page);

	if($sql)
	{
		
		if(isset($_FILES["img"])){
			move_uploaded_file($_FILES["img"]["tmp_name"], $targetfolder);	
		}
		//echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
		$pid = $_REQUEST['pid'];
		echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
		exit;
	
	}else
	{

		echo '<script type="text/javascript">alert("Page is not updated."); window.location="allcmspages.php";</script>';
		exit;

	}


} 

if(isset($_REQUEST['e_action'])  && $action == 'add')
{	
		
		$parentpage = $_POST['parentpage'] ? $_POST['parentpage'] : 0;
		$Hyperlink = $_POST['Hyperlink'];
		$title 		= replaceSpecialChar($_POST['txttitle']);
		$url 			= replaceSpecialChar($_POST['txturl']);
		$content 	= replaceSpecialChar($_POST['wysiwg_editor']);
		$content2 	= replaceSpecialChar($_POST['wysiwg_editor2']);
		$image 			= $_POST['img'];
		$imagefilename  = basename($_FILES["img"]["name"]);
		$targetfolder 	= $PicPath.$imagefilename;
		$txtpageorder 		= $_POST['txtpageorder'] ? $_POST['txtpageorder'] : 0;

		if(empty($_FILES["img"]['name'])){
			$imagefilename  = null;
		}else{
			$imagefilename  = rand().basename($_FILES["img"]["name"]);
		}
		
//		$wysiwg_editor 		= str_replace('&quot;','"',$_POST['wysiwg_editor']);
		$status		= replaceSpecialChar($_POST['txtstatus']);
		$header = $_POST['txtheader'];
		
		$footer = $_POST['txtfooter'];
		
		

		if($action != '' && $title != '' && $content != '' && $status != '')
		{
			 $query 	= "INSERT INTO ".$tbname."_cmspages(_PID, _Title , _Content, _Content2, _Order, _Image ,_Status, _Url,_Topdisplay,_Footerdisplay, _Hyperlink, _Level) VALUES(";

			 if($parentpage != '')
			 {
			 	$query .= "'".$parentpage."', ";
			 }else
			 {
			 	$query .= " NULL , ";
			 }

			 if($title != '')
			 {
			 	$query .= "'".$title."', ";
			 }else
			 {
			 	$query .= " NULL , ";
			 }

			 if($content != '')
			 {
			 	$query .= "'".$content."', ";
			 }else
			 {
			 	$query .= " NULL , ";
			 }

			 if($content2 != '')
			 {
			 	$query .= "'".$content2."', ";
			 }else
			 {
			 	$query .= " NULL , ";
			 }

			 if($txtpageorder != ''){
			 	$query .= "'".$txtpageorder."', ";
			 }else{
			 	$query .= " 0 , ";
			 }

			 if($imagefilename != '')
			 {
			 	$query .= "'".$imagefilename."', ";
			 }else
			 {
			 	$query .= " ' ' , ";
			 }

			 if($status != '')
			 {
			 	$query .= "'".$status."', ";
			 }else
			 {
			 	$query .= " NULL , ";
			 }

			 if($url != '')
			 {
			 	$query .= "'".$url."', ";
			 }else
			 {
			 	$query .= " NULL ,";
			 }

			 
			 $query .= "'0', ";
			 

			 if($footer != '')
			 {
			 	$query .= "'".$footer."', ";
			 }else
			 {
			 	$query .= " NULL , ";
			 }

			 if($Hyperlink != '')
			 {
			 	$query .= "'".$Hyperlink."', ";
			 }else
			 {
			 	$query .= " '0' , ";
			 }

			 $query .= "'2')";
			 /*echo $query;
			 exit;*/
			if($run 	= mysqli_query($con , $query))
			{
				$cmspage_id = mysqli_insert_id($con);
				if(isset($_FILES['img'])){
					move_uploaded_file($_FILES["img"]["tmp_name"], $targetfolder);
				}
				header("location:allcmspages.php?result=".encrypt('success',$encrypt));
				echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
				exit;
			}
			else
			{
				header("location:edit-2ndlvlcms.php?result=".encrypt('failed',$encrypt)."&e_action=".encrypt('add',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-2ndlvlcms.php?result=".encrypt('fillall',$encrypt)."&e_action=".encrypt('add',$encrypt));
			exit;
		}


}

if(isset($_REQUEST['e_action']) && decrypt($_REQUEST['e_action'],$encrypt) == 'delete')
{

	$id =  decrypt($_REQUEST['id'],$encrypt);

	$getimgname = mysqli_query($con , "SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");

	$resultimagename = mysqli_fetch_assoc($getimgname);

	$imagename = $resultimagename['_Image'];

	$target  = $PicPath.$imagename; 
	if($imagename != '')
	{
		if(file_exists($target))
		{
			unlink($target);
			$sql_deletechild = mysqli_query($con,"DELETE FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");

			if($sql_deletechild)
			{
				echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
			}
		}else
		{
			$sql_deletechild = mysqli_query($con,"DELETE FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");

			if($sql_deletechild)
			{
				echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
			}
		}	
	}else
	{
		$sql_deletechild = mysqli_query($con,"DELETE FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");

		if($sql_deletechild)
		{
			echo '<script type="text/javascript">window.location="allcmspages.php";</script>';
		}
	}
	
	

}

if($action == "delete_img")
{
		$imgid = decrypt($_REQUEST['id'],$encrypt);
		$pid = decrypt($_REQUEST['pid'],$encrypt);
		$getimg = mysqli_query($con,"SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$imgid."'");
		$imagerecord = mysqli_fetch_assoc($getimg);
		$dltimage = $_SERVER['DOCUMENT_ROOT'].'/bexley_new1/images/'.$imagerecord['_Image'];
		//echo $dltimage;
		//echo "DELETE FROM ".$tbnamr."_cmspages WHERE _Image = '".$imagerecord['_Image']."'";
		//exit;
		if(file_exists($dltimage))
		{
			unlink($dltimage);
			$imgsqldlt = mysqli_query($con,"UPDATE ".$tbname."_cmspages SET _Image = '' WHERE _Image = '".$imagerecord['_Image']."'");	
			echo '<script type="text/javascript">alert("Image deleted."); window.location="edit-2ndlvlcms.php?id='.encrypt($imagerecord["_ID"],$encrypt).'&pid='.encrypt($pid,$encrypt).'&e_action='.encrypt("managelevel",$encrypt).'";</script>';

		}else
		{
			echo '<script type="text/javascript">alert("Image not deleted."); window.location="allcmspages.php";</script>';
		}
}





?>