<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);

if($action == 'edit'){

$title = $sitename." : Edit Resource Type";
$id = decrypt($_REQUEST['id'],$encrypt);
$sel_clint = "select * from ".$tbname."_resourcetypes where _ID='".$id."'";
$rst_clint = mysqli_query($con,$sel_clint);
$row = mysqli_fetch_assoc($rst_clint);
$type = $row['_Type'];
$bread = "Edit Resource Type";
$btntext = 'Update';

}else{
    $title = $sitename." : Add Resource Type";
    $bread = "Add Resource Type";
    $btntext = 'Insert';
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="resourcetype.php">All Resouce Type</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmrtype" id="frmrtype" action="resourcetypeaction.php" method="post">
                                        <h3 class="heading_a"><span class="heading_text">Resource Type</span></h3>
                                        
                                        <div class="form-group">
                                            <label for="txtlname" class="col-sm-2 control-label">Resource Type</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="txtname" name="txttype" value="<?php echo $type;?>" required>
                                                <input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                                <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                            </div>
                                        </div>
                                        
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "resourcetype.php";
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
