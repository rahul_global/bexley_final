-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2017 at 03:47 AM
-- Server version: 5.6.32-78.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myquickd_bexley`
--

-- --------------------------------------------------------

--
-- Table structure for table `ww_assi_tutcli_rel`
--

CREATE TABLE IF NOT EXISTS `ww_assi_tutcli_rel` (
  `_ID` int(10) NOT NULL,
  `_ClientID` varchar(200) NOT NULL,
  `_TutorID` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_auditlog`
--

CREATE TABLE IF NOT EXISTS `ww_auditlog` (
  `_ID` int(10) NOT NULL,
  `_UserID` int(10) NOT NULL,
  `_Activity` text NOT NULL,
  `_Adatetime` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_auditlog`
--

INSERT INTO `ww_auditlog` (`_ID`, `_UserID`, `_Activity`, `_Adatetime`) VALUES
(1, 1, 'Logged Out', '2017-01-09 19:01:54'),
(2, 1, 'Logged In', '2017-02-15 18:05:03'),
(3, 1, 'Logged In', '2017-02-15 19:36:23'),
(4, 1, 'Logged Out', '2017-02-15 19:37:15'),
(5, 1, 'Logged In', '2017-02-15 19:37:43'),
(6, 1, 'Logged In', '2017-02-15 20:20:02'),
(7, 1, 'Logged In', '2017-02-16 11:25:27'),
(8, 1, 'Logged Out', '2017-02-16 11:46:14'),
(9, 1, 'Logged In', '2017-02-16 16:24:01'),
(10, 1, 'Deleted Client globalindiatechtest@gmail.com', '2017-02-16 18:19:04'),
(11, 1, 'Deleted Pupil globalindiatechtest@gmail.com', '2017-02-16 18:19:18'),
(12, 1, 'Deleted Client globalindiatechtest@gmail.com', '2017-02-16 18:35:42'),
(13, 1, 'Logged In', '2017-02-24 12:09:47'),
(14, 1, 'Logged Out', '2017-02-24 12:12:32'),
(15, 1, 'Logged In', '2017-02-24 13:05:42'),
(16, 1, 'Added New Client globalindiatechtest@gmail.com', '2017-02-24 15:18:06'),
(17, 1, 'Sent Welcome Mail To Client globalindiatechtest@gmail.com', '2017-02-24 15:18:26'),
(18, 1, 'Sent Welcome Mail To Client globalindiatechtest@gmail.com', '2017-02-24 15:21:28'),
(19, 1, 'Logged In', '2017-02-24 15:53:31'),
(20, 1, 'Logged Out', '2017-02-24 15:58:24'),
(21, 1, 'Logged In', '2017-02-24 19:35:40'),
(22, 1, 'Added New Client nicgilbey@gmail.com', '2017-02-24 19:37:46'),
(23, 1, 'Sent Welcome Mail To Client nicgilbey@gmail.com', '2017-02-24 19:38:03'),
(24, 1, 'Add New Categories - Aspergers', '2017-02-24 19:54:50'),
(25, 1, 'Added New Pupil ', '2017-02-24 19:55:06'),
(26, 1, 'Logged In', '2017-02-25 15:27:43'),
(27, 1, 'Logged Out', '2017-02-25 15:51:00'),
(28, 1, 'Logged In', '2017-04-21 17:19:56'),
(29, 1, 'Sent Welcome Mail To Client nicgilbey@gmail.com', '2017-04-21 17:20:36'),
(30, 1, 'Logged In', '2017-04-22 10:45:02'),
(31, 1, 'Logged Out', '2017-04-22 10:45:33'),
(32, 1, 'Logged In', '2017-05-02 17:20:07'),
(33, 1, 'Logged Out', '2017-05-02 19:01:58'),
(34, 1, 'Logged In', '2017-05-03 14:14:11'),
(35, 1, 'Logged In', '2017-05-03 14:18:08'),
(36, 1, 'Logged Out', '2017-05-03 14:36:01'),
(37, 1, 'Logged In', '2017-05-05 14:48:24'),
(38, 1, 'Add Pupil ', '2017-05-05 14:49:37'),
(39, 1, 'Add Pupil ', '2017-05-05 14:57:14'),
(40, 1, 'Updated Pupil ', '2017-05-05 15:02:23'),
(41, 1, 'Updated Pupil ', '2017-05-05 15:25:25'),
(42, 1, 'Updated Pupil ', '2017-05-05 15:25:33'),
(43, 1, 'Updated Pupil ', '2017-05-05 15:28:15'),
(44, 1, 'Updated Pupil ', '2017-05-05 15:28:30'),
(45, 1, 'Updated Pupil ', '2017-05-05 15:28:57'),
(46, 1, 'Updated Pupil ', '2017-05-05 15:30:00'),
(47, 1, 'Updated Pupil ', '2017-05-05 15:30:10'),
(48, 1, 'Updated Pupil ', '2017-05-05 15:30:24'),
(49, 1, 'Logged Out', '2017-05-05 15:33:19'),
(50, 1, 'Logged In', '2017-05-05 15:54:31'),
(51, 1, 'Delete Pupil ', '2017-05-05 16:00:24'),
(52, 1, 'Logged Out', '2017-05-05 16:02:41'),
(53, 1, 'Logged In', '2017-05-05 18:23:16'),
(54, 1, 'Logged In', '2017-05-05 18:42:30'),
(55, 1, 'Add Pupil ', '2017-05-05 18:44:40'),
(56, 1, 'Logged In', '2017-05-08 14:05:21'),
(57, 1, 'Logged In', '2017-05-08 14:15:29'),
(58, 1, 'Updated Pupil ', '2017-05-08 14:20:55'),
(59, 1, 'Updated Pupil ', '2017-05-08 14:21:18'),
(60, 1, 'Logged In', '2017-05-08 14:33:08'),
(61, 1, 'Add Pupil ', '2017-05-08 14:40:17'),
(62, 1, 'Logged In', '2017-05-08 14:49:01'),
(63, 1, 'Changed Status of globalindiatechtest@gmail.com To Active', '2017-05-08 14:49:50'),
(64, 1, 'Updated Client globalindiatechtest@gmail.com', '2017-05-08 14:49:50'),
(65, 1, 'Updated Client globalindiatechtest@gmail.com', '2017-05-08 14:53:17'),
(66, 1, 'Updated Client globalindiatechtest@gmail.com', '2017-05-08 14:56:17'),
(67, 1, 'Sent Welcome Mail To Client globalindiatechtest@gmail.com', '2017-05-08 15:20:05'),
(68, 1, 'Assigned Children  To Parent / Carer nicgilbey@gmail.com', '2017-05-08 15:49:29'),
(69, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-08 15:49:29'),
(70, 1, 'Updated Children ', '2017-05-08 17:48:10'),
(71, 1, 'Logged In', '2017-05-08 18:06:09'),
(72, 1, 'Updated Children ', '2017-05-08 18:07:36'),
(73, 1, 'Updated Children ', '2017-05-08 18:07:55'),
(74, 1, 'Updated Children ', '2017-05-08 18:08:20'),
(75, 1, 'Logged In', '2017-05-08 18:09:01'),
(76, 1, 'Changed Status of nicgilbey@gmail.com To Active', '2017-05-08 18:09:34'),
(77, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-08 18:09:34'),
(78, 1, 'Logged Out', '2017-05-08 18:55:00'),
(79, 1, 'Logged In', '2017-05-08 19:25:53'),
(80, 1, 'Logged In', '2017-05-09 10:33:29'),
(81, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-09 10:33:46'),
(82, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-09 10:34:00'),
(83, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-09 10:38:51'),
(84, 1, 'Changed Status of globalindiatechtest@gmail.com To Pending', '2017-05-09 10:58:58'),
(85, 1, 'Updated Parent / Carer globalindiatechtest@gmail.com', '2017-05-09 10:58:58'),
(86, 1, 'Logged In', '2017-05-09 12:13:39'),
(87, 1, 'Logged Out', '2017-05-09 12:13:48'),
(88, 1, 'Logged In', '2017-05-09 12:14:38'),
(89, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-09 12:16:02'),
(90, 1, 'Updated Children ', '2017-05-09 12:25:41'),
(91, 1, 'Updated Children ', '2017-05-09 12:25:56'),
(92, 1, 'Updated Children ', '2017-05-09 12:26:21'),
(93, 1, 'Assigned Children  To Parent / Carer nicgilbey@gmail.com', '2017-05-09 12:32:01'),
(94, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-09 12:32:01'),
(95, 1, 'Logged In', '2017-05-09 12:53:30'),
(96, 1, 'Updated Children ', '2017-05-09 13:31:02'),
(97, 1, 'Logged In', '2017-05-09 14:40:50'),
(98, 1, 'Assigned Children  To Parent / Carer nicgilbey@gmail.com', '2017-05-09 14:42:57'),
(99, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-09 14:42:57'),
(100, 1, 'Logged In', '2017-05-09 15:09:14'),
(101, 1, 'Logged In', '2017-05-09 16:19:15'),
(102, 1, 'Logged In', '2017-05-09 18:32:46'),
(103, 1, 'Updated Children ', '2017-05-09 18:33:12'),
(104, 1, 'Logged Out', '2017-05-09 18:35:17'),
(105, 1, 'Logged In', '2017-05-09 18:44:50'),
(106, 1, 'Removed Children  From Parent / Carer nicgilbey@gmail.com', '2017-05-09 18:56:34'),
(107, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-09 18:56:34'),
(108, 1, 'Changed Status of test@gmail.com To Active', '2017-05-09 18:57:08'),
(109, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-09 18:57:08'),
(110, 1, 'Updated Children ', '2017-05-09 18:57:32'),
(111, 1, 'Logged In', '2017-05-09 18:58:34'),
(112, 1, 'Logged In', '2017-05-10 00:39:15'),
(113, 1, 'Logged In', '2017-05-10 10:33:40'),
(114, 1, 'Logged In', '2017-05-10 11:26:51'),
(115, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-10 11:27:24'),
(116, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-10 11:27:45'),
(117, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-10 11:28:01'),
(118, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-10 11:29:27'),
(119, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-10 11:29:36'),
(120, 1, 'Logged In', '2017-05-10 13:35:40'),
(121, 1, 'Logged In', '2017-05-10 14:58:35'),
(122, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-10 15:00:29'),
(123, 1, 'Logged In', '2017-05-10 15:14:48'),
(124, 1, 'Add New Service - SNAP/ other services I currently attend', '2017-05-10 15:18:36'),
(125, 1, 'Add New Service - MCCH', '2017-05-10 15:18:57'),
(126, 1, 'Add New Service - Moorings', '2017-05-10 15:19:14'),
(127, 1, 'Add New Service - Charlton Ability Counts Programme', '2017-05-10 15:19:30'),
(128, 1, 'Add New Service - Parkwood Leisure targeted disability swimming lessons or sports sessions', '2017-05-10 15:19:45'),
(129, 1, 'Add New Service - Falcon Spartak gymnastics club', '2017-05-10 15:20:04'),
(130, 1, 'Add New Service - Beavers disability swim session', '2017-05-10 15:20:22'),
(131, 1, 'Add New Service - Bexley NAS', '2017-05-10 15:20:36'),
(132, 1, 'Add New Service - Other', '2017-05-10 15:20:53'),
(133, 1, 'Add New Service - Mainstream', '2017-05-10 15:21:18'),
(134, 1, 'Add New Service - Bexley Youth Services', '2017-05-10 15:21:39'),
(135, 1, 'Add New Service - Parkwood Leisure swimming lessons or sports sessions', '2017-05-10 15:21:55'),
(136, 1, 'Add New Service - Charlton', '2017-05-10 15:22:12'),
(137, 1, 'Add New Service - Other', '2017-05-10 15:22:28'),
(138, 1, 'Add New Service - Leisure Services I would like to access', '2017-05-10 15:22:44'),
(139, 1, 'Add New Service - Saturday Fun Club &#45; LBB', '2017-05-10 15:24:30'),
(140, 1, 'Add New Service - Youth Club &#45; LBB', '2017-05-10 15:24:46'),
(141, 1, 'Add New Service - Buddy Club &#45; LBB', '2017-05-10 15:25:00'),
(142, 1, 'Add New Service - Archway Mechanics scheme', '2017-05-10 15:25:14'),
(143, 1, 'Add New Service - Hydrotherapy sessions', '2017-05-10 15:25:36'),
(144, 1, 'Add New Service - Half Term Holiday Schemes &#45; LBB', '2017-05-10 15:25:54'),
(145, 1, 'Add New Service - Easter and Summer Holiday Schemes &#45; LBB', '2017-05-10 15:26:10'),
(146, 1, 'Add New Service - Made to Measure (Alternative funding)', '2017-05-10 15:26:27'),
(147, 1, 'Add New Service - Service you would like to see', '2017-05-10 15:26:42'),
(148, 1, 'Updated Children ', '2017-05-10 15:28:28'),
(149, 1, 'Updated Service - Archway Mechanics schemee', '2017-05-10 15:29:45'),
(150, 1, 'Updated Service - Archway Mechanics scheme', '2017-05-10 15:30:08'),
(151, 1, 'Logged In', '2017-05-10 15:41:16'),
(152, 1, 'Updated Children ', '2017-05-10 16:09:02'),
(153, 1, 'Logged In', '2017-05-10 16:15:12'),
(154, 1, 'Logged Out', '2017-05-10 16:21:33'),
(155, 1, 'Logged Out', '2017-05-10 16:21:58'),
(156, 1, 'Logged In', '2017-05-10 17:19:38'),
(157, 1, 'Logged In', '2017-05-10 17:35:37'),
(158, 1, 'Logged In', '2017-05-11 19:38:59'),
(159, 1, 'Logged In', '2017-05-12 10:29:37'),
(160, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-12 10:36:21'),
(161, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-05-12 10:37:30'),
(162, 1, 'Updated Parent / Carer test@gmail.com', '2017-05-12 10:37:42'),
(163, 1, 'Logged In', '2017-05-12 12:55:41'),
(164, 1, 'Logged In', '2017-05-22 15:46:06'),
(165, 1, 'Logged In', '2017-05-26 14:54:28'),
(166, 1, 'Updated Children ', '2017-05-26 14:57:44'),
(167, 1, 'Updated Children ', '2017-05-26 14:59:17'),
(168, 1, 'Updated Children ', '2017-05-26 14:59:29'),
(169, 1, 'Logged Out', '2017-05-26 15:01:30'),
(170, 1, 'Logged In', '2017-05-31 20:41:21'),
(171, 1, 'Logged In', '2017-05-31 20:41:57'),
(172, 1, 'Logged In', '2017-06-08 19:42:32'),
(173, 1, 'Logged In', '2017-06-13 15:43:37'),
(174, 1, 'Logged Out', '2017-06-13 17:28:41'),
(175, 1, 'Logged In', '2017-06-14 11:20:54'),
(176, 1, 'Logged In', '2017-06-14 16:38:19'),
(177, 1, 'Added New School - Barnehurst Infant School', '2017-06-14 16:40:12'),
(178, 1, 'Added New School - Barnehurst Junior School', '2017-06-14 16:40:21'),
(179, 1, 'Added New School - Barrington Primary School', '2017-06-14 16:40:31'),
(180, 1, 'Added New School - Bedonwell Infant and Nursery School', '2017-06-14 16:40:41'),
(181, 1, 'Added New School - Bedonwell Junior School', '2017-06-14 16:40:53'),
(182, 1, 'Added New School - Belmont Primary School', '2017-06-14 16:41:03'),
(183, 1, 'Added New School - Belvedere Infant School', '2017-06-14 16:41:11'),
(184, 1, 'Added New School - Belvedere Junior School', '2017-06-14 16:41:23'),
(185, 1, 'Added New School - Birkbeck Primary School', '2017-06-14 16:42:18'),
(186, 1, 'Added New School - Bishop Ridley CE VA Primary School', '2017-06-14 16:42:27'),
(187, 1, 'Added New School - Brampton Primary School', '2017-06-14 16:42:34'),
(188, 1, 'Added New School - Burnt Oak Junior School', '2017-06-14 16:42:45'),
(189, 1, 'Added New School - Bursted Wood Primary School', '2017-06-14 16:42:54'),
(190, 1, 'Added New School - Business Academy Bexley', '2017-06-14 16:43:05'),
(191, 1, 'Added New School - Castilion Primary School', '2017-06-14 16:43:16'),
(192, 1, 'Added New School - Chatsworth Infant School', '2017-06-14 16:43:27'),
(193, 1, 'Added New School - Christ Church (Erith) CE Primary School', '2017-06-14 16:43:37'),
(194, 1, 'Added New School - Crook Log Primary School', '2017-06-14 16:43:46'),
(195, 1, 'Added New School - Danson Primary School', '2017-06-14 16:43:54'),
(196, 1, 'Added New School - Days Lane Primary School', '2017-06-14 16:44:03'),
(197, 1, 'Added New School - Dulverton Primary School', '2017-06-14 16:44:12'),
(198, 1, 'Added New School - East Wickham Primary Academy', '2017-06-14 16:44:23'),
(199, 1, 'Added New School - Eastcote Primary School', '2017-06-14 16:44:31'),
(200, 1, 'Added New School - Foster&#39;s Primary School', '2017-06-14 16:44:40'),
(201, 1, 'Added New School - Gravel Hill Primary School', '2017-06-14 16:44:51'),
(202, 1, 'Added New School - Haberdashers&#39; Aske&#39;s Crayford Academy', '2017-06-14 16:45:12'),
(203, 1, 'Added New School - Hillsgrove Primary School', '2017-06-14 16:45:22'),
(204, 1, 'Added New School - Holy Trinity Lamorbey CE Primary School', '2017-06-14 16:45:30'),
(205, 1, 'Added New School - Hook Lane Primary School', '2017-06-14 16:45:40'),
(206, 1, 'Added New School - Hope Community School', '2017-06-14 16:45:48'),
(207, 1, 'Added New School - Hurst Primary School', '2017-06-14 16:45:57'),
(208, 1, 'Added New School - Jubilee Primary School', '2017-06-14 16:46:05'),
(209, 1, 'Added New School - Lessness Heath Primary School', '2017-06-14 16:46:18'),
(210, 1, 'Added New School - Longlands Primary School', '2017-06-14 16:46:26'),
(211, 1, 'Added New School - Mayplace Primary School', '2017-06-14 16:47:17'),
(212, 1, 'Added New School - Normandy Primary School', '2017-06-14 16:47:26'),
(213, 1, 'Added New School - Northumberland Heath Primary School', '2017-06-14 16:47:36'),
(214, 1, 'Added New School - Northwood Primary School', '2017-06-14 16:47:46'),
(215, 1, 'Added New School - Old Bexley CE Primary School', '2017-06-14 16:47:57'),
(216, 1, 'Added New School - Orchard School', '2017-06-14 16:48:07'),
(217, 1, 'Added New School - Our Lady of the Rosary RC Primary School', '2017-06-14 16:48:17'),
(218, 1, 'Added New School - Parkway Primary School', '2017-06-14 16:48:30'),
(219, 1, 'Added New School - Peareswood Primary School', '2017-06-14 16:48:41'),
(220, 1, 'Added New School - Pelham Primary School', '2017-06-14 16:48:49'),
(221, 1, 'Added New School - Pelham Primary School', '2017-06-14 16:48:59'),
(222, 1, 'Added New School - Royal Park Primary School', '2017-06-14 16:49:09'),
(223, 1, 'Added New School - St Augustine of Canterbury CE Primary School', '2017-06-14 16:49:18'),
(224, 1, 'Added New School - St Fidelis RC Primary School', '2017-06-14 16:49:27'),
(225, 1, 'Added New School - St John Fisher RC Primary School', '2017-06-14 16:49:34'),
(226, 1, 'Added New School - St Joseph&#39;s Catholic Primary School', '2017-06-14 16:49:41'),
(227, 1, 'Added New School - St Michael&#39;s East Wickham CE VA Primary School', '2017-06-14 16:49:51'),
(228, 1, 'Added New School - St Paulinus CE Primary School', '2017-06-14 16:49:58'),
(229, 1, 'Added New School - St Peter Chanel RC Primary School', '2017-06-14 16:50:47'),
(230, 1, 'Added New School - St Stephen&#39;s RC Primary School', '2017-06-14 16:50:57'),
(231, 1, 'Added New School - St Thomas More RC Primary School', '2017-06-14 16:51:06'),
(232, 1, 'Added New School - Sherwood Park Primary School', '2017-06-14 16:51:13'),
(233, 1, 'Added New School - Slade Green Primary School', '2017-06-14 16:51:22'),
(234, 1, 'Added New School - Upland Primary School', '2017-06-14 16:51:32'),
(235, 1, 'Added New School - Upton Primary School', '2017-06-14 16:51:46'),
(236, 1, 'Added New School - Willow Bank Primary School', '2017-06-14 16:51:57'),
(237, 1, 'Added New School - Bexleyheath Academy', '2017-06-14 16:52:09'),
(238, 1, 'Added New School - Blackfen School for Girls', '2017-06-14 16:52:27'),
(239, 1, 'Added New School - Business Academy Bexley', '2017-06-14 16:52:36'),
(240, 1, 'Added New School - Cleeve Park School', '2017-06-14 16:52:46'),
(241, 1, 'Added New School - Haberdashers&#39; Aske&#39;s Crayford Academy', '2017-06-14 16:52:56'),
(242, 1, 'Added New School - Harris Academy Falconwood', '2017-06-14 16:53:19'),
(243, 1, 'Added New School - Hurstmere School', '2017-06-14 16:54:15'),
(244, 1, 'Added New School - St Catherine&#39;s Catholic School for Girls', '2017-06-14 16:54:25'),
(245, 1, 'Added New School - St Columba&#39;s Catholic Boys&#39; School', '2017-06-14 16:54:34'),
(246, 1, 'Added New School - Trinity School', '2017-06-14 16:54:45'),
(247, 1, 'Added New School - Welling School', '2017-06-14 16:55:05'),
(248, 1, 'Added New School - Erith School', '2017-06-14 16:55:20'),
(249, 1, 'Added New School - Beths Grammar School (Boys)', '2017-06-14 16:55:28'),
(250, 1, 'Added New School - Bexley Grammar School', '2017-06-14 16:55:36'),
(251, 1, 'Added New School - Chislehurst and Sidcup Grammar School', '2017-06-14 16:55:44'),
(252, 1, 'Added New School - Townley Grammar School (Girls)', '2017-06-14 16:55:55'),
(253, 1, 'Added New School - Marlborough School', '2017-06-14 16:56:18'),
(254, 1, 'Added New School - Oakwood School', '2017-06-14 16:56:29'),
(255, 1, 'Added New School - Shenstone School', '2017-06-14 16:56:40'),
(256, 1, 'Added New School - Westbrooke School', '2017-06-14 16:56:53'),
(257, 1, 'Added New School - Woodside School', '2017-06-14 16:57:04'),
(258, 1, 'Added New School - Benedict House Preparatory School', '2017-06-14 16:57:14'),
(259, 1, 'Added New School - Merton Court School', '2017-06-14 16:57:26'),
(260, 1, 'Added New School - West Lodge School', '2017-06-14 16:57:40'),
(261, 1, 'Added New School - Break Through', '2017-06-14 16:57:49'),
(262, 1, 'Added New School - Park View Academy', '2017-06-14 16:57:58'),
(263, 1, 'Logged In', '2017-06-14 17:39:02'),
(264, 1, 'Logged In', '2017-06-14 18:02:50'),
(265, 1, 'Logged In', '2017-06-14 18:32:38'),
(266, 1, 'Logged In', '2017-06-20 12:46:07'),
(267, 1, 'Updated Parent / Carer nicgilbey@gmail.com', '2017-06-20 12:46:30'),
(268, 1, 'Updated Parent / Carer test@gmail.com', '2017-06-20 12:46:50'),
(269, 1, 'Logged In', '2017-06-20 13:16:00'),
(270, 1, 'Updated Children ', '2017-06-20 13:16:27'),
(271, 1, 'Updated Children ', '2017-06-20 13:18:31'),
(272, 1, 'Logged In', '2017-06-20 16:43:44'),
(273, 1, 'Logged In', '2017-06-20 17:42:50'),
(274, 1, 'Logged In', '2017-06-20 17:53:53'),
(275, 1, 'Logged Out', '2017-06-20 17:54:33'),
(276, 1, 'Logged In', '2017-07-19 13:02:05'),
(277, 1, 'Logged Out', '2017-07-19 13:06:18'),
(278, 1, 'Logged In', '2017-07-20 15:07:26'),
(279, 1, 'Logged In', '2017-07-20 15:10:51'),
(280, 1, 'Logged Out', '2017-07-20 15:40:08'),
(281, 1, 'Logged In', '2017-07-24 15:10:19'),
(282, 1, 'Logged In', '2017-07-24 18:43:01'),
(283, 1, 'Logged In', '2017-07-25 10:50:59'),
(284, 1, 'Logged Out', '2017-07-25 11:00:06'),
(285, 1, 'Logged In', '2017-07-25 14:27:12'),
(286, 1, 'Updated Booking Type - Ad Hoc Singles', '2017-07-25 14:33:25'),
(287, 1, 'Updated Booking Type - Ad Hoc Single', '2017-07-25 14:33:32'),
(288, 1, 'Add New Booking Type - test', '2017-07-25 14:34:41'),
(289, 1, 'Updated Booking Type - testss', '2017-07-25 14:34:47'),
(290, 1, 'Deleted Booking Type - testss', '2017-07-25 14:34:52'),
(291, 1, 'Logged In', '2017-07-26 11:19:43'),
(292, 1, 'Logged Out', '2017-07-26 11:20:09'),
(293, 1, 'Logged In', '2017-07-26 19:03:53'),
(294, 1, 'Logged In', '2017-08-01 17:50:33'),
(295, 1, 'Updated Booking Type - Little Explorers', '2017-08-01 17:54:19'),
(296, 1, 'Updated Booking Type - Building Buddies', '2017-08-01 17:54:28'),
(297, 1, 'Updated Booking Type - Saturday Youth Club', '2017-08-01 17:54:37'),
(298, 1, 'Updated Booking Type - Archway', '2017-08-01 17:54:47'),
(299, 1, 'Add New Booking Type - Hydropool', '2017-08-01 17:55:00'),
(300, 1, 'Add New Booking Type - Holiday Schemes', '2017-08-01 17:55:15'),
(301, 1, 'Add New Booking Type - Lego Club', '2017-08-01 17:55:23'),
(302, 1, 'Add New Booking Type - Creator Club', '2017-08-01 17:55:33'),
(303, 1, 'Add New Booking Type - Empire', '2017-08-01 17:55:41'),
(304, 1, 'Add New Booking Type - Fusion', '2017-08-01 17:55:48'),
(305, 1, 'Logged In', '2017-08-02 13:01:39'),
(306, 1, 'Logged In', '2017-08-02 16:02:29'),
(307, 1, 'Logged In', '2017-08-02 17:19:37'),
(308, 1, 'Add Children ', '2017-08-02 17:33:38'),
(309, 1, 'Delete Children ', '2017-08-02 17:33:45'),
(310, 1, 'Updated Children ', '2017-08-02 17:36:03'),
(311, 1, 'Updated Children ', '2017-08-02 17:36:11'),
(312, 1, 'Logged In', '2017-08-02 18:08:27'),
(313, 1, 'Add Children ', '2017-08-02 18:09:57'),
(314, 1, 'Delete Children ', '2017-08-02 18:10:16'),
(315, 1, 'Add Children ', '2017-08-02 18:10:59'),
(316, 1, 'Delete Children ', '2017-08-02 18:11:04'),
(317, 1, 'Logged In', '2017-08-09 16:52:13'),
(318, 1, 'Logged In', '2017-08-11 10:31:23'),
(319, 1, 'Logged In', '2017-08-11 15:37:49'),
(320, 1, 'Logged In', '2017-08-16 15:51:04'),
(321, 1, 'Logged In', '2017-08-16 16:22:08'),
(322, 1, 'Logged In', '2017-08-16 16:22:42'),
(323, 1, 'Logged Out', '2017-08-16 16:23:42'),
(324, 1, 'Logged Out', '2017-08-16 16:23:59'),
(325, 1, 'Logged In', '2017-08-17 16:08:51'),
(326, 1, 'Logged In', '2017-08-18 01:47:35'),
(327, 1, 'Logged In', '2017-08-18 10:37:42'),
(328, 1, 'Logged In', '2017-08-18 13:40:41'),
(329, 1, 'Logged In', '2017-08-18 13:42:28'),
(330, 1, 'Logged In', '2017-08-19 01:05:12'),
(331, 1, 'Logged In', '2017-08-21 16:46:13'),
(332, 1, 'Logged In', '2017-08-31 19:07:53'),
(333, 1, 'Logged In', '2017-08-31 19:14:07'),
(334, 1, 'Logged In', '2017-08-31 19:23:36'),
(335, 1, 'Add New Booking Type - Test', '2017-08-31 19:24:44'),
(336, 1, 'Updated Booking Type - Test', '2017-08-31 19:24:55'),
(337, 1, 'Deleted Booking Type - Test', '2017-08-31 19:25:08'),
(338, 1, 'Add New Booking Type - Test', '2017-08-31 19:28:47'),
(339, 1, 'Logged Out', '2017-08-31 19:31:39'),
(340, 1, 'Logged In', '2017-09-01 10:57:05'),
(341, 1, 'Deleted Booking Type - Test', '2017-09-01 12:15:36'),
(342, 1, 'Logged In', '2017-09-01 13:24:32'),
(343, 1, 'Updated Booking Type - Hydropool', '2017-09-01 13:27:38'),
(344, 1, 'Logged In', '2017-09-01 15:37:06'),
(345, 1, 'Logged In', '2017-09-01 20:04:39'),
(346, 1, 'Logged In', '2017-09-08 17:57:51'),
(347, 1, 'Logged In', '2017-09-20 20:05:38'),
(348, 1, 'Updated Booking Type - My Time', '2017-09-20 20:11:44'),
(349, 1, 'Updated Booking Type - Vibe', '2017-09-20 20:14:19'),
(350, 1, 'Logged In', '2017-09-21 11:00:41'),
(351, 1, 'Add New Booking Type - Hydropool Older group for 11 year olds and over', '2017-09-21 11:12:41'),
(352, 1, 'Logged In', '2017-09-22 18:27:53'),
(353, 1, 'Logged In', '2017-09-22 18:34:03'),
(354, 1, 'Logged In', '2017-09-25 14:37:33'),
(355, 1, 'Add New Booking Type - test', '2017-09-25 15:42:53'),
(356, 1, 'Updated Booking Type - test', '2017-09-25 15:44:53'),
(357, 1, 'Updated Booking Type - test', '2017-09-25 15:45:00'),
(358, 1, 'Deleted Booking Type - test', '2017-09-25 15:45:11'),
(359, 1, 'Logged Out', '2017-09-25 15:45:32'),
(360, 1, 'Logged In', '2017-10-10 18:06:06'),
(361, 1, 'Logged In', '2017-10-23 18:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `ww_blogs`
--

CREATE TABLE IF NOT EXISTS `ww_blogs` (
  `_ID` int(10) NOT NULL,
  `_Type` int(10) DEFAULT NULL,
  `_Title` varchar(200) DEFAULT NULL,
  `_Description` longtext NOT NULL,
  `_Image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_blogtype`
--

CREATE TABLE IF NOT EXISTS `ww_blogtype` (
  `_ID` int(10) NOT NULL,
  `_Type` varchar(255) NOT NULL,
  `_Status` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_booked_lessons`
--

CREATE TABLE IF NOT EXISTS `ww_booked_lessons` (
  `_ID` int(10) NOT NULL,
  `_PupilID` int(10) DEFAULT NULL,
  `_TutorID` int(10) DEFAULT NULL,
  `_SubjectID` int(10) DEFAULT NULL,
  `_LevelID` int(10) DEFAULT NULL,
  `_Date` date DEFAULT NULL,
  `_Fromtime` time DEFAULT NULL,
  `_Totime` time DEFAULT NULL,
  `_PaymentID` int(10) DEFAULT NULL,
  `_Cancel` tinyint(1) NOT NULL DEFAULT '0',
  `_Type` varchar(2) NOT NULL COMMENT '0 for trial and 1 for actual'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_booked_lessons`
--

INSERT INTO `ww_booked_lessons` (`_ID`, `_PupilID`, `_TutorID`, `_SubjectID`, `_LevelID`, `_Date`, `_Fromtime`, `_Totime`, `_PaymentID`, `_Cancel`, `_Type`) VALUES
(1, 3, NULL, NULL, NULL, '2017-08-29', '15:00:00', '16:00:00', NULL, 0, '4'),
(2, 3, NULL, NULL, NULL, '2017-08-29', '17:00:00', '18:00:00', NULL, 0, '2'),
(3, 3, NULL, NULL, NULL, '2017-08-30', '15:00:00', '16:00:00', NULL, 0, '4'),
(4, 3, NULL, NULL, NULL, '2017-08-31', '15:00:00', '17:00:00', NULL, 0, '2'),
(5, 3, NULL, NULL, NULL, '2017-08-30', '15:00:00', '17:00:00', NULL, 0, '2');

-- --------------------------------------------------------

--
-- Table structure for table `ww_bookingtype`
--

CREATE TABLE IF NOT EXISTS `ww_bookingtype` (
  `_ID` int(10) NOT NULL,
  `_Type` varchar(200) NOT NULL,
  `_capacity` int(11) DEFAULT NULL,
  `_active` enum('0','1') NOT NULL DEFAULT '0',
  `_Notes` longtext
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_bookingtype`
--

INSERT INTO `ww_bookingtype` (`_ID`, `_Type`, `_capacity`, `_active`, `_Notes`) VALUES
(1, 'Building Buddies', NULL, '0', NULL),
(2, 'My Time', NULL, '0', 'For disabled young people aged 11 to 18 years who meed the LBB Disabled Children&#39;s Service Criteria'),
(3, 'Little Explorers', NULL, '0', NULL),
(4, 'Archway', NULL, '0', NULL),
(5, 'Hydropool Younger group for under 10&#39;s', NULL, '0', 'This club has 2 time slots:\r\nif a child is 7 years old &ndash; Hydropool session is from 2&#45;3.30pm and if a child is 14 years old &ndash; time slot is from 3.30 &ndash; 5pm'),
(6, 'Holiday Schemes', NULL, '0', NULL),
(7, 'Lego Club', NULL, '0', NULL),
(8, 'Creator Club', NULL, '0', NULL),
(9, 'Empire', NULL, '0', NULL),
(10, 'Vibe', NULL, '0', 'For disabled young adults aged 18 &#45; 25 years with Autism or a mild learning disability'),
(13, 'Hydropool Older group for 11 year olds and over', NULL, '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ww_booking_schedule`
--

CREATE TABLE IF NOT EXISTS `ww_booking_schedule` (
  `_ID` int(10) NOT NULL,
  `_Name` varchar(255) NOT NULL,
  `_Fromdate` date NOT NULL,
  `_Todate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_booking_schedule`
--

INSERT INTO `ww_booking_schedule` (`_ID`, `_Name`, `_Fromdate`, `_Todate`) VALUES
(1, 'Summer Holiday 2017', '2017-02-22', '2017-02-25');

-- --------------------------------------------------------

--
-- Table structure for table `ww_clientmaster`
--

CREATE TABLE IF NOT EXISTS `ww_clientmaster` (
  `_ID` int(10) NOT NULL,
  `_Firstname` varchar(100) DEFAULT NULL,
  `_Lastname` varchar(100) DEFAULT NULL,
  `_Email` varchar(200) DEFAULT NULL,
  `_Carer1` varchar(255) DEFAULT NULL,
  `_Carer2` varchar(255) DEFAULT NULL,
  `_Carer3` varchar(255) DEFAULT NULL,
  `_Carer4` varchar(255) DEFAULT NULL,
  `_Pfirstname` varchar(200) DEFAULT NULL,
  `_Plastname` varchar(200) DEFAULT NULL,
  `_Pemail` varchar(200) DEFAULT NULL,
  `_Password` varchar(200) DEFAULT NULL,
  `_Phone` varchar(20) DEFAULT NULL,
  `_Address1` text,
  `_Address2` text,
  `_City` text,
  `_State` text,
  `_Country` varchar(255) DEFAULT NULL,
  `_Acedmicyear` varchar(100) DEFAULT NULL,
  `_Currschool` varchar(50) DEFAULT NULL,
  `_Newschool` varchar(50) DEFAULT NULL,
  `_Subject` varchar(50) DEFAULT NULL,
  `_Comments` text,
  `_Problem` varchar(50) DEFAULT NULL,
  `_Predays` varchar(200) DEFAULT NULL,
  `_Pretime` varchar(255) DEFAULT NULL COMMENT '0:Morning , 1:Afternoon , 2:Evening',
  `_Postcode` text,
  `_Contactmethod` enum('Mail','Phone') DEFAULT NULL,
  `_Status` enum('0','1','2') DEFAULT NULL COMMENT '0 for pending, 1 for active, 2 for inactive',
  `_Guardian` enum('Parent','Carer') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_clientmaster`
--

INSERT INTO `ww_clientmaster` (`_ID`, `_Firstname`, `_Lastname`, `_Email`, `_Carer1`, `_Carer2`, `_Carer3`, `_Carer4`, `_Pfirstname`, `_Plastname`, `_Pemail`, `_Password`, `_Phone`, `_Address1`, `_Address2`, `_City`, `_State`, `_Country`, `_Acedmicyear`, `_Currschool`, `_Newschool`, `_Subject`, `_Comments`, `_Problem`, `_Predays`, `_Pretime`, `_Postcode`, `_Contactmethod`, `_Status`, `_Guardian`) VALUES
(3, 'test', 'Moxon', 'test@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4c7r5s0=', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Carer'),
(4, 'Nic', 'Gilbey', 'nicgilbey@gmail.com', '1', '2', '3', '4', NULL, NULL, NULL, 'qp+s', '1234567890', 'test address 1', 'test address 2', 'NY', 'NY', 'NY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10001', NULL, '1', 'Carer');

-- --------------------------------------------------------

--
-- Table structure for table `ww_clipup_rel`
--

CREATE TABLE IF NOT EXISTS `ww_clipup_rel` (
  `_ID` int(10) NOT NULL,
  `_ClientID` int(10) NOT NULL,
  `_PupilID` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_clipup_rel`
--

INSERT INTO `ww_clipup_rel` (`_ID`, `_ClientID`, `_PupilID`) VALUES
(3, 4, 3),
(4, 3, 2),
(5, 4, 5),
(6, 4, 6),
(7, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ww_cmspages`
--

CREATE TABLE IF NOT EXISTS `ww_cmspages` (
  `_ID` int(10) NOT NULL,
  `_Title` varchar(255) NOT NULL,
  `_Content` longtext NOT NULL,
  `_Status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `_Url` varchar(255) NOT NULL,
  `_Topdisplay` varchar(50) DEFAULT NULL,
  `_Footerdisplay` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_cmspages`
--

INSERT INTO `ww_cmspages` (`_ID`, `_Title`, `_Content`, `_Status`, `_Url`, `_Topdisplay`, `_Footerdisplay`) VALUES
(1, 'Test Cms Page', '&lt;p&gt;This is Test Content&lt;/p&gt;', 'Active', 'www.google.com', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ww_communications`
--

CREATE TABLE IF NOT EXISTS `ww_communications` (
  `_ID` int(10) NOT NULL,
  `_UserID` varchar(50) NOT NULL,
  `_Usertype` enum('0','1','2') NOT NULL COMMENT '0 for Client(parent), 1 for pupil and 2 for tutor',
  `_Person_spoken` varchar(200) NOT NULL,
  `_Notes` longtext NOT NULL,
  `_Email` varchar(200) NOT NULL,
  `_Date` date NOT NULL,
  `_Time` time NOT NULL,
  `_Commtype` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_communications`
--

INSERT INTO `ww_communications` (`_ID`, `_UserID`, `_Usertype`, `_Person_spoken`, `_Notes`, `_Email`, `_Date`, `_Time`, `_Commtype`) VALUES
(1, '2', '1', 'Nic Gilbey', 'Test Notes', 'nicgilbey@gmail.com', '2017-05-10', '13:00:00', '4');

-- --------------------------------------------------------

--
-- Table structure for table `ww_communication_type`
--

CREATE TABLE IF NOT EXISTS `ww_communication_type` (
  `_ID` int(10) NOT NULL,
  `_Name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_communication_type`
--

INSERT INTO `ww_communication_type` (`_ID`, `_Name`) VALUES
(1, 'Missed Call'),
(2, 'SMS'),
(3, 'Email'),
(4, 'Call'),
(5, 'Meeting'),
(6, 'Agreement Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `ww_invoicepayment`
--

CREATE TABLE IF NOT EXISTS `ww_invoicepayment` (
  `_ID` int(10) NOT NULL,
  `_InvoiceID` int(10) NOT NULL,
  `_Amount` varchar(200) NOT NULL,
  `_Invoicedate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_invoices`
--

CREATE TABLE IF NOT EXISTS `ww_invoices` (
  `_ID` int(10) NOT NULL,
  `_ClientID` int(10) NOT NULL,
  `_PupilID` int(10) NOT NULL,
  `_TutorID` int(10) NOT NULL,
  `_SubjectID` int(10) NOT NULL,
  `_Rateph` varchar(200) NOT NULL,
  `_Total` varchar(200) NOT NULL,
  `_Created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_levels`
--

CREATE TABLE IF NOT EXISTS `ww_levels` (
  `_ID` int(10) NOT NULL,
  `_Title` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_problems`
--

CREATE TABLE IF NOT EXISTS `ww_problems` (
  `_ID` int(10) NOT NULL,
  `_Title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_pupcategories`
--

CREATE TABLE IF NOT EXISTS `ww_pupcategories` (
  `_ID` int(10) NOT NULL,
  `_Title` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_pupcategories`
--

INSERT INTO `ww_pupcategories` (`_ID`, `_Title`) VALUES
(1, 'Aspergers');

-- --------------------------------------------------------

--
-- Table structure for table `ww_pupcate_rel`
--

CREATE TABLE IF NOT EXISTS `ww_pupcate_rel` (
  `_ID` int(10) NOT NULL,
  `_PupilID` int(10) NOT NULL,
  `_CategoryID` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_pupcate_rel`
--

INSERT INTO `ww_pupcate_rel` (`_ID`, `_PupilID`, `_CategoryID`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ww_pupilmaster`
--

CREATE TABLE IF NOT EXISTS `ww_pupilmaster` (
  `_ID` int(10) NOT NULL,
  `_FirstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_LastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_FamilySurname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DOB` date DEFAULT NULL,
  `_Ethnicity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Gender` enum('Male','Female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_School` int(10) DEFAULT NULL,
  `_Residential` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Carer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CarerTitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CarerFirstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CarerLastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CarerPhone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CarerMobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_EmerContactName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_EmerContactNumber` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_RiscAssessment` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_RASocialName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_RASocialContact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DisableService` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DSSocialName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DSSocialContact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Statement` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_RecHours` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_ReceiveBudget` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_NicName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_MyDiagnosis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CommunicateBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_MyMobility` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_MedicalNeeds` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_MedicationFor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_PersonalCareNeed` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_PersonalCareFor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Behaviour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_WhenAnxious` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_WhenHappy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_LikedActivities` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DislikedActivities` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CalmBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_LikedFood` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DislikedFood` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Allergic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_AboutMe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_GPContact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_HospitalConsultant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_OutingPermitted` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_AdvertisePermitted` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_CompletedPassport` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_SunCream` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_EmergencyEscort` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_EmergencyAid` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_AbleSwim` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_GetPool` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Services` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_Extraservice` text COLLATE utf8_unicode_ci,
  `_OtherService1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_OtherService2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_OtherService3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_ReceiveBudgetLBB` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_DisabilityAllowance` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_WhyAccessSNAP` mediumtext COLLATE utf8_unicode_ci,
  `_WantAchieve` mediumtext COLLATE utf8_unicode_ci,
  `_OtherInfo` mediumtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ww_pupilmaster`
--

INSERT INTO `ww_pupilmaster` (`_ID`, `_FirstName`, `_LastName`, `_FamilySurname`, `_DOB`, `_Ethnicity`, `_Gender`, `_School`, `_Residential`, `_Carer`, `_CarerTitle`, `_CarerFirstName`, `_CarerLastName`, `_CarerPhone`, `_CarerMobile`, `_Email`, `_EmerContactName`, `_EmerContactNumber`, `_RiscAssessment`, `_RASocialName`, `_RASocialContact`, `_DisableService`, `_DSSocialName`, `_DSSocialContact`, `_Statement`, `_RecHours`, `_ReceiveBudget`, `_NicName`, `_MyDiagnosis`, `_CommunicateBy`, `_MyMobility`, `_MedicalNeeds`, `_MedicationFor`, `_PersonalCareNeed`, `_PersonalCareFor`, `_Behaviour`, `_WhenAnxious`, `_WhenHappy`, `_LikedActivities`, `_DislikedActivities`, `_CalmBy`, `_LikedFood`, `_DislikedFood`, `_Allergic`, `_AboutMe`, `_GPContact`, `_HospitalConsultant`, `_OutingPermitted`, `_AdvertisePermitted`, `_CompletedPassport`, `_SunCream`, `_EmergencyEscort`, `_EmergencyAid`, `_AbleSwim`, `_GetPool`, `_Services`, `_Extraservice`, `_OtherService1`, `_OtherService2`, `_OtherService3`, `_ReceiveBudgetLBB`, `_DisabilityAllowance`, `_WhyAccessSNAP`, `_WantAchieve`, `_OtherInfo`) VALUES
(2, 'Fia', 'Moxon', NULL, '1970-01-22', NULL, 'Male', NULL, NULL, NULL, 'mr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, 'yes', NULL, NULL, NULL, NULL, 'yes', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test servics', NULL, NULL, NULL, NULL, NULL, 'fdgthsdgfho fgoh ijsdfp;og hsdofghsdolffhdg s;dlfkhg ;lsdfhg;lsdfhf g;lsdf dg;lksdf g;lksdf g;lkhsdf g; lhsdfgl;khsdf;l ghsdf;lhgsd;flk hgd;slfkhg ;sdlfhg;lsdhf g;lsdkf g;lsdf ;lhsd fgl;h;dsl fg;lsdkhgf ;lksddf g;lksdf g;lkhf sdg;lkhsdf gl;hsdf;lk ghds;flgh sd;lfkh gds;lfkh gsd;lfkh gsd;lkfghf ;sdlghdf;sdlff gdsdgdf', NULL, NULL),
(3, 'Kitty', 'Gilbey', NULL, '1970-01-01', NULL, 'Male', NULL, NULL, NULL, 'mr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, 'yes', NULL, 'Autism', NULL, NULL, 'yes', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ww_pupilmaster_bkp05052017`
--

CREATE TABLE IF NOT EXISTS `ww_pupilmaster_bkp05052017` (
  `_ID` int(10) NOT NULL,
  `_Firstname` varchar(100) NOT NULL,
  `_Lastname` varchar(100) NOT NULL,
  `_Email` varchar(100) DEFAULT NULL,
  `_Password` varchar(255) DEFAULT NULL,
  `_Phone` varchar(20) DEFAULT NULL,
  `_Address_1` varchar(255) DEFAULT NULL,
  `_Address_2` varchar(255) DEFAULT NULL,
  `_City` varchar(100) DEFAULT NULL,
  `_State` varchar(100) DEFAULT NULL,
  `_Country` varchar(100) DEFAULT NULL,
  `_Zip` varchar(20) DEFAULT NULL,
  `_Frequency` enum('0','1','2') DEFAULT NULL COMMENT '0 for daily, 1 for weekly, and 2 for monthly',
  `_Created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `_Updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_pupilmaster_bkp05052017`
--

INSERT INTO `ww_pupilmaster_bkp05052017` (`_ID`, `_Firstname`, `_Lastname`, `_Email`, `_Password`, `_Phone`, `_Address_1`, `_Address_2`, `_City`, `_State`, `_Country`, `_Zip`, `_Frequency`, `_Created`, `_Updated`) VALUES
(2, '1', '2', '', '', '', '', '', '', '', '', '', NULL, '2017-02-24 19:55:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ww_pupilmaster_bkp10052017`
--

CREATE TABLE IF NOT EXISTS `ww_pupilmaster_bkp10052017` (
  `_ID` int(10) NOT NULL,
  `_FirstName` varchar(255) DEFAULT NULL,
  `_LastName` varchar(255) DEFAULT NULL,
  `_FamilySurname` varchar(255) DEFAULT NULL,
  `_DOB` date DEFAULT NULL,
  `_Ethnicity` varchar(255) DEFAULT NULL,
  `_Gender` enum('Male','Female') DEFAULT NULL,
  `_School` int(10) DEFAULT NULL,
  `_Residential` varchar(255) DEFAULT NULL,
  `_Carer` varchar(255) DEFAULT NULL,
  `_CarerTitle` varchar(100) DEFAULT NULL,
  `_CarerFirstName` varchar(255) DEFAULT NULL,
  `_CarerLastName` varchar(255) DEFAULT NULL,
  `_CarerPhone` varchar(100) DEFAULT NULL,
  `_CarerMobile` varchar(100) DEFAULT NULL,
  `_Email` varchar(255) DEFAULT NULL,
  `_EmerContactName` varchar(255) DEFAULT NULL,
  `_EmerContactNumber` varchar(100) DEFAULT NULL,
  `_RiscAssessment` enum('yes','no') DEFAULT NULL,
  `_RASocialName` varchar(255) DEFAULT NULL,
  `_RASocialContact` varchar(255) DEFAULT NULL,
  `_DisableService` enum('yes','no') DEFAULT NULL,
  `_DSSocialName` varchar(255) DEFAULT NULL,
  `_DSSocialContact` varchar(100) DEFAULT NULL,
  `_Statement` enum('yes','no') DEFAULT NULL,
  `_RecHours` varchar(50) DEFAULT NULL,
  `_ReceiveBudget` enum('yes','no') DEFAULT NULL,
  `_NicName` varchar(100) DEFAULT NULL,
  `_MyDiagnosis` varchar(255) DEFAULT NULL,
  `_CommunicateBy` varchar(255) DEFAULT NULL,
  `_MyMobility` varchar(255) DEFAULT NULL,
  `_MedicalNeeds` enum('yes','no') DEFAULT NULL,
  `_MedicationFor` varchar(255) DEFAULT NULL,
  `_PersonalCareNeed` enum('yes','no') DEFAULT NULL,
  `_PersonalCareFor` varchar(255) DEFAULT NULL,
  `_Behaviour` varchar(255) DEFAULT NULL,
  `_WhenAnxious` varchar(255) DEFAULT NULL,
  `_WhenHappy` varchar(255) DEFAULT NULL,
  `_LikedActivities` varchar(255) DEFAULT NULL,
  `_DislikedActivities` varchar(255) DEFAULT NULL,
  `_CalmBy` varchar(255) DEFAULT NULL,
  `_LikedFood` varchar(255) DEFAULT NULL,
  `_DislikedFood` varchar(255) DEFAULT NULL,
  `_Allergic` varchar(255) DEFAULT NULL,
  `_AboutMe` varchar(255) DEFAULT NULL,
  `_GPContact` varchar(255) DEFAULT NULL,
  `_HospitalConsultant` varchar(255) DEFAULT NULL,
  `_OutingPermitted` enum('yes','no') DEFAULT NULL,
  `_AdvertisePermitted` enum('yes','no') DEFAULT NULL,
  `_CompletedPassport` enum('yes','no') DEFAULT NULL,
  `_SunCream` enum('yes','no') DEFAULT NULL,
  `_EmergencyEscort` enum('yes','no') DEFAULT NULL,
  `_EmergencyAid` enum('yes','no') DEFAULT NULL,
  `_AbleSwim` enum('yes','no') DEFAULT NULL,
  `_GetPool` enum('yes','no') DEFAULT NULL,
  `_ServiceCurrentlyAttend` varchar(255) DEFAULT NULL,
  `_OtherService` varchar(255) DEFAULT NULL,
  `_Mainstream` varchar(255) DEFAULT NULL,
  `_MainstreamOther` varchar(255) DEFAULT NULL,
  `_LeisureService` varchar(255) DEFAULT NULL,
  `_LeisureOther` varchar(255) DEFAULT NULL,
  `_ReceiveBudgetLBB` enum('yes','no') DEFAULT NULL,
  `_DisabilityAllowance` enum('yes','no') DEFAULT NULL,
  `_WhyAccessSNAP` mediumtext,
  `_WantAchieve` mediumtext,
  `_OtherInfo` mediumtext
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_pupilmaster_bkp10052017`
--

INSERT INTO `ww_pupilmaster_bkp10052017` (`_ID`, `_FirstName`, `_LastName`, `_FamilySurname`, `_DOB`, `_Ethnicity`, `_Gender`, `_School`, `_Residential`, `_Carer`, `_CarerTitle`, `_CarerFirstName`, `_CarerLastName`, `_CarerPhone`, `_CarerMobile`, `_Email`, `_EmerContactName`, `_EmerContactNumber`, `_RiscAssessment`, `_RASocialName`, `_RASocialContact`, `_DisableService`, `_DSSocialName`, `_DSSocialContact`, `_Statement`, `_RecHours`, `_ReceiveBudget`, `_NicName`, `_MyDiagnosis`, `_CommunicateBy`, `_MyMobility`, `_MedicalNeeds`, `_MedicationFor`, `_PersonalCareNeed`, `_PersonalCareFor`, `_Behaviour`, `_WhenAnxious`, `_WhenHappy`, `_LikedActivities`, `_DislikedActivities`, `_CalmBy`, `_LikedFood`, `_DislikedFood`, `_Allergic`, `_AboutMe`, `_GPContact`, `_HospitalConsultant`, `_OutingPermitted`, `_AdvertisePermitted`, `_CompletedPassport`, `_SunCream`, `_EmergencyEscort`, `_EmergencyAid`, `_AbleSwim`, `_GetPool`, `_ServiceCurrentlyAttend`, `_OtherService`, `_Mainstream`, `_MainstreamOther`, `_LeisureService`, `_LeisureOther`, `_ReceiveBudgetLBB`, `_DisabilityAllowance`, `_WhyAccessSNAP`, `_WantAchieve`, `_OtherInfo`) VALUES
(2, 'Fia', 'Moxon', NULL, '1970-01-22', 'white euro', 'Male', NULL, NULL, 'gujarat', 'mr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, 'yes', NULL, NULL, NULL, NULL, 'yes', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fdgthsdgfho fgoh ijsdfp;og hsdofghsdolffhdg s;dlfkhg ;lsdfhg;lsdfhf g;lsdf dg;lksdf g;lksdf g;lkhsdf g; lhsdfgl;khsdf;l ghsdf;lhgsd;flk hgd;slfkhg ;sdlfhg;lsdhf g;lsdkf g;lsdf ;lhsd fgl;h;dsl fg;lsdkhgf ;lksddf g;lksdf g;lkhf sdg;lkhsdf gl;hsdf;lk ghds;flgh sd;lfkh gds;lfkh gsd;lfkh gsd;lkfghf ;sdlghdf;sdlff gdsdgdf', NULL, NULL),
(3, 'Kitty', 'Gilbey', NULL, '1970-01-01', NULL, 'Male', NULL, NULL, NULL, 'mr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, NULL, 'yes', NULL, 'yes', NULL, NULL, NULL, NULL, 'yes', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ww_resourcebooking`
--

CREATE TABLE IF NOT EXISTS `ww_resourcebooking` (
  `_ID` int(10) NOT NULL,
  `_ClientID` int(10) NOT NULL,
  `_ResourceID` int(10) NOT NULL,
  `_Created` datetime NOT NULL,
  `_Getpayment` enum('Yes','No') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_resources`
--

CREATE TABLE IF NOT EXISTS `ww_resources` (
  `_ID` int(10) NOT NULL,
  `_Type` varchar(200) DEFAULT NULL,
  `_Title` varchar(200) DEFAULT NULL,
  `_Image` varchar(200) DEFAULT NULL,
  `_Description` longtext,
  `_Price` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_resourcetypes`
--

CREATE TABLE IF NOT EXISTS `ww_resourcetypes` (
  `_ID` int(10) NOT NULL,
  `_Type` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_schools`
--

CREATE TABLE IF NOT EXISTS `ww_schools` (
  `_ID` int(10) NOT NULL,
  `_Name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_schools`
--

INSERT INTO `ww_schools` (`_ID`, `_Name`) VALUES
(1, 'Barnehurst Infant School'),
(2, 'Barnehurst Junior School'),
(3, 'Barrington Primary School'),
(4, 'Bedonwell Infant and Nursery School'),
(5, 'Bedonwell Junior School'),
(6, 'Belmont Primary School'),
(7, 'Belvedere Infant School'),
(8, 'Belvedere Junior School'),
(9, 'Birkbeck Primary School'),
(10, 'Bishop Ridley CE VA Primary School'),
(11, 'Brampton Primary School'),
(12, 'Burnt Oak Junior School'),
(13, 'Bursted Wood Primary School'),
(14, 'Business Academy Bexley'),
(15, 'Castilion Primary School'),
(16, 'Chatsworth Infant School'),
(17, 'Christ Church (Erith) CE Primary School'),
(18, 'Crook Log Primary School'),
(19, 'Danson Primary School'),
(20, 'Days Lane Primary School'),
(21, 'Dulverton Primary School'),
(22, 'East Wickham Primary Academy'),
(23, 'Eastcote Primary School'),
(24, 'Foster&#39;s Primary School'),
(25, 'Gravel Hill Primary School'),
(26, 'Haberdashers&#39; Aske&#39;s Crayford Academy'),
(27, 'Hillsgrove Primary School'),
(28, 'Holy Trinity Lamorbey CE Primary School'),
(29, 'Hook Lane Primary School'),
(30, 'Hope Community School'),
(31, 'Hurst Primary School'),
(32, 'Jubilee Primary School'),
(33, 'Lessness Heath Primary School'),
(34, 'Longlands Primary School'),
(35, 'Mayplace Primary School'),
(36, 'Normandy Primary School'),
(37, 'Northumberland Heath Primary School'),
(38, 'Northwood Primary School'),
(39, 'Old Bexley CE Primary School'),
(40, 'Orchard School'),
(41, 'Our Lady of the Rosary RC Primary School'),
(42, 'Parkway Primary School'),
(43, 'Peareswood Primary School'),
(44, 'Pelham Primary School'),
(45, 'Pelham Primary School'),
(46, 'Royal Park Primary School'),
(47, 'St Augustine of Canterbury CE Primary School'),
(48, 'St Fidelis RC Primary School'),
(49, 'St John Fisher RC Primary School'),
(50, 'St Joseph&#39;s Catholic Primary School'),
(51, 'St Michael&#39;s East Wickham CE VA Primary School'),
(52, 'St Paulinus CE Primary School'),
(53, 'St Peter Chanel RC Primary School'),
(54, 'St Stephen&#39;s RC Primary School'),
(55, 'St Thomas More RC Primary School'),
(56, 'Sherwood Park Primary School'),
(57, 'Slade Green Primary School'),
(58, 'Upland Primary School'),
(59, 'Upton Primary School'),
(60, 'Willow Bank Primary School'),
(61, 'Bexleyheath Academy'),
(62, 'Blackfen School for Girls'),
(63, 'Business Academy Bexley'),
(64, 'Cleeve Park School'),
(65, 'Haberdashers&#39; Aske&#39;s Crayford Academy'),
(66, 'Harris Academy Falconwood'),
(67, 'Hurstmere School'),
(68, 'St Catherine&#39;s Catholic School for Girls'),
(69, 'St Columba&#39;s Catholic Boys&#39; School'),
(70, 'Trinity School'),
(71, 'Welling School'),
(72, 'Erith School'),
(73, 'Beths Grammar School (Boys)'),
(74, 'Bexley Grammar School'),
(75, 'Chislehurst and Sidcup Grammar School'),
(76, 'Townley Grammar School (Girls)'),
(77, 'Marlborough School'),
(78, 'Oakwood School'),
(79, 'Shenstone School'),
(80, 'Westbrooke School'),
(81, 'Woodside School'),
(82, 'Benedict House Preparatory School'),
(83, 'Merton Court School'),
(84, 'West Lodge School'),
(85, 'Break Through'),
(86, 'Park View Academy');

-- --------------------------------------------------------

--
-- Table structure for table `ww_services`
--

CREATE TABLE IF NOT EXISTS `ww_services` (
  `_ID` int(10) NOT NULL,
  `_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_ParentID` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ww_services`
--

INSERT INTO `ww_services` (`_ID`, `_Name`, `_ParentID`) VALUES
(1, 'SNAP/ other services I currently attend', 0),
(2, 'MCCH', 1),
(3, 'Moorings', 1),
(4, 'Charlton Ability Counts Programme', 1),
(5, 'Parkwood Leisure targeted disability swimming lessons or sports sessions', 1),
(6, 'Falcon Spartak gymnastics club', 1),
(7, 'Beavers disability swim session', 1),
(8, 'Bexley NAS', 1),
(9, 'Other', 1),
(10, 'Mainstream', 0),
(11, 'Bexley Youth Services', 10),
(12, 'Parkwood Leisure swimming lessons or sports sessions', 10),
(13, 'Charlton', 10),
(14, 'Other', 10),
(15, 'Leisure Services I would like to access', 0),
(16, 'Saturday Fun Club &#45; LBB', 15),
(17, 'Youth Club &#45; LBB', 15),
(18, 'Buddy Club &#45; LBB', 15),
(19, 'Archway Mechanics scheme', 15),
(20, 'Hydrotherapy sessions', 15),
(21, 'Half Term Holiday Schemes &#45; LBB', 15),
(22, 'Easter and Summer Holiday Schemes &#45; LBB', 15),
(23, 'Made to Measure (Alternative funding)', 15),
(24, 'Service you would like to see', 15);

-- --------------------------------------------------------

--
-- Table structure for table `ww_statusmaster`
--

CREATE TABLE IF NOT EXISTS `ww_statusmaster` (
  `_ID` int(11) NOT NULL,
  `_Status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_stripe_detail`
--

CREATE TABLE IF NOT EXISTS `ww_stripe_detail` (
  `_ID` int(10) NOT NULL,
  `_TestSecretKey` varchar(255) NOT NULL,
  `_TestPublishableKey` varchar(255) NOT NULL,
  `_LiveSecretKey` varchar(255) NOT NULL,
  `_LivePublishableKey` varchar(255) NOT NULL,
  `_Type` enum('Test','Live') NOT NULL DEFAULT 'Test',
  `_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_subjects`
--

CREATE TABLE IF NOT EXISTS `ww_subjects` (
  `_ID` int(10) NOT NULL,
  `_Name` varchar(255) NOT NULL,
  `_IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_timeslot`
--

CREATE TABLE IF NOT EXISTS `ww_timeslot` (
  `_ID` int(10) NOT NULL,
  `_Bookingtype` int(10) NOT NULL,
  `_Date` date NOT NULL,
  `_Fromtime` time NOT NULL,
  `_Totime` time NOT NULL,
  `_Status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_timeslot`
--

INSERT INTO `ww_timeslot` (`_ID`, `_Bookingtype`, `_Date`, `_Fromtime`, `_Totime`, `_Status`) VALUES
(2, 4, '2017-08-12', '02:00:00', '04:00:00', '0'),
(4, 4, '2017-08-14', '02:00:00', '04:00:00', '0'),
(5, 4, '2017-08-15', '02:00:00', '04:00:00', '0'),
(7, 2, '2017-09-30', '10:30:00', '15:30:00', '0'),
(8, 2, '2017-10-14', '10:30:00', '15:30:00', '0'),
(33, 5, '2017-10-28', '14:00:00', '15:30:00', '0'),
(34, 5, '2017-11-04', '14:00:00', '15:30:00', '0'),
(35, 5, '2017-11-11', '14:00:00', '15:30:00', '0'),
(36, 5, '2017-11-18', '14:00:00', '15:30:00', '0'),
(37, 5, '2017-11-25', '14:00:00', '15:30:00', '0'),
(38, 5, '2017-12-02', '14:00:00', '15:30:00', '0'),
(39, 5, '2017-12-09', '14:00:00', '15:30:00', '0'),
(40, 5, '2017-12-16', '14:00:00', '15:30:00', '0'),
(41, 13, '2017-10-28', '15:30:00', '17:00:00', '0'),
(42, 13, '2017-11-04', '15:30:00', '17:00:00', '0'),
(43, 13, '2017-11-11', '15:30:00', '17:00:00', '0'),
(44, 13, '2017-11-18', '15:30:00', '17:00:00', '0'),
(45, 13, '2017-11-25', '15:30:00', '17:00:00', '0'),
(46, 13, '2017-12-02', '15:30:00', '17:00:00', '0'),
(47, 13, '2017-12-09', '15:30:00', '17:00:00', '0'),
(48, 13, '2017-12-16', '15:30:00', '17:00:00', '0'),
(49, 8, '2017-11-01', '16:00:00', '17:45:00', '0'),
(50, 8, '2017-11-08', '16:00:00', '17:45:00', '0'),
(51, 8, '2017-11-15', '16:00:00', '17:45:00', '0'),
(52, 8, '2017-11-22', '16:00:00', '17:45:00', '0'),
(53, 8, '2017-11-29', '16:00:00', '17:45:00', '0'),
(54, 8, '2017-12-06', '16:00:00', '17:45:00', '0'),
(55, 8, '2017-12-13', '16:00:00', '17:45:00', '0'),
(56, 1, '2017-11-04', '13:00:00', '15:45:00', '0'),
(57, 1, '2017-11-11', '13:00:00', '15:45:00', '0'),
(58, 1, '2017-11-18', '13:00:00', '15:45:00', '0'),
(59, 1, '2017-11-25', '13:00:00', '15:45:00', '0'),
(60, 1, '2017-12-02', '13:00:00', '15:45:00', '0'),
(61, 1, '2017-12-09', '13:00:00', '15:45:00', '0'),
(62, 1, '2017-12-16', '13:00:00', '15:45:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `ww_triallesson`
--

CREATE TABLE IF NOT EXISTS `ww_triallesson` (
  `_ID` int(10) NOT NULL,
  `_PupilID` int(10) NOT NULL,
  `_TutorID` int(10) NOT NULL,
  `_SubjectID` int(10) NOT NULL,
  `_LevelID` int(10) NOT NULL,
  `_Date` date NOT NULL,
  `_Fromtime` time NOT NULL,
  `_Totime` time NOT NULL,
  `_Created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_tutlevel_rel`
--

CREATE TABLE IF NOT EXISTS `ww_tutlevel_rel` (
  `_ID` int(10) NOT NULL,
  `_TutorID` int(10) NOT NULL,
  `_LevelID` int(10) NOT NULL,
  `_Approve` enum('0','1') DEFAULT NULL COMMENT '0 for not approve,1 for approve'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_tutoravailibility`
--

CREATE TABLE IF NOT EXISTS `ww_tutoravailibility` (
  `_ID` int(10) NOT NULL,
  `_TutorID` int(10) NOT NULL,
  `_Date` date NOT NULL,
  `_Fromtime` time NOT NULL,
  `_Totime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_tutormaster`
--

CREATE TABLE IF NOT EXISTS `ww_tutormaster` (
  `_ID` int(10) NOT NULL,
  `_Image` varchar(255) DEFAULT NULL,
  `_Firstname` varchar(100) NOT NULL,
  `_Lastname` varchar(100) NOT NULL,
  `_Email` varchar(100) DEFAULT NULL,
  `_Password` varchar(100) DEFAULT NULL,
  `_Address_1` varchar(255) DEFAULT NULL,
  `_Address_2` varchar(255) DEFAULT NULL,
  `_City` varchar(100) DEFAULT NULL,
  `_State` varchar(100) DEFAULT NULL,
  `_Country` varchar(100) DEFAULT NULL,
  `_Zip` varchar(20) DEFAULT NULL,
  `_Phone` varchar(20) DEFAULT NULL,
  `_Biography` longtext,
  `_Status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT ' 0 = pending , 1= active , 2 = reject',
  `_Experience` varchar(20) DEFAULT NULL,
  `_Available` varchar(200) DEFAULT NULL,
  `_Ratephour` varchar(200) DEFAULT NULL,
  `_Created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `_Updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_tutsub_rel`
--

CREATE TABLE IF NOT EXISTS `ww_tutsub_rel` (
  `_ID` int(10) NOT NULL,
  `_TutorID` int(10) NOT NULL,
  `_SubjectID` int(10) NOT NULL,
  `_Approve` enum('0','1') NOT NULL COMMENT '0 for not approve,1 for approve'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_tut_pup_assign`
--

CREATE TABLE IF NOT EXISTS `ww_tut_pup_assign` (
  `_ID` int(10) NOT NULL,
  `_TutorID` int(10) NOT NULL,
  `_PupilID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ww_usermaster`
--

CREATE TABLE IF NOT EXISTS `ww_usermaster` (
  `_ID` int(10) NOT NULL,
  `_Firstname` varchar(100) NOT NULL,
  `_Lastname` varchar(100) NOT NULL,
  `_Username` varchar(200) NOT NULL,
  `_Email` varchar(200) NOT NULL,
  `_Password` varchar(200) NOT NULL,
  `_Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ww_usermaster`
--

INSERT INTO `ww_usermaster` (`_ID`, `_Firstname`, `_Lastname`, `_Username`, `_Email`, `_Password`, `_Created`) VALUES
(1, 'Alex', 'Jordan', 'admin', 'alex@jordan.com', 'admin123', '2016-10-17 11:25:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ww_auditlog`
--
ALTER TABLE `ww_auditlog`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_blogs`
--
ALTER TABLE `ww_blogs`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_blogtype`
--
ALTER TABLE `ww_blogtype`
  ADD PRIMARY KEY (`_ID`), ADD UNIQUE KEY `_Type` (`_Type`);

--
-- Indexes for table `ww_booked_lessons`
--
ALTER TABLE `ww_booked_lessons`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_bookingtype`
--
ALTER TABLE `ww_bookingtype`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_booking_schedule`
--
ALTER TABLE `ww_booking_schedule`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_clientmaster`
--
ALTER TABLE `ww_clientmaster`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_clipup_rel`
--
ALTER TABLE `ww_clipup_rel`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_cmspages`
--
ALTER TABLE `ww_cmspages`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_communications`
--
ALTER TABLE `ww_communications`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_communication_type`
--
ALTER TABLE `ww_communication_type`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_invoicepayment`
--
ALTER TABLE `ww_invoicepayment`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_invoices`
--
ALTER TABLE `ww_invoices`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_levels`
--
ALTER TABLE `ww_levels`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_problems`
--
ALTER TABLE `ww_problems`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_pupcategories`
--
ALTER TABLE `ww_pupcategories`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_pupcate_rel`
--
ALTER TABLE `ww_pupcate_rel`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_pupilmaster`
--
ALTER TABLE `ww_pupilmaster`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_pupilmaster_bkp05052017`
--
ALTER TABLE `ww_pupilmaster_bkp05052017`
  ADD PRIMARY KEY (`_ID`), ADD UNIQUE KEY `_ID` (`_ID`);

--
-- Indexes for table `ww_pupilmaster_bkp10052017`
--
ALTER TABLE `ww_pupilmaster_bkp10052017`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_resourcebooking`
--
ALTER TABLE `ww_resourcebooking`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_resources`
--
ALTER TABLE `ww_resources`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_resourcetypes`
--
ALTER TABLE `ww_resourcetypes`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_schools`
--
ALTER TABLE `ww_schools`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_services`
--
ALTER TABLE `ww_services`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_statusmaster`
--
ALTER TABLE `ww_statusmaster`
  ADD PRIMARY KEY (`_ID`), ADD UNIQUE KEY `_Status` (`_Status`);

--
-- Indexes for table `ww_stripe_detail`
--
ALTER TABLE `ww_stripe_detail`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_subjects`
--
ALTER TABLE `ww_subjects`
  ADD PRIMARY KEY (`_ID`), ADD UNIQUE KEY `_Name` (`_Name`);

--
-- Indexes for table `ww_timeslot`
--
ALTER TABLE `ww_timeslot`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_triallesson`
--
ALTER TABLE `ww_triallesson`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_tutlevel_rel`
--
ALTER TABLE `ww_tutlevel_rel`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_tutoravailibility`
--
ALTER TABLE `ww_tutoravailibility`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_tutormaster`
--
ALTER TABLE `ww_tutormaster`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_tutsub_rel`
--
ALTER TABLE `ww_tutsub_rel`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_tut_pup_assign`
--
ALTER TABLE `ww_tut_pup_assign`
  ADD PRIMARY KEY (`_ID`);

--
-- Indexes for table `ww_usermaster`
--
ALTER TABLE `ww_usermaster`
  ADD PRIMARY KEY (`_ID`), ADD UNIQUE KEY `_Email` (`_Email`), ADD UNIQUE KEY `_Username` (`_Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ww_auditlog`
--
ALTER TABLE `ww_auditlog`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=362;
--
-- AUTO_INCREMENT for table `ww_blogs`
--
ALTER TABLE `ww_blogs`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_blogtype`
--
ALTER TABLE `ww_blogtype`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_booked_lessons`
--
ALTER TABLE `ww_booked_lessons`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ww_bookingtype`
--
ALTER TABLE `ww_bookingtype`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ww_booking_schedule`
--
ALTER TABLE `ww_booking_schedule`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ww_clientmaster`
--
ALTER TABLE `ww_clientmaster`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ww_clipup_rel`
--
ALTER TABLE `ww_clipup_rel`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ww_cmspages`
--
ALTER TABLE `ww_cmspages`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ww_communications`
--
ALTER TABLE `ww_communications`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ww_communication_type`
--
ALTER TABLE `ww_communication_type`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ww_invoicepayment`
--
ALTER TABLE `ww_invoicepayment`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_invoices`
--
ALTER TABLE `ww_invoices`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_levels`
--
ALTER TABLE `ww_levels`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_problems`
--
ALTER TABLE `ww_problems`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_pupcategories`
--
ALTER TABLE `ww_pupcategories`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ww_pupcate_rel`
--
ALTER TABLE `ww_pupcate_rel`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ww_pupilmaster`
--
ALTER TABLE `ww_pupilmaster`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ww_pupilmaster_bkp05052017`
--
ALTER TABLE `ww_pupilmaster_bkp05052017`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ww_pupilmaster_bkp10052017`
--
ALTER TABLE `ww_pupilmaster_bkp10052017`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ww_resourcebooking`
--
ALTER TABLE `ww_resourcebooking`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_resources`
--
ALTER TABLE `ww_resources`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_resourcetypes`
--
ALTER TABLE `ww_resourcetypes`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_schools`
--
ALTER TABLE `ww_schools`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `ww_services`
--
ALTER TABLE `ww_services`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ww_statusmaster`
--
ALTER TABLE `ww_statusmaster`
  MODIFY `_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_stripe_detail`
--
ALTER TABLE `ww_stripe_detail`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_subjects`
--
ALTER TABLE `ww_subjects`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_timeslot`
--
ALTER TABLE `ww_timeslot`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `ww_triallesson`
--
ALTER TABLE `ww_triallesson`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_tutlevel_rel`
--
ALTER TABLE `ww_tutlevel_rel`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_tutoravailibility`
--
ALTER TABLE `ww_tutoravailibility`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_tutormaster`
--
ALTER TABLE `ww_tutormaster`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_tutsub_rel`
--
ALTER TABLE `ww_tutsub_rel`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_tut_pup_assign`
--
ALTER TABLE `ww_tut_pup_assign`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ww_usermaster`
--
ALTER TABLE `ww_usermaster`
  MODIFY `_ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
