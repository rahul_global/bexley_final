<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$type = decrypt($_REQUEST['type'],$encrypt);

$seltype = "select * from ".$tbname."_resourcetypes where _ID='".$type."'";
$rowtype =mysqli_fetch_assoc(mysqli_query($con,$seltype));
$typetext = $rowtype['_Type'];

if($action == 'edit'){

$title = $sitename." : Edit Resource";
$id = decrypt($_REQUEST['id'],$encrypt);
$sel_clint = "select * from ".$tbname."_resources where _ID='".$id."'";
$rst_clint = mysqli_query($con,$sel_clint);
$row = mysqli_fetch_assoc($rst_clint);
$restitle = $row['_Title'];
$desc = $row['_Description'];
$img = $row['_Image'];
$price = $row['_Price'];
$adminresourcepath = "uploads/resources/";
$bread = "Edit Resource";
$btntext = 'Update';

}else{
    $title = $sitename." : Add Resource";
    $bread = "Add Resource";
    $btntext = 'Insert';
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="resource.php?type=<?php echo encrypt($type,$encrypt); ?>">Resource-<?php echo $typetext; ?></a></li><li><?php echo $bread; ?></li> 
                </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="resourceaction.php" method="post" enctype="multipart/form-data">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
                                        
                                        <div class="form-group">
                                            <label for="txttitle" class="col-sm-2 control-label">Title&nbsp;:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="txttitle" name="txttitle" value="<?php echo $restitle;?>" required>
                                                <input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                                <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                            </div>
                                        </div>

                                        <?php 
                                        if($img == ""){ ?>

                                            <div class="form-group">
                                                <label for="txtimg" class="col-sm-2 control-label">Image&nbsp;:</label>
                                                <div class="col-sm-10">
                                                    <input type="file" class="btn btn-default" id="txtimg" name="txtimg">
                                                </div>
                                            </div>

                                <?php    }else{ ?>

                                            <div class="form-group">
                                                <label for="txtimg" class="col-sm-2 control-label">Image&nbsp;:</label>
                                                <div class="col-sm-10">
                                                    <div class="col-sm-3">
                                                        <img src="<?php echo $httpaddress.$adminresourcepath.$img; ?>" class="gen-img">
                                                        <a  class="cancel-sec" href="resourceaction.php?id=<?php echo encrypt($id,$encrypt); ?>&e_action=<?php echo encrypt('deletepic',$encrypt); ?>&type=<?php echo encrypt($type,$encrypt); ?>"><span class="el-icon-remove bs_ttip" title="" data-original-title="Delete Image">Delete</span></a>
                                                    </div>
                                                </div>
                                            </div>

                            <?php        }

                                        ?>
                                        

                                        <div class="form-group">
                                            <label for="txtdesc" class="col-sm-2 control-label">Description&nbsp;:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="txtdesc" name="txtdesc" required><?php echo $desc;?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtprice" class="col-sm-2 control-label">Price&nbsp;:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="txtprice" name="txtprice" value="<?php echo $price;?>" required>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label for="txtprice" class="col-sm-2 control-label">Resource type&nbsp;:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="txttexttype" name="txttexttype" value="<?php echo $typetext;?>" readonly>
                                                <input type="hidden" name="txttype" id="txttype" value="<?php echo $type; ?>" />
                                            </div>
                                        </div>
                                        
                                        
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        var type = "<?php echo $_REQUEST['type']; ?>";
        $("#btncancle").click(function (){
            window.location = "resource.php?type="+type;
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
