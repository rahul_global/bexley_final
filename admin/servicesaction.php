<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$name = replaceSpecialChar($_REQUEST['txtname']);
$parentid = $_REQUEST['parentid'];

if($action == "add"){

			

		$str = "INSERT INTO " . $tbname . "_services (";
     
     	$str .= " _Name , _ParentID) VALUES(";
     
		if ($name != "")
		 	$str = $str . "'" . $name . "' ,";
		else
		  	$str = $str . "null ,";
        
		if ($parentid != "")
		 	$str = $str . " " . $parentid . " ";
		else
		  	$str = $str . "0";
		
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Add New Service - ".$name);
		//log create end
		header("location:services.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:services.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_services SET ";
	    
	    if ($name != "")
			$str = $str . "_Name = '" . $name . "', ";
		else
	        $str = $str . "_Name = null ,";
	
		if ($parentid != "")
			$str = $str . "_ParentID = '" . $parentid . "' ";
		else
	        $str = $str . "_ParentID = 0 ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Service - ".$name);
		//log create end
		header("location:services.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:services.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){
	//log create start
		$scl_qry  = "SELECT _Name FROM ".$tbname."_services WHERE _ID = ".$id;
		$run_scl = mysqli_query($con, $scl_qry);
		$fetch_scl = mysqli_fetch_assoc($run_scl);
		$service = $fetch_scl['_Name'];
	//log create end

	$delclient = "delete from ".$tbname."_schools where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Service - ".$service);
		//log create end
		header("location:services.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:services.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>