<?php 
include_once('db/dbopen.php');

if(isset($_SESSION['userid']) && $_SESSION['userid'] != '')
{
	header("location:dashboard.php");
	exit;
}

$title = "Bexley Snap CRM : Login";

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title;?></title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- bootstrap framework -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!-- google webfonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- elegant icons -->
        <link href="assets/icons/elegant/style.css" rel="stylesheet" media="screen">
        <!-- main stylesheet -->
		<link href="assets/css/main.min.css" rel="stylesheet" media="screen">

        <!-- jQuery -->
        <script src="assets/js/jquery.js"></script>
		<style>
			.error { color : #ff0000; }
			.success { color : #00ff00; }
		</style>
    </head>
    <body class="login_page">
	
	<div class="login_header">
            <!-- <img src="assets/img/logo.png" alt="site_logo"> -->
            <h2 style="color: #fff;">Bexley Snap CRM</h2>
        </div>
        <div class="login_register_form">
            <div class="form_wrapper animated-short">
                <h3 class="sepH_c"><span>Login</span></h3>
				<div id="response_msg" class=""></div>
                <form action="" name="login_frm" id="login_frm" method="post">
					<span class="error"></span>
                    <div class="input-group input-group-lg sepH_a">
                        <span class="input-group-addon"><span class="icon_profile"></span></span>
                        <input type="text" class="form-control" placeholder="Username" name="txtusername" id="txtusername">
                    </div>
					<span class="error"></span>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><span class="icon_key_alt"></span></span>
                        <input type="password" class="form-control" placeholder="Password" name="txtpassword" id="txtpassword">
                    </div>
					<div class="sepH_c text-right">
					</div>
                    <div class="form-group sepH_c">
                        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Login" name="e_action">
                    </div>
                </form>
            </div>
        </div>
		<script src="assets/js/jquery.validate.js"></script>
		
        <script>
            $(function () {
				$("#login_frm").validate({
					rules : {
						txtusername : {
							required : true
						},
						txtpassword : {
							required : true
						}
					},
					errorPlacement: function(error, element) {
						error.appendTo(element.parent().prev('span'));
					},
					messages : {
						txtusername : {
							required : "Please Enter Username."
						},
						txtpassword : {
							required : "Please Enter Password."
						}
					},
					submitHandler : function(){
						var dataString = $("#login_frm").serialize();
						$.ajax({
							url : "ajax/loginaction.php",
							data : dataString,
							type : "POST",
							dataType : "json",
							success : function(response)
							{
								if(response.result == 'success')
								{
									console.log('success');
									$("#response_msg").html('<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>'+response.msg+'</strong></div>');
									window.location.href = "dashboard.php";
								}
								else
								{
									console.log(' not success');
									$("#response_msg").html('<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>'+response.msg+'</strong></div>');
								}
							}
						})
					}
					
				});
            });
        </script>
    </body>
</html>