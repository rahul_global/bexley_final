<?php 
require_once('../db/dbopen.php');

$tutor_id 			= decrypt($_POST['tutor_id'],$encrypt);

$firstname 			= replaceSpecialChar($_POST['txtfname']);
$lastname 			= replaceSpecialChar($_POST['txtlname']);
$email 				= replaceSpecialChar($_POST['txtemail']);
$phone	 			= replaceSpecialChar($_POST['txtphone']);
//$subject 			= replaceSpecialChar($_POST['txtsubject']);
$subject 			= implode(',',$_POST['txtsubject']);
$address1 			= replaceSpecialChar($_POST['txtadd1']);
$address2 			= replaceSpecialChar($_POST['txtadd2']);
$city 				= replaceSpecialChar($_POST['txtcity']);
$state	 			= replaceSpecialChar($_POST['txtstate']);
$country			= replaceSpecialChar($_POST['txtcountry']);
$zip	 			= replaceSpecialChar($_POST['txtzip']);

$action 			= replaceSpecialChar($_POST['btnsubmit']);

if($firstname != '' && $lastname != '' && $email != ''  && $phone != ''  && $address1 != '' && $address2 != '' && $city != '' && $state != ''  && $country != ''  && $zip != '' && $action != '')
{
	if($action == "Update")
	{
		$status = $_POST['txtstatus'];
		$check_qry = "SELECT _ID , _Firstname , _Lastname , _Email , _Phone , _Subject , _Address_1 , _Address_2 , _City , _State , _Country , _Zip , _Status FROM ".$tbname."_tutormaster WHERE _ID = $tutor_id";
		$run_check = mysqli_query($con, $check_qry);
		$num = mysqli_num_rows($run_check);
		if($num > 0)
		{
			$fetch_data = mysqli_fetch_assoc($run_check);
			
			$query 	= "UPDATE ".$tbname."_tutormaster SET ";
			
			if($firstname != $fetch_data['_Firstname'])
			{
				$query .= " _Firstname = '$firstname' ,";
			}
			if($lastname != $fetch_data['_Lastname'])
			{
				$query .= " _Lastname = '$lastname' ,";
			}
			if($email != $fetch_data['_Email'])
			{
				$query .= " _Email = '$email' ,";
			}
			if($phone != $fetch_data['_Phone'])
			{
				$query .= " _Phone = '$phone' ,";
			}
			if($subject != $fetch_data['_Subject'])
			{
				$query .= " _Subject = '$subject' ,";
			}
			if($address1 != $fetch_data['_Address_1'])
			{
				$query .= " _Address_1 = '$address1' ,";
			}
			if($address2 != $fetch_data['_Address_2'])
			{
				$query .= " _Address_2 = '$address2' ,";
			}
			if($city != $fetch_data['_City'])
			{
				$query .= " _City = '$city' ,";
			}
			if($state != $fetch_data['_State'])
			{
				$query .= " _State = '$state' ,";
			}
			if($country != $fetch_data['_Country'])
			{
				$query .= " _Country = '$country' ,";
			}
			if($zip != $fetch_data['_Zip'])
			{
				$query .= " _Zip = '$zip' ,";
			}
			if($status != $fetch_data['_Status'])
			{
				$query .= " _Status = '$status' ,";
			}
			
			$query .= " _Updated = '$today'";
			
			$query = rtrim($query , ',');			
			
			$query .= " WHERE _ID = $tutor_id";
			
			if($run = mysqli_query($con , $query))
			{
				$result = array("result"=>"success","msg"=>"Data Updated Successfully.");
			}
			else
			{
				$result = array("result"=>"failed","msg"=>"Error Occurred.","query"=>$query);
			}
		}
		else
		{
			$result = array("result"=>"failed","msg"=>"Incorrect Data.");
		}
	}
	else if($action == "Add")
	{	
		$password = $_POST['txtpassword'];
		$query 	= "INSERT INTO ".$tbname."_tutormaster(_Firstname , _Lastname , _Email , _Password , _Phone , _Subject , _Address_1 , _Address_2 , _City , _State , _Country , _Zip , _Created) VALUES('$firstname' , '$lastname' , '$email' , '$password' , '$phone' , '$subject' , '$address1' , '$address2' , '$city' , '$state' , '$country' , '$zip' , '$today')";
		if($run 	= mysqli_query($con , $query))
		{
			$result = array("result"=>"success","msg"=>"Data Inserted Successfully.");
		}
		else
		{
			$result = array("result"=>"failed","msg"=>"Incorrect Data.");
		}
	}
}
else
{
	$result = array("result"=>"failed","msg"=>"Incorrect Data.");
}

echo json_encode($result);
?>