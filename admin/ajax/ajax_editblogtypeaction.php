<?php 
require_once('../db/dbopen.php');

$blogtype_id		= decrypt($_POST['blogtype_id'],$encrypt);

$blogtype 			= replaceSpecialChar($_POST['txtbtype']);
$status				= $_POST['txtstatus'];
$action 			= replaceSpecialChar($_POST['btnsubmit']);

if($blogtype != '' && $action != '')
{
	if($action == "Update")
	{
		$check_qry = "SELECT _ID , _Type , _Status FROM ".$tbname."_blogtype WHERE _ID = $blogtype_id";
		$run_check = mysqli_query($con, $check_qry);
		$num = mysqli_num_rows($run_check);
		if($num > 0)
		{
			$fetch_data = mysqli_fetch_assoc($run_check);
			
			$qry  	= "UPDATE ".$tbname."_blogtype SET ";
			$query 	= "";
			
			if($blogtype != $fetch_data['_Type'])
			{
				$query .= " _Type = '$blogtype'";
			}
			if($status != $fetch_data['_Status'])
			{
				$query .= " _Status = '$status'";
			}
			
			$query = rtrim($query , ',');			
			
			if($query != '')
			{
				$query .= " WHERE _ID = $blogtype_id";
				$query = $qry.''.$query;
				if($run = mysqli_query($con , $query))
				{
					$result = array("result"=>"success","msg"=>"Data Updated Successfully.");
				}
				else
				{
					$result = array("result"=>"failed","msg"=>"Error Occurred.","query"=>$query);
				}
			}
			else
			{
				$result = array("result"=>"success","msg"=>"Data Updated Successfully.");
			}
		}
		else
		{
			$result = array("result"=>"failed","msg"=>"Incorrect Data.");
		}
	}
	else if($action == "Add")
	{	
		$query 	= "INSERT INTO ".$tbname."_blogtype(_Type , _Status) VALUES('$blogtype','$status')";
		if($run 	= mysqli_query($con , $query))
		{
			$result = array("result"=>"success","msg"=>"Data Inserted Successfully.");
		}
		else
		{
			$result = array("result"=>"failed","msg"=>"Incorrect Data.");
		}
	}
}
else
{
	$result = array("result"=>"failed","msg"=>"Incorrect Data.");
}

echo json_encode($result);
?>