<?php 
require_once('../db/dbopen.php');

$pupil_id 			= decrypt($_POST['pupil_id'],$encrypt);

$clientid 			= $_POST['txtclient'];
$firstname 			= replaceSpecialChar($_POST['txtfname']);
$lastname 			= replaceSpecialChar($_POST['txtlname']);
$email 				= replaceSpecialChar($_POST['txtemail']);
$phone	 			= replaceSpecialChar($_POST['txtphone']);
$address1 			= replaceSpecialChar($_POST['txtadd1']);
$address2 			= replaceSpecialChar($_POST['txtadd2']);
$city 				= replaceSpecialChar($_POST['txtcity']);
$state	 			= replaceSpecialChar($_POST['txtstate']);
$country			= replaceSpecialChar($_POST['txtcountry']);
$zip	 			= replaceSpecialChar($_POST['txtzip']);

$action 			= replaceSpecialChar($_POST['btnsubmit']);

if($firstname != '' && $lastname != '' && $email != ''  && $phone != ''  && $address1 != '' && $address2 != '' && $city != '' && $state != ''  && $country != ''  && $zip != '' && $action != '')
{
	if($action == "Update")
	{
		$check_qry = "SELECT _ID , _ClientID , _Firstname , _Lastname , _Email , _Phone , _Address_1 , _Address_2 , _City , _State , _Country , _Zip FROM ".$tbname."_pupilmaster WHERE _ID = $pupil_id";
		$run_check = mysqli_query($con, $check_qry);
		$num = mysqli_num_rows($run_check);
		if($num > 0)
		{
			$fetch_data = mysqli_fetch_assoc($run_check);
			
			$query 	= "UPDATE ".$tbname."_pupilmaster SET ";
			
			if($clientid != $fetch_data['_ClientID'])
			{
				$query .= " _ClientID = '$clientid' ,";
			}
			if($firstname != $fetch_data['_Firstname'])
			{
				$query .= " _Firstname = '$firstname' ,";
			}
			if($lastname != $fetch_data['_Lastname'])
			{
				$query .= " _Lastname = '$lastname' ,";
			}
			if($email != $fetch_data['_Email'])
			{
				$query .= " _Email = '$email' ,";
			}
			if($phone != $fetch_data['_Phone'])
			{
				$query .= " _Phone = '$phone' ,";
			}
			if($address1 != $fetch_data['_Address_1'])
			{
				$query .= " _Address_1 = '$address1' ,";
			}
			if($address2 != $fetch_data['_Address_2'])
			{
				$query .= " _Address_2 = '$address2' ,";
			}
			if($city != $fetch_data['_City'])
			{
				$query .= " _City = '$city' ,";
			}
			if($state != $fetch_data['_State'])
			{
				$query .= " _State = '$state' ,";
			}
			if($country != $fetch_data['_Country'])
			{
				$query .= " _Country = '$country' ,";
			}
			if($zip != $fetch_data['_Zip'])
			{
				$query .= " _Zip = '$zip' ,";
			}
			
			$query .= " _Updated = '$today'";
			
			$query = rtrim($query , ',');			
			
			$query .= " WHERE _ID = $pupil_id";
			
			if($run = mysqli_query($con , $query))
			{
				$result = array("result"=>"success","msg"=>"Data Updated Successfully.");
			}
			else
			{
				$result = array("result"=>"failed","msg"=>"Error Occurred.","query"=>$query);
			}
		}
		else
		{
			$result = array("result"=>"failed","msg"=>"Incorrect Data.");
		}
	}
	else if($action == "Add")
	{	
		$query 	= "INSERT INTO ".$tbname."_pupilmaster(_ClientID , _Firstname , _Lastname , _Email , _Phone , _Address_1 , _Address_2 , _City , _State , _Country , _Zip , _Created) VALUES($clientid , '$firstname' , '$lastname' , '$email' , '$phone' , '$address1' , '$address2' , '$city' , '$state' , '$country' , '$zip' , '$today')";
		if($run 	= mysqli_query($con , $query))
		{
			$result = array("result"=>"success","msg"=>"Data Inserted Successfully.");
		}
		else
		{
			$result = array("result"=>"failed","msg"=>"Incorrect Data.");
		}
	}
}
else
{
	$result = array("result"=>"failed","msg"=>"Incorrect Data.");
}

echo json_encode($result);
?>