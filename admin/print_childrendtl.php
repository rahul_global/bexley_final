<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$title 		= $sitename." : Children Details";

if(isset($_GET['id']))
{
	//$id 		= decrypt($_GET['id'],$encrypt);
	$id = $_GET['id'];
	$pupil_qry 	= "select * FROM ".$tbname."_pupilmaster where _ID='".$id."'";
	$run_pupil 	= mysqli_query($con,$pupil_qry);
	$num_pupil 	= mysqli_num_rows($run_pupil);

	if($num_pupil >	0)
	{
		$fetch_pupil = mysqli_fetch_assoc($run_pupil);

		$sqlclient = "select * from ".$tbname."_clipup_rel where _PupilID = '".$id."'";
		$rsclient = mysqli_fetch_assoc(mysqli_query($con,$sqlclient));
		$clientid = $rsclient['_ClientID'];

		$sqlcate = "select * from ".$tbname."_pupcate_rel where _PupilID = '".$id."'";
		$fetchcate = mysqli_fetch_assoc(mysqli_query($con,$sqlcate));
		$categoryid = $fetchcate['_CategoryID'];

		if($fetch_pupil['_ClientID'] != null || $fetch_pupil['_ClientID'] != 0 || $fetch_pupil['_ClientID'] != '')
		{
			$carer_qry = "SELECT CONCAT(_Firstname, ' ' , _Lastname) Fullname FROM ".$tbname."_clientmaster WHERE _Status = '1' AND _ID = ".$fetch_pupil['_ClientID'];
			if($carer_run = mysqli_query($con,$carer_qry))
			{
				$carer_num = mysqli_num_rows($carer_run);
				if($carer_num > 0)
				{
					$carer_fetch = mysqli_fetch_assoc($carer_run);
					$carername = $carer_fetch['Fullname'];
				}
			}
		}
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
        <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<style>
		.pad{padding:7px;}
		.rgt{text-align:right;}
		</style>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
			
			<u><h3 style="text-align:center;">Childrens Details</h3></u>
            <!-- main content -->
            <div id="main">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                            <div id="response_msg" class=""></div>
                                <h3 class="heading_a" style="text-align:center;"><span class="heading_text">General info</span></h3>
									<div class="col-sm-12 col-md-12 pad">
										<label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Child Surname: </label>
										<div class="col-sm-5 col-md-5">
											<?php echo isset($fetch_pupil['_LastName'])?$fetch_pupil['_LastName']:'-';?>
										</div>
									</div>
									<div class="col-sm-12 col-md-12 pad">
										<label for="txtfname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Child First Name: </label>
										<div class="col-sm-5 col-md-5">
											<?php echo isset($fetch_pupil['_FirstName'])?$fetch_pupil['_FirstName']:'-';?>
										</div>
									</div>
									<div class="col-sm-12 col-md-12 pad">
										<label for="txtfname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Carer Name: </label>
										<div class="col-sm-5 col-md-5">
											<?php echo isset($carername) && $carername != '' ? $carername : '-';?>
										</div>
									</div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Family Surname: <br>(if different)</label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_FamilySurname'])?$fetch_pupil['_FamilySurname']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Date of Birth: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DOB'])?date('d/m/Y',strtotime($fetch_pupil['_DOB'])):'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Child Ethnicity: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Ethnicity'])?$fetch_pupil['_Ethnicity']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Gender: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Gender']) ? $fetch_pupil['_Gender'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">School Name: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_School'])?$fetch_pupil['_School']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Daily or Residential: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Residential'])?$fetch_pupil['_Residential']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Parent or Carer: <br>(Please state)</label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Carer'])?$fetch_pupil['_Carer']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Title: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CarerTitle'])?$fetch_pupil['_CarerTitle']:'-';?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 pad">
                                            <label for="txtfname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">First Name: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CarerFirstName'])?$fetch_pupil['_CarerFirstName']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtlname" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Surname: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CarerLastName'])?$fetch_pupil['_CarerLastName']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Telephone: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CarerPhone'])?$fetch_pupil['_CarerPhone']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Mobile: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CarerMobile'])?$fetch_pupil['_CarerMobile']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Email: </label>
                                            <div class="col-sm-5 col-md-5">
                                               <?php echo isset($fetch_pupil['_Email'])?$fetch_pupil['_Email']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Alternative Emergency Contact Name: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_EmerContactName'])?$fetch_pupil['_EmerContactName']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Alternative Emergency Contact Number: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_EmerContactNumber'])?$fetch_pupil['_EmerContactNumber']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I  am a looked after child or have needs and risks subject to a Safeguarding plan, risk assessment: </label>
                                            <div class="col-sm-5 col-md-5">
                                               <?php echo isset($fetch_pupil['_RiscAssessment']) ? $fetch_pupil['_RiscAssessment'] :'-' ;?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Social Worker Name:  </label>
                                            <div class="col-sm-5 col-md-5">
                                               <?php echo isset($fetch_pupil['_RASocialName'])?$fetch_pupil['_RASocialName']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Contact Number:  </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_RASocialContact'])?$fetch_pupil['_RASocialContact']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I  am registered with the Disabled Children’s Service: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DisableService'])?$fetch_pupil['_DisableService']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Social Worker Name:  </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DSSocialName'])?$fetch_pupil['_DSSocialName']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Contact Number:  </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DSSocialContact'])?$fetch_pupil['_DSSocialContact']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I have a statement of Special Educational Needs or Education, Health and Care plan: </label>
                                            <div class="col-sm-5 col-md-5">
                                               <?php echo isset($fetch_pupil['_Statement']) ? $fetch_pupil['_Statement'] : '';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Number of recommended hours: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_RecHours'])?$fetch_pupil['_RecHours']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I receive a personal budget or Direct Payments from London Borough of Bexley: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_ReceiveBudget']) ? $fetch_pupil['_ReceiveBudget'] : '';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I like to be called (please use other names by which your child likes to be called): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_NicName'])?$fetch_pupil['_NicName']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">My diagnosis is (please list all): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_MyDiagnosis'])?$fetch_pupil['_MyDiagnosis']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I communicate by (e.g. verbal, PECS / symbols): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CommunicateBy'])?$fetch_pupil['_CommunicateBy']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">My mobility is (include if your child is able to swim): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_MyMobility'])?$fetch_pupil['_MyMobility']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I have Medical needs (including tube feeding): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_MedicalNeeds']) ? $fetch_pupil['_MedicalNeeds'] : $fetch_pupil['_MedicalNeeds'] ;?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Please indicate if your child needs regular medication and for what –: </label>
                                            <div class="col-sm-5 col-md-5">
												 <?php echo isset($fetch_pupil['_MedicationFor'])?$fetch_pupil['_MedicationFor']:'-';?>
												 <br>
												 <br>you will need to complete a ‘green’ Medication Consent Form if they require medication whilst attending a leisure scheme. <br>
All medications must be in their original packaging with the child/young person’s name clearly marked on them.
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I have Personal Care needs: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_PersonalCareNeed']) ? $fetch_pupil['_PersonalCareNeed'] : '';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Please indicate if your child needs regular support with personal care and to what level –: </label>
                                            <div class="col-sm-5 col-md-5">
												<?php echo isset($fetch_pupil['_PersonalCareFor'])?$fetch_pupil['_PersonalCareFor']:'-';?><br><br>You will need to provide appropriate changes of clothes, nappies, pads, wipes. Without these items you may be contact to collect your child if we cannot meet their personal care needs
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">This means I may do (please indicate particular behaviours of your child): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Behaviour'])?$fetch_pupil['_Behaviour']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">When I do this, it means I am anxious (please indicate signs your child displays when getting anxious or distressed): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_WhenAnxious'])?$fetch_pupil['_WhenAnxious']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">When I do this, it means I am happy (please indicate behaviour displayed): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_WhenHappy'])?$fetch_pupil['_WhenHappy']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I like to (please indicate activities your child/young person likes to do): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_LikedActivities'])?$fetch_pupil['_LikedActivities']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I do not like to (please indicate activities your child/young person does not like to do): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DislikedActivities'])?$fetch_pupil['_DislikedActivities']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I like to be calmed by (please indicate how we can calm your child if distressed or angry): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CalmBy'])?$fetch_pupil['_CalmBy']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I like to eat/drink (please indicate which food/drink): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_LikedFood'])?$fetch_pupil['_LikedFood']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I do not like / I cannot have to eat/drink (please indicate which food /drink): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DislikedFood'])?$fetch_pupil['_DislikedFood']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I am allergic to (Please include food allergies): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Allergic'])?$fetch_pupil['_Allergic']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Other things you should know about me: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_AboutMe'])?$fetch_pupil['_AboutMe']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">GPs Contact Details (GP Address and contact number to be used in an emergency): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_GPContact'])?$fetch_pupil['_GPContact']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Hospital Consultant (Name of Hospital and contact number to be used in an emergency): </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_HospitalConsultant'])?$fetch_pupil['_HospitalConsultant']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Permission for outings with SNAP local parks,  shops facilities, community: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_OutingPermitted'])?$fetch_pupil['_OutingPermitted']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Take photos / video for external/publicity website flyers, advertising and presentations: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_AdvertisePermitted']) ? $fetch_pupil['_AdvertisePermitted'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Take a photo to add to  completed passport: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_CompletedPassport']) ?$fetch_pupil['_CompletedPassport'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Apply sun cream as required: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_SunCream'])? $fetch_pupil['_SunCream'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Emergency escort in ambulance: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_EmergencyEscort']) ? $fetch_pupil['_EmergencyEscort'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Give Emergency 1st Aid: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_EmergencyAid']) ?$fetch_pupil['_EmergencyAid'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I am able to swim: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_AbleSwim'])? $fetch_pupil['_AbleSwim'] :'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">happy to get in a swimming pool: </label>
                                            <div class="col-sm-5 col-md-5">
												<?php echo isset($fetch_pupil['_GetPool'])? $fetch_pupil['_GetPool'] :'-';?>
                                            </div>
                                        </div>
                                        <h3 class="heading_a" style="text-align:center;"><span class="heading_text">Services, Signposting and Goal setting</span></h3>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">SNAP/ other services I currently attend: </label>
                                            <div class="col-sm-5 col-md-5">
												<?php echo isset($fetch_pupil['_ServiceCurrentlyAttend'])?$fetch_pupil['_ServiceCurrentlyAttend']:'-';?>
                                            </div>
                                        </div>
										<div class="form-group" id="os">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Other Service: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_OtherService'])?$fetch_pupil['_OtherService']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Mainstream: </label>
											<?php
											$mainstream_service = explode(',',$fetch_pupil['_Mainstream']);
											?>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_Mainstream'])?$fetch_pupil['_Mainstream']:'-';?>
                                            </div>
                                        </div>
										<div class="form-group" id="oms">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Other Mainstream Service: </label>
                                            <div class="col-sm-5 col-md-5">
                                               <?php echo isset($fetch_pupil['_MainstreamOther'])?$fetch_pupil['_MainstreamOther']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Leisure Services I would like to access: </label>
											<?php
											$leisure_service = explode(',',$fetch_pupil['_LeisureService']);
											?>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_LeisureService'])?$fetch_pupil['_LeisureService']:'-';?>
                                            </div>
                                        </div>
										<div class="form-group" id="ols">
                                            <label for="txtphone" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Other Leisure Service: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_LeisureOther'])?$fetch_pupil['_LeisureOther']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I receive a personal Budget from LBB: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_ReceiveBudgetLBB'])?$fetch_pupil['_ReceiveBudgetLBB']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">I receive Disability Living Allowance: </label>
                                            <div class="col-sm-5 col-md-5">
                                                <?php echo isset($fetch_pupil['_DisabilityAllowance'])?$fetch_pupil['_DisabilityAllowance']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Why I need or want to access SNAP services: </label>
                                            <div class="col-sm-5 col-md-5" style="text-align:justify;">
                                                <?php echo isset($fetch_pupil['_WhyAccessSNAP'])?$fetch_pupil['_WhyAccessSNAP']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Positive things I want to achieve as a result of attending: </label>
                                            <div class="col-sm-5 col-md-5" style="text-align:justify;">
                                               <?php echo isset($fetch_pupil['_WantAchieve'])?$fetch_pupil['_WantAchieve']:'-';?>
                                            </div>
                                        </div>
										<div class="col-sm-12 col-md-12 pad">
                                            <label for="txtemail" class="col-md-offset-1 col-sm-5 col-md-5 control-label rgt">Any other information (referred services): </label>
                                            <div class="col-sm-5 col-md-5" style="text-align:justify;">
                                               <?php echo isset($fetch_pupil['_OtherInfo'])?$fetch_pupil['_OtherInfo']:'-';?>
                                            </div>
                                        </div>
                                </div>
                            </div>
						</div>
            </div>
        </div>        
    </body>
</html>