<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

$username =  $_SESSION['user_data']['_Firstname'].' '.$_SESSION['user_data']['_Lastname'];

$title = $sitename." : Profile";

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a> / Profile</li>        </ul>
            </nav>
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->
			<div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1 class="user_profile_name" id="admin_name"><?php echo $username;?></h1>
                            <p class="user_profile_info">Administrator</p>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-12">
							<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</script> Data Updated Successfully.</strong></div>
							<?php } else if(isset($result) && $result == 'failed') {?> 
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button> <strong>Error!</strong> Error Occurred.</div>
							<?php } ?>
							<div id="response_msg" class=""></div>
                            <form class="form-horizontal" method="post" action="myprofileaction.php" role="form" name="my_profile_frm" id="my_profile_frm">
                                <h3 class="heading_a"><span class="heading_text">General info</span></h3>
								<div class="form-group">
                                    <label for="pro_first_name" class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="pro_first_name" name="pro_first_name" value="<?php echo $_SESSION['user_data']['_Firstname'];?>" required>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="pro_last_name" class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="pro_last_name" name="pro_last_name" value="<?php echo $_SESSION['user_data']['_Lastname'];?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pro_email" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="pro_email" name="pro_email" value="<?php echo $_SESSION['user_data']['_Email'];?>" required>
                                    </div>
                                </div>
								 <div class="form-group">
                                    <label for="pro_username" class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="pro_username" name="pro_username" value="<?php echo $_SESSION['user_data']['_Username'];?>" required>
                                    </div>
                                </div>
								 <div class="form-group">
                                    <label for="pro_current_password" class="col-sm-2 control-label">Current Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="pro_current_password" name="pro_current_password" value="Password" required>
                                    </div>
                                </div>
								 <div class="form-group">
                                    <label for="pro_new_password" class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="pro_new_password" name="pro_new_password" value="">
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <button class="btn-primary btn">Save</button>
                                        <button class="btn-default btn" onclick="window.location='dashboard.php';return false;">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                
				</div>
            </div>
			<!-- main content -->
        </div>
    </body>
</html>
