<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$title = $sitename." : Parent / Carer Bookings";
if(isset($_REQUEST['done'])){
	$done = decrypt($_REQUEST['done'],$encrypt);
}

$clientid = decrypt($_REQUEST['id'],$encrypt);

$selcli = mysqli_query($con,"select concat(_Firstname,' ',_Lastname) clientname from ww_clientmaster where _ID = '".$clientid."'");
$rscli = mysqli_fetch_assoc($selcli);
$clientname = $rscli['clientname'];


    $sel_client = "SELECT bl.* , (time_to_sec(timediff(bl._Totime, bl._Fromtime )) / 3600) Difference , if(tm._Ratephour > 0 , tm._Ratephour , $tutionfee) RPH ,((time_to_sec(timediff(bl._Totime, bl._Fromtime )) / 3600)* if(tm._Ratephour > 0 , tm._Ratephour , ".$tutionfee.") * 10) totalcost ,CONCAT(tm._Firstname , ' ' ,tm._Lastname) Tutorname , CONCAT(pm._Firstname , ' ' ,pm._Lastname) Pupilname , s._Name Subject , l._Title _Level,pm._Email pupilemail FROM ".$tbname."_booked_lessons bl LEFT JOIN ".$tbname."_tutormaster tm on tm._ID = bl._TutorID LEFT JOIN ".$tbname."_pupilmaster pm ON pm._ID = bl._PupilID LEFT JOIN ".$tbname."_subjects s ON s._ID = bl._SubjectID LEFT JOIN ".$tbname."_levels l ON l._ID = bl._LevelID WHERE bl._PupilID IN (SELECT _PupilID FROM ww_clipup_rel WHERE _ClientID = ".$clientid.") AND bl._Type = '1' group by bl._SubjectID,bl._PupilID";
     $rst_client = mysqli_Query($con,$sel_client);
    

//echo $sel_client;exit;


$lnkbread = "<a href='edit-client.php?id=".$_GET['id']."&e_action=".encrypt('edit',$encrypt)."'>".$clientname."</a>";
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
                $(function() {
                    // footable
                    yukon_datatables.p_plugins_tables_datatable();
                })
        </script>
        <style type="text/css">
        .pending{
            color: #f6b738 !important;
            font-weight: bolder;
        }
        .activex{
            color: #87be4a !important;
            font-weight: bolder;
        }
        .inactive{
            color: #d83b4b !important;
            font-weight: bolder;
        }
        </style>


    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allclient.php">Parents / Carers</a></li><li><?php echo $lnkbread; ?></li><li>Bookings</li>
                </ul>
            </nav>

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <?php 
                        if($done == "11"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Resource Lesson Data Updated Successfully.
                            </div>
                    <?php   }
                            if($done == "1"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Lesson Data Not Updated Successfully.
                                    </div>
                            <?php   }
                        ?>   
                 
                                           
                    </div>
                     <!-- <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <a href="generateinvoice.php?id=<?php echo encrypt($clientid,$encrypt); ?>&e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Genrate Invoice</a>
                    </div>
                        
                    </div> --> 
                    <div class="row">
                        <div class="col-md-12">
                        <h3 class="heading_a"><span class="heading_text">Parent / Carer Bookings</span></h3>
                            <table id="datatable_demo" class="table table-bordered table-striped " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Children</th>
                                        <th>Tutor</th>
                                        <th>Subject</th>
                                        <th>Level</th>
                                        <th>Start Date</th>
                                        <th>From </th>
                                        <th>To</th>
                                        <!-- <th>Lesson Type</th> -->
										<th>Action</th>
										<th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                           
                                            if(mysqli_num_rows($rst_client) > 0){                                           
                                            while ($row = mysqli_fetch_assoc($rst_client)){

                                                $chkinv = "Select * from ".$tbname."_invoices where _PupilID = '".$row['_PupilID']."' and _SubjectID = '".$row['_SubjectID']."'";
                                                $rstinv = mysqli_query($con,$chkinv);
                                                if(mysqli_num_rows($rstinv) > 0 ){
                                                    $rowinvs = mysqli_fetch_assoc($rstinv);
                                                    $invoicno = $rowinvs['_ID']; 
                                                }else{

                                                    $insinv = "Insert into ".$tbname."_invoices (_ClientID,_PupilID,_TutorID,_SubjectID,_Rateph,_Total,_Created) VALUES (".$clientid." , ".$row['_PupilID']." , ".$row['_TutorID']." , ".$row['_SubjectID']." , '".$row['RPH']."' , '".$row['totalcost']."' , NOW())";
                                                    $rstinsinv = mysqli_query($con,$insinv);

                                                    $create_log = auditlog($msg = "Genrate Invoice For Pupil ".$row['pupilemail']);

                                                    $invoicno = mysqli_insert_id($con);
                                                }
                                                if($row['_Type'] == '0'){
                                                    $type = "Trial";
                                                }else{
                                                    $type = "Normal";
                                                }
                                           
                                                ?>
                                                <tr >
                                                    <td><?php echo $row['Pupilname']; ?></td>
                                                    <td><?php echo $row['Tutorname']; ?></td>
                                                    <td><?php echo $row['Subject']; ?></td>
                                                    <td><?php echo $row['_Level']; ?></td>
                                                    <td><?php echo date("d M Y", strtotime($row['_Date']));  ?></td>
                                                    <td><?php echo $row['_Fromtime']; ?></td>
                                                    <td><?php echo $row['_Totime']; ?></td>
                                                    <!-- <td><?php echo $type; ?></td> -->
													<!--<td><a href="generateinvoice.php?id=<?php echo encrypt($clientid,$encrypt); ?>&e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Invoice</a> </td>
													-->
													
													<td><a href="generateinvoice.php?id=<?php echo encrypt($invoicno,$encrypt); ?>&e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Invoice</a></td>
													<td><a href="actuallessons.php?id=<?php echo encrypt($row['_SubjectID'],$encrypt); ?>&pid=<?php echo encrypt($row['_PupilID'],$encrypt); ?>" class="btn btn-primary">View</a></td>
                                                </tr>


                                        <?php   }
                                            }
                                        ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main content -->
            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

        
        
        
    </body>
</html>
