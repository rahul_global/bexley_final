<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$title = $_REQUEST['txttitle'];


if($action == "add"){

			

		$str = "INSERT INTO " . $tbname . "_problems (";
     
     	$str .= " _Title) VALUES(";
     
		if ($title != "")
		 	$str = $str . "'" . replaceSpecialChar($title) . "' ";
		else
		  	$str = $str . "null ";
        
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Added New Problem Area - ".replaceSpecialChar($title));
		//log create end
		header("location:problem.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:problem.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_problems SET ";
	    
	    if ($title != "")
			$str = $str . "_Title = '" . replaceSpecialChar($title) . "' ";
		else
	        $str = $str . "_Title = null ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Problem Area - ".replaceSpecialChar($title));
		//log create end
		header("location:problem.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:problem.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){

	//log create start
		$prb_qry  = "SELECT _Title FROM ".$tbname."_problems WHERE _ID = ".$id;
		$run_prb = mysqli_query($con, $prb_qry);
		$fetch_prb = mysqli_fetch_assoc($run_prb);
		$title = $fetch_prb['_Title'];
	//log create end

	$delclient = "delete from ".$tbname."_problems where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Problem Area - ".$title);
		//log create end
		
		header("location:problem.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:problem.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>