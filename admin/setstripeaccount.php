<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$btntext 	= "Add";
$bread 		= "Stripe Account";
$title 		= $sitename." : Stripe Account";
if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}
$query = "SELECT * FROM ".$tbname."_stripe_detail";
$run = mysqli_query($con , $query);
$num = mysqli_num_rows($run);
if($num > 0)
{
	$fetch = mysqli_fetch_assoc($run);
	$btntext = "Update";
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><?php echo $bread; ?></li>
				</ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($result) && $result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                                    <?php } else if(isset($result) && $result == 'failed') {?> 
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button> <strong>Error!</strong> Error Occurred.</div>
									<?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="stripe_frm" id="stripe_frm" method="post" action="stripeaction.php">
                                        <h3 class="heading_a"><span class="heading_text">Stripe Account Info</span></h3>
                                        <div class="form-group">
                                            <label for="txtsecretkey" class="col-sm-2 control-label">Test Secret Key</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtsecretkey" name="txtsecretkey" value="<?php echo isset($fetch['_TestSecretKey']) ? $fetch['_TestSecretKey'] : ''; ?>" PLACEHOLDER="Test Secret Key" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtpublishablekey" class="col-sm-2 control-label">Test Publishable Key</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtpublishablekey" name="txtpublishablekey" value="<?php echo isset($fetch['_TestPublishableKey']) ? $fetch['_TestPublishableKey'] : ''; ?>" PLACEHOLDER="Test Publishable Key" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtlsecretkey" class="col-sm-2 control-label">Live Secret Key</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtlsecretkey" name="txtlsecretkey" value="<?php echo isset($fetch['_LiveSecretKey']) ? $fetch['_LiveSecretKey'] : ''; ?>" PLACEHOLDER="Live Secret Key" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtlpublishablekey" class="col-sm-2 control-label">Live Publishable Key</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtlpublishablekey" name="txtlpublishablekey" value="<?php echo isset($fetch['_LivePublishableKey']) ? $fetch['_LivePublishableKey'] : ''; ?>" PLACEHOLDER="Live Publishable Key" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Api Type</label>
                                            <div class="col-sm-9">
                                                <select id="txttype" name="txttype" class="form-control" required>
													<option value=""> -Select Type- </option>
													<option value="Test" <?php echo (isset($fetch['_Type']) && $fetch['_Type'] == 'Test')?"selected='selected'":'';  ?>>Test</option>
													<option value="Live" <?php echo (isset($fetch['_Type']) && $fetch['_Type'] == 'Live')?"selected='selected'":'';  ?>>Live</option>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle" onclick='window.location ="setstripeaccount.php";return false;'>Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>        
    </body>
</html>
