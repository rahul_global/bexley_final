<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$result 	 = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

$title       = $sitename.": Add Time Slot";
$id          = decrypt($_REQUEST['id'],$encrypt);

$qry         = "SELECT a.* , b._Type FROM ".$tbname."_timeslot a LEFT JOIN ".$tbname."_bookingtype b ON b._ID = a._Bookingtype WHERE a._Bookingtype = ".$id." ORDER BY _Date DESC";
$run         = mysqli_query($con ,$qry);
$num         = mysqli_num_rows($run);

$bread 		 = "Time Slots";
$btntext 	 = 'Update';

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
            <link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
             <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="js/timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css" />
        <script type="text/javascript">
                $(function() {
                         $(".dtpicker").datepicker({ 
                              dayOfWeekStart : 1,
                              format: 'd-m-yyyy',
                              lang:'en',
                              minDate: 0,
                              /*autoclose:true,*/
                              startDate:  new Date(),
							  multidate: true
                        });
                    $('.timepicker').timepicker({ 'timeFormat': 'H:i' }); 
                    //yukon_datatables.p_plugins_tables_datatable();
					$('#datatable_demo').DataTable({"bSort" : false});
                })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="bookingtypes.php">Clubs</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if($result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Timeslot  Added Successfully.</strong></div>
                                    <?php } else if($result == 'failed') { ?>
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error Occurred.</strong></div>
									<?php } else if($result == 'deleted') { ?>
											<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Timeslot Deleted Successfully.</strong></div>
									<?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="timeslotaction.php" method="post">
										<input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>">
                                       <h3 class="heading_a"><span class="heading_text">Add New Time Slot</span></h3>
                                        <?php /* ?><div class="form-group">
                                            <label for="txtbtype" class="col-sm-2 control-label">Booking Type</label>
                                            <div class="col-sm-9">
                                                <select name="txtbtype" id="txtbtype" class="form-control">
                                                      <option value=""> -Select Booking Type- </option>
                                                          <?php 
                                                            $seltype = "Select * from ".$tbname."_bookingtype where _ID in (".$bookingtypes.")";
                                                            $rsttype = mysqli_query($con,$seltype);
                                                            while ($rowtype = mysqli_fetch_assoc($rsttype)) { ?>
                                                              <option value="<?php echo $rowtype['_ID'] ?>"><?php echo $rowtype['_Type'] ?></option>    
                                                    <?php   }
                                                          ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="frequency" style="display: none;">
                                            <label class="control-label col-sm-2" for="txtfreq">Booking Frequency</label>
                                            <div class="col-sm-9">
                                                <select name="txtfreq" id="txtfreq" class="form-control">
                                                  <option value=""> -Select Frequency- </option>
                                                  <option value="0">Daily</option>
                                                  <option value="1">Weekly</option>
                                                  <option value="2">Monthly</option>
                                              </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Select Tutor</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="tutorid" id="txttutors" required>
                                                    <option value=""> -Select Tutor- </option>
                                                        <?php 
                                                                $sqltut = "select * from ".$tbname."_tutormaster where _ID in (".$tutors.") group by _Firstname";
                                                                $rsttut = mysqli_query($con,$sqltut);
                                                                $i = 0;
                                                                while ($rowtut = mysqli_fetch_assoc($rsttut)) { 
                                                                ?>

                                                                    <option value="<?php echo encrypt($rowtut['_ID'],$encrypt); ?>"><?php echo $rowtut['_Firstname']." ".$rowtut['_Lastname']; ?></option>            
                                                        <?php  
                                                        $i++;
                                                        }
                                                        ?>
                                                     </select>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div><?php */ ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="txtdate">Date</label>
                                            <div class="col-sm-9">
                                                <input class="form-control dtpicker" placeholder="Date " type="text" name="txtdate" id="txtdate">
                                                <input type="hidden" name="action" value="<?php echo encrypt('add',$encrypt); ?>" />
                                                <input type="hidden" name="clientid" value="<?php echo encrypt($id,$encrypt) ?>">
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="txtftime">From Time</label>
                                            <div class="col-sm-9">
                                                 <input class="form-control timepicker" placeholder="From Time" type="text" name="txtftime" id="txtftime" required>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="txtttime">To Time</label>
                                            <div class="col-sm-9">
                                                <input class="form-control timepicker" placeholder="To time " type="text" name="txtttime" id="txtttime" required>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="Add Slot" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                        <!-- For Accordian -->
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                             <h3 class="heading_a"><span class="heading_text">Time Slots</span></h3>
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-bordered " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Club</th>
                                        <th>Date</th>
                                        <th>From Time</th>
                                        <th>To time</th>
                                        <!--<th>Status</th>-->
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      <?php 
                                        if($num > 0){
											while($fetch = mysqli_fetch_assoc($run)){ 
                                      ?>
                                      <tr>
                                          <td><?php echo $fetch['_Type']; ?></td>  
                                          <td><?php echo date("d-m-Y",strtotime($fetch['_Date'])); ?></td>
                                          <td><?php echo date("h:i a",strtotime($fetch['_Fromtime'])); ?></td>
                                          <td><?php echo date("h:i a",strtotime($fetch['_Totime'])); ?></td>
                                          <?php /*<td><?php echo ($fetch['_Status'] == "1")?"Booked":"Not Booked"; ?></td><?php */ ?>
                                          <td><a href="timeslotaction.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&bid=<?php echo encrypt($fetch['_Bookingtype'],$encrypt);?>&action=<?php echo encrypt('delete',$encrypt); ?>" onclick="return confirm('Are you sure want to delete this?');">Delete</a></td>
                                      </tr>
										<?php } }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
		<script type="text/javascript">
			$(document).ready(function (){
				$("#btncancle").click(function (){
					window.location = "bookingtypes.php";
					return false;
				});       
			});
		</script>
    </body>
</html>
