<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}


if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

$title = $sitename." : CMS Pages";

$id = decrypt($_REQUEST['id'],$encrypt);
$sql = mysqli_query($con,"SELECT * FROM ".$tbname."_cmspages WHERE _ID = '".$id."'");
$getppage = mysqli_fetch_assoc($sql);


?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<script>
			$(function() {
				// footable
				yukon_datatables.p_plugins_tables_datatable();
			})
			function confirmdel(){

                    var del=confirm("Are you sure you want to delete this item?");
                    if (del==true){
                    //alert ("record deleted")
                    return true;
                    }else{
                    return false;
                    }

            }
        </script>
		
		<style type="text/css">
        .pending{
            color: #f6b738 !important;
			font-weight : bolder;
        }
        .activex{
            color: #87be4a !important;
			font-weight : bolder;
        }
        .inactive{
            color: #d83b4b !important;
			font-weight : bolder;
        }
        </style>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li>CMS Pages</li>        </ul>
            </nav>
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                    	<h3 class="heading_a"><span class="heading_text">Child of <?php echo $getppage['_Title'] ?> PAGE</span></h3>
						<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
						<?php } else if(isset($result) && $result == 'failed') {?> 
								<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button> <strong>Error!</strong> Error Occurred.</div>
						<?php } ?>
						<div class="" style="float:right;margin:15px;">
                             <?php
                                 if(isset($_SESSION['usertype']) && $_SESSION['usertype'] == 'admin')
                                 {
                                    ?>
                                        <a href="edit-2ndlvlcms.php?e_action=<?php echo encrypt('add',$encrypt); ?>&pid=<?php echo $_REQUEST['id']; ?>" class="btn btn-primary">Add 2nd level Page</a>
                                    <?php
                                 }
                            ?>
							
						</div>
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Order</th>
                                        <th>Title</th>
                                        <th>Status</th>
                                         <?php
                                            if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                                            {
                                                ?>
                                                    <th>Manage level 2 page</th>
                                                    <th>View</th>
                                                <?php                                                
                                            }else
                                            {
                                                ?>
                                                    <th>Edit</th>
                                                    <th>Manage level 2 page</th>
                                                    <th>Delete</th>
                                                <?php
                                            }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $query = "select * FROM ".$tbname."_cmspages WHERE _PID = '".decrypt($_REQUEST['id'],$encrypt)."' ";
                                            $run = mysqli_Query($con,$query);
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
													$PID = $fetch['_PID'];
													switch($fetch['_Status']){
														case('Active'):
															$cls = 'activex';
															break;
														case('Inactive') :
															$cls = 'inactive';
															break;
														default :
															$cls = 'inactive';
															break;
													}
												?>
													<tr>
                                                        <td><?php echo $fetch['_Order'] ? $fetch['_Order'] : 0; ?></td>
														<td><?php echo ucwords($fetch['_Title']); ?></td>
														<td class="<?php echo $cls;?>"><?php echo $fetch['_Status']; ?></td>
                                                        <?php
                                                           if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                                                           {
                                                                ?>
                                                                    <td><a href="3ndlvlcms.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('managelevel',$encrypt); ?>" class="btn btn-primary">Manage page level</a></td>
                                                                    <td><a href="edit-2ndlvlcms.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('managelevel',$encrypt); ?>" class="btn btn-primary">View</a></td>
                                                                    
                                                                <?php
                                                           }else
                                                           {
                                                                ?>
                                                                    <td><a href="edit-2ndlvlcms.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&pid=<?php echo encrypt($fetch['_PID'],$encrypt); ?>&e_action=<?php echo encrypt('managelevel',$encrypt); ?>" class="btn btn-primary">Edit</a></td>
                                                                    <td><a href="3ndlvlcms.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('managelevel',$encrypt); ?>" class="btn btn-primary">Manage page level</a></td>
                                                                    <td><a href="2ndlvlcmsaction.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary" onclick="return confirmdel();" >Delete</a></td>    
                                                                <?php
                                                           } 
                                                        ?>
													</tr>
											<?php   
												}
											}
											?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			<!-- main content -->
        </div>

        
        
        
    </body>
</html>
