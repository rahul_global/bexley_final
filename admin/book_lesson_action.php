<?php
//SESSION_START();
include_once('db/dbopen.php');

$clientid 	= decrypt($_POST['clientid'],$encrypt);
$tutorid 	= decrypt($_POST['tutorid'],$encrypt);
$date 		= $_POST['from-input'];
$fromtime 	= $_POST['txtftime'];
$totime 	= $_POST['txtttime'];
$btnsubmit 	= $_POST['btnsubmit'];
$action  	= $_POST['action_type'];

//print_r($_POST);exit;
if($btnsubmit == 'Go')
{
	if($action == 'add')
	{
		$avail_query = "SELECT count(*) _Total FROM `ww_tutoravailibility` WHERE '$date' = _Date AND _TutorID = $tutorid  AND '$fromtime' BETWEEN _Fromtime AND _Totime AND '$totime' BETWEEN _Fromtime AND _Totime";
		$run_avail = mysqli_query($con , $avail_query);
		$fetch_avail = mysqli_fetch_assoc($run_avail);
		if($fetch_avail['_Total'] > 0)
		{
			$time_query = "SELECT count(*) _Total FROM ww_booked_lessons WHERE _TutorID = $tutorid AND '$date' = _Date AND '$fromtime' BETWEEN _Fromtime and _Totime and '$totime' BETWEEN _Fromtime and _Totime AND _Cancel = 0";
			
			//echo $time_query = "SELECT count(*) _Total FROM ww_booked_lessons WHERE _TutorID = $tutorid AND '$date' = _Date AND '$fromtime' > _Fromtime and '$fromtime' < _Totime";
			
			/*
			$time_query_2 = "SELECT count(*) _Total FROM ww_booked_lessons WHERE _TutorID = $tutorid AND '$date' = _Date AND _Fromtime BETWEEN '$fromtime' and '$totime'";
			$run_time_2 = mysqli_query($con , $time_query_2);
			$fetch_time_2 = mysqli_fetch_assoc($run_time_2);
			*/
			$run_time = mysqli_query($con , $time_query);
			$fetch_time = mysqli_fetch_assoc($run_time);
			if($fetch_time['_Total'] > 0)
			{
				header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('already',$encrypt));
				exit;
			}
			else
			{
				
				$query = "INSERT INTO ".$tbname."_booked_lessons(`_ClientID` , `_TutorID` , `_Date` , `_Fromtime` , `_Totime`) VALUES ($clientid , $tutorid , '$date' , '$fromtime' , '$totime')";
				
				if($run = mysqli_query($con , $query))
				{
					$query = "SELECT _ID , _Date , _Fromtime , _Totime FROM ".$tbname."_tutoravailibility WHERE _TutorID = $tutorid";
					$run = mysqli_query($con , $query);
					
					$query1 = "SELECT a._ID , a._Date , a._Fromtime , a._Totime , a._ClientID , CONCAT(b._Firstname,' ',b._Lastname) _Name FROM ".$tbname."_booked_lessons a LEFT JOIN ".$tbname."_pupilmaster b ON b._ClientID = a._ClientID WHERE a._TutorID = $tutorid AND a._Cancel = 0";
					$run1 = mysqli_query($con , $query1);
					
					$query2 = "SELECT a._ID , a._Date , a._Fromtime , a._Totime , a._ClientID , CONCAT(b._Firstname,' ',b._Lastname) _Name FROM ".$tbname."_booked_lessons a LEFT JOIN ".$tbname."_pupilmaster b ON b._ClientID = a._ClientID WHERE a._TutorID = $tutorid AND a._Cancel = 1";
					$run2 = mysqli_query($con , $query2);
					
					$num = mysqli_num_rows($run);
					if($num > 0)
					{
						$count = 1;
							$xml_contents = '<?xml version="1.0"?>
								<monthly>';
						while($fetch = mysqli_fetch_assoc($run))
						{
						
							$xml_contents .= '<event><id>'.$count.'</id>
								<name>Available</name>
								<startdate>'.$fetch['_Date'].'</startdate>
								<enddate>'.$fetch['_Date'].'</enddate>
								<starttime>'.$fetch['_Fromtime'].'</starttime>
								<endtime>'.$fetch['_Totime'].'</endtime>
								<color>#01a078</color>
								<url></url></event>';
						
							$count++;
						}

						while($fetch1 = mysqli_fetch_assoc($run1))
						{
						
							$xml_contents .= '<event><id>'.$count.'</id>
								<name>Booked ['.$fetch1['_Name'].']</name>
								<startdate>'.$fetch1['_Date'].'</startdate>
								<enddate>'.$fetch1['_Date'].'</enddate>
								<starttime>'.$fetch1['_Fromtime'].'</starttime>
								<endtime>'.$fetch1['_Totime'].'</endtime>
								<color>#f44242</color>
								<url></url></event>';
						
							$count++;
						}
						
						while($fetch2 = mysqli_fetch_assoc($run2))
						{
						
							$xml_contents .= '<event><id>'.$count.'</id>
								<name>Booking Cancel ['.$fetch2['_Name'].']</name>
								<startdate>'.$fetch2['_Date'].'</startdate>
								<enddate>'.$fetch2['_Date'].'</enddate>
								<starttime>'.$fetch2['_Fromtime'].'</starttime>
								<endtime>'.$fetch2['_Totime'].'</endtime>
								<color>#ffb128</color>
								<url></url></event>';
						
							$count++;
						}
						
							$xml_contents .= '</monthly>';
								
						if(!is_dir('../xml'))
						{
							mkdir('../xml');
						}
						$a = fopen("../xml/".$tutorid.".xml" , "w+");
						fwrite($a , $xml_contents);
						fclose($a);
					}
					
					header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('success',$encrypt));
					exit;
				}
				else
				{
					header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('failed',$encrypt));
					exit;
				}
			
			}
		}
		else
		{
			header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('undefined',$encrypt));
			exit;
		}
	}
	else if($action == 'cancel')
	{
		$avail_query = "SELECT count(*) _Total FROM ".$tbname."_booked_lessons WHERE `_ClientID` = $clientid AND `_TutorID` = $tutorid AND `_Date` = '$date'  AND `_Fromtime` = '$fromtime' AND `_Totime` = '$totime' AND _Cancel = 0";
		
		$run_avail = mysqli_query($con , $avail_query);
		$fetch_avail = mysqli_fetch_assoc($run_avail);
		if($fetch_avail['_Total'] > 0)
		{
				$query = "UPDATE ".$tbname."_booked_lessons SET _Cancel = 1 WHERE `_ClientID` = $clientid AND `_TutorID` = $tutorid AND `_Date` = '$date'  AND `_Fromtime` = '$fromtime' AND `_Totime` = '$totime'";
				
				if($run = mysqli_query($con , $query))
				{
					$query = "SELECT _ID , _Date , _Fromtime , _Totime FROM ".$tbname."_tutoravailibility WHERE _TutorID = $tutorid";
					$run = mysqli_query($con , $query);
					
					$query1 = "SELECT a._ID , a._Date , a._Fromtime , a._Totime , a._ClientID , CONCAT(b._Firstname,' ',b._Lastname) _Name FROM ".$tbname."_booked_lessons a LEFT JOIN ".$tbname."_pupilmaster b ON b._ClientID = a._ClientID WHERE a._TutorID = $tutorid AND a._Cancel = 0";
					$run1 = mysqli_query($con , $query1);
					
					$query2 = "SELECT a._ID , a._Date , a._Fromtime , a._Totime , a._ClientID , CONCAT(b._Firstname,' ',b._Lastname) _Name FROM ".$tbname."_booked_lessons a LEFT JOIN ".$tbname."_pupilmaster b ON b._ClientID = a._ClientID WHERE a._TutorID = $tutorid AND a._Cancel = 1";
					$run2 = mysqli_query($con , $query2);
					
					$num = mysqli_num_rows($run);
					if($num > 0)
					{
						$count = 1;
							$xml_contents = '<?xml version="1.0"?>
								<monthly>';
						while($fetch = mysqli_fetch_assoc($run))
						{
						
							$xml_contents .= '<event><id>'.$count.'</id>
								<name>Available</name>
								<startdate>'.$fetch['_Date'].'</startdate>
								<enddate>'.$fetch['_Date'].'</enddate>
								<starttime>'.$fetch['_Fromtime'].'</starttime>
								<endtime>'.$fetch['_Totime'].'</endtime>
								<color>#01a078</color>
								<url></url></event>';
						
							$count++;
						}

						while($fetch1 = mysqli_fetch_assoc($run1))
						{
						
							$xml_contents .= '<event><id>'.$count.'</id>
								<name>Booked ['.$fetch1['_Name'].']</name>
								<startdate>'.$fetch1['_Date'].'</startdate>
								<enddate>'.$fetch1['_Date'].'</enddate>
								<starttime>'.$fetch1['_Fromtime'].'</starttime>
								<endtime>'.$fetch1['_Totime'].'</endtime>
								<color>#f44242</color>
								<url></url></event>';
						
							$count++;
						}
						
						while($fetch2 = mysqli_fetch_assoc($run2))
						{
						
							$xml_contents .= '<event><id>'.$count.'</id>
								<name>Booking Cancel ['.$fetch2['_Name'].']</name>
								<startdate>'.$fetch2['_Date'].'</startdate>
								<enddate>'.$fetch2['_Date'].'</enddate>
								<starttime>'.$fetch2['_Fromtime'].'</starttime>
								<endtime>'.$fetch2['_Totime'].'</endtime>
								<color>#ffb128</color>
								<url></url></event>';
						
							$count++;
						}
						
							$xml_contents .= '</monthly>';
								
						if(!is_dir('../xml'))
						{
							mkdir('../xml');
						}
						$a = fopen("../xml/".$tutorid.".xml" , "w+");
						fwrite($a , $xml_contents);
						fclose($a);
					}
					
					header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('success',$encrypt));
					exit;
				}
				else
				{
					header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('failed',$encrypt));
					exit;
				}
		}
		else
		{
			header("location:book_lesson.php?cid=".encrypt($clientid,$encrypt)."&tid=".encrypt($tutorid,$encrypt)."&result=".encrypt('undefined',$encrypt));
			exit;
		}
	}
}
?>