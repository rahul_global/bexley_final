<?php 
include_once('db/dbopen.php');
include 'classresizeimage.php';
/*echo "<pre>";
print_r($_REQUEST);exit;*/
session_start();
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action 		= decrypt($_REQUEST['e_action'],$encrypt);
$id 			= decrypt($_REQUEST['id'],$encrypt);
$orgtype 		= $_REQUEST['type'];
$Adminresoucepath = "uploads/blogs/";


if($action == "add"){
		$type 			= $_REQUEST['txttype'];
		$title 			= $_REQUEST['txttitle'];
		$description 	= $_REQUEST['wysiwg_editor'];
		$price 			= $_REQUEST['txtprice'];
		
		if ($_FILES["txtimg"]["size"] > 0) {
                if ($_FILES["txtimg"]["error"] > 0)
                    echo "Return Code: " . $_FILES["txtimg"]["error"] . "<br />";
                else {
                    $splitfilename1 = strtolower($_FILES["txtimg"]["name"]);
                    $exts1 = explode(".", $splitfilename1);
                    $n1 = count($exts1) - 1;
                    $exts1 = $exts1[$n1];
                    $datetime1 = date("YmdHis") . genRandomString(4);
                    $Thumbnail1 = "";
                    $Thumbnail1 = $datetime1 . "." . $exts1;

                    if (file_exists($AdminProductPicPath1 . $Thumbnail1))
                        echo " already exists. ";
                    else {
//Thumbnail
                        $myfile1 = new UploadedImage;
                        $myfile1->file = $_FILES["txtimg"];
                        $myfile1->maxwidth = 335;
                        $myfile1->maxheight = 200;
                        $myfile1->relscale = false;
                        $myfile1->newfile = $datetime1;
                        $myfile1->maxsize = 10485760000;
                        $myfile1->pictype = "Thumbnail";
                        $myfile1->transfer = "Copy";
                        $myfile1->uploaddir = $Adminresoucepath;
                        if (!$myfile1->upload_file()) {
                            echo $myfile1->ErrorMsg;
                            exit();
                        }
                        move_uploaded_file($_FILES["txtimg"]["tmp_name"],$Adminresoucepath . $Thumbnail1);
                    }
                }
            } else {
                $Thumbnail1 = $_REQUEST['txtimg'];                
            }



		$str = "INSERT INTO " . $tbname . "_blogs (";
     
     	$str .= " _Type,_Title,_Image,_Description) VALUES(";
     
        if ($type != "")
		 	$str = $str . "" . $type . ", ";
		else
		  	$str = $str . "null, ";

		if ($title != "")
		 	$str = $str . "'" . replaceSpecialChar($title) . "', ";
		else
		  	$str = $str . "null, ";

		if ($Thumbnail1 != "")
		 	$str = $str . "'" . $Thumbnail1 . "', ";
		else
		  	$str = $str . "null, ";

		if ($description != "")
		 	$str = $str . "'" . replaceSpecialChar($description) . "'";
		else
		  	$str = $str . "null";
        
        $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$btype_qry = "SELECT _Type FROM ".$tbname."_blogtype WHERE _ID = ".$type;
			$run_btype = mysqli_query($con , $btype_qry);
			$fetch_btype = mysqli_fetch_assoc($run_btype);
			$blogtype = $fetch_btype['_Type'];
			
			$create_log = auditlog($msg = "Added New Blog ".replaceSpecialChar($title)." From Blog Type ".$blogtype);
		//log create end

		header("location:blog.php?done=".encrypt("00",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;
	}else{
		header("location:blog.php?done=".encrypt("01",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;	
	}

}
if($action == "edit"){
	$type 			= $_REQUEST['txttype'];
	$title 			= $_REQUEST['txttitle'];
	$description 	= $_REQUEST['wysiwg_editor'];
	$price 			= $_REQUEST['txtprice'];
		if ($_FILES["txtimg"]["size"] > 0) {
                if ($_FILES["txtimg"]["error"] > 0)
                    echo "Return Code: " . $_FILES["txtimg"]["error"] . "<br />";
                else {
                    $splitfilename1 = strtolower($_FILES["txtimg"]["name"]);
                    $ytg = explode(".", $splitfilename1);
                    $n1 = count($exts1) - 1;
                    $exts1 = $exts1[$n1];
                    $datetime1 = date("YmdHis") . genRandomString(4);
                    $Thumbnail1 = "";
                    $Thumbnail1 = $datetime1 . "." . $exts1;

                    if (file_exists($AdminProductPicPath1 . $Thumbnail1))
                        echo " already exists. ";
                    else  {
//Thumbnail
                        $myfile1 = new UploadedImage;
                        $myfile1->file = $_FILES["txtimg"];
                        $myfile1->maxwidth = 335;
                        $myfile1->maxheight = 200;
                        $myfile1->relscale = false;
                        $myfile1->newfile = $datetime1;
                        $myfile1->maxsize = 10485760000;
                        $myfile1->pictype = "Thumbnail";
                        $myfile1->transfer = "Copy";
                        $myfile1->uploaddir = $Adminresoucepath;
                        if (!$myfile1->upload_file()) {
                            echo $myfile1->ErrorMsg;
                            exit();
                        }
                        move_uploaded_file($_FILES["txtimg"]["tmp_name"],$Adminresoucepath . $Thumbnail1);
                    }
                }
            } else {
                $Thumbnail1 = $_REQUEST['txtimg'];                
            }




		$str = "UPDATE " . $tbname . "_blogs SET ";
	    
	    if ($type != "")
			$str = $str . "_Type = '" . replaceSpecialChar($type) . "', ";
		else
	        $str = $str . "_Type = null, ";

	    if ($title != "")
			$str = $str . "_Title = '" . replaceSpecialChar($title) . "', ";
		else
	        $str = $str . "_Title = null, ";

	    if ($Thumbnail1 != "")
			$str = $str . "_Image = '" . replaceSpecialChar($Thumbnail1) . "', ";
		else
	        $str = $str . "_Image = null, ";

	    if ($description != "")
			$str = $str . "_Description = '" . replaceSpecialChar($description) . "'";
		else
	        $str = $str . "_Description = null";

	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$btype_qry = "SELECT _Type FROM ".$tbname."_blogtype WHERE _ID = ".$type;
			$run_btype = mysqli_query($con , $btype_qry);
			$fetch_btype = mysqli_fetch_assoc($run_btype);
			$blogtype = $fetch_btype['_Type'];
			
			$create_log = auditlog($msg = "Updated Blog ".replaceSpecialChar($title)." From Blog Type ".$blogtype);
		//log create end
		header("location:blog.php?done=".encrypt("11",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;
	}else{
		header("location:blog.php?done=".encrypt("1",$encrypt)."&type=".encrypt($type,$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){
	//log create start
		$blog_qry = "SELECT * FROM ".$tbname."_blogs WHERE _ID = ".$id;
		$run_blog = mysqli_query($con , $blog_qry);
		$fetch_blog = mysqli_fetch_assoc($run_blog);
		$type = $fetch_blog['_Type'];
		$title = $fetch_blog['_Title'];
		
		$btype_qry = "SELECT _Type FROM ".$tbname."_blogtype WHERE _ID = ".$type;
		$run_btype = mysqli_query($con , $btype_qry);
		$fetch_btype = mysqli_fetch_assoc($run_btype);
		$blogtype = $fetch_btype['_Type'];

	//log create end

	$selimg = "SELECT _Image FROM ".$tbname."_blogs WHERE _ID='".$id."'";
	$rowimg = mysqli_fetch_assoc(mysqli_query($con,$selimg));
	$background = $rowimg['_Image'];

	
	$delclient = "DELETE FROM ".$tbname."_blogs WHERE _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel && $background != ''){
		if (file_exists($Adminresoucepath . $background)) {
			unlink($Adminresoucepath . $background);
		}
		//log create start
		$create_log = auditlog($msg = "Deleted Blog ".$title." From Blog Type ".$blogtype);
		//log create end
		header("location:blog.php?done=".encrypt("22",$encrypt)."&type=".$orgtype);
		exit;
	}else{
		header("location:blog.php?done=".encrypt("2",$encrypt)."&type=".$orgtype);
		exit;	
	}

}
if($action == "deletepic"){

	//echo $orgtype;exit;
	$selimg = "SELECT _Image FROM ".$tbname."_blogs WHERE _ID='".$id."'";
	$rowimg = mysqli_fetch_assoc(mysqli_query($con,$selimg));
	$background = $rowimg['_Image'];

	
	if (file_exists($Adminresoucepath . $background)) {
	    unlink($Adminresoucepath . $background);
		$delclient = "UPDATE ".$tbname."_blogs SET _Image = '' WHERE _ID = '".$id."'";
		$rstdel = mysqli_query($con,$delclient);
	}


	if($rstdel){
		
		header("location:edit-blog.php?type=".$orgtype."&id=".encrypt($id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
		exit;
	}else{
		header("location:edit-blog.php?type=".$orgtype."&id=".encrypt($id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
		exit;	
	}

}
?>