<?php
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

	$title = $sitename." : Booking Schedule";
	$bread = "Booking Schedule";
	$lnkbread = "Booking Schedule";
	$result = decrypt($_REQUEST['result'],$encrypt);
?>

<!DOCTYPE html>
<html>
	<head>
	<?php include 'topscript.php'; ?>
            <!-- date picker -->
	  		<link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
             <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/timepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="css/timepicker.css" />
		<script>
                $(function() {
                    // footable
                    yukon_datatables.p_plugins_tables_datatable();
                })
            function confirmdel(){

                var del=confirm("Are you sure you want to delete this item?");
                if (del==true){
                   //alert ("record deleted")
                   return true;
                }else{
                    return false;
                }
                
            }
        </script>
	</head>
	<body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Home</a></li><li><?php echo $lnkbread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
							<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
							<?php }else if(isset($result) && $result == 'failed'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
							<?php }else if(isset($result) && $result == 'alreadytrial'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Schedule Already Set.</div>
							<?php
								}else if(isset($result) && $result == 'fillall'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Please Select Proper Dates.</div>
							<?php
								}
							?>
                                    <div id="response_msg" class=""></div>
                                   <form class="form-horizontal" role="form" name="book_actual" id="book_actual" action="bookingscheduleaction.php" method="post">
										
                                        <h3 class="heading_a"><span class="heading_text">Booking Schedule</span></h3>
                                        
										
                                        
                                    <div class="row">
                                    	<div id="trial" class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                            
                                                
                                                 <div class="form-group">
                                                    <label for="txtname" class="col-sm-3 control-label">Name</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="txtname" name="txtname" required>
                                                    </div>
                                                </div>                                        
                                        </div>
										<div id="trial" class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
											
												
												 <div class="form-group">
		                                            <label for="form-input" class="col-sm-3 control-label">From Date</label>
		                                            <div class="col-sm-9">
		                                                <input type="text" class="form-control" id="dp_basic" name="txtfdate" required>
		                                            </div>
		                                        </div>                                        
										</div>
										<div id="actual" class="col-md-4 col-lg-4 col-sm-12 col-xs-12" >
											
												<div class="form-group">
		                                            <label for="form-input" class="col-sm-3 control-label">To Date</label>
		                                            <div class="col-sm-9">
		                                                <input type="text" class="form-control" id="dp_basic1" name="txttdate" required>
		                                            </div>
		                                        </div>                                        
										</div>
																				
										<div class="form-group text-center" id="btngroup" >
                                            <div class="col-sm-10">
                                                <input type="Submit" class="btn-primary btn" value="Add New Schedule" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
										
                                    </div>
								</form>
                                </div>
                            </div>

                            <div class="row">
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-bordered " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $sel_client = "select * from ".$tbname."_booking_schedule";
                                            $rst_client = mysqli_Query($con,$sel_client);
                                            while ($row_client = mysqli_fetch_assoc($rst_client)){
                                                
                                                ?>
                                                <tr>
                                                    <td><?php echo $row_client['_Name']; ?></td>
                                                    <td><?php echo date("d-m-Y",strtotime($row_client['_Fromdate'])); ?></td>
                                                    <td><?php echo date("d-m-Y",strtotime($row_client['_Todate'])); ?></td>
                                                    <!-- <td><a href="edit-client.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>" class="btn btn-primary">Edit</a></td>
                                                    <td><a href="clientaction.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary" onclick="return confirmdel();">Delete</a></td> -->
                                                </tr>


                                        <?php   }

                                        ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "booking_schedule.php";
            return false;
        });
    });
</script> 
<script type="text/javascript">
		$(function() {
			var date = Date();
			$('.time').timepicker({ 'timeFormat': 'H:i' });
			$('#dp_basic').datepicker({startDate: date, format: 'dd/mm/yyyy' });
			$('#dp_basic1').datepicker({startDate: date, format: 'dd/mm/yyyy' });
		})
</script>
    </body>
</html>