<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$btntext 	= "Add";
$bread 		= "Add Blog Type";
$title 		= $sitename." : Add Blog Type";
$tutor_subject = array();

if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

if(isset($_GET['e_action']))
{
	$action 	= decrypt($_GET['e_action'],$encrypt);

	if($action == 'edit')
	{
		$title 		= $sitename." : Edit Blog Type";
		$id 		= decrypt($_GET['id'],$encrypt);
		$query	 	= "select _ID, _Type , _Status FROM ".$tbname."_blogtype where _ID='".$id."'";
		$run 		= mysqli_query($con,$query);
		$num	 	= mysqli_num_rows($run);
		if($num >	0)
		{
			$fetch	 = mysqli_fetch_assoc($run);
			
			$bread 		= "Edit Blog Type";
			$btntext 	= 'Update';
		}
		else
		{
			header("location:allblogtypes.php");
			exit;
		}
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">Blog Type</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($result) && $result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                                    <?php } else if(isset($result) && $result == 'failed') {?> 
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
									<?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" method="post" action="blogtypeaction.php" name="edit_btype_frm" id="edit_btype_frm">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
										
										<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
										<input type="hidden" name="blogtype_id" id="blogtype_id" value="<?php echo isset($_GET['id'])?$_GET['id']:''; ?>">
                                        <div class="form-group">
                                            <label for="txtbtype" class="col-sm-2 control-label">Blog Type</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="txtbtype" name="txtbtype" value="<?php echo isset($fetch['_Type'])?$fetch['_Type']:'';?>" PLACEHOLDER="TYPE" required>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="txtbtype" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="txtstatus" name="txtstatus" required>
													<option value="">Select Status</option>
													<option value="Active" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == 'Active')?"selected='Selected'":'';?>>Active</option>
													<option value="Inactive" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == 'Inactive')?"selected='Selected'":'';?>>Inactive</option>
												</select>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle" onclick='window.location = "allblogtypes.php";return false;'>Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>        
    </body>
</html>
