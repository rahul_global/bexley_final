<?php 
require_once("db/dbopen.php");
$create_log = auditlog($msg = "Logged Out");

unset($_SESSION['userid']);
unset($_SESSION['username']);
unset($_SESSION['user_data']);
//session_destroy();

header("location:login.php");
exit;
?>