<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$subid = array();
$levid = array();
		$title 		= $sitename." : View Tutor";
		$id 		= decrypt($_GET['id'],$encrypt);
		$tutor_qry 	= "select _ID , _Firstname , _Lastname , _Email , _Phone , _Status,_Experience FROM ".$tbname."_tutormaster where _ID='".$id."'";
		
		$run_tutor 	= mysqli_query($con,$tutor_qry);
		$num_tutor 	= mysqli_num_rows($run_tutor);

		$selsub = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$id."'";
		$rstsub = mysqli_query($con,$selsub);
		if(mysqli_num_rows($rstsub) > 0){
			
			while($rssub = mysqli_fetch_assoc($rstsub)){
				array_push($subid, $rssub['_SubjectID']);
			}  
			
		}
		$sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$id."'";
		$rstlev = mysqli_query($con,$sellev);
		if(mysqli_num_rows($rstlev) > 0){
			
			while($rssub = mysqli_fetch_assoc($rstlev)){
				array_push($levid, $rssub['_LevelID']);
			}  
			
		}
$fetch_tutor = mysqli_fetch_assoc($run_tutor);

$lnkbread = "View Tutor";
	
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">Dashboard</a></li><li><?php echo $lnkbread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($result) && $result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                                    <?php } else if(isset($result) && $result == 'failed') {?> 
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
									<?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" method="post"  name="edit_tutor_frm" id="edit_tutor_frm" action="viewtutaction.php">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
										
										<input type="hidden" name="txtaction" id="txtaction" value="<?php echo $action;?>">
										<input type="hidden" name="tutor_id" id="tutor_id" value="<?php echo isset($_GET['id'])?$_GET['id']:''; ?>">
										
                                        <div class="form-group">
                                            <label for="txtfname" class="col-sm-2 control-label">Tutor Name</label>
                                            <div class="col-sm-9">
                                               <?php echo $fetch_tutor['_Firstname']." ".$fetch_tutor['_Lastname']; ?>
                                            </div>
										</div>
                                         
                                        <div class="form-group">
                                            <label for="txtphone" class="col-sm-2 control-label">Phone</label>
                                            <div class="col-sm-9">
                                                <?php echo ($fetch_tutor['_Phone'])?$fetch_tutor['_Phone']:"-";?>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtemail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <?php echo ($fetch_tutor['_Email'])?$fetch_tutor['_Email']:'-';?>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										
										<div class="form-group">
                                            <label for="txtsubject" class="col-sm-2 control-label">Subject</label>
                                            <div class="col-sm-9">
                                            	<?php 

                                                                        $selsubid = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$fetch_tutor['_ID']."' ";
                                                                        $rstselsub = mysqli_query($con,$selsubid);
                                                                        $subjects = array();
                                                                        while ($rowsubje = mysqli_fetch_assoc($rstselsub)) {
                                                                                array_push($subjects,$rowsubje['_SubjectID']);
                                                                        }
                                                                        $newsub = implode(",", $subjects);

                                                                        if(isset($newsub) && $newsub != ""){
                                                                        $selsub = "Select _Name from ".$tbname."_subjects where _ID in (".$newsub.")" ;
                                                                        $rstsub = mysqli_Query($con,$selsub);
                                                                        $sub = array();
                                                                        while($rowsub = mysqli_fetch_assoc($rstsub)){
                                                                        //echo $rowsub['_Name'].",";
                                                                        array_push($sub, $rowsub['_Name']);
                                                                        
                                                                            
                                                                        }
                                                                            $sub = implode(",", $sub);
                                                                            if($sub != ''){
                                                                                echo $sub;
                                                                            }else{
                                                                                echo "-";
                                                                            }
                                                                        }

                                                                ?>
                                            </div>
											
                                        </div>
										<div class="form-group">
											<label for="txtlevel" class="col-sm-2 control-label">Levels&nbsp;</label>
											<div class="col-sm-9">
												<?php


                                                                $sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$fetch_tutor['_ID']."'";
                                                                $rstlev = mysqli_query($con,$sellev);
                                                                $levid = array();
                                                                if(mysqli_num_rows($rstlev) > 0){
                                                                    
                                                                    while($rssub = mysqli_fetch_assoc($rstlev)){
                                                                        array_push($levid, $rssub['_LevelID']);
                                                                    }  
                                                                    
                                                                }
                                                                $newlevel = implode(",", $levid);

                                                                if(isset($newlevel) && $newlevel != ""){
                                                                $sellev = "Select _Title from ".$tbname."_levels where _ID in (".$newlevel.")" ;
                                                                $rstlev = mysqli_Query($con,$sellev);
                                                                $lev = array();
                                                                while($rowlev = mysqli_fetch_assoc($rstlev)){
                                                                //echo $rowlev['_Name'].",";
                                                                array_push($lev, $rowlev['_Title']);
                                                                
                                                                    
                                                                }
                                                                    $lev = implode(",", $lev);
                                                                    if($lev != ''){
                                                                        echo $lev;
                                                                    }else{
                                                                        echo "-";
                                                                    }
                                                                }


                                                    ?>
											</div>
											
										</div>
										
										<div class="form-group">
											<label for="txtexp" class="col-sm-2 control-label">Experience&nbsp;</label>
											<div class="col-sm-9">
												<?php echo ($fetch_tutor['_Experience'])?$fetch_tutor['_Experience']:"-"; ?>
											</div>
											
										</div>
										
										
											<div class="form-group">
												<label for="txtstatus" class="col-sm-2 control-label">Status</label>
												<div class="col-sm-9">
													 <select class="form-control" name="txtstatus" id="txtstatus" >
														<option value="0" <?php echo ($fetch_tutor['_Status'] == '0')?"selected='selected'":''; ?>>Pending</option>
														<option value="1" <?php echo ($fetch_tutor['_Status'] == '1')?"selected='selected'":''; ?>>Active</option>
														<option value="2" <?php echo ($fetch_tutor['_Status'] == '2')?"selected='selected'":''; ?>>Reject</option>
													 </select>
												</div>
												<div class="col-sm-1">
													<span class="required_field"></span>
												</div>
											</div>
									
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
											<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('change',$encrypt); ?>">
                                                <input type="submit" class="btn-primary btn" value="Submit" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle" onclick="window.location = 'dashboard.php';return false;">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
    </body>
</html>
