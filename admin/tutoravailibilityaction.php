<?php
session_start();
include_once('db/dbopen.php');
	
$avail_date		= explode(', ',$_POST['from-input']);
$dates = implode("','", $avail_date);

$from_time		= $_POST['txtftime'];
$to_time		= $_POST['txtttime'];
$action 		= $_POST['action_type'];
$id 			= decrypt($_POST['id'],$encrypt);

$size = sizeof($avail_date);
$success = 0;
if($action == 'add')
{
	$query = "SELECT count(*) _Total FROM `ww_tutoravailibility` WHERE _TutorID = ".$id." AND _Date IN ('".$dates."') AND (('$from_time' BETWEEN _Fromtime AND  _Totime) OR ('$to_time' BETWEEN _Fromtime AND _Totime))";
	
	$run = mysqli_query($con , $query);
	$fetch = mysqli_fetch_assoc($run);
	if($fetch['_Total'] > 0)
	{
		header("location:tutor_availibility.php?id=".encrypt($id,$encrypt)."&result=".encrypt("overlap",$encrypt));
		exit;
	}
	else
	{
		foreach($avail_date as $date)
		{
			$query = "INSERT INTO ".$tbname."_tutoravailibility(`_TutorID`,`_Date`,`_Fromtime`,`_Totime`) VALUES (".$id.",'$date','$from_time','$to_time')";
			if($run = mysqli_query($con , $query))
			{
				$success = $success + 1;
			}
		}

		if($size == $success)
		{
			$query = "SELECT _ID , _Date , _Fromtime , _Totime FROM ".$tbname."_tutoravailibility WHERE _TutorID = ".$id;
			$run = mysqli_query($con , $query);
			$num = mysqli_num_rows($run);
				if($num > 0)
				{
					$count = 1;
					$xml_contents = '<?xml version="1.0"?>
							<monthly>';
					while($fetch = mysqli_fetch_assoc($run))
					{
						
						$xml_contents .= '<event><id>'.$count.'</id>
							<name>Available</name>
							<startdate>'.$fetch['_Date'].'</startdate>
							<enddate>'.$fetch['_Date'].'</enddate>
							<starttime>'.$fetch['_Fromtime'].'</starttime>
							<endtime>'.$fetch['_Totime'].'</endtime>
							<color>#ffb128</color>
							<url></url></event>';
							
						$count++;
					}	
					$xml_contents .= '</monthly>';
					if(!is_dir('../xml'))
					{
						mkdir('../xml');
					}
					$a = fopen("../xml/".$id.".xml" , "w+");
					fwrite($a , $xml_contents);
					fclose($a);
				}
				
				//log create start
					$log_query = "SELECT _Email FROM ".$tbname."_tutormaster WHERE _ID = ".$id;
					$run_log = mysqli_query($con , $log_query);
					$fetch_log = mysqli_fetch_assoc($run_log);
					$tutoremail = $fetch_log['_Email'];
					$create_log = auditlog($msg = "Set Availablity of Staff ".$tutoremail);
				//log create end
				header("location:tutor_availibility.php?id=".encrypt($id,$encrypt)."&result=".encrypt("success",$encrypt));
				exit;
		}
		else if($size > $success && $success > 0)
		{
			header("location:tutor_availibility.php?id=".encrypt($id,$encrypt)."&result=".encrypt("error",$encrypt));
			exit;
		}
		else
		{
			header("location:tutor_availibility.php?id=".encrypt($id,$encrypt)."&result=".encrypt("failed",$encrypt));
			exit;
		}
	}
}
else if($action == 'delete')
{
	
	foreach($avail_date as $date)
	{
		//log create start
		$qry_isfind = "SELECT _ID FROM ".$tbname."_tutoravailibility WHERE _TutorID = ".$id." AND _Date = '$date' AND _Fromtime = '$from_time' AND _Totime = '$to_time'";
		$run_isfind = mysqli_query($con , $qry_isfind);
		$is_find = mysqli_num_rows($run_isfind);
		//log create end
		
		$query = "DELETE FROM ".$tbname."_tutoravailibility WHERE _TutorID = ".$id." AND _Date = '$date' AND _Fromtime = '$from_time' AND _Totime = '$to_time'";
		
		if($run = mysqli_query($con , $query))
		{
			//log create start
			$qry_isfind1 = "SELECT _ID FROM ".$tbname."_tutoravailibility WHERE _TutorID = ".$id." AND _Date = '$date' AND _Fromtime = '$from_time' AND _Totime = '$to_time'";
			$run_isfind1 = mysqli_query($con , $qry_isfind1);
			$is_find1 = mysqli_num_rows($run_isfind1);
			if($is_find == 1 && $is_find1 == 0)
			{
				$log_tutor = "SELECT _Email FROM ".$tbname."_tutormaster WHERE _ID = ".$id;
				$run_log_tutor = mysqli_query($con , $log_tutor);
				$fetch_log_tutor = mysqli_fetch_assoc($run_log_tutor);
				$t_email = $fetch_log_tutor['_Email'];
				$create_log = auditlog($msg = "Deleted Availablity of Staff ".$t_email);
			}
			//log create end
			
			$success++;
		}
		else
		{
			header("location:tutor_availibility.php?id=".encrypt($id,$encrypt)."&result=".encrypt("failed",$encrypt));
			exit;
		}			
	}
	$query = "SELECT _ID , _Date , _Fromtime , _Totime FROM ".$tbname."_tutoravailibility WHERE _TutorID = ".$id;
	
	$run = mysqli_query($con , $query);
	$num = mysqli_num_rows($run);
	if($num > 0)
	{
		$count = 1;
		$xml_contents = '<?xml version="1.0"?>
				<monthly>';
		while($fetch = mysqli_fetch_assoc($run))
		{
			
			$xml_contents .= '<event><id>'.$count.'</id>
				<name>Available</name>
				<startdate>'.$fetch['_Date'].'</startdate>
				<enddate>'.$fetch['_Date'].'</enddate>
				<starttime>'.$fetch['_Fromtime'].'</starttime>
				<endtime>'.$fetch['_Totime'].'</endtime>
				<color>#ffb128</color>
				<url></url></event>';
				
			$count++;
		}	
		$xml_contents .= '</monthly>';
		if(!is_dir('../xml'))
		{
			mkdir('../xml');
		}
		$a = fopen("../xml/".$id.".xml" , "w+");
		fwrite($a , $xml_contents);
		fclose($a);
	}
	else
	{
		$xml_contents .= '';
		if(!is_dir('../xml'))
		{
			mkdir('../xml');
		}
		$a = fopen("../xml/".$id.".xml" , "w+");
		fwrite($a , $xml_contents);
		fclose($a);
	}
	header("location:tutor_availibility.php?id=".encrypt($id,$encrypt)."&result=".encrypt("success",$encrypt));
	exit;
}
?>