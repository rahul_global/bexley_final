<?php 
include_once('db/dbopen.php');
$days = array( 0=> 'Monday', 1=> 'Tuesday', 2=> 'Wednesday', 3=> 'Thursday', 4=> 'Friday', 5=> 'Saturday', 6=> 'Sunday' );
$times = array(0=>'Morning' , 1=>'Afternoon' , 2=>'Evening');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$pupil = array();
$sel_pupil = "Select * FROM ".$tbname."_pupilmaster";
$run_pupil = mysqli_query($con , $sel_pupil);

if($action == 'edit'){

$title = $sitename." : Edit Family";
$id = decrypt($_REQUEST['id'],$encrypt);

$get_pupil = "SELECT * FROM ".$tbname."_clipup_rel WHERE _ClientID = ".$id;
$run_get_pupil = mysqli_query($con , $get_pupil);
while($fetch_get_pupil = mysqli_fetch_assoc($run_get_pupil))
{
	$pupil[] = $fetch_get_pupil['_PupilID'];
}

//print_r($pupil);exit;
$sel_clint = "select * from ".$tbname."_clientmaster where _ID='".$id."'";
$rst_clint = mysqli_query($con,$sel_clint);
$row = mysqli_fetch_assoc($rst_clint);

$fname 		= $row['_Firstname'];
$lname 		= $row['_Lastname'];
$email 		= $row['_Email'];
$phone 		= $row['_Phone'];
$password 	= decrypt($row['_Password'],$encrypt);
$address1 	= $row['_Address1'];
$address2 	= $row['_Address2'];
$city 		= $row['_City'];
$state 		= $row['_State'];
$country 	= $row['_Country'];
$postcode 	= $row['_Postcode'];
$status 	= $row['_Status'];
$alltutor 	= $row['_Assigntutor'];
$tutorid 	= $row['_TutorID'];
$tutor 		= explode(",", $alltutor);
$subject 	= explode(",", $row['_Subjectareas']);


$pupilfirstname = $row['_Pfirstname'];
$pupillastname 	= $row['_Plastname'];
$pupilemail		= $row['_Pemail'];
$academicyear 	= $row['_Acedmicyear'];
$cur_school 	= $row['_Currschool'];
$new_school 	= $row['_Newschool'];
$comment 		= $row['_Comments'];
$problem 		= $row['_Problem'];
$preferred_days = $row['_Predays'] != '' ? explode(',',$row['_Predays']) : '';
$preferred_time = $row['_Pretime'] != '' ? explode(',',$row['_Pretime']) : '';
$contact_method = $row['_Contactmethod'];
//print_r($preferred_days);exit;
$bread 		= "Edit Family";
$btntext 	= 'Update';

}else{
    $title 		= $sitename." : Add Family";
    $bread 		= "Add Family";
    $btntext 	= 'Insert';
    $tutor 		= array();
    $subject 	= array();
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allclient.php">All Families</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="clientaction.php" method="post">

                                         <?php 
                                        if($action == 'edit')
                                        {
                                        ?>
                                            <div class="form-group">
                                                <label for="txtstatus" class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-9">
                                                     <select class="form-control" name="txtstatus" id="txtstatus" required>
                                                        <option value="">-Select Status-</option>
                                                        <option value="0" <?php echo ($status == '0')?"selected='selected'":''; ?>>Pending</option>
                                                        <option value="1" <?php echo ($status == '1')?"selected='selected'":''; ?>>Active</option>
                                                        <option value="2" <?php echo ($status == '2')?"selected='selected'":''; ?>>Reject</option>
                                                     </select>
                                                </div>
                                                <div class="col-sm-1">
                                                    <span class="required_field">*</span>
                                                </div>
                                            </div>
                                        <?php
                                        }else{ ?>
                                                    <input type="hidden" name="txtstatus" value="0">
                                        <?php   } ?>
										<h3 class="heading_a"><span class="heading_text">Login Credentials</span></h3>
                                        <div class="form-group">
                                            <label for="txtemail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" id="txtemail" name="txtemail" value="<?php echo $email;?>" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="pro_new_password" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtpassword" name="txtpassword" value="<?php echo $password; ?>" >
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>
										
										<h3 class="heading_a"><span class="heading_text">Carers</span></h3>
                                        <div class="form-group">
                                            <label for="carer1" class="col-sm-2 control-label">Carer 1</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="carer1" name="carer1" value="<?php echo isset($row['_Carer1'])?$row['_Carer1']:''; ?>">
                                            </div>
											<div class="col-sm-1">
											</div>
										</div>
										<div class="form-group">
                                            <label for="carer2" class="col-sm-2 control-label">Carer 2</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="carer2" name="carer2" value="<?php echo isset($row['_Carer2'])?$row['_Carer2']:''; ?>">
                                            </div>
											<div class="col-sm-1">
											</div>
										</div>
										<div class="form-group">
                                            <label for="carer3" class="col-sm-2 control-label">Carer 3</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="carer3" name="carer3" value="<?php echo isset($row['_Carer3'])?$row['_Carer3']:''; ?>">
                                            </div>
											<div class="col-sm-1">
											</div>
										</div>
										<div class="form-group">
                                            <label for="carer4" class="col-sm-2 control-label">Carer 4</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="carer4" name="carer4" value="<?php echo isset($row['_Carer4'])?$row['_Carer4']:''; ?>">
                                            </div>
											<div class="col-sm-1">
											</div>
										</div>
										
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
                                        <!--<div class="form-group">
                                            <label for="txtfname" class="col-sm-2 control-label">First Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtfname" name="txtfname" value="<?php echo $fname;?>" required >
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>-->
                                        <div class="form-group">
                                            <label for="txtlname" class="col-sm-2 control-label">Family Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtlname" name="txtlname" value="<?php echo $lname;?>" required>
                                                <input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                                <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        
										<!--<h3 class="heading_a"><span class="heading_text">Children Details</span></h3>
										<div class="form-group">
                                            <label for="pro_new_password" class="col-sm-2 control-label">Firstname</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pupilfirstname" name="pupilfirstname" value="<?php echo $pupilfirstname; ?>" PLACEHOLDER="FIRSTNAME" readonly>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="pro_new_password" class="col-sm-2 control-label">Lastname</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pupillastname" name="pupillastname" value="<?php echo $pupillastname; ?>" PLACEHOLDER="LASTNAME" readonly>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="pro_new_password" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pupilemail" name="pupilemail" value="<?php echo $pupilemail; ?>" PLACEHOLDER="EMAIL" readonly>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
                                        <hr/>-->
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Parent / Carer</label>
                                            <div class="col-sm-9">
												<input type="radio" class="" name="txtguardian" id="txtguardian" value="Parent" <?php echo isset($row['_Guardian']) && $row['_Guardian'] == 'Parent'?"checked='checked'":($action == 'add'?"checked='checked'":'');?>> Parent
												<input type="radio" class="" name="txtguardian" id="txtguardian" value="Carer" <?php echo isset($row['_Guardian']) && $row['_Guardian'] == 'Carer'?"checked='checked'":'';?>> Carer
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
										
										
										
                                        <div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Children</label>
                                            <div class="col-sm-9">
                                                 <select name="txtpupils[]" class="form-control" multiple style="height: 200px;">
                                                    <?php 
                                                    while($fetch_pupil = mysqli_fetch_assoc($run_pupil))
                                                    {
                                                    ?>
                                                        <option value="<?php echo $fetch_pupil['_ID'];?>" <?php if(in_array($fetch_pupil['_ID'],$pupil)){echo "Selected";}?>><?php echo $fetch_pupil['_FirstName'].' '.$fetch_pupil['_LastName'];?></option>
                                                    <?php 
                                                    }
                                                    ?>
                                                 </select>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field"></span>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="txtphone" class="col-sm-2 control-label">Phone</label>
                                            <div class="col-sm-9">
                                                <input type="" class="form-control" id="txtphone" name="txtphone" value="<?php echo $phone;?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                         <div class="form-group">
                                            <label for="txtadd1" class="col-sm-2 control-label">Address 1</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtadd1" id="txtadd1" value = "<?php echo $address1; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtadd2" class="col-sm-2 control-label">Address 2</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtadd2" id="txtadd2" value="<?php echo $address2; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtcity" class="col-sm-2 control-label">City</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtcity" id="txtcity" value="<?php echo $city; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtstate" class="col-sm-2 control-label">State</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtstate" id="txtstate" value="<?php echo $state; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtcountry" class="col-sm-2 control-label">Country</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" cols="10" rows="4" name="txtcountry" id="txtcountry" value="<?php echo $country; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Postcode</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" cols="10" rows="4" name="txtzip" id="txtzip" value="<?php echo $postcode; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
										 
									<!-- 	 
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Academic School Year</label>
                                            <div class="col-sm-9">
												 <select id="txtyear" name="txtyear" class="form-control" readonly>
													<option value=""> -Select Academic Year- </option>
													<?php if($academicyear != ''){?>
													<option value="<?php echo $academicyear; ?>" selected><?php echo $academicyear; ?></option>
													<?php } 
													for($a=date('Y');$a<=date('Y')+10;$a++)
													{
													?>
														<option value="<?php echo $a;?>"><?php echo $a;?></option>
													<?php
													}
													?>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div> 		
										
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Current School</label>
                                            <div class="col-sm-9">
                                                <select id="txtcschool" name="txtcschool" class="form-control" readonly>
													<option value=""> -Select School- </option>
													<?php 
														$sqlschl = "Select * from ".$tbname."_schools";
														$rstschool = mysqli_query($con,$sqlschl);
														while($rowschl = mysqli_fetch_assoc($rstschool) ){ ?>
															<option value="<?php echo $rowschl['_ID']; ?>" <?php if($rowschl['_ID'] == $cur_school){echo 'selected';} ?>><?php echo $rowschl['_Name']; ?></option>	
												<?php	}
													?> 
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div> 
										
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">School Applying For</label>
                                            <div class="col-sm-9">
                                                <select id="txtafschool" name="txtafschool" class="form-control" readonly>
													<option value=""> -Select School- </option>
													<?php 
														$sqlschl1 = "Select * from ".$tbname."_schools";
														$rstschool1 = mysqli_query($con,$sqlschl1);
														while($rowschl1 = mysqli_fetch_assoc($rstschool1) ){ ?>
															<option value="<?php echo $rowschl1['_ID']; ?>" <?php echo $rowschl1['_ID'] == $new_school ? 'selected' : ''; ?>><?php echo $rowschl1['_Name']; ?></option>	
												<?php	}
													?> 
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div> 
										
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Other Comments</label>
                                            <div class="col-sm-9">
                                                  <textarea name="txtcomments" id="txtcomments" class="form-control" readonly><?php echo $comment; ?></textarea>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div> 
										
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Problem Areas</label>
                                            <div class="col-sm-9">
                                                <select id="txtcproblem" name="txtcproblem" class="form-control" readonly>
													<option value=""> -Select Problem Areas- </option>
													<?php 
														$sqlprb = "Select * from ".$tbname."_problems";
														$rstprb = mysqli_query($con,$sqlprb);
														while($rowsprb = mysqli_fetch_assoc($rstprb) ){ ?>
															<option value="<?php echo $rowsprb['_ID']; ?>" <?php echo $rowsprb['_ID'] == $problem ? 'selected' : ''; ?>><?php echo $rowsprb['_Title']; ?></option>	
												<?php	}
													?> 
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div> 
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Preferred Days</label>
                                            <div class="col-sm-9">
                                                <select name="txtdays[]" class="form-control" size="7" readonly multiple>
													<?php 
													foreach($days as $key=>$val)
													{
													?>
														<option value="<?php echo $key;?>" <?php echo ($row['_Predays'] != '' && in_array($key , $preferred_days) == true) ? "selected='selected'" : '';?>><?php echo $val;?></option>
													<?php
													}
													?>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div> 
										
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Preferred Time</label>
                                            <div class="col-sm-9">
                                                <select name="txtptime[]" class="form-control" size="3" readonly multiple>
													<?php 
													foreach($times as $key=>$val)
													{
													?>
														<option value="<?php echo $key;?>" <?php echo ($row['_Pretime'] != '' && in_array($key , $preferred_time) == true) ? "selected='selected'" : '';?>><?php echo $val;?></option>
													<?php
													}
													?>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Preferred Contact Method</label>
                                            <div class="col-sm-9">
												<select id="txtcmethod" name="txtcmethod" class="form-control" readonly>
													<option value=""> -Select Method- </option>
													<option value="Mail" <?php echo $contact_method == 'Mail'?'selected':''; ?>>Mail</option>
													<option value="Phone" <?php echo $contact_method == 'Phone'?'selected':''; ?>>Phone Call</option>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>  -->
                                            
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                              <?php if($action == 'edit'){ ?>
                                                <a href="addclientcom.php?id=<?php echo encrypt($id,$encrypt); ?>&type=<?php echo encrypt('0',$encrypt); ?>" class ="btn btn-default" >Add Communication</a>
                                                <?php } ?>
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "allclient.php";
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
