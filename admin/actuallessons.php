<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$title = $sitename." : View Booking";
if(isset($_REQUEST['done'])){
	$done = decrypt($_REQUEST['done'],$encrypt);
}

$subjectid = decrypt($_REQUEST['id'],$encrypt);
$pupilid = decrypt($_REQUEST['pid'],$encrypt);

//log create start
    $pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$pupilid;
    $run_pemail = mysqli_query($con , $pemail_query);
    $fetch_pemail = mysqli_fetch_assoc($run_pemail);
    $pupilemail = $fetch_pemail['_Email'];
    $subject_query = "SELECT _Name FROM ".$tbname."_subjects WHERE _ID = ".$subjectid;
    $run_subject = mysqli_query($con , $subject_query);
    $fetch_subject = mysqli_fetch_assoc($run_subject);
    $subject_log = $fetch_subject['_Name'];
    $create_log = auditlog($msg = "View Booking Details For Pupil  ".$pupilemail." For Subject ".$subject_log);
//log create end

$qry = "select * FROM ww_clipup_rel WHERE _PupilID = ".$pupilid;
$selcli = mysqli_query($con,$qry);
$rscli = mysqli_fetch_assoc($selcli);
//print_r($rscli);exit;
$clientid = $rscli['_ClientID'];

$selcli = mysqli_query($con,"select concat(_Firstname,' ',_Lastname) clientname from ww_clientmaster where _ID = '".$clientid."'");
$rscli = mysqli_fetch_assoc($selcli);
$clientname = $rscli['clientname'];

    $sel_client = "select bl.* , DATEDIFF(bl._Date , now()) is_past_date,concat(tm._Firstname,' ',tm._Lastname) as tutorname,concat(pm._Firstname,' ',pm._Lastname) as pupilname,sb._Name,ls._Title from ".$tbname."_booked_lessons as bl left join ".$tbname."_tutormaster as tm on bl._TutorID = tm._ID left join ".$tbname."_pupilmaster as pm on bl._PupilID = pm._ID left join ".$tbname."_subjects as sb on sb._ID = bl._SubjectID left join ".$tbname."_levels ls on ls._ID = bl._LevelID WHERE bl._PupilID = ".$pupilid." AND bl._SubjectID = ".$subjectid;

    $rst_client = mysqli_Query($con,$sel_client);

//echo $sel_client;exit;


$lnkbread = "<a href='edit-client.php?id=".encrypt($clientid,$encrypt)."&e_action=".encrypt('edit',$encrypt)."'>".$clientname."</a>";

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
                $(function() {
                    // footable
                    yukon_datatables.p_plugins_tables_datatable();
                })
        </script>
        <style type="text/css">
        .pending{
            color: #f6b738 !important;
            font-weight: bolder;
        }
        .activex{
            color: #87be4a !important;
            font-weight: bolder;
        }
        .inactive{
            color: #d83b4b !important;
            font-weight: bolder;
        }
        </style>


    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allclient.php">Clients</a></li><li><?php echo $lnkbread; ?></li><li>Lesson Details</li>
                </ul>
            </nav>

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <?php 
                        if($done == "11"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Resource Lesson Data Updated Successfully.
                            </div>
                    <?php   }
                            if($done == "1"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Lesson Data Not Updated Successfully.
                                    </div>
                            <?php   }
                        ?>   
                 
                                           
                    </div>
                     <div class="row">
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-bordered table-striped " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Pupil</th>
                                        <th>Tutor</th>
                                        <th>Subject</th>
                                        <th>Level</th>
                                        <th>Date</th>
                                        <th>From </th>
                                        <th>To</th>
                                        <th>Lessson Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                           
                                            if(mysqli_num_rows($rst_client) > 0){                                           
                                            while ($row = mysqli_fetch_assoc($rst_client)){
                                                if($row['_Type'] == '0'){
                                                    $type = "Trial";
                                                }else{
                                                    $type = "Normal";
                                                }
                                           
                                                ?>
                                                <tr >
                                                    <td><?php echo $row['pupilname']; ?></td>
                                                    <td><?php echo $row['tutorname']; ?></td>
                                                    <td><?php echo $row['_Name']; ?></td>
                                                    <td><?php echo $row['_Title']; ?></td>
                                                    <td><?php echo date("d M Y", strtotime($row['_Date']));  ?></td>
                                                    <td><?php echo $row['_Fromtime']; ?></td>
                                                    <td><?php echo $row['_Totime']; ?></td>
                                                    <td><?php echo $type; ?></td>
                                                </tr>


                                        <?php   }
                                            }
                                        ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main content -->
            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

        
        
        
    </body>
</html>
