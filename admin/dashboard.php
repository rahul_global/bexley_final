<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}


$title = $sitename." : Dashboard";

$result = decrypt($_GET['result'],$encrypt);

$sqltut = "select _ID, concat(_Firstname,' ',_Lastname) as tutor from ".$tbname."_tutormaster where _Status = '0' ";
$rsttut = mysqli_query($con,$sqltut);

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
                    <script type="text/javascript">
                        $(document).ready(function(){
                          //   $('#myTable').DataTable();
                          //   $('#myTable1').DataTable();
                          //   $('#Clienttable').DataTable();
                        });
                    </script>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>        </ul>
            </nav>

            <!-- main content -->
            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Bexley Snap CRM Dashboard</h2>
                        <?php if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                             <h3 class="heading_a"><span class="heading_text">Carer/Parent awaiting approval</span></h3>

                                <table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Carer/Parent Name</th>
                                                <th>Email</th>
                                                <th>view</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                    $selcli = "Select * from ".$tbname."_clientmaster where _Status = '0'";
                                                    $rstcli = mysqli_query($con,$selcli);

                                                    $numcli = mysqli_num_rows($rstcli);
                                                    if($numcli > 0)
                                                    {
                                                        while ($rowcli = mysqli_fetch_assoc($rstcli))
                                                        { 
                                                        ?>
                                                            <tr>
                                                                <td><?php echo $rowcli['_Firstname'].' '.$rowcli['_Lastname']; ?></td>
                                                                <td><?php echo $rowcli['_Email']; ?></td>
                                                              
                                                                <td><a href="edit-client.php?id=<?php echo encrypt($rowcli['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>">View</td>
                                                            </tr>
                                                    <?php   
                                                        }
                                                    }
                                                    ?>
                                            
                                        </tbody>
                                    </table>
                            
                        </div>
                        
                    </div>

                    <!-- <div class="row">
                        <div class="col-md-12">
                             <h3 class="heading_a"><span class="heading_text">Tutors awaiting approval</span></h3>

                                <table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Tutor</th>
                                                <th>Subject</th>
                                                <th>Level</th>
                                                <th>view</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                    $numtut = mysqli_num_rows($rsttut);
                                                    if($numtut > 0)
                                                    {
                                                        while ($rowtuts = mysqli_fetch_assoc($rsttut))
                                                        { 
                                                        ?>
                                                            <tr>
                                                                <td><?php echo $rowtuts['tutor']; ?></td>
                                                                <td><?php 

                                                                        $selsubid = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$rowtuts['_ID']."' ";
                                                                        $rstselsub = mysqli_query($con,$selsubid);
                                                                        $subjects = array();
                                                                        while ($rowsubje = mysqli_fetch_assoc($rstselsub)) {
                                                                                array_push($subjects,$rowsubje['_SubjectID']);
                                                                        }
                                                                        $newsub = implode(",", $subjects);

                                                                        if(isset($newsub) && $newsub != ""){
                                                                        $selsub = "Select _Name from ".$tbname."_subjects where _ID in (".$newsub.")" ;
                                                                        $rstsub = mysqli_Query($con,$selsub);
                                                                        $sub = array();
                                                                        while($rowsub = mysqli_fetch_assoc($rstsub)){
                                                                        //echo $rowsub['_Name'].",";
                                                                        array_push($sub, $rowsub['_Name']);
                                                                        
                                                                            
                                                                        }
                                                                            $sub = implode(",", $sub);
                                                                            if($sub != ''){
                                                                                echo $sub;
                                                                            }else{
                                                                                echo "-";
                                                                            }
                                                                        }

                                                                ?></td>
                                                                <td><?php


                                                                            $sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$rowtuts['_ID']."'";
                                                                            $rstlev = mysqli_query($con,$sellev);
                                                                            $levid = array();
                                                                            if(mysqli_num_rows($rstlev) > 0){
                                                                                
                                                                                while($rssub = mysqli_fetch_assoc($rstlev)){
                                                                                    array_push($levid, $rssub['_LevelID']);
                                                                                }  
                                                                                
                                                                            }
                                                                            $newlevel = implode(",", $levid);

                                                                            if(isset($newlevel) && $newlevel != ""){
                                                                            $sellev = "Select _Title from ".$tbname."_levels where _ID in (".$newlevel.")" ;
                                                                            $rstlev = mysqli_Query($con,$sellev);
                                                                            $lev = array();
                                                                            while($rowlev = mysqli_fetch_assoc($rstlev)){
                                                                            //echo $rowlev['_Name'].",";
                                                                            array_push($lev, $rowlev['_Title']);
                                                                            
                                                                                
                                                                            }
                                                                                $lev = implode(",", $lev);
                                                                                if($lev != ''){
                                                                                    echo $lev;
                                                                                }else{
                                                                                    echo "hi";
                                                                                    echo "-";
                                                                                }
                                                                            }


                                                                ?></td>
                                                              
                                                                <td><a href="edit-tutor.php?id=<?php echo encrypt($rowtuts['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>">View</td>
                                                            </tr>
                                                    <?php   
                                                        }
                                                    }
                                                    ?>
                                            
                                        </tbody>
                                    </table>
                            
                        </div>
                        
                    </div> -->
                    <!-- for view subject approve -->
                    <!-- <hr/>
                    <div class="row">
                        <div class="col-md-12">
                             <h3 class="heading_a"><span class="heading_text">Subjects Awaiting Approval</span></h3>

                                <table id="myTable" class="table table-striped " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Tutor</th>
                                                <th>Subjects</th>
                                                <th>Approve Subject</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                    $sqltutsub = "select _ID, concat(_Firstname,' ',_Lastname) as tutor from ".$tbname."_tutormaster";
                                                    $rsttutsub = mysqli_query($con,$sqltutsub);
                                                    $numtutsub = mysqli_num_rows($rsttutsub);
                                                    if($numtutsub > 0)
                                                    {
                                                        while ($rowtutssub = mysqli_fetch_assoc($rsttutsub))
                                                        { 
                                                        ?>
                                                            <tr>
                                                                <td><?php echo $rowtutssub['tutor']; ?></td>
                                                                <td><?php 

                                                                        $selsubid = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$rowtutssub['_ID']."' and _Approve = '0'";
                                                                        $rstselsub = mysqli_query($con,$selsubid);
                                                                        $subjects = array();
                                                                        while ($rowsubje = mysqli_fetch_assoc($rstselsub)) {
                                                                                array_push($subjects,$rowsubje['_SubjectID']);
                                                                        }
                                                                        $newsub = implode(",", $subjects);

                                                                        if(isset($newsub) && $newsub != ""){
                                                                        $selsub = "Select _Name from ".$tbname."_subjects where _ID in (".$newsub.")" ;
                                                                        $rstsub = mysqli_Query($con,$selsub);
                                                                        $sub = array();
                                                                        while($rowsub = mysqli_fetch_assoc($rstsub)){
                                                                        //echo $rowsub['_Name'].",";
                                                                        array_push($sub, $rowsub['_Name']);
                                                                        
                                                                            
                                                                        }
                                                                            $sub = implode(",", $sub);
                                                                            if($sub != ''){
                                                                                echo $sub;
                                                                            }else{
                                                                                echo "-";
                                                                            }
                                                                        }

                                                                ?></td>
                                                                
                                                                <td><a href="approve_subject.php?id=<?php echo encrypt($rowtutssub['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('approve',$encrypt); ?>">Approve Subjects</a></td>
                                                            </tr>
                                                    <?php   
                                                        }
                                                    }
                                                    ?>
                                            
                                        </tbody>
                                    </table>
                            
                        </div>
                        
                    </div> -->
                   
                    <!-- for view subject approve -->
                    <!-- <div class="row">
                        <div class="col-md-12">
                             <h3 class="heading_a"><span class="heading_text">Levels Awaiting Approval</span></h3>

                                <table id="myTable1" class="table table-striped " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Tutor</th>
                                                <th>Levels</th>
                                                <th>Approve Subject</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                    $sqltutlev = "select _ID, concat(_Firstname,' ',_Lastname) as tutor from ".$tbname."_tutormaster";
                                                    $rsttutlev = mysqli_query($con,$sqltutlev);
                                                    $numtutlev = mysqli_num_rows($rsttutlev);
                                                    if($numtutlev > 0)
                                                    {
                                                        while ($rowtutslev = mysqli_fetch_assoc($rsttutlev))
                                                        { 
                                                        ?>
                                                            <tr>
                                                                <td><?php echo $rowtutslev['tutor']; ?></td>
                                                                <td><?php


                                                                            $sellev1 = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$rowtutslev['_ID']."' and _Approve = '0'";
                                                                            $rstlev1 = mysqli_query($con,$sellev1);
                                                                            $levid1 = array();
                                                                            if(mysqli_num_rows($rstlev1) > 0){
                                                                                
                                                                                while($rssub = mysqli_fetch_assoc($rstlev1)){
                                                                                    array_push($levid1, $rssub['_LevelID']);
                                                                                }  
                                                                                
                                                                            }
                                                                            $newlevel1 = implode(",", $levid1);

                                                                            if(isset($newlevel1) && $newlevel1 != ""){
                                                                            $sellev11 = "Select _Title from ".$tbname."_levels where _ID in (".$newlevel1.")" ;
                                                                            $rstlev11 = mysqli_Query($con,$sellev11);
                                                                            $lev1 = array();
                                                                            while($rowlev1 = mysqli_fetch_assoc($rstlev11)){
                                                                            //echo $rowlev1['_Name'].",";
                                                                            array_push($lev1, $rowlev1['_Title']);
                                                                            
                                                                                
                                                                            }
                                                                                $lev1 = implode(",", $lev1);
                                                                                if($lev1 != ''){
                                                                                    echo $lev1;
                                                                                }else{
                                                                                    echo "hi";
                                                                                    echo "-";
                                                                                }
                                                                            }


                                                                ?></td>
                                                                
                                                                <td><a href="approve_level.php?id=<?php echo encrypt($rowtutslev['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('approve',$encrypt); ?>">Approve Levels</a></td>
                                                            </tr>
                                                    <?php   
                                                        }
                                                    }
                                                    ?>
                                            
                                        </tbody>
                                    </table>
                            
                        </div>
                        
                    </div> -->
                </div>
            </div>

        </div>

        
        
        
    </body>
</html>
