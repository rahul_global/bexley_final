<?php 
require_once('db/dbopen.php');

$action = decrypt($_REQUEST['e_action'],$encrypt);

	if($action == "edit")
	{
		$blogtype_id		= decrypt($_POST['blogtype_id'],$encrypt);

		$blogtype 			= replaceSpecialChar($_POST['txtbtype']);
		$status				= $_POST['txtstatus'];
		if($blogtype != '' && $action != '' && $status != '' && $blogtype_id != '')
		{
			$check_qry = "SELECT _ID , _Type , _Status FROM ".$tbname."_blogtype WHERE _ID = $blogtype_id";
			$run_check = mysqli_query($con, $check_qry);
			$num = mysqli_num_rows($run_check);
			if($num > 0)
			{
				$fetch_data = mysqli_fetch_assoc($run_check);
				
				$qry  	= "UPDATE ".$tbname."_blogtype SET ";
				$query 	= "";
				
				if($blogtype != $fetch_data['_Type'])
				{
					$query .= " _Type = '$blogtype'";
				}
				if($status != $fetch_data['_Status'])
				{
					$query .= " _Status = '$status'";
				}
				
				$query = rtrim($query , ',');			
				
				if($query != '')
				{
					$query .= " WHERE _ID = $blogtype_id";
					$query = $qry.''.$query;
					if($run = mysqli_query($con , $query))
					{
						//log create start
							$create_log = auditlog($msg = "Updated Blog Type - ".$blogtype);
						//log create end
						header("location:allblogtypes.php?result=".encrypt('success',$encrypt));
						exit;
					}
					else
					{
						header("location:edit-btype.php?result=".encrypt('failed',$encrypt)."&id=".$blogtype_id."&e_action=".encrypt('edit',$encrypt));
						exit;
					}
				}
				else
				{
					header("location:allblogtypes.php?result=".encrypt('success',$encrypt));
					exit;
				}
			}
			else
			{
				header("location:edit-btype.php?result=".encrypt('failed',$encrypt)."&id=".$blogtype_id."&e_action=".encrypt('edit',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-btype.php?result=".encrypt('failed',$encrypt)."&id=".$blogtype_id."&e_action=".encrypt('edit',$encrypt));
			exit;
		}
	}
	else if($action == "add")
	{	
		$blogtype 			= replaceSpecialChar($_POST['txtbtype']);
		$status				= $_POST['txtstatus'];
		if($blogtype != '' && $action != '' && $status != '' )
		{
			$query 	= "INSERT INTO ".$tbname."_blogtype(_Type , _Status) VALUES('$blogtype','$status')";
			if($run 	= mysqli_query($con , $query))
			{
				//log create start
					$create_log = auditlog($msg = "Added New Blog Type - ".$blogtype);
				//log create end
				header("location:allblogtypes.php?result=".encrypt('success',$encrypt));
				exit;
			}
			else
			{
				header("location:edit-btype.php?result=".encrypt('failed',$encrypt)."&e_action=".encrypt('add',$encrypt));
				exit;
			}
		}
	}
	else if($action == "delete")
	{
		//log create start
			$blg_qry  = "SELECT _Type FROM ".$tbname."_blogtype WHERE _ID = ".$id;
			$run_blg = mysqli_query($con, $blg_qry);
			$fetch_blg = mysqli_fetch_assoc($run_blg);
			$blogtype = $fetch_blg['_Type'];
		//log create end

		$id 	= decrypt($_GET['id'],$encrypt);	
		$query 	= "DELETE  FROM ".$tbname."_blogtype WHERE _ID = $id";
		
		if($run = mysqli_query($con , $query))
		{
			//log create start
				$create_log = auditlog($msg = "Deleted Blog Type - ".$blogtype);
			//log create end
			header("location:allblogtypes.php?result=".encrypt('success',$encrypt));
			exit;
		}
		else
		{
			header("location:allblogtypes.php?result=".encrypt('failed',$encrypt));
			exit;
		}
	}
?>