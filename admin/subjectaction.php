<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$fname = $_REQUEST['txtname'];
$status = $_REQUEST['txtstatus']; 	//0 for pending, 1 for active, 2 for inactive

if($action == "add"){

			

		$str = "INSERT INTO " . $tbname . "_subjects (";
     
     	$str .= " _Name,_IsActive) VALUES(";
     
        if ($fname != "")
            $str = $str . "'" . replaceSpecialChar($fname) . "', ";
        else
            $str = $str . "null, ";

		if ($status != "")
		 	$str = $str . "'" . replaceSpecialChar($status) . "' ";
		else
		  	$str = $str . "null ";
        
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Added New Subject - ".replaceSpecialChar($fname));
		//log create end
		header("location:subjectareas.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:subjectareas.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_subjects SET ";
	    
	    if ($fname != "")
			$str = $str . "_Name = '" . replaceSpecialChar($fname) . "', ";
		else
	        $str = $str . "_Name = null, ";
	
		if ($status != "")
	        $str = $str . "_IsActive = '" . $status . "' ";
	    else
	        $str = $str . "_IsActive = null ";

	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated Subject - ".replaceSpecialChar($fname));
		//log create end
		header("location:subjectareas.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:subjectareas.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){

	$delclient = "delete from ".$tbname."_subjects where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted Subject - ".$fname);
		//log create end	

		header("location:subjectareas.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:subjectareas.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>