<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}
$title = $sitename." : Communications";
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<script>
			$(function() {
				// footable
				yukon_datatables.p_plugins_tables_datatable();
			})
		</script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="javascript:void(0)">Communications</a></li>
				</ul>
            </nav>
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-md-12">
                			<h2 style="color: #20638F;margin: 0px;">Communications</h2>
                		</div>
                	</div>
                    <div class="row">
                        <div class="col-md-12">
							
                            <table id="datatable_demo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
										<th>User Type</th>
                                        <th>Person Spoken</th>
                                        <th>Notes</th>
										<th>Email</th>
										<th>Date</th>
                                        <th>Time</th>
                                        <th>Communication Type</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $comm_qry = "SELECT a.* , b._Name as `_Comtype` FROM ".$tbname."_communications a LEFT JOIN ".$tbname."_communication_type b ON b._ID = a._Commtype";
                                            $run_comm = mysqli_Query($con,$comm_qry);
											$num_comm = mysqli_num_rows($run_comm);
											if($num_comm > 0)
											{
												while ($fetch_comm = mysqli_fetch_assoc($run_comm))
												{ 
													$usertype = $fetch_comm['_Usertype'];
													$tablename = $usertype == 0 ? 'clientmaster' : 'pupilmaster';
													
													$nameqry = "SELECT * FROM ".$tbname."_".$tablename." WHERE _ID = ".$fetch_comm['_UserID'];
													$runname = mysqli_query($con,$nameqry);
													$fetchname = mysqli_fetch_assoc($runname);
													
													$firstname = $usertype == 0 ? $fetchname['_Firstname'] : $fetchname['_FirstName'] ;
													$lastname = $usertype == 0 ? $fetchname['_Lastname'] : $fetchname['_LastName'] ;
													
													$date = str_replace('-','/',$fetch_comm['_Date']);
													$date = date('d-m-Y',strtotime($date));
												?>
													<tr>
														<td><?php echo $firstname.' '.$lastname; ?></td>
														<td><?php echo $usertype == 0 ? 'Carer' : 'Children';?></td>
														<td><?php echo $fetch_comm['_Person_spoken'];?></td>
														<td><?php echo $fetch_comm['_Notes'];?></td>
														<td><?php echo $fetch_comm['_Email'];?></td>
														<td><?php echo $date;?></td>
														<td><?php echo $fetch_comm['_Time'];?></td>
														<td><?php echo $fetch_comm['_Comtype'];?></td>
														<td><a href="viewcommunication.php?id=<?php echo encrypt($fetch_comm['_ID'],$encrypt);?>" class="btn btn-primary">View</a></td>
													</tr>
											<?php   
												}
											}
											?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			<!-- main content -->
        </div>
    </body>
</html>