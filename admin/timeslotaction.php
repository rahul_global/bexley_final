<?php
session_start();

include_once('db/dbopen.php');
//print_r($_REQUEST);
$dates            = explode(',',$_REQUEST['txtdate']);
$fromtime 		  = date("H:i:s",strtotime($_POST['txtftime'])); 
$totime	 	      = date("H:i:s",strtotime($_POST['txtttime']));
$id 			  = isset($_POST['id']) ? str_replace(" ", "+", $_POST['id']) : '';
$bookingtype      = $id != '' ? decrypt($id,$encrypt) : '';

$action			  = decrypt($_REQUEST['action'],$encrypt);

	
if($action == "add"){
	$b = 0;
	for($a = 0 ; $a < sizeof($dates);$a++){
		$date = date("Y-m-d",strtotime($dates[$a]));
	
		$str = "INSERT INTO " . $tbname . "_timeslot (";
     
     	$str .= " _Date,_Bookingtype,_Fromtime,_Totime) VALUES(";
     
          if ($date != "")
              $str = $str . "'" . $date . "', ";
          else
              $str = $str . "null, ";

          if ($bookingtype != "")
              $str = $str . "'" . $bookingtype . "', ";
          else
              $str = $str . "null, ";

          if ($fromtime != "")
              $str = $str . "'" . $fromtime . "', ";
          else
              $str = $str . "null, ";

          if ($totime != "")
              $str = $str . "'" . $totime . "' ";
          else
              $str = $str . "null ";
        
          $str = $str . ") ";

         //echo $str;exit;
		 
		if($rstins = mysqli_query($con,$str)){
			$b++;
		}
	}
		 
	if($b == sizeof($dates)){
		
		header('location:addtimeslot.php?id='.encrypt($bookingtype,$encrypt).'&result='.encrypt('success',$encrypt));
		exit;

	}else{
		
		header('location:addtimeslot.php?id='.encrypt($bookingtype,$encrypt).'&result='.encrypt('failed',$encrypt));
		exit;

	}

}

if($action == "delete"){	
	$id               = isset($_GET['id']) ? str_replace(" ", "+", $_GET['id']) : '';
	$id               = $id != '' ? decrypt($id,$encrypt) : '';
	
	$bid 			  = isset($_GET['bid']) ? str_replace(" ", "+", $_GET['bid']) : '';
	$bookingtype      = $bid != '' ? decrypt($bid,$encrypt) : '';
	
	$del = "Delete from ".$tbname."_timeslot where _ID = '".$id."'";
	$rsdel = mysqli_query($con,$del);

	if($rsdel){
		header('location:addtimeslot.php?id='.encrypt($bookingtype,$encrypt).'&result='.encrypt('deleted',$encrypt));
		exit;
	}else{
		header('location:addtimeslot.php?id='.encrypt($bookingtype,$encrypt).'&result='.encrypt('failed',$encrypt));
		exit;
	}
}
?>