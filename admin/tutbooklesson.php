<?php 
session_start();
include_once('db/dbopen.php');
//print_r($_REQUEST);
$action = decrypt($_REQUEST['e_action'],$encrypt);
$tutorid = decrypt($_REQUEST['tutorid'],$encrypt);
$subject = $_REQUEST['txtsubject'];
$level = $_REQUEST['txtlevels'];
$frequency = $_REQUEST['txtfreq']; // o for daily 1 for weekly and 2 for monthly
$pupilid = $_REQUEST['txtpupil'];
$txttrdate = $_REQUEST['txttrdate'];
$txttrftime = $_REQUEST['txttrftime'];
$txttrttime = $_REQUEST['txttrttime'];
$txtacdate = $_REQUEST['txtacdate'];
$txtacftime = $_REQUEST['txtacftime'];
$txtacttime = $_REQUEST['txtacttime'];

$trialdate = str_replace("/", "-", $txttrdate);
$actualdate = str_replace("/", "-", $txtacdate);
$newtrdate = date("Y-m-d",strtotime($trialdate));
$newacdate = date("Y-m-d",strtotime($actualdate));

if($action = "book"){

		$sql = "select * from ".$tbname."_booked_lessons where _TutorID ='".$tutorid."' and _PupilID = '".$pupilid."' AND _SubjectID = ".$subject." and _Type = '0'";
		$rstcheck = mysqli_query($con,$sql);
		
		if(mysqli_num_rows($rstcheck) > 0){
			
			header("location:booklesson.php?id=".encrypt($tutorid,$encrypt)."&result=".encrypt('already',$encrypt));
			exit;
		}else{

				$sqltrial = "select * from ".$tbname."_booked_lessons where _TutorID = '".$tutorid."' and _PupilID = '".$pupilid."'  AND _SubjectID = ".$subject." and _Type = '0'";
				$rsttrial = mysqli_query($con,$sqltrial);
				
				//log create start
				$pupillog_qry = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$pupilid;
				$run_plog = mysqli_query($con , $pupillog_qry);
				$fetch_plog = mysqli_fetch_assoc($run_plog);
				$tutorlog_qry = "SELECT _Email FROM ".$tbname."_tutormaster WHERE _ID = ".$tutorid;
				$run_tlog = mysqli_query($con , $tutorlog_qry);
				$fetch_tlog = mysqli_fetch_assoc($run_tlog);
				$pupillog_email = $fetch_plog['_Email'];
				$tutorlog_email = $fetch_tlog['_Email'];
				//log create end
				
				if(mysqli_num_rows($rsttrial) > 0){
					header("location:booklesson.php?id=".encrypt($tutorid,$encrypt)."&result=".encrypt('alreadytrial',$encrypt));
					exit;
				}else{

					$type = '0';

					$str = "INSERT INTO " . $tbname . "_booked_lessons (";
			     
			     	$str .= " _PupilID,_TutorID,_SubjectID,_LevelID,_Date,_Fromtime,_Totime,_Type) VALUES(";
			     
			          if ($pupilid != "")
			              $str = $str . "'" . $pupilid . "', ";
			          else
			              $str = $str . "null, ";

			          if ($tutorid != "")
			              $str = $str . "'" . $tutorid . "', ";
			          else
			              $str = $str . "null, ";

			          if ($subject != "")
			              $str = $str . "'" . $subject . "', ";
			          else
			              $str = $str . "null, ";

			          if ($level != "")
			              $str = $str . "'" . $level . "', ";
			          else
			              $str = $str . "null, ";

			          if ($newtrdate != "")
			              $str = $str . "'" . $newtrdate . "', ";
			          else
			              $str = $str . "null, ";
			          if ($txttrftime != "")
			              $str = $str . "'" . $txttrftime . "', ";
			          else
			              $str = $str . "null, ";
			          if ($txttrttime != "")
			              $str = $str . "'" . $txttrttime . "', ";
			          else
			              $str = $str . "null, ";

			          if ($type != "")
			              $str = $str . "'" . $type . "' ";
			          else
			              $str = $str . "null ";

			          $str = $str . ") ";

					//echo $str;exit;
					$rstins = mysqli_query($con,$str);
					
					//log create start
						$create_log = auditlog($msg = "Booked Trial Lesson of Pupil ".$pupillog_email." For Staff ".$tutorlog_email);
					//log create end
				}

				// for actual lesson
		          $arrdate = array();
		          $arrdate[0] = $newacdate;
		          for($i=0;$i<9;$i++){

			          	if($frequency == "0"){
			          		$newacdate = date("Y-m-d", strtotime('+1 days',strtotime($newacdate)));
			          		//$newacdate = date("Y")
			          	}else if($frequency == "1"){
			          		$newacdate = date("Y-m-d", strtotime('+7 days',strtotime($newacdate)));

			          	}else if($frequency == "2"){
			          		$newacdate = date("Y-m-d", strtotime('+30 days',strtotime($newacdate)));

			          	}
		          		array_push($arrdate, $newacdate);
		          }
		          $type = '1';
		          
		          for($j=0;$j<count($arrdate);$j++){

		        	$insstr = "INSERT INTO " . $tbname . "_booked_lessons (";
		     
			     	$insstr .= " _PupilID,_TutorID,_SubjectID,_LevelID,_Date,_Fromtime,_Totime,_Type) VALUES(";
			     
			        if ($pupilid != "")
			              $insstr = $insstr . "'" . $pupilid . "', ";
			          else
			              $insstr = $insstr . "null, ";

			          if ($tutorid != "")
			              $insstr = $insstr . "'" . $tutorid . "', ";
			          else
			              $insstr = $insstr . "null, ";

			          if ($subject != "")
			              $insstr = $insstr . "'" . $subject . "', ";
			          else
			              $insstr = $insstr . "null, ";

			          if ($level != "")
			              $insstr = $insstr . "'" . $level . "', ";
			          else
			              $insstr = $insstr . "null, ";

			          if ($arrdate[$j] != "")
			              $insstr = $insstr . "'" . $arrdate[$j] . "', ";
			          else
			              $insstr = $insstr . "null, ";
			          if ($txtacftime != "")
			              $insstr = $insstr . "'" . $txtacftime . "', ";
			          else
			              $insstr = $insstr . "null, ";
			          if ($txtacttime != "")
			              $insstr = $insstr . "'" . $txtacttime . "', ";
			          else
			              $insstr = $insstr . "null, ";

			          if ($type != "")
			              $insstr = $insstr . "'" . $type . "' ";
			          else
			              $insstr = $insstr . "null ";
			          
			        	$insstr = $insstr . ") ";
						//echo $insstr;
						mysqli_query($con , $insstr);
		          }
				 
				//log create start
					$create_log = auditlog($msg = "Booked Actual Lesson of Pupil ".$pupillog_email." For Staff ".$tutorlog_email);
				//log create end
				
				$tutor_qry = "SELECT CONCAT(_Firstname , ' ' , _Lastname) fullname from ".$tbname."_tutormaster WHERE _ID = ".$tutorid;
				$run_tutor = mysqli_query($con , $tutor_qry);
				$fetch_tutor = mysqli_fetch_assoc($run_tutor);
				$tutor = $fetch_tutor['fullname'];
				
				$pupil_qry = "SELECT CONCAT(_Firstname , ' ' , _Lastname) fullname from ".$tbname."_pupilmaster WHERE _ID = ".$pupilid;
				$run_pupil = mysqli_query($con , $pupil_qry);
				$fetch_pupil = mysqli_fetch_assoc($run_pupil);
				$pupil = $fetch_pupil['fullname'];
				
				$subject = "Winterwood- New Booking";
				$msg = "Action : New booking";
				$message = "
					<html>
						<head>
							<title>Winterwood- New Booking</title>
						</head>
						<body>
							<p>Tutor: $tutor<br>
							Pupil: $pupil<br>
							$msg</p>
						</body>
					</html>";

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: <nicgilbey@gmail.com>' . "\r\n";
				//$headers .= 'Cc: @example.com' . "\r\n";

				$result = send_mail($to , $subject , $message , $headers);
				  
				header("location:alltutor.php?result=".encrypt('success',$encrypt));
				exit;
		}
}	

?>