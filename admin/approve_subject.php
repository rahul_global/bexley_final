<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
if(isset($_GET['result']))
{
    $result = decrypt($_GET['result'],$encrypt);
}

if($action == 'approve'){

$title = "Winterwood : Approve Subjects";
$id = decrypt($_REQUEST['id'],$encrypt);
$sel = "select ts.*,sb._Name as subjectname,sb._ID as subjectid from ".$tbname."_tutsub_rel as ts left join ".$tbname."_subjects as sb on ts._SubjectID = sb._ID where _Tutorid='".$id."'";
$rst = mysqli_query($con,$sel);
$tutorname = "Select concat(_Firstname,' ',_Lastname) name from ".$tbname."_tutormaster where _ID = '".$id."'";
$rowname = mysqli_fetch_assoc(mysqli_query($con,$tutorname));
$name = $rowname['name'];
$bread = "Approve Subjects";
$btntext = 'Update';


}else{
    $title = "Winterwood : Approve Subjects";
    $bread = "Approve Subjects";
    $btntext = 'Insert';
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script type="text/javascript">
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script> 
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">All tutor</a></li><li><?php echo $name; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                   <?php if(isset($result) && $result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Subject approved successfully.</div>
                                    <?php } ?>
                                    <?php if(isset($result) && $result == 'failed'){ ?>
                                            <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Subject not approved successfully.</div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                     <h3 class="heading_a"><span class="heading_text">Approve Subjects</span></h3>
                                     
                                    <table id="datatable_demo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Subject Name</th>
                                                <th>Approve Subject</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                                <?php 
                                                    while ($row = mysqli_fetch_assoc($rst)) { ?>
                                                        <tr>
                                                            <td><?php echo $row['subjectname'];?></td>
                                                            <td>
                                                                <?php if($row['_Approve'] == 0 ){ ?>
                                                                <a href="apporvesub.php?id=<?php echo encrypt($row['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('approve',$encrypt); ?>&tid=<?php echo encrypt($row['_TutorID'],$encrypt); ?>">Approve</a>
                                                                <?php }else{
                                                                        echo "Approved";
                                                                    } 
                                                                ?>
                                                            </td>
                                                        </tr>                                                        
                                            <?php   }
                                                ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                         <button id="btncancle" class="btn btn-primary">Back</button>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "alltutor.php";
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
