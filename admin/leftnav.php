	<?php
	$current_file = basename($_SERVER['PHP_SELF']);
    ?>
    <nav id="main_menu">
                <div class="menu_wrapper">
                    <?php
                        if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                        {
                            ?>
                                 <ul>
                                    <li class="first_level <?php if ($current_file == "dashboard.php")  {  echo "section_active";  } ?>">
                                        <a href="dashboard.php">
                                            <span class="icon_house_alt first_level_icon"></span>
                                            <span class="menu-title">Dashboard</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "allcmspages.php")  {  echo "section_active";  } ?>">
                                        <a href="allcmspages.php">
                                            <span class="first_level_icon icon_globe-2"></span>
                                            <span class="menu-title">CMS Pages</span>
                                        </a>
                                        
                                    </li>
                                </ul>
                            <?php
                            
                        }else
                        {
                            ?>
                               <ul>
                                    <li class="first_level <?php if ($current_file == "dashboard.php")  {  echo "section_active";  } ?>">
                                        <a href="dashboard.php">
                                            <span class="icon_house_alt first_level_icon"></span>
                                            <span class="menu-title">Dashboard</span>
                                        </a>
                                    </li>
                                    <li class="first_level">
                                        <a href="javascript:void(0)">
                                            <span class="el-icon-group first_level_icon"></span>
                                            <span class="menu-title">Users</span>
                                        </a>
                                        <ul>
                                            <li class="submenu-title">Users</li>
                                            <li><a <?php if ($current_file == "allclient.php" || $current_file == "edit-client.php" || $current_file == "book_lesson.php" || $current_file == "actuallessons.php" || $current_file == "viewbookings.php" || $current_file == "generateinvoice.php" )  { ?> class="act_nav" <?php } ?> href="allclient.php">Families</a></li>
                                            <li><a href="alltutor.php" <?php if ($current_file == "alltutor.php" || $current_file == "edit-tutor.php" || $current_file == "tutor_availibility.php" || $current_file == "approve_subject.php" || $current_file == "booklesson.php" || $current_file == "viewbookedlesson.php" || $current_file == "approve_level.php" )  { ?> class="act_nav" <?php } ?>>Staff</a></li>
                                            <?php /* ?>
                                            <li><a href="allpupil.php" <?php if ($current_file == "allpupil.php" || $current_file == "edit-pupil.php") { ?> class="act_nav" <?php } ?>>Children</a></li>

                                            <li><a href="allpupil2.php" <?php if ($current_file == "allpupil.php" || $current_file == "edit-pupil.php") { ?> class="act_nav" <?php } ?>>Children2</a></li>
                                            <?php */ ?>
                                           <!--  <li><a href="allteam.php" <?php if ($current_file == "allteam.php" || $current_file == "edit-team.php") { ?> class="act_nav" <?php } ?>>Winterwood Team Member</a></li> -->
                                        </ul>
                                    </li>
                                    <!-- <li class="first_level">
                                        <a href="javascript:void(0)">
                                            <span class="icon_folder-alt first_level_icon"></span>
                                            <span class="menu-title">Resources</span>
                                        </a>
                                        <ul class="my-menu">
                                            <li class="submenu-title">Resource</li>
                                            <?php 
                                                $sel_rs = "select * from ".$tbname."_resourcetypes";
                                                $rst_rs = mysqli_query($con,$sel_rs);
                                                while($rowrs = mysqli_fetch_assoc($rst_rs))
                                                {
                                                ?>
                                                    <li><a href="resource.php?type=<?php echo encrypt($rowrs['_ID'],$encrypt); ?>" <?php if (($current_file == "resource.php" || $current_file == "edit-resource.php") && $type == $rowrs['_ID'] )  { ?> class="act_nav" <?php } ?>><?php echo $rowrs['_Type']; ?></a></li>
                                                <?php
                                                }
                                                ?>
                                                <li><a href="resourcetype.php"  <?php if (($current_file == "resourcetype.php") && $type == $rowrs['_ID'] )  { ?> class="act_nav" <?php } ?> ><span class="menu-title">Resources Types</span></a></li>
                                        </ul>
                                    </li> -->
                                    <li class="first_level">
                                        <a href="javascript:void(0)">
                                            <span class="el-icon-blogger first_level_icon"></span>
                                            <span class="menu-title">Blogs</span>
                                        </a>
                                        <ul>
                                            <li class="submenu-title">Blogs</li>
                                            <?php 
                                                $sel_rs = "select * from ".$tbname."_blogtype where _Status='Active' ";
                                                $rst_rs = mysqli_query($con,$sel_rs);
                                            while($rowrs = mysqli_fetch_assoc($rst_rs)){ ?>
                                                <li><a href="blog.php?type=<?php echo encrypt($rowrs['_ID'],$encrypt); ?>" <?php if (($current_file == "blog.php" || $current_file == "edit-blog.php") && $type == $rowrs['_ID'] )  { ?> class="act_nav" <?php } ?>><?php echo $rowrs['_Type']; ?></a></li>
                                            <?php
                                            }
                                            ?>
                                            <li><a href="allblogtypes.php"  <?php if (($current_file == "allblogtypes.php") && $type == $rowrs['_ID'] )  { ?> class="act_nav" <?php } ?> ><span class="menu-title">Categories</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="first_level">
                                        <a href="javascript:void(0)">
                                            <span class="icon_lightbulb_alt first_level_icon"></span>
                                            <span class="menu-title">Bookings</span>
                                        </a>
                                        <ul>
                                            <li class="submenu-title">Bookings</li>
                                            <!-- <li><a href="bookedresource.php" <?php if (($current_file == "bookedresource.php" || $current_file == "viewbooking.php") && $type == $rowrs['_ID'] )  { ?> class="act_nav" <?php } ?> >Resource Bookings</a></li> -->
                                            <li><a href="bookedlesson.php" <?php if ($current_file == "bookedlesson.php" )  { ?> class="act_nav" <?php } ?>>Bookings</a></li>
                                           <!--  <li><a href="bookedtrial.php" <?php if ($current_file == "bookedtrial.php" )  { ?> class="act_nav" <?php } ?>>Trial Lesson Bookings</a></li> -->
                                        </ul>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "allcmspages.php")  {  echo "section_active";  } ?>">
                                        <a href="allcmspages.php">
                                            <span class="first_level_icon icon_globe-2"></span>
                                            <span class="menu-title">CMS Pages</span>
                                        </a>
                                        
                                    </li>
                                    <li class="first_level <?php if ($current_file == "subjectareas.php")  {  echo "section_active";  } ?>">
                                        <a href="subjectareas.php">
                                            <span class="icon_documents_alt first_level_icon"></span>
                                            <span class="menu-title">Subjects</span>
                                        </a>
                                    </li>
                                     <li class="first_level <?php if ($current_file == "allprreport.php" || $current_file == "viewreport.php")  {  echo "section_active";  } ?>">
                                        <a href="allprreport.php" >
                                            <span class="social_pinterest bs_ttip first_level_icon" title="" data-original-title=".social_pinterest"></span>
                                            <span class="menu-title">Reports</span>
                                        </a>
                                    </li>
                                    <!--<li class="first_level <?php if ($current_file == "allrequest.php" || $current_file == "viewrequest.php")  {  echo "section_active";  } ?>">
                                        <a href="" >
                                            <span class="icon_mail_alt first_level_icon"></span>
                                            <span class="menu-title">Request Message</span>
                                        </a>
                                    </li> -->
                                    <li class="first_level <?php if ($current_file == "levels.php" || $current_file == "edit-levels.php")  {  echo "section_active";  } ?>">
                                        <a href="levels.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Levels</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "problem.php" || $current_file == "edit-problem.php")  {  echo "section_active";  } ?>">
                                        <a href="problem.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Problem Areas</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "categories.php" || $current_file == "edit-categories.php")  {  echo "section_active";  } ?>">
                                        <a href="categories.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Child's Categories</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "school.php" || $current_file == "edit-school.php")  {  echo "section_active";  } ?>">
                                        <a href="school.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Venues</span>
                                        </a>
                                    </li>
                                    <?php if(isset($_SESSION['userid']) && $_SESSION['userid'] == '1' ){ ?>
                                    <li class="first_level">
                                        <a href="javascript:void(0)">
                                            <span class="el-icon-group first_level_icon"></span>
                                            <span class="menu-title">Stripe</span>
                                        </a>
                                        <ul>
                                            <li class="submenu-title">Set Strip Account</li>
                                            <li><a <?php if ($current_file == "setstripeaccount.php")  { ?> class="act_nav" <?php } ?> href="setstripeaccount.php">Set Strip Account</a></li>
                                            <li><a href="payments.php" <?php if ($current_file == "payments.php")  { ?> class="act_nav" <?php } ?>>Payment History</a></li>
                                        </ul>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "auditlog.php")  {  echo "section_active";  } ?>">
                                        <a href="auditlog.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Audit Logs</span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li class="first_level <?php if ($current_file == "booking_schedule.php")  {  echo "section_active";  } ?>">
                                        <a href="booking_schedule.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Booking Schedule</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "bookingtypes.php" || $current_file == "edit-bookingtype.php" || $current_file == 'addtimeslot.php')  {  echo "section_active";  } ?>">
                                        <a href="bookingtypes.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">Clubs</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "allcommunication.php" || $current_file ==  "viewcommunication.php")  {  echo "section_active";  } ?>">
                                        <a href="allcommunication.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">All Communications</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "services.php")  {  echo "section_active";  } ?>">
                                        <a href="services.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">All Services</span>
                                        </a>
                                    </li>
                                    <li class="first_level <?php if ($current_file == "filemanager.php")  {  echo "section_active";  } ?>">
                                        <a href="filemanager.php" >
                                            <span class="el-icon-tasks first_level_icon"></span>
                                            <span class="menu-title">File Manager</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            <?php
                        }
                    ?>
                    
                </div>
                <div class="menu_toggle">
                    <span class="icon_menu_toggle">
                        <i class="arrow_carrot-2left toggle_left"></i>
                        <i class="arrow_carrot-2right toggle_right" style="display:none"></i>
                    </span>
                </div>
            </nav>