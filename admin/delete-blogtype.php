<?php 
include_once('db/dbopen.php');


if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

if(isset($_GET['e_action']))
{
	$action = decrypt($_GET['e_action'],$encrypt);
	$id 	= decrypt($_GET['id'],$encrypt);	
	$query 	= "Update ".$tbname."_blogtype SET _Status = 'Inactive' WHERE _ID = $id";
	if($run = mysqli_query($con , $query))
	{
		header("location:allblogtypes.php?result=success");
		exit;
	}
	else
	{
		header("location:allblogtypes.php?result=failed");
		exit;
	}
}
else
{
	header("location:allblogtypes.php");
	exit;
}
?>