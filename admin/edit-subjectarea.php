<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);

if($action == 'edit'){

$title = $sitename." : Edit Subject Area";
$id = decrypt($_REQUEST['id'],$encrypt);
$sel_clint = "select * from ".$tbname."_subjects where _ID='".$id."'";
$rst_clint = mysqli_query($con,$sel_clint);
$row = mysqli_fetch_assoc($rst_clint);
$name = $row['_Name'];
$status = $row['_IsActive'];
$bread = "Edit Subject Area";
$btntext = 'Update';

}else{
    $title = $sitename." : Add Subject Area";
    $bread = "Add Subject Area";
    $btntext = 'Insert';
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="subjectareas.php">All Subject Area</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" name="frmclient" id="frmclient" action="subjectaction.php" method="post">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
                                        
                                        <div class="form-group">
                                            <label for="txtlname" class="col-sm-2 control-label">Subject Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="txtname" name="txtname" value="<?php echo $name;?>" required>
                                                <input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                                <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                            </div>
                                        </div>
                                        
                                        <?php 
                                        if($action == 'edit')
                                        {
                                        ?>
                                            <div class="form-group">
                                                <label for="txtstatus" class="col-sm-2 control-label">Status</label>
                                                <div class="col-sm-10">
                                                     <select class="form-control" name="txtstatus" id="txtstatus" required>
                                                        <option value="">-Select Status-</option>
                                                        <option value="0" <?php echo ($status == '0')?"selected='selected'":''; ?>>Not Active</option>
                                                        <option value="1" <?php echo ($status == '1')?"selected='selected'":''; ?>>Active</option>
                                                     </select>
                                                </div>
                                            </div>
                                        <?php
                                        }else{ ?>
                                                <div class="form-group">
                                                    <label for="txtstatus" class="col-sm-2 control-label">Status</label>
                                                    <div class="col-sm-10">
                                                         <select class="form-control" name="txtstatus" id="txtstatus" required>
                                                            <option value="">-Select Status-</option>
                                                            <option value="0">Not Active</option>
                                                            <option value="1">Active</option>
                                                         </select>
                                                    </div>
                                                </div>
                                        <?php   } ?>
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "subjectareas.php";
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
