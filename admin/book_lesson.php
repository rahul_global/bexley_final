<?php
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

	$tutorid = decrypt($_GET['tid'],$encrypt);
	$clientid = decrypt($_GET['cid'],$encrypt);
	
	$result = decrypt($_GET['result'],$encrypt);

	$query = "SELECT _ID, _Image ,CONCAT(_Firstname ,' ',_Lastname) _Name , _Email , _Password , _Address_1 , _Address_2 , _City , _State , _Country , _Zip , _Phone , _Subject , _Biography FROM ".$tbname."_tutormaster WHERE _ID = $tutorid";
	$run = mysqli_query($con,$query);
	$fetch = mysqli_fetch_assoc($run);
	
	$img_path = "uploads/tutors/";
	
	$img = (isset($fetch['_Image'])&& $fetch['_Image'] != NULL)?$fetch['_Image']:'no_image.png';
	$title = $sitename." : Book Lesson";
?>
<!DOCTYPE html>
<html>
	<?php include 'script_availibility.php'; ?>
	<body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="allclient.php">Clients</a></li><li><?php echo $bread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
							<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
							<?php }else if(isset($result) && $result == 'failed'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
							<?php }else if(isset($result) && $result == 'overlap'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Overlapping Occurred.</div>
							<?php
								}else if(isset($result) && $result == 'already'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Already Booked.</div>
							<?php
								}else if(isset($result) && $result == 'undefined'){ ?>
							  <div role="alert" class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Selected Tutor Is Not Available At This Time.</div>
							  <?php
								}
							?>
                                    <div id="response_msg" class=""></div>
                                   <form class="form-horizontal" role="form" name="book_lesson" id="book_lesson" action="book_lesson_action.php" method="post" enctype="multipart/form-data">
										<input type="hidden" name="tutorid" value="<?php echo encrypt($tutorid,$encrypt); ?>">
										<input type="hidden" name="clientid" value="<?php echo encrypt($clientid,$encrypt); ?>">
                                        <h3 class="heading_a"><span class="heading_text">Book Lessons</span></h3>
                                        <div class="form-group">
											<label for="txtftime" class="col-sm-2 control-label">Tutor Name</label>
											<div class="col-sm-3">
												<h4><?php echo $fetch['_Name']; ?></h4>
											</div>
										</div>
                                        <div class="form-group">
                                            <label for="form-input" class="col-sm-2 control-label">Dates</label>
                                            <div class="col-sm-10">
                                                <div id="page">
													<li class="demo" style="list-style: none;">
														<div class="box"  id="from--input">
															<input type="text" name="from-input" id="from-input" required>
														</div>
														<div class="code-box" style="display:none">
															<pre class="code prettyprint">
																$('#from-input').multiDatesPicker({dateFormat: 'yy-mm-dd', minDate: new Date()});
															</pre>
														</div>
													</li>
												</div>
                                            </div>
                                        </div>                                        
										<div class="form-group">
											<label for="txtftime" class="col-sm-2 control-label">From Time</label>
											<div class="col-sm-3">
												<input id="timeformatExample1" type="text" class="time" name="txtftime" required/> 
											</div>
										</div>
										<div class="form-group">
											<label for="txtttime" class="col-sm-2 control-label">To Time</label>
											<div class="col-sm-3">
												<input name="txtttime" id="timeformatExample2" type="text" class="time" required/>
											</div>
										</div>
										<div class="form-group">
											<label for="action_type" class="col-sm-2 control-label">From Time</label>
											<div class="col-sm-3">
												<select name="action_type" id="action_type" required>
													<option value="">-Select Action-</option>
													<option value="add">Add</option>
													<option value="cancel">Cancel Lesson</option>
												</select>
											</div>
										</div>
										
										<script type="text/javascript">
											$(function() {
											$('#timeformatExample1').timepicker({ 'timeFormat': 'H:i' });
											$('#timeformatExample2').timepicker({ 'timeFormat': 'H:i' });
											//$('#timeformatExample2').timepicker({ 'timeFormat': 'h:i A' });
											})
										</script>										
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="Go" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
									<div class="page" style="text-align: center;">
										<div style="width:100%; max-width:600px; display:inline-block;">
											<div class="monthly" id="mycalendar"></div>
										</div>
									</div>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript" src="js/monthly.js"></script>
<script type="text/javascript">
	$(window).load( function() {

		$('#mycalendar').monthly({
			mode: 'event',
			//jsonUrl: 'events.json',
			//dataType: 'json'
			xmlUrl: '../xml/<?php echo $tutorid; ?>.xml'
		});

		$('#mycalendar2').monthly({
			mode: 'picker',
			target: '#mytarget',
			setWidth: '250px',
			startHidden: true,
			showTrigger: '#mytarget',
			stylePast: true,
			disablePast: true
		});

	switch(window.location.protocol) {
	case 'http:':
	case 'https:':
	// running on a server, should be good.
	break;
	case 'file:':
	alert('Just a heads-up, events will not work when run locally.');
	}

	});
</script>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "allclient.php";
            return false;
        });
    });
</script> 
    </body>
</html>