<?php 
require_once('db/dbopen.php');
$action 			= decrypt($_REQUEST['e_action'],$encrypt);
/*echo "<pre>";
print_r($_REQUEST);
exit;
*/
	$txtcarer            = $_POST['carer'];
    $txtclname           = replaceSpecialChar($_POST['txtclname']);
	$txtcfname           = replaceSpecialChar($_POST['txtcfname']);
	$txtfsurname         = replaceSpecialChar($_POST['txtfsurname']);
	$dob                 = $_POST['txtdob'];
	$dobcon              = str_replace("/", "-", $dob);
	$txtdob              = date('Y-m-d',strtotime($dobcon));
	$txtethnicity        = $_POST['txtethnicity'];
	$txtgender           = replaceSpecialChar($_POST['txtgender']);
	$txtschool           = $_POST['txtschool'];
	$txtresidential      = replaceSpecialChar($_POST['txtresidential']);
	$txtparent           = $_POST['txtparent'];
	$txttitle            = replaceSpecialChar($_POST['txttitle']);
	$txtfname            = replaceSpecialChar($_POST['txtfname']);
	$txtlname            = replaceSpecialChar($_POST['txtlname']);
	//$txtfullname       = $txttitle.' '.$txtfname.' '.$txtlname;
	$txtphone            = replaceSpecialChar($_POST['txtphone']);
	$txtmobile           = replaceSpecialChar($_POST['txtmobile']);
	$email               = $_POST['txtemail'];
	$txtemergname        = replaceSpecialChar($_POST['txtemergname']);
	$txtemergnum         = replaceSpecialChar($_POST['txtemergnum']);
	$txtriscassess       = $_POST['txtriscassess'];
	//$txtraworkername   = replaceSpecialChar($_POST['txtraworkername']);
	//$txtrasocialnum    = replaceSpecialChar($_POST['txtrasocialnum']);
	$txtcservice         = $_POST['txtcservice'];
	$txtcsworkername     = replaceSpecialChar($_POST['txtcsworkername']);
	$txtcssocialnum      = replaceSpecialChar($_POST['txtcssocialnum']);
	$txtstatement        = $_POST['txtstatement'];
	$txtnumhr            = replaceSpecialChar($_POST['txtnumhr']);
	$txtborough          = $_POST['txtborough'];
	$txtnicname          = replaceSpecialChar($_POST['txtnicname']);
	$txtdiagnosis        = replaceSpecialChar($_POST['txtdiagnosis']);
	$txtcommunicateby    = replaceSpecialChar($_POST['txtcommunicateby']);
	$txtmobility         = replaceSpecialChar($_POST['txtmobility']);
	$txtmedicalneeds     = $_POST['txtmedicalneeds'];
	$txtmedicalindicate  = replaceSpecialChar($_POST['txtmedicalindicate']);
	$txtpersonalneeds    = $_POST['txtpersonalneeds'];
	$txtpersonalindicate = replaceSpecialChar($_POST['txtpersonalindicate']);
	$txtchildbehave      = replaceSpecialChar($_POST['txtchildbehave']);
	$txtchildanxious     = replaceSpecialChar($_POST['txtchildanxious']);
	$txtchildhappy       = replaceSpecialChar($_POST['txtchildhappy']);
	$txtchildactivity    = replaceSpecialChar($_POST['txtchildactivity']);
	$txtchilddislike     = replaceSpecialChar($_POST['txtchilddislike']);
	$txtchildcalm        = replaceSpecialChar($_POST['txtchildcalm']);
	$txtchildfood        = replaceSpecialChar($_POST['txtchildfood']);
	$txtchilddislikefood = replaceSpecialChar($_POST['txtchilddislikefood']);
	$txtchildallergic    = replaceSpecialChar($_POST['txtchildallergic']);
	$txtchildabout       = replaceSpecialChar($_POST['txtchildabout']);
	$txtchildgpcontact   = replaceSpecialChar($_POST['txtchildgpcontact']);
	$txtchildhospital    = replaceSpecialChar($_POST['txtchildhospital']);
	$txtchildpermition   = $_POST['txtchildpermition'];
	$txtchildpublicity   = $_POST['txtchildpublicity'];
	$txtchildtakepic     = $_POST['txtchildtakepic'];
	$txtchildsuncream    = $_POST['txtchildsuncream'];
	$txtchildescort      = $_POST['txtchildescort'];
	$txtchildaid         = $_POST['txtchildaid'];
	$txtchildswim        = $_POST['txtchildswim'];
	$txtchildgetswim     = $_POST['txtchildgetswim'];

	if(isset($_POST['txtservices']) && !empty($_POST['txtservices']))
	{
		$txtservices = implode(',',$_POST['txtservices']);
	}
	$extraservice     = replaceSpecialChar($_REQUEST['txtextraser']);
	$txtotherservice1 = replaceSpecialChar($_POST['txtotherservice1']);
	$txtotherservice2 = replaceSpecialChar($_POST['txtotherservice2']);
	$txtotherservice3 = replaceSpecialChar($_POST['txtotherservice3']);
	$txtbudget        = $_POST['txtbudget'];
	$txtallowance     = $_POST['txtallowance'];
	$txtsnap          = replaceSpecialChar($_POST['txtsnap']);
	$txtpositive      = replaceSpecialChar($_POST['txtpositive']);
	$txtinfo          = replaceSpecialChar($_POST['txtinfo']);
	$profile_id = $_REQUEST['profile_id'];
	
	
	if($action == "edit")
	{
		$pupil_id 			= decrypt($_POST['pupil_id'],$encrypt);

		if($txtcfname != '' && $txtclname != '')
		{
			$check_qry = "SELECT * FROM ".$tbname."_pupilmaster WHERE _ID = $pupil_id";
			$run_check = mysqli_query($con, $check_qry);
			$num = mysqli_num_rows($run_check);
			if($num > 0)
			{
				$fetch_data = mysqli_fetch_assoc($run_check);
				
				$query 	= "UPDATE ".$tbname."_pupilmaster SET ";
				
				if(isset($txtcfname) && $txtcfname!= ''){
					$query .= "`_FirstName` = '".$txtcfname."' ,";
				}else{
					$query .= "`_FirstName` = null,";
				}

				if(isset($txtclname) && $txtclname!= ''){
					$query .= "`_LastName` = '".$txtclname."' ,";
				}else{
					$query .= "`_LastName` = null,";
				}
				/*if(isset($txtcarer) && $txtcarer!= '')
				{
					$query .= "`_ClientID` = '".$txtcarer."' ,";
				}
				else
				{
					$query .= "`_ClientID` = null,";
				}*/
				if(isset($txtfsurname) && $txtfsurname!= ''){

					$query .= "`_FamilySurname` = '".$txtfsurname."' ,";

				}else{

					$query .= "`_FamilySurname` = null,";

				}		
				if(isset($txtdob) && $txtdob!= ''){
					
					$query .= "`_DOB` = '".$txtdob."' ,";

				}else{
					
					$query .= "`_DOB` = null,";

				}
				if(isset($txtethnicity) && $txtethnicity!= ''){
					
					$query .= "`_Ethnicity` = '".$txtethnicity."' ,";

				}else{
					
					$query .= "`_Ethnicity` = null,";

				}
				if(isset($txtgender) && $txtgender!= ''){
					
					$query .= "`_Gender` = '".$txtgender."' ,";

				}else{
					
					$query .= "`_Gender` = null,";

				}
				if(isset($extraservice) && $extraservice!= ''){
					
					$query .= "_Extraservice = '".$extraservice."' ,";

				}else{
					
					$query .= "_Extraservice = null,";

				}

				if(isset($profile_id) && $profile_id!= ''){
					$query .= "_Profileid = '".$profile_id."' ,";
				}else{
					$query .= "_Profileid = null,";
				}

				if(isset($txtschool) && $txtschool!= ''){
					
					$query .= "`_School`= ".$txtschool." ,";

				}else{
					
					$query .= "`_School`=null,";

				}
				if(isset($txtresidential) && $txtresidential!= ''){
					
					$query .= "`_Residential` = '".$txtresidential."' ,";

				}else{
					
					$query .= "`_Residential` = null,";

				}
				if(isset($txtparent) && $txtparent!= ''){
					
					$query .= "`_Carer` = '".$txtparent."' ,";

				}else{
					
					$query .= "`_Carer` = null,";

				}
				
				if(isset($txttitle) && $txttitle!= ''){
					
					$query .= "`_CarerTitle` = '".$txttitle."' ,";

				}else{
					
					$query .= "`_CarerTitle` = null,";

				}
				
				if(isset($txtfname) && $txtfname!= ''){
					
					$query .= "`_CarerFirstName` = '".$txtfname."' ,";

				}else{
					
					$query .= "`_CarerFirstName` = null,";

				}
				
				if(isset($txtlname) && $txtlname!= ''){
					
					$query .= "`_CarerLastName` = '".$txtlname."' ,";

				}else{
					
					$query .= "`_CarerLastName` = null,";

				}
				
				if(isset($txtphone) && $txtphone!= ''){
					
					$query .= "`_CarerPhone` = '".$txtphone."' ,";

				}else{
					
					$query .= "`_CarerPhone` = null,";

				}
				
				if(isset($txtmobile) && $txtmobile!= ''){
					
					$query .= "`_CarerMobile` = '".$txtmobile."' ,";

				}else{
					
					$query .= "`_CarerMobile` = null,";

				}
				if(isset($email) && $email!= ''){
					
					$query .= "`_Email` = '".$email."' ,";

				}else{
					
					$query .= "`_Email` = null,";

				}
				if(isset($txtemergname) && $txtemergname!= ''){
					
					$query .= "`_EmerContactName` = '".$txtemergname."' ,";

				}else{
					
					$query .= "`_EmerContactName` = null,";

				}
				
				if(isset($txtemergnum) && $txtemergnum!= ''){
					
					$query .= "`_EmerContactNumber`='".$txtemergnum."' ,";

				}else{
					
					$query .= "`_EmerContactNumber` = null,";

				}
				if(isset($txtriscassess) && $txtriscassess!= ''){
					
					$query .= "`_RiscAssessment` = '".$txtriscassess."' ,";

				}else{
					
					$query .= "`_RiscAssessment` = null,";

				}
				/*if(isset($txtraworkername) && $txtraworkername!= ''){
					
					$query .= "`_RASocialName` = '".$txtraworkername."' ,";

				}else{
					*/
					$query .= "`_RASocialName` = null,";

				/*}
				if(isset($txtrasocialnum) && $txtrasocialnum!= ''){
					
					$query .= "`_RASocialContact` ='".$txtrasocialnum."' ,";

				}else{
					*/
					$query .= "`_RASocialContact` = null,";

				/* } */
				if(isset($txtcservice) && $txtcservice!= ''){
					
					$query .= "`_DisableService` = '".$txtcservice."' ,";

				}else{
					
					$query .= "`_DisableService` = null,";

				}
				if(isset($txtcsworkername) && $txtcsworkername!= ''){
					
					$query .= "`_DSSocialName` = '".$txtcsworkername."' ,";

				}else{
					
					$query .= "`_DSSocialName` = null,";

				}
				if(isset($txtcssocialnum) && $txtcssocialnum!= ''){
					
					$query .= "`_DSSocialContact` = '".$txtcssocialnum."' ,";

				}else{
					
					$query .= "`_DSSocialContact` = null,";

				}
				if(isset($txtstatement) && $txtstatement!= ''){
					
					$query .= "`_Statement` = '".$txtstatement."' ,";

				}else{
					
					$query .= "`_Statement` = null,";

				}
				if(isset($txtnumhr) && $txtnumhr!= ''){
					
					$query .= "`_RecHours` = '".$txtnumhr."' ,";

				}else{
					
					$query .= "`_RecHours` = null,";

				}
				if(isset($txtborough) && $txtborough!= ''){
					
					$query .= "`_ReceiveBudget` = '".$txtborough."' ,";

				}else{
					
					$query .= "`_ReceiveBudget` = null,";
				}
				if(isset($txtnicname) && $txtnicname!= ''){

					$query .= "`_NicName` = '".$txtnicname."' ,";

				}else{
					
					$query .= "`_NicName` = null,";

				}
				if(isset($txtdiagnosis) && $txtdiagnosis!= ''){
					
					$query .= "`_MyDiagnosis` = '".$txtdiagnosis."' ,";

				}else{
					
					$query .= "`_MyDiagnosis` = null,";

				}
				if(isset($txtcommunicateby) && $txtcommunicateby!= ''){
					
					$query .= "`_CommunicateBy` = '".$txtcommunicateby."' ,";

				}else{
					
					$query .= "`_CommunicateBy` = null,";

				}
				
				if(isset($txtmobility) && $txtmobility!= ''){
					
					$query .= "`_MyMobility` = '".$txtmobility."' ,";

				}else{
					
					$query .= "`_MyMobility` = null,";

				}
				
				if(isset($txtmedicalneeds) && $txtmedicalneeds!= ''){
					
					$query .= "`_MedicalNeeds` = '".$txtmedicalneeds."' ,";

				}else{
					
					$query .= "`_MedicalNeeds` = null,";

				}
				
				if(isset($txtmedicalindicate) && $txtmedicalindicate!= ''){
					
					$query .= "`_MedicationFor` = '".$txtmedicalindicate."' ,";

				}else{
					
					$query .= "`_MedicationFor` = null,";

				}
				
				if(isset($txtpersonalneeds) && $txtpersonalneeds!= ''){
					
					$query .= "`_PersonalCareNeed` = '".$txtpersonalneeds."' ,";

				}else{
					
					$query .= "`_PersonalCareNeed` = null,";

				}
				if(isset($txtpersonalindicate) && $txtpersonalindicate!= ''){
					
					$query .= "`_PersonalCareFor` = '".$txtpersonalindicate."' ,";

				}else{
					
					$query .= "`_PersonalCareFor` = null,";

				}
				if(isset($txtchildbehave) && $txtchildbehave!= ''){
					
					$query .= "`_Behaviour` = '".$txtchildbehave."' ,";

				}else{
					
					$query .= "`_Behaviour` = null,";

				}
				if(isset($txtchildanxious) && $txtchildanxious!= ''){
					
					$query .= "`_WhenAnxious` = '".$txtchildanxious."' ,";

				}else{
					
					$query .= "`_WhenAnxious` = null,";

				}
				if(isset($txtchildhappy) && $txtchildhappy!= ''){
					
					$query .= "`_WhenHappy` = '".$txtchildhappy."' ,";

				}else{
					
					$query .= "`_WhenHappy` = null,";

				}
				if(isset($txtchildactivity) && $txtchildactivity!= ''){
					
					$query .= "`_LikedActivities` = '".$txtchildactivity."' ,";

				}else{
					
					$query .= "`_LikedActivities` = null,";

				}
				if(isset($txtchilddislike) && $txtchilddislike!= ''){
					
					$query .= "`_DislikedActivities` = '".$txtchilddislike."' ,";

				}else{
					
					$query .= "`_DislikedActivities` = null,";

				}
				if(isset($txtchildcalm) && $txtchildcalm!= ''){
					
					$query .= "`_CalmBy` = '".$txtchildcalm."' ,";

				}else{
					
					$query .= "`_CalmBy` = null,";

				}
				if(isset($txtchildfood) && $txtchildfood!= ''){
					
					$query .= "`_LikedFood` = '".$txtchildfood."' ,";

				}else{
					
					$query .= "`_LikedFood` = null,";
				}
				if(isset($txtchilddislikefood) && $txtchilddislikefood!= ''){
					
					$query .= "`_DislikedFood` = '".$txtchilddislikefood."' ,";

				}else{
					
					$query .= "`_DislikedFood` = null,";

				}
				if(isset($txtchildallergic) && $txtchildallergic!= ''){
					
					$query .= "`_Allergic` = '".$txtchildallergic."' ,";

				}else{
					
					$query .= "`_Allergic` = null,";

				}
				if(isset($txtchildabout) && $txtchildabout!= ''){
					
					$query .= "`_AboutMe` = '".$txtchildabout."' ,";

				}else{
					
					$query .= "`_AboutMe` = null,";

				}
				if(isset($txtchildgpcontact) && $txtchildgpcontact!= ''){
					
					$query .= "`_GPContact` = '".$txtchildgpcontact."' ,";

				}else{
					
					$query .= "`_GPContact` = null,";

				}
				if(isset($txtchildhospital) && $txtchildhospital!= ''){
					
					$query .= "`_HospitalConsultant` = '".$txtchildhospital."' ,";

				}else{
					
					$query .= "`_HospitalConsultant` = null,";

				}
				if(isset($txtchildpermition) && $txtchildpermition!= ''){
					
					$query .= "`_OutingPermitted` = '".$txtchildpermition."' ,";

				}else{
					
					$query .= "`_OutingPermitted` = null,";

				}
				if(isset($txtchildpublicity) && $txtchildpublicity!= ''){
					
					$query .= "`_AdvertisePermitted` = '".$txtchildpublicity."' ,";

				}else{
					
					$query .= "`_AdvertisePermitted` = null,";

				}
				if(isset($txtchildtakepic) && $txtchildtakepic!= ''){
					
					$query .= "`_CompletedPassport` = '".$txtchildtakepic."' ,";

				}else{
					
					$query .= "`_CompletedPassport` = null,";

				}
				if(isset($txtchildsuncream) && $txtchildsuncream!= ''){

					$query .= "`_SunCream` = '".$txtchildsuncream."' ,";

				}else{

					$query .= "`_SunCream` = null,";

				}
				if(isset($txtchildescort) && $txtchildescort!= ''){

					$query .= "`_EmergencyEscort` = '".$txtchildescort."' ,";
				
				}else{

					$query .= "`_EmergencyEscort` = null,";
				
				}
				if(isset($txtchildaid) && $txtchildaid!= ''){

					$query .= "`_EmergencyAid` = '".$txtchildaid."' ,";
				
				}else{

					$query .= "`_EmergencyAid` = null,";
				
				}
				if(isset($txtchildswim) && $txtchildswim!= ''){

					$query .= "`_AbleSwim` = '".$txtchildswim."' ,";

				}else{

					$query .= "`_AbleSwim` = null,";

				}
				if(isset($txtchildgetswim) && $txtchildgetswim!= ''){

					$query .= "`_GetPool` = '".$txtchildgetswim."' ,";

				}else{

					$query .= "`_GetPool` = null,";
				}
				/*if(isset($txtservices) && $txtservices!= ''){

					$query .= "`_Services` = '".$txtservices."' ,";
				
				}else{
					*/
					$query .= "`_Services` = null,";
/*
				}
				if(isset($txtotherservice1) && $txtotherservice1!= ''){

					$query .= "`_OtherService1` = '".$txtotherservice1."' ,";

				}else{
*/
					$query .= "`_OtherService1` = null,";
/*
				}
				if(isset($txtotherservice2) && $txtotherservice2!= ''){

					$query .= "`_OtherService2` = '".$txtotherservice2."' ,";

				}else{
*/
					$query .= "`_OtherService2` = null,";
/*
				}
				if(isset($txtotherservice3) && $txtotherservice3!= ''){

					$query .= "`_OtherService3` = '".$txtotherservice3."' ,";

				}else{
*/
					$query .= "`_OtherService3` = null,";

				/* } */
				if(isset($txtbudget) && $txtbudget!= ''){

					$query .= "`_ReceiveBudgetLBB` = '".$txtbudget."' ,";

				}else{

					$query .= "`_ReceiveBudgetLBB` = null,";

				}
				if(isset($txtallowance) && $txtallowance!= ''){

					$query .= "`_DisabilityAllowance` = '".$txtallowance."' ,";

				}else{

					$query .= "`_DisabilityAllowance` = null,";

				}
				if(isset($txtsnap) && $txtsnap!= ''){

					$query .= "`_WhyAccessSNAP` = '".$txtsnap."' ,";

				}else{

					$query .= "`_WhyAccessSNAP` = null,";

				}
				if(isset($txtpositive) && $txtpositive!= ''){

					$query .= "`_WantAchieve` = '".$txtpositive."' ,";

				}else{

					$query .= "`_WantAchieve` = null,";

				}
				if(isset($txtinfo) && $txtinfo!= ''){

					$query .= "`_OtherInfo` = '".$txtinfo."'";

				}else{
					
					$query .= "`_OtherInfo` = null";

				}
			
				$query .= " WHERE _ID = ".$pupil_id;
				
				$run = mysqli_query($con , $query);

				if($run)
				{	
					$sel_rel = "Select * from ".$tbname."_clipup_rel where _PupilID = '".$pupil_id."'";
					$rstrel  = mysqli_query($con,$sel_rel);
					if($txtcarer != ''){

						if(mysqli_num_rows($rstrel) > 0){

							$updrel = "update ".$tbname."_clipup_rel set _ClientID = '".$txtcarer."' where _PupilID = '".$pupil_id."'";
							mysqli_query($con,$updrel);

						}else{

							$insstu = "insert into ".$tbname."_clipup_rel (_ClientID,_PupilID) values ('".$txtcarer."','".$pupil_id."')";
							mysqli_query($con,$insstu);

						}
					}else{
						if(mysqli_num_rows($rstrel) > 0){
							$delrel = "Delete from ".$tbname."_clipup_rel where _PupilID = '".$pupil_id."'";
							mysqli_query($con,$delrel);
						}
					}
					//log create start
						/*$pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$pupil_id;
						$run_pemail = mysqli_query($con , $pemail_query);
						$fetch_pemail = mysqli_fetch_assoc($run_pemail);
						$pupilemail = $fetch_pemail['_Email'];*/
					//	$create_log = auditlog($msg = "Updated Children ".$email);
					//log create end
					
					header("location:allpupil.php?result=".encrypt('success',$encrypt));
					exit;
				}
				else
				{
					header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&id=".encrypt($pupil_id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
					exit;
				}
			}
			else
			{
				header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&id=".encrypt($pupil_id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&id=".encrypt($pupil_id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
			exit;
		}
	}
	else if($action == "add")
	{	

			if($txtcfname != '' && $txtclname != '')
			{
				$query 	= "INSERT INTO ".$tbname."_pupilmaster(_FirstName ,_LastName  , _FamilySurname , _DOB , _Ethnicity , _Gender , _School , _Residential, _Profileid , _Carer , _CarerTitle , _CarerFirstName , _CarerLastName , _CarerPhone , _CarerMobile , _Email , _EmerContactName , _EmerContactNumber , _RiscAssessment , _RASocialName , _RASocialContact , _DisableService , _DSSocialName , _DSSocialContact , _Statement ,_Extraservice, _RecHours , _ReceiveBudget , _NicName , _MyDiagnosis , _CommunicateBy , _MyMobility , _MedicalNeeds , _MedicationFor , _PersonalCareNeed , _PersonalCareFor , _Behaviour , _WhenAnxious , _WhenHappy , _LikedActivities , _DislikedActivities , _CalmBy , _LikedFood , _DislikedFood , _Allergic , _AboutMe , _GPContact , _HospitalConsultant , _OutingPermitted , _AdvertisePermitted , _CompletedPassport , _SunCream , _EmergencyEscort , _EmergencyAid , _AbleSwim , _GetPool , _Services , _OtherService1 , _OtherService2 , _OtherService3 , _ReceiveBudgetLBB , _DisabilityAllowance , _WhyAccessSNAP , _WantAchieve , _OtherInfo) VALUES ( ";
				
				if(isset($txtcfname) && $txtcfname!= ''){
					$query .= "'".$txtcfname."' ,";
				}else{
					$query .= "null,";
				}
				if(isset($txtclname) && $txtclname!= ''){
					$query .= "'".$txtclname."' ,";
				}else{
					$query .= "null,";
				}
				/*if(isset($txtcarer) && $txtcarer!= '')
				{
					$query .= "'".$txtcarer."' ,";
				}
				else
				{
					$query .= "null,";
				}*/
				if(isset($txtfsurname) && $txtfsurname!= '')
				{
					$query .= "'".$txtfsurname."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtdob) && $txtdob!= '')
				{
					$query .= "'".$txtdob."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtethnicity) && $txtethnicity!= '')
				{
					$query .= "'".$txtethnicity."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtgender) && $txtgender!= '')
				{
					$query .= "'".$txtgender."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtschool) && $txtschool!= '')
				{
					$query .= $txtschool." ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtresidential) && $txtresidential!= '')
				{
					$query .= "'".$txtresidential."' ,";
				}
				else
				{
					$query .= "null,";
				}

				if(isset($profile_id) && $profile_id!= '') {
					$query .= "'".$profile_id."' ,";
				} else{
					$query .= "null,";
				}

				if(isset($txtparent) && $txtparent!= '')
				{
					$query .= "'".$txtparent."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txttitle) && $txttitle!= '')
				{
					$query .= "'".$txttitle."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtfname) && $txtfname!= '')
				{
					$query .= "'".$txtfname."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtlname) && $txtlname!= '')
				{
					$query .= "'".$txtlname."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtphone) && $txtphone!= '')
				{
					$query .= "'".$txtphone."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtmobile) && $txtmobile!= '')
				{
					$query .= "'".$txtmobile."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($email) && $email!= '')
				{
					$query .= "'".$email."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtemergname) && $txtemergname!= '')
				{
					$query .= "'".$txtemergname."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtemergnum) && $txtemergnum!= '')
				{
					$query .= "'".$txtemergnum."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtriscassess) && $txtriscassess!= '')
				{
					$query .= "'".$txtriscassess."' ,";
				}
				else
				{
					$query .= "null,";
				}
				/*if(isset($txtraworkername) && $txtraworkername!= '')
				{
					$query .= "'".$txtraworkername."' ,";
				}
				else
				{*/
					$query .= "null,";
				/*}
				if(isset($txtrasocialnum) && $txtrasocialnum!= '')
				{
					$query .= "'".$txtrasocialnum."' ,";
				}
				else
				{*/
					$query .= "null,";
				/* } */
				if(isset($txtcservice) && $txtcservice!= '')
				{
					$query .= "'".$txtcservice."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtcsworkername) && $txtcsworkername!= '')
				{
					$query .= "'".$txtcsworkername."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtcssocialnum) && $txtcssocialnum!= '')
				{
					$query .= "'".$txtcssocialnum."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtstatement) && $txtstatement!= '')
				{
					$query .= "'".$txtstatement."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($extraservice) && $extraservice!= '')
				{
					$query .= "'".$extraservice."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtnumhr) && $txtnumhr!= '')
				{
					$query .= "'".$txtnumhr."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtborough) && $txtborough!= '')
				{
					$query .= "'".$txtborough."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtnicname) && $txtnicname!= '')
				{
					$query .= "'".$txtnicname."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtdiagnosis) && $txtdiagnosis!= '')
				{
					$query .= "'".$txtdiagnosis."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtcommunicateby) && $txtcommunicateby!= '')
				{
					$query .= "'".$txtcommunicateby."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtmobility) && $txtmobility!= '')
				{
					$query .= "'".$txtmobility."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtmedicalneeds) && $txtmedicalneeds!= '')
				{
					$query .= "'".$txtmedicalneeds."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtmedicalindicate) && $txtmedicalindicate!= '')
				{
					$query .= "'".$txtmedicalindicate."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtpersonalneeds) && $txtpersonalneeds!= '')
				{
					$query .= "'".$txtpersonalneeds."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtpersonalindicate) && $txtpersonalindicate!= '')
				{
					$query .= "'".$txtpersonalindicate."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildbehave) && $txtchildbehave!= '')
				{
					$query .= "'".$txtchildbehave."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildanxious) && $txtchildanxious!= '')
				{
					$query .= "'".$txtchildanxious."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildhappy) && $txtchildhappy!= '')
				{
					$query .= "'".$txtchildhappy."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildactivity) && $txtchildactivity!= '')
				{
					$query .= "'".$txtchildactivity."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchilddislike) && $txtchilddislike!= '')
				{
					$query .= "'".$txtchilddislike."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildcalm) && $txtchildcalm!= '')
				{
					$query .= "'".$txtchildcalm."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildfood) && $txtchildfood!= '')
				{
					$query .= "'".$txtchildfood."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchilddislikefood) && $txtchilddislikefood!= '')
				{
					$query .= "'".$txtchilddislikefood."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildallergic) && $txtchildallergic!= '')
				{
					$query .= "'".$txtchildallergic."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildabout) && $txtchildabout!= '')
				{
					$query .= "'".$txtchildabout."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildgpcontact) && $txtchildgpcontact!= '')
				{
					$query .= "'".$txtchildgpcontact."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildhospital) && $txtchildhospital!= '')
				{
					$query .= "'".$txtchildhospital."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildpermition) && $txtchildpermition!= '')
				{
					$query .= "'".$txtchildpermition."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildpublicity) && $txtchildpublicity!= '')
				{
					$query .= "'".$txtchildpublicity."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildtakepic) && $txtchildtakepic!= '')
				{
					$query .= "'".$txtchildtakepic."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildsuncream) && $txtchildsuncream!= '')
				{
					$query .= "'".$txtchildsuncream."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildescort) && $txtchildescort!= '')
				{
					$query .= "'".$txtchildescort."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildaid) && $txtchildaid!= '')
				{
					$query .= "'".$txtchildaid."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildswim) && $txtchildswim!= '')
				{
					$query .= "'".$txtchildswim."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtchildgetswim) && $txtchildgetswim!= '')
				{
					$query .= "'".$txtchildgetswim."' ,";
				}
				else
				{
					$query .= "null,";
				}
				/*if(isset($txtservices) && $txtservices!= '')
				{
					$query .= "'".$txtservices."' ,";
				}
				else
				{*/
					$query .= "null,";
				/*}
				if(isset($txtotherservice1) && $txtotherservice1!= '')
				{
					$query .= "'".$txtotherservice1."' ,";
				}
				else
				{*/
					$query .= "null,";
				/*}
				if(isset($txtotherservice2) && $txtotherservice2!= '')
				{
					$query .= "'".$txtotherservice2."' ,";
				}
				else
				{*/
					$query .= "null,";
				/*}
				if(isset($txtotherservice3) && $txtotherservice3!= '')
				{
					$query .= "'".$txtotherservice3."' ,";
				}
				else
				{*/
					$query .= "null,";
				/* } */
				
				if(isset($txtbudget) && $txtbudget!= '')
				{
					$query .= "'".$txtbudget."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtallowance) && $txtallowance!= '')
				{
					$query .= "'".$txtallowance."' ,";
				}
				else
				{
					$query .= "null,";
				}
				
				if(isset($txtsnap) && $txtsnap!= '')
				{
					$query .= "'".$txtsnap."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtpositive) && $txtpositive!= '')
				{
					$query .= "'".$txtpositive."' ,";
				}
				else
				{
					$query .= "null,";
				}
				if(isset($txtinfo) && $txtinfo!= '')
				{
					$query .= "'".$txtinfo."'";
				}
				else
				{
					$query .= "null";
				}
			
			$query .= ")";
			//echo $query;exit;
			$run 	= mysqli_query($con , $query);
			$lid = mysqli_insert_id($con);

			
			if($run)
			{
				$last_id = mysqli_insert_id($con);

				if($txtcarer != '')
					{
							$insstu = "insert into ".$tbname."_clipup_rel (_ClientID,_PupilID) values ('".$txtcarer."','".$last_id."')";
							mysqli_query($con,$insstu);
					}
				//log create start
				/*	$create_log = auditlog($msg = "Add Children ".$email);*/
				//log create end
				header("location:allpupil.php?result=".encrypt('success',$encrypt));
				exit;
			}
			else
			{
				header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&e_action=".encrypt('add',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&e_action=".encrypt('add',$encrypt));
			exit;
		}
	}
	else if($action == 'delete')
	{
		$id 	= decrypt($_GET['id'],$encrypt);	
		
		//log create start
		$pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$id;
		$run_pemail   = mysqli_query($con , $pemail_query);
		$fetch_pemail = mysqli_fetch_assoc($run_pemail);
		$pupilemail   = $fetch_pemail['_Email'];
		$create_log   = auditlog($msg = "Delete Children ".$pupilemail);
		//log create end
		
		$query 	= "DELETE FROM ".$tbname."_pupilmaster WHERE _ID = $id";
		if($run = mysqli_query($con , $query))
		{
			$delete_rel = "DELETE FROM  ".$tbname."_pupcate_rel WHERE _PupilID = $id";
			mysqli_query($con , $delete_rel);
			header("location:allpupil.php?result=".encrypt('success',$encrypt));
			exit;
		}
		else
		{
			header("location:allpupil.php?result=".encrypt('failed',$encrypt));
			exit;
		}
	}

?>