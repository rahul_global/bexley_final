<?php
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$subid = array();
$pupilid = array();
$levid = array();
//_Frequency 0 for daily, 1 for weekly, and 2 for monthly
	$lessonid = decrypt($_GET['id'],$encrypt);
	
	$query = "SELECT bl.* , CONCAT(tm._Firstname , ' ' , tm._Lastname) Tutorname , tm._ID Tutorid , CONCAT(pm._Firstname , ' ' , pm._Lastname) Pupilname FROM ".$tbname."_booked_lessons bl LEFT JOIN ".$tbname."_tutormaster tm on tm._ID = bl._TutorID LEFT JOIN ".$tbname."_pupilmaster pm on pm._ID = bl._PupilID WHERE bl._ID = ".$lessonid;
	$run = mysqli_query($con , $query);
	$fetch = mysqli_fetch_assoc($run);
	
	$tutorid = $fetch['_TutorID'];
	$pupilid = $fetch['_PupilID'];

	$selsub = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$tutorid."' and _Approve = '1'";
	$rstsub = mysqli_query($con,$selsub);
	if(mysqli_num_rows($rstsub) > 0){
		
		while($rssub = mysqli_fetch_assoc($rstsub)){
			array_push($subid, $rssub['_SubjectID']);
		}  
		
	}
	$sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$tutorid."' and _Approve = '1'";
	$rstlev = mysqli_query($con,$sellev);
	if(mysqli_num_rows($rstlev) > 0){
		
		while($rssub = mysqli_fetch_assoc($rstlev)){
			array_push($levid, $rssub['_LevelID']);
		}  
		
	}
	
	$title = $sitename." : Edit Booked Lesson";
	$bread = "Edit Booked Lesson";
	$lnkbread = "<a href='edit-tutor.php?id=".$_GET['id']."&e_action=".encrypt('edit',$encrypt)."'>".$fetch['Tutorname']."</a>";
?>
<!DOCTYPE html>
<html>
	<head>
	<?php include 'topscript.php'; ?>
            <!-- date picker -->
	  		<link href="assets/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" media="screen">
             <script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/timepicker.js"></script>
		<link rel="stylesheet" type="text/css" href="css/timepicker.css" />
		
	</head>
	<body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">All Staff</a></li><li><?php echo $lnkbread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
							<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
							<?php }else if(isset($result) && $result == 'failed'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
							<?php }else if(isset($result) && $result == 'overlap'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Booking is Already done for this Pupil.</div>
							<?php
								}else if(isset($result) && $result == 'already'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Booking is Already done for this Pupil.</div>
							<?php
								}else if(isset($result) && $result == 'undefined'){ ?>
							  <div role="alert" class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Selected Staff Is Not Available At This Time.</div>
							  <?php
								}
							?>
                                    <div id="response_msg" class=""></div>
                                   <form class="form-horizontal" role="form" name="edit_booked_lesson_frm" id="edit_booked_lesson_frm" action="editbookedlessonaction.php" method="post">
                                    
                                    <div class="row">
                                    	
												<h3 class="heading_a"><span class="heading_text">Edit Lesson</span></h3>
												<div class="form-group">
											<label for="txtftime" class="col-sm-3 control-label">Name</label>
											<div class="col-sm-5">
												<h4><?php echo $fetch['Tutorname']; ?></h4>
											</div>
										</div>
										<div class="form-group">
                                            <label for="form-input" class="col-sm-3 control-label">Pupil Name</label>
                                            <div class="col-sm-5">
												<h4><?php echo $fetch['Pupilname']; ?></h4>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtsubject" class="col-sm-3 control-label">Select Approved Subject</label>
                                            <div class="col-sm-5">
                                                
												<select class="form-control" name="txtsubject" required oninvalid="this.setCustomValidity('Please Select Approved Subject')">
													<option value="">-Select Approved Subject-</option>
													<?php
													$subjectid = implode(",", $subid);
													$subject_qry = "SELECT _ID , _Name FROM ".$tbname."_subjects WHERE _ID in (".$subjectid.") ";
													$run_subject = mysqli_query($con , $subject_qry);
													while($fetch_subject = mysqli_fetch_assoc($run_subject))
													{
													?>
														<option <?php if($fetch_subject['_ID'] == $fetch['_SubjectID']){ ?> selected <?php }  ?> value="<?php echo $fetch_subject['_ID'];?>"><?php echo $fetch_subject['_Name'];?></option>
													<?php
													}
													?>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
	                                    <div class="form-group">
												<label for="txtlevels" class="col-sm-3 control-label">Select Approved Level&nbsp;</label>
												<div class="col-sm-5">
													<select name="txtlevels" class="form-control" required oninvalid="this.setCustomValidity('Please Select Approved Level')">
														<option value="">-Select Approved Level-</option>
														<?php
														$levelid = implode(",",$levid);
														$sellevels = "Select _ID,_Title from ".$tbname."_levels where _ID in (".$levelid.")";
														$rstlevels = mysqli_query($con,$sellevels);
														while($rowlevel = mysqli_fetch_assoc($rstlevels)){
															?>
															<option <?php if($rowlevel['_ID'] == $fetch['_LevelID']){ ?> selected <?php } ?> value="<?php echo $rowlevel['_ID']; ?>"><?php echo $rowlevel['_Title']; ?></option>

												<?php 	} 
														?>
													</select>
												</div>
												
											</div>
												 <div class="form-group">
		                                            <label for="form-input" class="col-sm-3 control-label">Date</label>
		                                            <div class="col-sm-5">
		                                                <input type="text" class="form-control" id="dp_basic" name="txttrdate"  value = "<?php echo $fetch['_Date'];?>" required>
		                                            </div>
		                                        </div>                                        
												<div class="form-group">
													<label for="txtftime" class="col-sm-3 control-label">From Time</label>
													<div class="col-sm-5">
														<input id="timeformatExample1" type="text" class="time" name="txttrftime" value="<?php echo $fetch['_Fromtime'];?>" required style="width: 100%;" /> 
													</div>
												</div>
												<div class="form-group">
													<label for="txtttime" class="col-sm-3 control-label">To Time</label>
													<div class="col-sm-5">
														<input name="txttrttime" id="timeformatExample2" type="text" class="time" value="<?php echo $fetch['_Totime'];?>" required style="width: 100%;"/>
													</div>
												</div>
												
												<input type="hidden" name="lessonid" value="<?php echo encrypt($lessonid,$encrypt); ?>">
												<input type="hidden" name="tutorid" value="<?php echo encrypt($fetch['Tutorid'],$encrypt); ?>">
												<input type="hidden" name="e_action" value="<?php echo encrypt('edit',$encrypt); ?>">
											
										
										<div class="form-group text-center" id="btngroup" >
                                            <div class="col-sm-10">
                                                <input type="Submit" class="btn-primary btn" value="Go" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle">Cancel</button>
                                            </div>
                                        </div>
										
                                    </div>
								</form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "viewbookedlesson.php?id=<?php echo encrypt($tutorid ,$encrypt); ?>";
            return false;
        });
        /*$("#txttype").change(function(){
        	//alert($(this).val());
        	var type = $(this).val();
        	if(type == '1'){
        		$("#trial").show();
        		$("#actual").hide();
        		$("#btngroup").show();

        	}else if(type == '2'){
        		$("#trial").hide();
	       		$("#actual").show();
	       		$("#btngroup").show();
        	}else{
        		$("#trial").hide();
        		$("#actual").hide();
        		$("#btngroup").hide();
        	}
        });

        // form submition
        $("#btnsubmit").click(function(){
        	var pupil = $("#txtpupil").val();
        	var type = $("#txttype").val();
        	if(pupil == ""){
        		alert("Please Select Pupil");
        		return false;
        	}else{
        		if(type == "1"){
        			$("#trpupil").val(pupil);
        			$("#book_trial").submit();
        		}else{
        			$("#acpupil").val(pupil);
        			$("#book_actual").submit();
        		}
        	}

        });*/
    });
</script> 
<script type="text/javascript">
		$(function() {
			var date = Date();
			$('.time').timepicker({ 'timeFormat': 'H:i' });
			$('#dp_basic').datepicker({startDate: date, format: 'dd/mm/yyyy' });
			$('#dp_basic1').datepicker({startDate: date, format: 'dd/mm/yyyy' });
		})
</script>
    </body>
</html>