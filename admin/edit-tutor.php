<?php 
include_once('db/dbopen.php');
if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$btntext 	= "Add";

$bread 		= "Add Staff";
$title 		= $sitename." : Add Staff";
$tutor_subject = array();
if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
	//print_r($_REQUEST);exit;
}
$subid = array();
$pupilid = array();
$levid = array();
if(isset($_GET['e_action']))
{
	$action 	= decrypt($_GET['e_action'],$encrypt);

	if($action == 'edit')
	{
		$title 		= $sitename." : Edit Staff";
		$id 		= decrypt($_GET['id'],$encrypt);
		$tutor_qry 	= "select _ID , _Firstname , _Lastname , _Email , _Password , _Phone , _Address_1 , _Address_2 , _City , _State , _Country , _Zip , _Biography , _Status,_Experience,_Ratephour FROM ".$tbname."_tutormaster where _ID='".$id."'";
		
		$run_tutor 	= mysqli_query($con,$tutor_qry);
		$num_tutor 	= mysqli_num_rows($run_tutor);

		$selpupil = "select * from ".$tbname."_tut_pup_assign where _TutorID = '".$id."'";
		$rstpupil = mysqli_query($con,$selpupil);
		if(mysqli_num_rows($rstpupil) > 0){
			
			while($rspupil = mysqli_fetch_assoc($rstpupil)){
				array_push($pupilid, $rspupil['_PupilID']);
			}  
			
		}
		$selsub = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$id."'";
		$rstsub = mysqli_query($con,$selsub);
		if(mysqli_num_rows($rstsub) > 0){
			
			while($rssub = mysqli_fetch_assoc($rstsub)){
				array_push($subid, $rssub['_SubjectID']);
			}  
			
		}

		$sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$id."'";
		$rstlev = mysqli_query($con,$sellev);
		if(mysqli_num_rows($rstlev) > 0){
			
			while($rssub = mysqli_fetch_assoc($rstlev)){
				array_push($levid, $rssub['_LevelID']);
			}  
			
		}
		if($num_tutor >	0)
		{
			$fetch_tutor = mysqli_fetch_assoc($run_tutor);
			$tutor_subject = explode(',',$fetch_tutor['_Subject']);
			$bread 		= "Edit Staff";
			$btntext 	= 'Update';
			//print_r($fetch_tutor);
		}
		else
		{
			header("location:alltutor.php");
			exit;
		}
	}
	if($bread == "Edit Staff"){
		$ttfullname = $fetch_tutor['_Firstname']." ".$fetch_tutor['_Lastname'];
		$lnkbread = "<a href='edit-tutor.php?id=".$_GET['id']."&e_action=".$_GET['e_action']."'>".$ttfullname."</a>";
	}else{
		$lnkbread = 'Add Staff';
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">All Staff</a></li><li><?php echo $lnkbread; ?></li>        </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($result) && $result == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                                    <?php } else if(isset($result) && $result == 'failed') {?> 
											<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Error!</strong> Error Occurred.</div>
									<?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form class="form-horizontal" role="form" method="post"  name="edit_tutor_frm" id="edit_tutor_frm" action="tutoraction.php">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
										
										<input type="hidden" name="txtaction" id="txtaction" value="<?php echo $action;?>">
										<input type="hidden" name="tutor_id" id="tutor_id" value="<?php echo isset($_GET['id'])?$_GET['id']:''; ?>">

										<?php 
										if($action == 'edit')
										{
										?>
											<div class="form-group">
												<label for="txtstatus" class="col-sm-2 control-label">Status</label>
												<div class="col-sm-9">
													 <select class="form-control" name="txtstatus" id="txtstatus" >
														<option value="0" <?php echo ($fetch_tutor['_Status'] == '0')?"selected='selected'":''; ?>>Pending</option>
														<option value="1" <?php echo ($fetch_tutor['_Status'] == '1')?"selected='selected'":''; ?>>Active</option>
														<option value="2" <?php echo ($fetch_tutor['_Status'] == '2')?"selected='selected'":''; ?>>Reject</option>
													 </select>
												</div>
												<div class="col-sm-1">
													<span class="required_field"></span>
												</div>
											</div>
										<?php
										}
										?>
										
                                        <div class="form-group">
                                            <label for="txtfname" class="col-sm-2 control-label">First Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtfname" name="txtfname" value="<?php echo isset($fetch_tutor['_Firstname'])?$fetch_tutor['_Firstname']:'';?>" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                         
                                        <div class="form-group">
                                            <label for="txtlname" class="col-sm-2 control-label">Last Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtlname" name="txtlname" value="<?php echo isset($fetch_tutor['_Lastname'])?$fetch_tutor['_Lastname']:'';?>" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="txtphone" class="col-sm-2 control-label">Phone</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtphone" name="txtphone" value="<?php echo isset($fetch_tutor['_Phone'])?$fetch_tutor['_Phone']:'';?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
                                         <h3 class="heading_a"><span class="heading_text">Login Credentials</span></h3>
                                        <div class="form-group">
                                            <label for="txtemail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" id="txtemail" name="txtemail" value="<?php echo isset($fetch_tutor['_Email'])?$fetch_tutor['_Email']:'';?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
											<label for="txtpassword" class="col-sm-2 control-label">Password</label>
											<div class="col-sm-9">
												 <input type="text" class="form-control" cols="10" rows="4" name="txtpassword" id="txtpassword" value="<?php echo decrypt($fetch_tutor['_Password'],$encrypt); ?>">
											</div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
										</div>
										<hr/>
										<div class="form-group">
                                            <label for="txtsubject" class="col-sm-2 control-label">Subject</label>
                                            <div class="col-sm-9">
                                                <!--<input type="text" class="form-control" id="txtsubject" name="txtsubject" value="<?php echo isset($fetch_tutor['_Subject'])?$fetch_tutor['_Subject']:'';?>" PLACEHOLDER="SUBJECT">-->
												<select class="form-control" name="txtsubject[]" multiple style="height: 200px;">
													<?php
													$subject_qry = "SELECT _ID , _Name FROM ".$tbname."_subjects WHERE _IsActive = 1 order by _ID = 2 DESC, _ID = 6 DESC ,_ID = 9 DESC,  _Name ASC";
													$run_subject = mysqli_query($con , $subject_qry);
													while($fetch_subject = mysqli_fetch_assoc($run_subject))
													{
													?>
														<option value="<?php echo $fetch_subject['_ID'];?>" <?php if(in_array($fetch_subject['_ID'], $subid)){ ?> SELECTED <?php } ?>><?php echo $fetch_subject['_Name'];?></option>
													<?php
													}
													?>
												</select>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
											<label for="txtlevel" class="col-sm-2 control-label">Levels&nbsp;:</label>
											<div class="col-sm-9">
												<select name="txtlevels[]" class="form-control" multiple style="height: 150px;">
													<?php
													$sellevels = "Select _ID,_Title from ".$tbname."_levels order by _Title asc";
													$rstlevels = mysqli_query($con,$sellevels);
													while($rowlevel = mysqli_fetch_assoc($rstlevels)){
														?>
														<option <?php if(in_array($rowlevel['_ID'], $levid)){ ?>Selected <?php } ?> value="<?php echo $rowlevel['_ID']; ?>"><?php echo $rowlevel['_Title']; ?></option>

											<?php 	} 
													?>
												</select>
											</div>
											
										</div>
										<div class="form-group">
											<label for="txtratep" class="col-sm-2 control-label">Students&nbsp;:</label>
											<div class="col-sm-9">
												<select name="txtstudents[]" class="form-control" multiple style="height: 200px;">
													<?php
													
													$selpupil = "Select _ID,CONCAT(_Firstname,' ',_Lastname) as name from ".$tbname."_pupilmaster order by _Firstname asc";
													$rstpupil = mysqli_query($con,$selpupil);
													while($rowpupil = mysqli_fetch_assoc($rstpupil)){
														?>
														<option <?php if(in_array($rowpupil['_ID'], $pupilid)){ ?>Selected <?php } ?> value="<?php echo $rowpupil['_ID']; ?>"><?php echo $rowpupil['name']; ?></option>

											<?php 	} 
													?>
												</select>
											</div>
											
										</div>
										<div class="form-group">
											<label for="txtbiography" class="col-sm-2 control-label">Biography&nbsp;:</label>
											<div class="col-sm-9">
												<textarea class="form-control" id="txtbiography" name="txtbiography"><?php echo isset($fetch_tutor['_Biography'])?$fetch_tutor['_Biography']:''; ?></textarea>
											</div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
										</div>
										
										<div class="form-group">
											<label for="txtexp" class="col-sm-2 control-label">Experience&nbsp;:</label>
											<div class="col-sm-9">
												<input name="txtexp" class="form-control" type="number" step="0.5" value="<?php echo $fetch_tutor['_Experience']; ?>">
											</div>
											
										</div>
										<div class="form-group">
											<label for="txtratep" class="col-sm-2 control-label">Rate Per Hour&nbsp;:</label>
											<div class="col-sm-9">
												<input name="txtratep" class="form-control" type="text" value="<?php echo $fetch_tutor['_Ratephour']; ?>">
											</div>
											
										</div>
										<div class="form-group">
                                            <label for="txtratep" class="col-sm-2 control-label">Availability&nbsp;:</label>
                                            <div class="col-sm-9">
                                                <input name="txtratep" class="form-control" type="text" value="<?php echo $fetch_tutor['_Available']; ?>">
                                            </div>
                                            
                                        </div>
										

                                         <div class="form-group">
                                            <label for="txtadd1" class="col-sm-2 control-label">Address 1</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtadd1" id="txtadd1" value = "<?php echo isset($fetch_tutor['_Address_1'])?$fetch_tutor['_Address_1']:''; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtadd2" class="col-sm-2 control-label">Address 2</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtadd2" id="txtadd2" value="<?php echo isset($fetch_tutor['_Address_2'])?$fetch_tutor['_Address_2']:''; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="txtcity" class="col-sm-2 control-label">City</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtcity" id="txtcity" value="<?php echo isset($fetch_tutor['_City'])?$fetch_tutor['_City']:''; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtstate" class="col-sm-2 control-label">State</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" name="txtstate" id="txtstate" value="<?php echo isset($fetch_tutor['_State'])?$fetch_tutor['_State']:''; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtcountry" class="col-sm-2 control-label">Country</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" cols="10" rows="4" name="txtcountry" id="txtcountry" value="<?php echo isset($fetch_tutor['_Country'])?$fetch_tutor['_Country']:''; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
										<div class="form-group">
                                            <label for="txtzip" class="col-sm-2 control-label">Postcode</label>
                                            <div class="col-sm-9">
                                                 <input type="text" class="form-control" cols="10" rows="4" name="txtzip" id="txtzip" value="<?php echo isset($fetch_tutor['_Zip'])?$fetch_tutor['_Zip']:''; ?>">
                                            </div>
											<div class="col-sm-1">
												<span class="required_field"></span>
											</div>
                                        </div>
                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
											<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <button class="btn-default btn" id="btncancle" onclick="window.location = 'alltutor.php';return false;">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
    </body>
</html>
