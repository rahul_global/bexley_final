<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}


if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

$title = $sitename." : Blog Types";

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<script>
			$(function() {
				// footable
				yukon_datatables.p_plugins_tables_datatable();
			})
			function confirmdel(){

                    var del=confirm("Are you sure you want to delete this item?");
                    if (del==true){
                    //alert ("record deleted")
                    return true;
                    }else{
                    return false;
                    }

            }
        </script>
		
		<style type="text/css">
        .pending{
            color: #f6b738 !important;
			font-weight : bolder;
        }
        .activex{
            color: #87be4a !important;
			font-weight : bolder;
        }
        .inactive{
            color: #d83b4b !important;
			font-weight : bolder;
        }
        </style>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">Blog Types</a></li>        </ul>
            </nav>
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
						<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
						<?php } else if(isset($result) && $result == 'failed') {?> 
								<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button> <strong>Error!</strong> Error Occurred.</div>
						<?php } ?>
						<div class="" style="float:right;margin:15px;">
							<a href="edit-btype.php?e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Add Type</a>
						</div>
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
										<th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $btype_qry = "select _ID, _Type , _Status FROM ".$tbname."_blogtype";
                                            $run_btype = mysqli_Query($con,$btype_qry);
											$num_btype = mysqli_num_rows($run_btype);
											if($num_btype > 0)
											{
												while ($fetch_btype = mysqli_fetch_assoc($run_btype))
												{ 
													switch($fetch_btype['_Status']){
														case('Active'):
															$cls = 'activex';
															break;
														case('Inactive') :
															$cls = 'inactive';
															break;
														default :
															$cls = 'inactive';
															break;
													}
												?>
													<tr>
														<td><?php echo ucwords($fetch_btype['_Type']); ?></td>
														<td class="<?php echo $cls;?>"><?php echo $fetch_btype['_Status']; ?></td>
														<td><a href="edit-btype.php?id=<?php echo encrypt($fetch_btype['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>" class="btn btn-primary">Edit</a></td>
														<td><a href="blogtypeaction.php?id=<?php echo encrypt($fetch_btype['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary" onclick="return confirmdel();">Delete</a></td>
													</tr>
											<?php   
												}
											}
											?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			<!-- main content -->
        </div>

        <script>
			$(function(){
				$("#delete_btype").click(function(){
					var conf = confirm("Are You Sure Want To Delete?");
					if(conf != true)
					{
						return false;
					}
					return true;
				})
			})
		</script>
        
        
    </body>
</html>
