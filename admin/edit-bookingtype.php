<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);

if($action == 'edit'){

$title = "Bexley Snap CRM : Edit Club";
$id = decrypt($_REQUEST['id'],$encrypt);
$sel_clint = "select * from ".$tbname."_bookingtype where _ID='".$id."'";
$rst_clint = mysqli_query($con,$sel_clint);
$row = mysqli_fetch_assoc($rst_clint);
$name = $row['_Type'];
$cap = $row['_capacity'];
$category = $row['_Category_id'];
$active = $row['_active'];
$notes = $row['_Notes'];
$price = $row['_Price'];
$price2 = $row['_Price2'];
$profile_id = $row['_Profileid'];
$page_id = $row['_PageID'];
$content = $row['_Content'];
$intro_content = $row['_Intro_Content'];
$banner_image = $row['_BannerImage'];
$days = $row['_Days'];
$time = $row['_Time'];
$location = $row['_Location'];
$stuff = $row['_Stuff'];
$emergency = $row['_Emergency'];
$bread = "Edit Club";
$btntext = 'Update';

/* get page id for club */

$name_decode = $name;
/*echo $name_decode;
exit;*/
mysqli_query($con, "set names 'utf8'");
$sql_get_pageid = "SELECT * FROM {$tbname}_cmspages WHERE _Title = '$name_decode' AND IS_Club = '1'";
$query_pageid = mysqli_query($con , $sql_get_pageid);
$numrows_pageid = mysqli_num_rows($query_pageid);

if($numrows_pageid > 0)
{
    $pageid_result = mysqli_fetch_assoc($query_pageid);
    $pageid = $pageid_result['_ID'];    
}else
{
    //echo 'club not exists';
    $pageid = $page_id; 
}
 
/*echo $pageid;
exit;
*//*echo '<pre>';
print_r($pageid_result);
echo '</pre>';
exit;*/
}else{
    $title = "Bexley Snap CRM : Add Club";
    $bread = "Add Club";
    $btntext = 'Insert';
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script type="text/javascript">
            $(function() {
                // wysiwg editor
                yukon_wysiwg.p_forms_extended_elements();
            })
        </script>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="bookingtypes.php">Clubs</a></li><li><?php echo $bread; ?></li> 
                </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-12">
                                    <?php if(isset($_GET['result']) && $_GET['result'] == 'success'){ ?>
                                            <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Data Updated Successfully.</strong></div>
                                    <?php } ?>
                                    <div id="response_msg" class=""></div>
                                    <form enctype="multipart/form-data" class="form-horizontal" role="form" name="frmclient" id="frmclient" action="bookingtypeaction.php" method="post">
                                        <h3 class="heading_a"><span class="heading_text">General info</span></h3>
                                        
                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Club</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txttype" name="txttype" value="<?php echo $name;?>" PLACEHOLDER="Club" required>
                                                <input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action,$encrypt); ?>">
                                                <input type="hidden" name="id" value="<?php echo encrypt($id,$encrypt); ?>" />
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Capacity</label>
                                            <div class="col-sm-9">
                                                <input type="number" class="form-control" id="txtcap" name="txtcap" value="<?php echo $cap;?>" PLACEHOLDER="Capacity" required>
                                            </div>
											<div class="col-sm-1">
												<span class="required_field">*</span>
											</div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Category Id</label>
                                            <div class="col-sm-9">
                                                <input type="number" class="form-control" id="txtcat" name="txtcat" value="<?php echo $category;?>" PLACEHOLDER="Category" required>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Club Profile Id</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="profile_id" name="profile_id">
                                                    <option value="0">Select Club Profile Id</option>
                                                    <option <?php if($profile_id == 1) { echo 'selected';} ?> value="1">Red</option>
                                                    <option <?php if($profile_id == 2) { echo 'selected';} ?> value="2">Blue</option>
                                                    <option <?php if($profile_id == 3) { echo 'selected';} ?> value="3">Green</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Page Id</label>
                                            <div class="col-sm-9">
                                                <input type="number" class="form-control" id="txtpage_id" name="txtpage_id" value="<?php echo $pageid; ?>" PLACEHOLDER="Page ID" required>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Active</label>
                                            <div class="col-sm-9">
                                                <input type="checkbox" class="" id="txtactive" name="txtactive" <?php  if(isset($active) && $active == 1) { ?> checked="checked" <?php } ?> value="1" >
                                            </div>
											
                                        </div>
                                        <div class="form-group">
                                            <label for="txtnotes" class="col-sm-2 control-label">Notes</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="txtnotes" name="txtnotes"><?php echo $notes;?></textarea>
                                            </div>
                                            <div class="col-sm-1">
                                                <span class="required_field">*</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Price</label>
                                            <div class="col-sm-9">
                                                <input type="number" step=0.01 class="form-control" id="txtprice" name="txtprice" value="<?php echo $price;?>" PLACEHOLDER="Price" required>
                                            </div>
                                        </div>
                                        

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Cost2</label>
                                            <div class="col-sm-9">
                                                <input type="number" step=0.01 class="form-control" id="txtprice2" name="txtprice2" value="<?php echo $price2;?>" PLACEHOLDER="Price 2" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Introduction Content</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control ckeditor" id="wysiwg_editor2" name="intro_content"  <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>><?php echo isset($intro_content)?$intro_content:'';?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">intro_Content</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control ckeditor" id="wysiwg_editor" name="wysiwg_editor2"  <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>><?php echo isset($content)?$content:'';?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Banner Image</label>
                                            <div class="col-sm-9">
                                                <?php

                                                    if($banner_image != '' && file_exists($_SERVER['DOCUMENT_ROOT']."/Bexley/admin/banners/".$banner_image))
                                                    {
                                                        if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin')
                                                        {
                                                            ?>
                                                                <img src="<?php echo 'banners/'.$banner_image; ?>" class="img-responsive">
                                                            <?php
                                                        }else
                                                        {
                                                            ?>
                                                        <input type="hidden" name="banner_image" class="form-control" value="<?php echo $banner_image; ?>">
                                                        <img src="<?php echo "banners/".$banner_image; ?>" class="img-responsive" ><a href="bookingtypeaction.php?id=<?php echo encrypt($id,$encrypt);?>&e_action=<?php echo encrypt('delete_img',$encrypt); ?>" >Delete</a>
                                                            <?php
                                                        }
                                                        ?>
                                                        
                                                        <?php   
                                                    }else
                                                    {
                                                        ?>
                                                            <input type="file" name="banner_image" class="form-control" <?php if(isset($_SESSION['usertype']) && $_SESSION['usertype'] != 'admin'){ echo 'disabled'; } ?>>
                                                        <?php
                                                    }
                                                ?>
                                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Days</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtdays" name="txtdays" value="<?php echo $days;?>" PLACEHOLDER="Days">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Time</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txttime" name="txttime" value="<?php echo $time;?>" PLACEHOLDER="Time">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Location</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtlocation" name="txtlocation" value="<?php echo $location;?>" PLACEHOLDER="Location">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Stuff</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtstuff" name="txtstuff" value="<?php echo $stuff;?>" PLACEHOLDER="Stuff" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txttype" class="col-sm-2 control-label">Emergency</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="txtemergency" name="txtemergency" value="<?php echo $emergency;?>" PLACEHOLDER="Emergency">
                                            </div>
                                        </div>

                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <input type="submit" class="btn-primary btn" value="<?php echo $btntext; ?>" name="btnsubmit" id="btnsubmit" />
                                                <a href="bookingtypes.php" class="btn-default btn" id="btncancle">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
            </div>            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>
<script type="text/javascript">
    $(document).ready(function (){
        $("#btncancle").click(function (){
            window.location = "bookingtypes.php";
            return false;
        });
    });
</script>
        
        
        
    </body>
</html>
