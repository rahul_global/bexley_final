<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

$title = $sitename.": Families";
if(isset($_REQUEST['done'])){

$done = decrypt($_REQUEST['done'],$encrypt);

}


?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
                $(function() {
                    // footable
                    yukon_datatables.p_plugins_tables_datatable();
                })
            function confirmdel(){

                var del=confirm("Are you sure you want to delete this item?");
                if (del==true){
                   //alert ("record deleted")
                   return true;
                }else{
                    return false;
                }
                
            }
        </script>
        <style type="text/css">
        .pending{
            color: #f6b738 !important;
            font-weight: bolder;
        }
        .activex{
            color: #87be4a !important;
            font-weight: bolder;
        }
        .inactive{
            color: #d83b4b !important;
            font-weight: bolder;
        }
        </style>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li>Families</li>
                </ul>
            </nav>

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <?php 
                        if($done == "11"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Family Data Updated Successfully.
                            </div>
                    <?php   }
                        if($done == "00"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Family Data Added Successfully.
                            </div>
                    <?php   }
                        if($done == "22"){ ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                <strong>Success!</strong> Family Data Deleted Successfully.
                            </div>
                    <?php   }
                            if($done == "2"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Family Data Not deleted Successfully.
                                    </div>
                    <?php   }
                            if($done == "1"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Family Data Not Updated Successfully.
                                    </div>
                            <?php   }
                            if($done == "01"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Some thing went wrong!</strong> Family Data Not Added Successfully.
                                    </div>
                            <?php   }
                            if($done == "111"){ ?>
                                    <div role="alert" class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Success!</strong> Welcome Mail Sent Successfully.
                                    </div>
                            <?php   }
                             if($done == "222"){ ?>
                                    <div role="alert" class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button>
                                        <strong>Failed!</strong> There is no email id of the Family.
                                    </div>
                            <?php   }
                        ?>   
                 
                                           
                    </div>
                    <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <a href="edit-client.php?e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Add New Family</a>
                    </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-bordered " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Children</th>
                                        <!-- <th>Address</th> -->
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Bookings</th>
                                        <th>Children</th>
                                        <th>Notify</th>
										<th>Communication</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $sel_client = "select * from ".$tbname."_clientmaster ORDER BY _Created DESC";
                                            $rst_client = mysqli_Query($con,$sel_client);
                                            while ($row_client = mysqli_fetch_assoc($rst_client)){
                                                
                                                $address = $row_client['_Address1'].",".$row_client['_Address2'].",".$row_client['_City'].",".$row_client['_State'].",".$row_client['_Country']."-".$row_client['_Postcode']; 
                                                //print_r($alltutes);
                                                if($row_client['_Status'] == "0"){
                                                    $status = "Pending";
                                                    $cls = "pending";
                                                }else if($row_client['_Status'] == "1"){
                                                    $status = "Active";
                                                    $cls = "activex";
                                                }else{
                                                    $status = "Inactive";
                                                    $cls = "inactive";
                                                }


                                                ?>
                                                <tr >
                                                    <td><?php echo $row_client['_Lastname']; ?></td>
                                                    <td><?php echo $row_client['_Email']; ?></td>
                                                    <td><?php echo $row_client['_Phone']; ?></td>
                                                    <td><?php 
                                                        $selpup = "Select cl.*,concat(pm._Firstname,' ',pm._Lastname) as pupil from ".$tbname."_clipup_rel cl left join ".$tbname."_pupilmaster pm on pm._ID = cl._PupilID where cl._ClientID = '".$row_client['_ID']."' ";
                                                        $rstpup = mysqli_query($con,$selpup);
                                                        if(mysqli_num_rows($rstpup) > 0){
                                                            $pupils = array();
                                                            while($rowpup = mysqli_fetch_assoc($rstpup)){
                                                                array_push($pupils, $rowpup['pupil']);
                                                            }
                                                            $pupilname = implode(",", $pupils);
                                                            echo $pupilname;

                                                        }else{
                                                            echo "-";
                                                        }
                                                    ?></td>
                                                    <!-- <td><?php if($row_client['_Address1'] != ''){ echo $address; }else{ echo "-";}  ?></td> -->
                                                    <td class="<?php echo $cls; ?>"><?php echo $status; ?></td>
                                                    <td><?php echo $row_client['_Created'] ? $row_client['_Created'] : '-'; ?></td>
													<td><a href="viewbookings.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>" class="btn btn-primary" <?php if($row_client['_Status'] == "0" || $row_client['_TutorID'] == "0"){echo 'disabled';} ?>>Bookings</a></td>

                                                    <td><a href="allpupil.php?cid=<?php echo encrypt($row_client['_ID'],$encrypt); ?>" class="btn btn-primary" <?php if($row_client['_Status'] == "0" || $row_client['_TutorID'] == "0"){echo 'disabled';} ?>>Children</a></td>

                                                    <td><a href="sendwelcomeclient.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>" class="btn btn-primary">Send Welcome Email</a></td>
													
													<td><a href="addclientcom.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>&type=<?php echo encrypt('0',$encrypt); ?>" class="btn btn-primary">Add Communication</a></td>
                                                    <td><a href="edit-client.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>" class="btn btn-primary">Edit</a></td>
                                                    <td><a href="clientaction.php?id=<?php echo encrypt($row_client['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary" onclick="return confirmdel();">Delete</a></td>
                                                </tr>


                                        <?php   }

                                        ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main content -->
            
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->

        </div>

        
        
        
    </body>
</html>
