<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:login.php");
	exit;
}

if(isset($_GET['result']))
{
	$result = decrypt($_GET['result'],$encrypt);
}

$title = $sitename." : Staff";



?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <?php include 'topscript.php'; ?>
		<script>
			$(function() {
				// footable
				yukon_datatables.p_plugins_tables_datatable();
			})
			function confirmdel(){

                    var del=confirm("Are you sure you want to delete this item?");
                    if (del==true){
                    //alert ("record deleted")
                    return true;
                    }else{
                    return false;
                    }

            }
        </script>
		
		<style type="text/css">
        .pending{
            color: #f6b738 !important;
			font-weight : bolder;
        }
        .activex{
            color: #87be4a !important;
			font-weight : bolder;
        }
        .inactive{
            color: #d83b4b !important;
			font-weight : bolder;
        }
        </style>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            		<header id="main_header">
            			<?php include 'header.php'; ?>		
            		</header>
            	

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="alltutor.php">Staff</a></li>        </ul>
            </nav>
            
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
			<!-- main content -->

            <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
						<?php if(isset($result) && $result == 'success'){ ?>
									<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
							<?php } ?>
							<?php if(isset($result) && $result == 'Failed'){ ?>
									<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
							<?php } ?>
						<div class="" style="float:right;margin:15px;">
							<a href="edit-tutor.php?e_action=<?php echo encrypt('add',$encrypt); ?>" class="btn btn-primary">Add Staff</a>
						</div>
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Approved Levels</th>
                                      	<th>Approved Subjects</th>
                                      	<th>Status</th>
                                      	<th>Approve Subjects</th>
                                      	<th>Approve Levels</th>
										<th>Availability</th>
										<!-- <th>Set Fees</th> -->
										<th>Book Lesson</th> 
										<th>Bookings</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                           $tutor_qry = "select _ID, _Firstname , _Lastname , _Email , _Phone , CONCAT(_Address_1 , ', ' , _Address_2 , ', ' , _City , ', ' , _State , ', ' , _Country , '- ' , _Zip ) Address , _Status FROM ".$tbname."_tutormaster";
                                            $run_tutor = mysqli_Query($con,$tutor_qry);
											$num_tutor = mysqli_num_rows($run_tutor);
											if($num_tutor > 0)
											{
												while ($fetch_tutor = mysqli_fetch_assoc($run_tutor))
												{ 
													switch($fetch_tutor['_Status']){
														case('0') : {
															$status = "Pending";
															$cls = "pending";
															break;
														}
														case('1') : {
															$status = "Active";
															$cls = "activex";
															break;
														}
														case('2') : {
															$status = "Rejected";
															$cls = "inactive";
															break;
														}
														default : {
															$status = "Pending";
															$cls = "pending";
														}
													}
													$selsubid = "select * from ".$tbname."_tutsub_rel where _TutorID = '".$fetch_tutor['_ID']."' and _Approve = '1'";
													$rstselsub = mysqli_query($con,$selsubid);
													$subjects = array();
													while ($rowsubje = mysqli_fetch_assoc($rstselsub)) {
															array_push($subjects,$rowsubje['_SubjectID']);
													}
													$newsub = implode(",", $subjects);

													$sellev = "select * from ".$tbname."_tutlevel_rel where _TutorID = '".$fetch_tutor['_ID']."' and _Approve = '1'";
													$rstlev = mysqli_query($con,$sellev);
													$levid = array();
													if(mysqli_num_rows($rstlev) > 0){
														
														while($rssub = mysqli_fetch_assoc($rstlev)){
															array_push($levid, $rssub['_LevelID']);
														}  
														
													}
													$newlevel = implode(",", $levid);
												?>
													<tr>
														<td><?php echo ucfirst($fetch_tutor['_Firstname'])." ".ucfirst($fetch_tutor['_Lastname']); ?></td>
														<td><?php
																//$subjects = $fetch_tutor['_Subject'];
															if(isset($newlevel) && $newlevel != ""){
																$sellev = "Select _Title from ".$tbname."_levels where _ID in (".$newlevel.")" ;
																$rstlev = mysqli_Query($con,$sellev);
																$lev = array();
																while($rowlev = mysqli_fetch_assoc($rstlev)){
																//echo $rowlev['_Name'].",";
																array_push($lev, $rowlev['_Title']);
																
																	
																}
																	$lev = implode(",", $lev);
																	if($lev != ''){
																		echo $lev;
																	}else{
																		echo "-";
																	}
																}
															?>
														</td>
														<td><?php
																//$subjects = $fetch_tutor['_Subject'];
															if(isset($newsub) && $newsub != ""){
																$selsub = "Select _Name from ".$tbname."_subjects where _ID in (".$newsub.")" ;
																$rstsub = mysqli_Query($con,$selsub);
																$sub = array();
																while($rowsub = mysqli_fetch_assoc($rstsub)){
																//echo $rowsub['_Name'].",";
																array_push($sub, $rowsub['_Name']);
																
																	
																}
																	$sub = implode(",", $sub);
																	if($sub != ''){
																		echo $sub;
																	}else{
																		echo "-";
																	}
																}
															?>
														</td>
														<td class="<?php echo $cls; ?>"><?php echo $status; ?></td>
														<td><a href="approve_subject.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('approve',$encrypt); ?>">Approve Subjects</a></td>
														<td><a href="approve_level.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('approve',$encrypt); ?>">Approve Levels</a></td>
														<td><a href="tutor_availibility.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>" class="btn btn-primary">Set</td>
														<!-- <td><a href="setfees.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>" class="btn btn-primary">Set</a></td> -->
														<td><a href="booklesson.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>" class="btn btn-primary">Book</a></td>
														<td><a href="viewbookedlesson.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>" class="btn btn-primary">View Booking</a></td>
														<td><a href="edit-tutor.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>" class="btn btn-primary">Edit</a></td>
														<td><a href="tutoraction.php?id=<?php echo encrypt($fetch_tutor['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('delete',$encrypt); ?>" class="btn btn-primary" id="delete_tutor" onclick="return confirmdel();">Delete</a></td>
													</tr>
											<?php   
												}
											}
											
											?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			<!-- main content -->
        </div>

    </body>
</html>
