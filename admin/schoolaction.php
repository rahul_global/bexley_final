<?php 
include_once('db/dbopen.php');

if(!isset($_SESSION['userid']) || $_SESSION['userid'] == '')
{
	header("location:index.php");
	exit;
}
$action = decrypt($_REQUEST['e_action'],$encrypt);
$id = decrypt($_REQUEST['id'],$encrypt);
$name = $_REQUEST['txtname'];


if($action == "add"){

			

		$str = "INSERT INTO " . $tbname . "_schools (";
     
     	$str .= " _Name) VALUES(";
     
		if ($name != "")
		 	$str = $str . "'" . replaceSpecialChar($name) . "' ";
		else
		  	$str = $str . "null ";
        
          $str = $str . ") ";

		$rstins = mysqli_query($con,$str);

	if($rstins){
		//log create start
			$create_log = auditlog($msg = "Added New School - ".replaceSpecialChar($name));
		//log create end
		header("location:school.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:school.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}
if($action == "edit"){

		$str = "UPDATE " . $tbname . "_schools SET ";
	    
	    if ($name != "")
			$str = $str . "_Name = '" . replaceSpecialChar($name) . "' ";
		else
	        $str = $str . "_Name = null ";
	
	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		//log create start
			$create_log = auditlog($msg = "Updated School - ".$name);
		//log create end
		header("location:school.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:school.php?done=".encrypt("1",$encrypt));
		exit;	
	}

	

} 
if($action == "delete"){

	$delclient = "delete from ".$tbname."_schools where _ID = '".$id."'";
	$rstdel = mysqli_query($con,$delclient); 


	if($rstdel){
		//log create start
			$create_log = auditlog($msg = "Deleted School - ".$school);
		//log create end
		
		header("location:school.php?done=".encrypt("22",$encrypt));
		exit;
	}else{
		header("location:school.php?done=".encrypt("2",$encrypt));
		exit;	
	}

}
?>