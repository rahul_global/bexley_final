<link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16" />

<style type="text/css">



img.wp-smiley, img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='challengers-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic&#038;ver=4.6.1' type='text/css' media='all' />
<link rel='stylesheet' id='challengers-myfontastic-css'  href='https://file.myfontastic.com/M2uYkCnDNsAV5Sx6GYFaNa/icons.css?ver=4.6.1' type='text/css' media='all' />
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel='stylesheet' id='challengers-slick-css'  href='css/slick.css?ver=4.6.1' type='text/css' media='all' />
<link rel='stylesheet' id='challengers-calendar-css'  href='css/atc-style.css?ver=4.6.1' type='text/css' media='all' />
<link rel='stylesheet' id='challengers-styles-css'  href='css/styles.min.css?ver=4.6.1' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css?ver=4.5.1' type='text/css' media='all' />
<link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/owl.theme.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/owl.transitions.css' type='text/css' media='all' />

<script type='text/javascript' src='js/jquery.min.js?ver=4.6.1'></script>
<script type='text/javascript' src='js/header-scripts.min.js?ver=4.6.1'></script>
<!--<script type='text/javascript' src='js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>-->
<link rel="stylesheet" href="css/bootstrap-timepicker.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript" src="js/bootstrap-timepicker.min.js"></script>

<style type="text/css">



.menu{
	line-height: 3;
	padding: 40px 0px;
	position: relative;
}
.menu > li {
	border-bottom: 1px solid #B7B7B7;
}

.menu > li > a{
	text-decoration:none;
}

.menu > li:last-child {
	border : none;
}
</style>