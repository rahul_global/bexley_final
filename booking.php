<?php
session_start();
if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}
include_once('admin/db/dbopen.php');



$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

$clientid = $_SESSION['frontuserid'];

$pupilqry = "SELECT a.* FROM ".$tbname."_pupilmaster a LEFT JOIN ".$tbname."_clipup_rel b ON a._ID = b._PupilID WHERE b._ClientID = ".$clientid;
/*echo $pupilqry;
exit;*/
$prun = mysqli_query($con , $pupilqry);
$pnum = mysqli_num_rows($prun);
if($pnum > 0){
	$pids_array = array();
	while($pfetch = mysqli_fetch_assoc($prun)){
		$pids_array[] = $pfetch['_ID'];
	}
	$pids = implode(',',$pids_array);
	
	$tutorqry = "SELECT a.* FROM ".$tbname."_tutormaster a LEFT JOIN ".$tbname."_tut_pup_assign b ON a._ID = b._TutorID WHERE b._PupilID IN (".$pids.") GROUP BY a._ID";
	$trun = mysqli_query($con , $tutorqry);
	$tnum = mysqli_num_rows($trun);
	if($tnum > 0){
		$tids_array = array();
		while($tfetch = mysqli_fetch_assoc($trun)){
			$tids_array[] = $tfetch['_ID'];
		}
		$tids = implode(',',$tids_array);
		
		$levelqry = "SELECT a.* FROM ".$tbname."_levels a LEFT JOIN ".$tbname."_tutlevel_rel b ON a._ID = b._LevelID WHERE b._TutorID IN (".$tids.") GROUP BY a._ID ORDER BY a._ID";
		$lrun = mysqli_query($con , $levelqry);
		$lnum = mysqli_num_rows($lrun);
		
		
		$subjectqry = "SELECT a.* FROM ".$tbname."_subjects a LEFT JOIN ".$tbname."_tutsub_rel b ON a._ID = b._SubjectID WHERE b._TutorID IN (".$tids.") GROUP BY a._ID ORDER BY a._Name";
		$srun = mysqli_query($con , $subjectqry);
		$snum = mysqli_num_rows($srun);
	}
} 

$timestamp = time()+date("Z");
$chk=gmdate("Y/m/d H:i:s",$timestamp);

$btqry = "SELECT * FROM ".$tbname."_bookingtype WHERE _active = '1' ORDER BY _Type";
$runbt = mysqli_query($con , $btqry);
$numbt = mysqli_num_rows($runbt);




$bts = array();
$qry1 = "select _Tsids from ".$tbname."_paypalbooking ";
$rs1 = mysqli_query($con,$qry1);
$n1 = mysqli_num_rows($rs1);

if($n1 > 0)
{
	while($row1 = mysqli_fetch_assoc($rs1))
	{
		$tarr = array();
		$tarr = explode(",",$row1['_Tsids']);

		foreach($tarr as $sdf)
		{
			array_push($bts,$sdf);
		}

	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
				.form-control1{height:10px !important;}
				#accordion .form-group > p {
					font-size: 14px;
				}
			</style>
		  <script type="text/javascript">
			$(document).ready(function() {
				var totalclub = <?php echo $numbt; ?>;
				
				for(var a = 0 ; a < totalclub ; a++){
					$("#txtdate"+a).datepicker({ 
						dayOfWeekStart : 1,
						format: 'd-m-yyyy',
						lang:'en',
						minDate: 0,
						/*autoclose:true,*/
						startDate:  new Date(),
						multidate: true
					});
					
					$('#txtftime'+a).timepicker({showMeridian : false}); 
					$('#txtttime'+a).timepicker({showMeridian : false}); 
				}
			});
		  </script>
		  <script type="text/javascript">
				function checktime(cftime , cttime){
				 
					var acftime = cftime;
					var acttime = cttime;
					
					var acstartd = acftime.split(":");
					var acendd = acttime.split(":");
					
					var astart = new Date(0, 0, 0, acstartd[0], acstartd[1], 0);
					var aend = new Date(0, 0, 0, acendd[0], acendd[1], 0);
					
					if(aend.getTime() <= astart.getTime())
					{
					  return false;
					}
					return true;
				} 
			  
				function validate(){
					var err     = '';
					
					if(document.frmbooking.txtpupil.value==''){
					  err += "Please Select Pupil.\n"
					}
					
					/*var totaltt = 0;
					var ttype   = document.getElementsByName('txttype[]');
					
					var datetxt = '';
					
					var datepicks = [];
					
					for(tt = 0 ; tt < ttype.length ; tt++){
						if(ttype[tt].checked == true){
							totaltt = 1;
							//alert(ttype[tt]);
							datepicks.push(ttype[tt].value);
						}
					}
					
					if(totaltt == 0){
						err += "Please Select Booking Type.\n";
					}
					else {
						
						//alert(datepicks.length);
						var tderr1  = 0;
						var tderr2 = 0;
						var tderr3 = 0;
						var tderr4 = 0;
						var tderr5 = 0;
						for(var td = 0; td < datepicks.length ; td++){
							if(document.getElementById("txtdate"+datepicks[td]).value == ''){
								tderr1 = 1;
							}
							//alert(document.getElementById("txtdate"+datepicks[td]).value);
							
							
							if(document.getElementById("txtftime"+datepicks[td]).value == ''){
							  tderr2 = 2;
							}
							if(document.getElementById("txtttime"+datepicks[td]).value == ''){
							  tderr3 = 3;
							}
							if(document.getElementById("txtftime"+datepicks[td]).value != '' && document.getElementById("txtttime"+datepicks[td]).value != ''){

							  if(checktime(document.getElementById("txtftime"+datepicks[td]).value , document.getElementById("txtttime"+datepicks[td]).value) == false){
								 tderr4 = 4;
							  }else{

								var acfromTime = document.getElementById("txtftime"+datepicks[td]).value;
								var actoTime = document.getElementById("txtttime"+datepicks[td]).value;
								
								start = acfromTime.split(":");
								end = actoTime.split(":");
								var startDate = new Date(0, 0, 0, start[0], start[1], 0);
								var endDate = new Date(0, 0, 0, end[0], end[1], 0);
								var diff = endDate.getTime() - startDate.getTime();
								var hours = Math.floor(diff / 1000 / 60 / 60);
								
								if(hours < 1){
									tderr5 = 5;
								}
							  }
							}
							
							
						}
					}
					
					
					if(tderr1 == 1){
						err += 'Please Select Lesson Date For All Selected Club.\n';
					}
					if(tderr2 == 2){
						err += "Please Select From Time.\n";
					}
					if(tderr3 == 3){
						err += "Please Select To Time.\n";
					}
					if(tderr4 == 4){
						err += "Please Select To time greater than the from Time For lesson booking.\n";
					}
					if(tderr5 == 5){
						err += "Please Select Lesson Booking Duration at least One Hour.\n";
					}*/
					
					
					if(err == "")
					{
						return true;
					}
					else
					{
						alert(err);
						return false;
					}
				}
				  
				 
				function getDateTime() {
					var now     = new Date(); 
					var year    = now.getFullYear();
					var month   = now.getMonth()+1; 
					var day     = now.getDate();
					var hour    = now.getHours();
					var minute  = now.getMinutes();
					var second  = now.getSeconds(); 
					if(month.toString().length == 1) {
						var month = '0'+month;
					}
					if(day.toString().length == 1) {
						var day = '0'+day;
					}   
					if(hour.toString().length == 1) {
						var hour = '0'+hour;
					}
					if(minute.toString().length == 1) {
						var minute = '0'+minute;
					}
					if(second.toString().length == 1) {
						var second = '0'+second;
					}   
					var dateTime = year+'/'+month+'/'+day+' '+hour+':'+minute+':'+second;   
					 return dateTime;
				}
				
				function isNumberKey(evt) {
					var charCode = (evt.which) ? evt.which : event.keyCode;
					if (charCode != 46 && charCode > 31
					&& (charCode < 48 || charCode > 57))
						return false;

					return true;
				}
			</script>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-4">
					<?php include 'user_menu.php'; ?>
				</div>
				<div class="col-md-8">
					<h2 class="text-center">Booking</h2>
					<br>
					<?php 
					if($result != ''){ 
						if($result == 'already'){
				?>
							<div class="alert alert-danger">Lesson(s) Already Booked</div>
				<?php 
						} else if ($result == 'failed'){
				?>
							<div class="alert alert-danger">Error Occurred</div>
				<?php 
						} else if($result == 'success'){
				?>
							<div class="alert alert-success">Lesson(s) Booked Successfully</div>
				<?php		
						} else if($result == 'mailnotsent')
						{
				?>
							<div class="alert alert-success">Lesson(s) Booked Successfully without mail sent</div>
				<?php			
						}

					} 
				 ?>
				<div style="margin-bottom:25px;" class="alert alert-info">
					Please select your Child from the dropdown below.<br>
					You can then click on each of our Clubs and select the sessions you would like to book.<br>
					Bexley Snap will approve and confirm all bookings and notify you as soon as possible.
				</div>
				<form name="frmbooking" id="frmbooking" method="post" action="bookingv.php" onsubmit="return validate();">
					<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('add', $encrypt);?>">
					<div class="form-group">
					  <label for="txtpupil">Child:</label>
					  <select class="form-control" id="txtpupil" name="txtpupil" required>
						<option value="">--- Select Child ---</option>
						<?php 
						if(isset($pnum) && $pnum > 0 ){
							mysqli_data_seek($prun,0);
							while($pfetch = mysqli_fetch_assoc($prun)){
						?>
							<option value="<?php echo $pfetch['_ID'];?>"><?php echo $pfetch['_FirstName'].' '.$pfetch['_LastName'];?></option>
						<?php }} ?>
					  </select>
					</div>
					
					<div class="form-group">
						<label for="txttype">Club:</label>
						<!-- accordion start -->
						
						<div class="panel-group" id="accordion">
							<?php
								$z = 0;
								while($fetchbt = mysqli_fetch_assoc($runbt)){
							?>
							<div class="panel panel-default">
							  <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $z;?>">
								<div class="panel-heading">
									<h4 class="panel-title">
										<?php echo $fetchbt['_Type'];?> ( <?php echo $fetchbt['_capacity']; ?> )
										<?php $available_place = $fetchbt['_capacity'] ? $fetchbt['_capacity'] : 0;?>
									</h4>
									
								</div>
							   </a>
							  <!--<div id="collapse<?php echo $z;?>" class="panel-collapse collapse <?php echo $z == 0 ? 'in' : '';?>">-->
							  <div id="collapse<?php echo $z;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php /* ?><div class="form-group">
									  <label for="txtdate">Club:</label>
									  <input type="checkbox" name="txttype[]" class="form-control" style="opacity:1;-webkit-appearance: checkbox;height:23px !important;display:block;position:relative;" value="<?php echo $fetchbt['_ID'];?>">
									</div><?php */ ?>
									<?php
									/*$slotqry = "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype = ".$fetchbt['_ID']." AND _Status = '0'";*/
									$slotqry = "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype = ".$fetchbt['_ID'];
									/*echo $slotqry;
									exit;*/
									$slotrun = mysqli_query($con , $slotqry);
									$numslot = mysqli_num_rows($slotrun);
									?>
									<div class="form-group">
										<?php if($fetchbt['_Notes'] != ""){ ?>	

											<p>Notes: <?php echo $fetchbt['_Notes']; ?></p>

									<?php	} ?>
									</div>
									
									<div class="form-group">
									  <label for="txtdate">Session(s):</label>
									  <?php 
											if($numslot > 0){ 
									  ?>
											<table class="table">
												<thead>
												  <tr>
													<th></th>
													<th>Date</th>
													<th>From</th>
													<th>To</th>
													<th>Price</th>
													<th>Places Available</th>
												  </tr>
												</thead>
												<tbody>
										<?php 
												while($fetchslot = mysqli_fetch_assoc($slotrun)){


												$btscount = array_count_values($bts);
												$tc = $btscount[$fetchslot['_ID']];

												if(!isset($tc) || $tc == ''){ $tc="0";}else{$tc = $tc;}

												$result_get_assigned_club['_capacity'];

												$ftc = 0;
												$ftc = $fetchbt['_capacity'] - $tc;
										?>												
													<tr>
														<td>
															
															<input type="checkbox" class="form-control" name="slot[]" value="<?php echo $fetchslot['_ID'];?>" style="opacity:1;-webkit-appearance: checkbox;height:23px !important;display:block;position:relative;">
														</td>
														<td>
															<?php echo date("d-M-Y",strtotime($fetchslot['_Date']));?>
															<input type="hidden" name="txtdate<?php echo $fetchslot['_ID'];?>" value="<?php echo date("Y-m-d",strtotime($fetchslot['_Date']));?>">
														</td>
														<td>
															<?php echo date("H:i A" ,strtotime($fetchslot['_Fromtime']));?>
															<input type="hidden" name="txtftime<?php echo $fetchslot['_ID'];?>" value="<?php echo date("H:i A" ,strtotime($fetchslot['_Fromtime']));?>">
														</td>
														<td>
															<?php echo date("H:i A" ,strtotime($fetchslot['_Totime'])); ?>
															<input type="hidden" name="txtttime<?php echo $fetchslot['_ID'];?>" value="<?php echo date("H:i A" ,strtotime($fetchslot['_Totime']));?>">
														</td>
														<td>
															&#163;<?php echo number_format((float)$fetchbt['_Price'], 2, '.', ''); ?>
															<input type="hidden" name="txtprice<?php echo $fetchslot['_ID'];?>" value="<?php echo number_format((float)$fetchbt['_Price'], 2, '.', ''); ?>">
														</td>

														<td>
															<?php echo $ftc; ?>
															<input type="hidden" name="txtplav<?php echo $fetchslot['_ID'];?>" value="<?php echo $ftc; ?>">
														</td>
													</tr>
										<?php } ?>
												</tbody>
											</table>
									 <?php } else { ?>
											<p style="text-indent:50px;">There are currently no available time slots for this club.</p>
									 <?php } ?>
									</div>
									
									
									<?php /* ?><div class="form-group">
									  <label for="txtdate">Date(s):</label>
									  <input type="text" class="form-control" id="txtdate<?php echo $fetchbt['_ID'];?>" class="dtpicker" placeholder="MM/DD/YYYY" name="txtdate<?php echo $fetchbt['_ID'];?>">
									</div>
									<div class="form-group">
									  <label for="txtftime">From Time:</label>
									  <input type="text" class="form-control" id="txtftime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtftime<?php echo $fetchbt['_ID'];?>">
									</div>
									<div class="form-group">
									  <label for="txtttime">To Time:</label>
									  <input type="text" class="form-control" id="txtttime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtttime<?php echo $fetchbt['_ID'];?>">
									</div><?php */ ?>
								</div>
							  </div>
							</div>
							
							<?php $z++; } ?>
						</div> 
  
  <!-- accordion end -->
						
					  <!--<label for="txttype">Club:</label>
					  <select class="form-control" id="txttype" name="txttype" required onchange="change_freq(this.value);">
							<option value="">--- Select Club ---</option>
							<?php
								while($fetchbt = mysqli_fetch_assoc($runbt)){
							?>
									<option value="<?php echo $fetchbt['_ID'];?>"><?php echo $fetchbt['_Type'];?></option>
							<?php } ?>
					  </select>-->
					</div>
					
					<!--<div class="form-group" id="frequency">
					  <label for="txtfreq">Frequency:</label>
					  <select class="form-control" id="txtfreq" name="txtfreq" required>
							<option value="">--- Select Frequency ---</option>
							<option value="0">Daily</option>
							<option value="1">Weekly</option>
							<option value="2">Monthly</option>
					  </select>
					</div>-->
					
					<!--<div class="form-group">
					  <label for="txtdate">Date:</label>
					  <input type="text" class="form-control" id="txtdate" class="dtpicker" placeholder="MM/DD/YYYY" name="txtdate" required>
					</div>-->
					
					<!--<div class="form-group">
					  <label for="txtftime">From Time:</label>
					  <input type="text" class="form-control" id="txtftime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtftime<?php echo $fetchbt['_ID'];?>" required>
					</div>
					<div class="form-group">
					  <label for="txtttime">To Time:</label>
					  <input type="text" class="form-control" id="txtttime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtttime<?php echo $fetchbt['_ID'];?>" required>
					</div>-->
					
					<!--<div class="form-group">
					  <label for="txttutor">Tutor:</label>
					  <select class="form-control" id="txttutor" name="txttutor" required>
						<option value="">--- Select Tutor ---</option>
						<?php 
						if(isset($tnum) && $tnum > 0 ){
							mysqli_data_seek($trun,0);
							while($tfetch = mysqli_fetch_assoc($trun)){
						?>
							<option value="<?php echo $tfetch['_ID'];?>"><?php echo $tfetch['_Firstname'].' '.$tfetch['_Lastname'];?></option>
						<?php }} ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="txttutor">Subject:</label>
					  <select class="form-control" id="txtsubject" name="txtsubject" required>
						<option value="">--- Select Subject ---</option>
						<?php 
						if(isset($snum) && $snum > 0 ){
							mysqli_data_seek($srun,0);
							while($sfetch = mysqli_fetch_assoc($srun)){
						?>
							<option value="<?php echo $sfetch['_ID'];?>"><?php echo $sfetch['_Name'];?></option>
						<?php }} ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="txtlevel">Level:</label>
					  <select class="form-control" id="txtlevel" name="txtlevel" required>
						<option value="">--- Select Level ---</option>
						<?php 
						if(isset($lnum) && $lnum > 0 ){
							mysqli_data_seek($lrun,0);
							while($lfetch = mysqli_fetch_assoc($lrun)){
						?>
							<option value="<?php echo $lfetch['_ID'];?>"><?php echo $lfetch['_Title'];?></option>
						<?php }} ?>
					  </select>
					</div>-->
					
					<button type="submit" class="btn btn-primary" style="margin-bottom:30px">Book</button>
				</form>
				</div>
			</div>
			<!-- <div class="col-md-offset-2 col-md-8" style="margin-bottom:20px;">
				<?php 
					if($result != ''){ 
						if($result == 'already'){
				?>
							<div class="alert alert-danger">Lesson(s) Already Booked</div>
				<?php 
						} else if ($result == 'failed'){
				?>
							<div class="alert alert-danger">Error Occurred</div>
				<?php 
						} else if($result == 'success'){
				?>
							<div class="alert alert-success">Lesson(s) Booked Successfully</div>
				<?php		
						}
					} 
				 ?>
				<div style="margin-bottom:25px;" class="alert alert-info">
					Please select your Child from the dropdown below.<br>
					You can then click on each of our Clubs and select the sessions you would like to book.<br>
					Bexley Snap will approve and confirm all bookings and notify you as soon as possible.
				</div>
				<form name="frmbooking" id="frmbooking" method="post" action="bookingaction.php" onsubmit="return validate();">
					<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('add', $encrypt);?>">
					<div class="form-group">
					  <label for="txtpupil">Child:</label>
					  <select class="form-control" id="txtpupil" name="txtpupil" required>
						<option value="">--- Select Child ---</option>
						<?php 
						if(isset($pnum) && $pnum > 0 ){
							mysqli_data_seek($prun,0);
							while($pfetch = mysqli_fetch_assoc($prun)){
						?>
							<option value="<?php echo $pfetch['_ID'];?>"><?php echo $pfetch['_FirstName'].' '.$pfetch['_LastName'];?></option>
						<?php }} ?>
					  </select>
					</div>
					
					<div class="form-group">
						<label for="txttype">Club:</label>
						<!-- accordion start -->
						
						<div class="panel-group" id="accordion">
							<?php
								$z = 0;
								while($fetchbt = mysqli_fetch_assoc($runbt)){
							?>
							<div class="panel panel-default">
							  <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $z;?>">
								<div class="panel-heading">
									<h4 class="panel-title">
										<?php echo $fetchbt['_Type'];?>
									</h4>
									
								</div>
							   </a>
							  <!--<div id="collapse<?php echo $z;?>" class="panel-collapse collapse <?php echo $z == 0 ? 'in' : '';?>">-->
							  <div id="collapse<?php echo $z;?>" class="panel-collapse collapse">
								<div class="panel-body">
									<?php /* ?><div class="form-group">
									  <label for="txtdate">Club:</label>
									  <input type="checkbox" name="txttype[]" class="form-control" style="opacity:1;-webkit-appearance: checkbox;height:23px !important;display:block;position:relative;" value="<?php echo $fetchbt['_ID'];?>">
									</div><?php */ ?>
									<?php
									/*$slotqry = "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype = ".$fetchbt['_ID']." AND _Status = '0'";*/
									$slotqry = "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype = ".$fetchbt['_ID'];
									$slotrun = mysqli_query($con , $slotqry);
									$numslot = mysqli_num_rows($slotrun);
									?>
									<div class="form-group">
										<?php if($fetchbt['_Notes'] != ""){ ?>	

											<p>Notes: <?php echo $fetchbt['_Notes']; ?></p>

									<?php	} ?>
									</div>
									
									<div class="form-group">
									  <label for="txtdate">Session(s):</label>
									  <?php 
											if($numslot > 0){ 
									  ?>
											<table class="table">
												<thead>
												  <tr>
													<th></th>
													<th>Date</th>
													<th>From</th>
													<th>To</th>
													<th>Price</th>
													<th>Places Available</th>
												  </tr>
												</thead>
												<tbody>
										<?php 
												while($fetchslot = mysqli_fetch_assoc($slotrun)){

												$btscount = array_count_values($bts);
												$tc = $btscount[$fetchslot['_ID']];

												if(!isset($tc) || $tc == ''){ $tc="0";}else{$tc = $tc;}

												$result_get_assigned_club['_capacity'];

												$ftc = 0;
												$ftc = $fetchbt['_capacity'] - $tc;

										?>										
													<tr>
														<td>
															<input type="checkbox" class="form-control" name="slot[]" value="<?php echo $fetchslot['_ID'];?>" style="opacity:1;-webkit-appearance: checkbox;height:23px !important;display:block;position:relative;">
														</td>
														<td>
															<?php echo date("d-M-Y",strtotime($fetchslot['_Date']));?>
															<input type="hidden" name="txtdate<?php echo $fetchbt['_ID'];?>" value="<?php echo date("Y-m-d",strtotime($fetchslot['_Date']));?>">
														</td>
														<td>
															<?php echo date("H:i A" ,strtotime($fetchslot['_Fromtime']));?>
															<input type="hidden" name="txtftime<?php echo $fetchbt['_ID'];?>" value="<?php echo date("H:i A" ,strtotime($fetchslot['_Fromtime']));?>">
														</td>
														<td>
															<?php echo date("H:i A" ,strtotime($fetchslot['_Totime'])); ?>
															<input type="hidden" name="txtttime<?php echo $fetchbt['_ID'];?>" value="<?php echo date("H:i A" ,strtotime($fetchslot['_Totime']));?>">
														</td>

														<td>
															&#163;<?php echo number_format((float)$fetchbt['_Price'], 2, '.', ''); ?>
															<input type="hidden" name="txtprice<?php echo $fetchslot['_ID'];?>" value="<?php echo number_format((float)$fetchbt['_Price'], 2, '.', ''); ?>">
														</td>

														<td>
															<?php echo $ftc; ?>
															<input type="hidden" name="txtplav<?php echo $fetchslot['_ID'];?>" value="<?php echo $ftc; ?>">
														</td>
													</tr>
										<?php } ?>
												</tbody>
											</table>
									 <?php } else { ?>
											<p style="text-indent:50px;">There are currently no available time slots for this club.</p>
									 <?php } ?>
									</div>
									
									
									<?php /* ?><div class="form-group">
									  <label for="txtdate">Date(s):</label>
									  <input type="text" class="form-control" id="txtdate<?php echo $fetchbt['_ID'];?>" class="dtpicker" placeholder="MM/DD/YYYY" name="txtdate<?php echo $fetchbt['_ID'];?>">
									</div>
									<div class="form-group">
									  <label for="txtftime">From Time:</label>
									  <input type="text" class="form-control" id="txtftime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtftime<?php echo $fetchbt['_ID'];?>">
									</div>
									<div class="form-group">
									  <label for="txtttime">To Time:</label>
									  <input type="text" class="form-control" id="txtttime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtttime<?php echo $fetchbt['_ID'];?>">
									</div><?php */ ?>
								</div>
							  </div>
							</div>
							
							<?php $z++; } ?>
						</div> 
  
  <!-- accordion end -->
						
					  <!--<label for="txttype">Club:</label>
					  <select class="form-control" id="txttype" name="txttype" required onchange="change_freq(this.value);">
							<option value="">--- Select Club ---</option>
							<?php
								while($fetchbt = mysqli_fetch_assoc($runbt)){
							?>
									<option value="<?php echo $fetchbt['_ID'];?>"><?php echo $fetchbt['_Type'];?></option>
							<?php } ?>
					  </select>-->
					</div>
					
					<!--<div class="form-group" id="frequency">
					  <label for="txtfreq">Frequency:</label>
					  <select class="form-control" id="txtfreq" name="txtfreq" required>
							<option value="">--- Select Frequency ---</option>
							<option value="0">Daily</option>
							<option value="1">Weekly</option>
							<option value="2">Monthly</option>
					  </select>
					</div>-->
					
					<!--<div class="form-group">
					  <label for="txtdate">Date:</label>
					  <input type="text" class="form-control" id="txtdate" class="dtpicker" placeholder="MM/DD/YYYY" name="txtdate" required>
					</div>-->
					
					<!--<div class="form-group">
					  <label for="txtftime">From Time:</label>
					  <input type="text" class="form-control" id="txtftime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtftime<?php echo $fetchbt['_ID'];?>" required>
					</div>
					<div class="form-group">
					  <label for="txtttime">To Time:</label>
					  <input type="text" class="form-control" id="txtttime<?php echo $fetchbt['_ID'];?>" class="time" placeholder="10:30" name="txtttime<?php echo $fetchbt['_ID'];?>" required>
					</div>-->
					
					<!--<div class="form-group">
					  <label for="txttutor">Tutor:</label>
					  <select class="form-control" id="txttutor" name="txttutor" required>
						<option value="">--- Select Tutor ---</option>
						<?php 
						if(isset($tnum) && $tnum > 0 ){
							mysqli_data_seek($trun,0);
							while($tfetch = mysqli_fetch_assoc($trun)){
						?>
							<option value="<?php echo $tfetch['_ID'];?>"><?php echo $tfetch['_Firstname'].' '.$tfetch['_Lastname'];?></option>
						<?php }} ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="txttutor">Subject:</label>
					  <select class="form-control" id="txtsubject" name="txtsubject" required>
						<option value="">--- Select Subject ---</option>
						<?php 
						if(isset($snum) && $snum > 0 ){
							mysqli_data_seek($srun,0);
							while($sfetch = mysqli_fetch_assoc($srun)){
						?>
							<option value="<?php echo $sfetch['_ID'];?>"><?php echo $sfetch['_Name'];?></option>
						<?php }} ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="txtlevel">Level:</label>
					  <select class="form-control" id="txtlevel" name="txtlevel" required>
						<option value="">--- Select Level ---</option>
						<?php 
						if(isset($lnum) && $lnum > 0 ){
							mysqli_data_seek($lrun,0);
							while($lfetch = mysqli_fetch_assoc($lrun)){
						?>
							<option value="<?php echo $lfetch['_ID'];?>"><?php echo $lfetch['_Title'];?></option>
						<?php }} ?>
					  </select>
					</div>-->
					
			</div> 
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>
<script type="text/javascript">
	function change_freq(type){
		if(type == '1' || type == '2'){
			document.getElementById('frequency').style.display = 'block';
		} else {
			document.getElementById('frequency').style.display = 'none';
		}
	}
	$(document).ready(function(){
		$('#txtpupil').change(function(){
        		var child = $(this).val();

        		  $.ajax({
        		  	url: "getclubs.php", 
        		  	data : { data : child } ,
        		  	success: function( response ){
			           //alert( response );
			           console.log(response);
			           $('#accordion').html(response);
			    	}
				});
        });
	});
</script>