<?php 
session_start();
if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

include_once('admin/db/dbopen.php');
//print_r($_REQUEST);exit;
/*Array ( [id] => rQ== [txtemail] => nicgilbey@gmail.com [txtpassword] => 123 [txtphone] => 1234567890 [txtaddress1] => test address 1 [txtaddress2] => test address 2 [txtcity] => NY [txtstate] => NY [txtcountry] => NY [txtzip] => 10001 [PHPSESSID] => 62ad527c4013e05d2fee3dea0d170971 )  */

$id       = isset($_POST['id']) ? decrypt($_POST['id'],$encrypt) : '';
$email    = isset($_POST['txtemail']) ? $_POST['txtemail'] : '';
$password = isset($_POST['txtpassword']) ? $_POST['txtpassword'] : '';
$phone    = isset($_POST['txtphone']) ? $_POST['txtphone'] : '';
$address1 = isset($_POST['txtaddress1']) ? $_POST['txtaddress1'] : '';
$address2 = isset($_POST['txtaddress2']) ? $_POST['txtaddress2'] : '';
$city     = isset($_POST['txtcity']) ? $_POST['txtcity'] : '';
$state    = isset($_POST['txtstate']) ? $_POST['txtstate'] : '';
$country  = isset($_POST['txtcountry']) ? $_POST['txtcountry'] : '';
$postcode = isset($_POST['txtzip']) ? $_POST['txtzip'] : '';

$qry = "SELECT * FROM ".$tbname."_clientmaster WHERE _Email = '".$email."' AND _ID != ".$id;
$ru  = mysqli_query($con , $qry);
$num = mysqli_num_rows($ru);
if($num > 0){
	header("location:profile.php?result=".encrypt('already',$encrypt));
	exit;
} else {
	$query = "UPDATE ".$tbname."_clientmaster SET ";
	if($email != ''){
		$query .= "`_Email` = '".$email."' ,";
	} else {
		$query .= "`_Email` = null ,";
	}

	if($password != ''){
		$query .= "`_Password` = '".encrypt($password,$encrypt)."' ,";
	}

	if($phone != ''){
		$query .= "`_Phone` = '".$phone."' ,";
	} else {
		$query .= "`_Phone` = null ,";
	}

	if($address1 != ''){
		$query .= "`_Address1` = '".$address1."' ,";
	} else {
		$query .= "`_Address1` = null ,";
	}

	if($address2 != ''){
		$query .= "`_Address2` = '".$address2."' ,";
	} else {
		$query .= "`_Address2` = null ,";
	}

	if($city != ''){
		$query .= "`_City` = '".$city."' ,";
	} else {
		$query .= "`_City` = null ,";
	}

	if($state != ''){
		$query .= "`_State` = '".$state."' ,";
	} else {
		$query .= "`_State` = null ,";
	}

	if($country != ''){
		$query .= "`_Country` = '".$country."' ,";
	} else {
		$query .= "`_Country` = null ,";
	}

	if($postcode != ''){
		$query .= "`_Postcode` = '".$postcode."' ";
	} else {
		$query .= "`_Postcode` = null ";
	}

	$query .= " WHERE _ID = ".$id;
	$result = mysqli_query($con,$query);


	if($result){ 
		header("location:profile.php?result=".encrypt('success',$encrypt));
		exit;
	} else {
		header("location:profile.php?result=".encrypt('failed',$encrypt));
		exit;
	}
}
?>