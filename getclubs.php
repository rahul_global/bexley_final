<?php
session_start();
if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}
include_once('admin/db/dbopen.php');


$bts = array();
$qry1 = "select _Tsids from ".$tbname."_paypalbooking ";
$rs1 = mysqli_query($con,$qry1);
$n1 = mysqli_num_rows($rs1);

if($n1 > 0)
{
	while($row1 = mysqli_fetch_assoc($rs1))
	{
		$tarr = array();
		$tarr = explode(",",$row1['_Tsids']);

		foreach($tarr as $sdf)
		{
			array_push($bts,$sdf);
		}

	}
}



$childid = $_REQUEST['data'];

if($childid):
$getclubids = mysqli_query($con , "Select _ClubID from ".$tbname."_childclub_rel WHERE _ChildID = '".$childid."'");

$cids = mysqli_fetch_assoc($getclubids);

$cid = $cids['_ClubID'];

$get_assigned_club = "select * from {$tbname}_bookingtype WHERE _ID IN ( $cid ) AND _active = '1' ORDER BY _Type";

$sql_get_assigned_club = mysqli_query($con , $get_assigned_club);
$data = array();
$html = '';
$z = 0;

if(mysqli_num_rows($sql_get_assigned_club) > 0):
while($result_get_assigned_club = mysqli_fetch_assoc($sql_get_assigned_club))
{
	$available_place = $result_get_assigned_club['_capacity'] ? $result_get_assigned_club['_capacity'] : 0;

	/*$qry1 = "select _ID from * from {$tbname}_timeslot where _Bookingtype = '".$result_get_assigned_club['_ID']."' ";
	$rs1 = mysqli_query($con,$qry1);
	$n1 = mysqli_num_rows($rs1);
	$ctotts = '';

	if($n1 > 0)
	{
		while($row1 = mysqli_fetch_assoc($rs1))
		{
			$ctotts .= $row1['_ID'].',';
		}		
	}

	$ctotts = rtrim($tcts,",");*/



	$html .= '<div class="panel panel-default">';
	$html .= '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$z.'">
								<div class="panel-heading">
									<h4 class="panel-title">
										'.$result_get_assigned_club['_Type'].' ( '.$result_get_assigned_club['_capacity'].' )
									</h4>
									
								</div>
							   </a>';
	$html .= '<div id="collapse'.$z.'" class="panel-collapse collapse">';
	$html .= '<div class="panel-body">';
	$html .= '<div class="form-group">';
	$html .= '<label for="txtdate">Session(s):</label>';
	$html .= '<table class="table">
					<thead>
	  				    <tr>
							<th></th>
							<th>Date</th>
							<th>From</th>
							<th>To</th>
							<th>Price</th>
							<th>Places Available</th>
						</tr>
					</thead>
					<tbody>';
	// "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype = '".$result_get_assigned_club['_ID']."' AND _Date >= NOW() "
	$get_timeslot = mysqli_query($con , "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype = '".$result_get_assigned_club['_ID']."' ");
	if(mysqli_num_rows($get_timeslot) > 0){
		while($resultget_timeslot = mysqli_fetch_assoc($get_timeslot)){

			$btscount = array_count_values($bts);
			//$sdf = $resultget_timeslot['_ID'];
			$tc = $btscount[$resultget_timeslot['_ID']];

			if(!isset($tc) || $tc == ''){ $tc="0";}else{$tc = $tc;}

			$result_get_assigned_club['_capacity'];

			$ftc = 0;
			$ftc = $result_get_assigned_club['_capacity'] - $tc;

			$html .= '<tr>';
			$html .= '<td>';
			$html .= '<input type="checkbox" class="form-control" name="slot[]" value="'.$resultget_timeslot['_ID'].'" style="opacity:1;-webkit-appearance: checkbox;height:23px !important;display:block;position:relative;">';
			$html .= '</td>';
			$html .= '<td>';
			$html .= date("d-M-Y",strtotime($resultget_timeslot['_Date'])).'<input type="hidden" name="txtdate'.$resultget_timeslot['_ID'].'" value="'.date("Y-m-d",strtotime($resultget_timeslot['_Date'])).'">';
			$html .= '</td>';
			$html .= '<td>';
			$html .= date("H:i:s",strtotime($resultget_timeslot['_Fromtime'])).'<input type="hidden" name="txtdate'.$resultget_timeslot['_ID'].'" value="'.date("H:i:s",strtotime($resultget_timeslot['_Fromtime'])).'">';
			$html .= '</td>';
			$html .= '<td>';
			$html .= date("H:i:s",strtotime($resultget_timeslot['_Totime'])).'<input type="hidden" name="txtdate'.$resultget_timeslot['_ID'].'" value="'.date("H:i:s",strtotime($resultget_timeslot['_Totime'])).'">';
			$html .= '</td>';

			$html .= '<td>';
			$html .= '&#163;'.number_format((float)$result_get_assigned_club['_Price'], 2, '.', '').'<input type="hidden" name="txtprice'.$resultget_timeslot['_ID'].'" value="'.number_format((float)$result_get_assigned_club['_Price'], 2, '.', '').'">';
			$html .= '</td>';

			$html .= '<td>';
			$html .= $ftc.'<input type="hidden" name="txtplav'.$resultget_timeslot['_ID'].'" value="'.$ftc.'">';
			$html .= '</td>';

			$html .= '</tr>';
		}	
	}else{
		$html .= '<tr>
			<td rowspan="4">No timeslot available.</td>
		</tr>';
	}			
	$html .= '</tbody>';
	$html .= '</table>';	
	$html .= '</div>';	
	$html .= '</div>';			
	$html .= '</div>';	  
	$html .= '</div>'; 

$z++;
}
else:
	$html .= '<p>No clubs found</p>'; 
endif;
else:
	$html .= '<p>No clubs found</p>'; 
endif;
echo $html;
?>