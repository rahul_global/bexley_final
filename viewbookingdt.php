<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-2">
					<?php include 'user_menu.php'; ?>

				</div>
				<div class="col-md-10">
					<?php
						$slot = decrypt($_REQUEST['sl'],$encrypt);
						$slot = explode(",", $slot);

						$query  = "SELECT bt._ID,bt._Type,bt._Price FROM ".$tbname."_timeslot ts inner join ".$tbname."_bookingtype as bt on bt._ID = ts._Bookingtype WHERE ts._ID IN ('" . implode("','",$slot) . "') group by ts._Bookingtype order by bt._Type";
				    	
						$run 	= mysqli_query($con , $query);
						$nrun = mysqli_num_rows($run);
						?>
				    	
			    		
			    			
			    			<table class="table">
								<?php
								if($nrun > 0)
								{
									$prctot = 0;
									while($fetch = mysqli_fetch_assoc($run))
									{
										$prc = 0;
										$prc = $fetch['_Price'];
										?>
										
										<tr>	
											<td><u>Club : <?php echo $fetch['_Type']; ?></u></td>
										</tr>
										<?php

										$qrsts = "SELECT * FROM ".$tbname."_timeslot WHERE _ID IN ('" . implode("','",$slot) . "') and _Bookingtype = '".$fetch['_ID']."' order by _Date";

										$rsts = mysqli_query($con,$qrsts);
										$nsts = mysqli_num_rows($rsts);

										if($nsts > 0)
										{
											$cnt = 0;
										?>
										<tr>
											<td>
												<table class="table">
													<thead>
														  <tr>
														  	<th>Sr no.</th>
															<th>Date</th>
															<th>From Time</th>
															<th>To Time</th>
															<th>Price</th>
														  </tr>
													</thead>
													<tbody>
													<?php
													while($fetchts = mysqli_fetch_assoc($rsts))
													{
														$cnt++;
														?>
														<tr>
															<td><?php echo $cnt; ?></td>
															<td><?php echo date("d-M-Y",strtotime($fetchts['_Date']));?></td>
															<td><?php echo $fetchts['_Fromtime']; ?></td>
															<td><?php echo $fetchts['_Totime']; ?></td>
															<td>&#163;<?php echo number_format((float)$prc, 2, '.', ''); ?></td>
														</tr>
														<?php
														$prctot += $prc;
													}?>

													</tbody>
												</table>
											</td>
										</tr>
										<?php	
										}
									}
									?>

									<tr>	
										<td>
											<table class="table">
												<thead>
													  <tr>
													  	<th>&nbsp;</th>
													  	<th>&nbsp;</th>
													  	<th>&nbsp;</th>
													  	<th>&nbsp;</th>
													  	<th style="text-align: right;padding-right:50px;"><b><u>Total : &#163;<?php echo number_format((float)$prctot, 2, '.', ''); ?></u></b></th>
													  </tr>
												</thead>
											</table>	
										</td>
									</tr>
								<?php	
								}
								?>
							</table>
					
				</div>
	    	</div>
	    	<div class="col-md-12">
	    			<center>
	    				<a class="btn2 btn-alt btn-bounce" href="viewbooking.php" title="Back">Ok</a>
	    			</center>
			</div>

			<div class="col-md-12">&nbsp;</div>

		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>