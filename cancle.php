<?php
session_start();
include_once('admin/db/dbopen.php');
?>
<html>
<head>
<title>bexleysnap</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />

<style>
.list-teasers.row a:hover{
	color:#fff;
}
.list-teasersxx li a {
    color:#fff;
}
.list-teasers li a:hover {
    color:#fff;
}
.club_info
{
	text-align:left!important;
}

table.club_info { 
    border-spacing: 0;
    border-collapse: collapse;
}
.club_info table { 
    border-spacing: 0;
    border-collapse: collapse;
}

.club_info td
{
	text-align:left!important;
}
.club_info td:nth-child(1) {  
  /* your stuff here */
  font-weight:bold;
}


.club table{
	text-align:left!important;
	padding-top:10px;
}

.club table { 
    border-spacing: 0!important;
    border-collapse: collapse!important;
}

table.club
 { 
    border-spacing: 0;
    border-collapse: collapse;
}

.club tbody{
	padding:0;
	margin:0;
}

.club td{
	text-align:left!important;
}

.club td:nth-child(1) {  
  font-weight:bold;
  width:200px;
  padding-right:0;
}

.club td:nth-child(2) {  
  width:700px;
}

</style>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
<?php include_once('menu.php');
?>


<div id="js-main-content" class="main-content"> 
	<section class="section section-content">
	    <div class="container">
	    	
	    	<div class="row">
	    		<div class="col-md-12">
	    			<center><h2>The order was canceled. Please try again later.</h2></center>
	    		</div>
	    	</div>

    		<div class="row">
	    		<div class="col-md-12">
	    			<center><input type="button" name="ok" value="ok" onclick="location.href = 'index.php';"></ins></center>
	    		</div>
	    	</div>			
	    </div>
	</section>
</div>
<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script type='text/javascript' src='js/owl.carousel.js'></script>
<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>
<style>
    #owl-demo .item{
        display: block;
        cursor: pointer;
        background: #ffd800;
        padding: 30px 0px;
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
    }
    #owl-demo .item:hover{
      background: #F2CD00;
    }
    </style>
<style type="text/css">
td{
    width: 50%;
    text-align: left;
    /*float: left;*/
    padding: 5px;
}
tr{
   display: block;
   width: 100%;
}
th {
    width: 50% !important;
    text-align: left;
    padding: 15px;
    /*float: left;*/
    font-weight: 600;
}
</style>
</body>

</html>