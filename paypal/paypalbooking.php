<?php
session_start();
include_once('../admin/db/dbopen.php');

/*  PHP Paypal IPN Integration Class Demonstration File
 *  4.16.2005 - Micah Carrick, email@micahcarrick.com
 *
 *  This file demonstrates the usage of paypal.class.php, a class designed  
 *  to aid in the interfacing between your website, paypal, and the instant
 *  payment notification (IPN) interface.  This single file serves as 4 
 *  virtual pages depending on the "action" varialble passed in the URL. It's
 *  the processing page which processes form data being submitted to paypal, it
 *  is the page paypal returns a user to upon success, it's the page paypal
 *  returns a user to upon canceling an order, and finally, it's the page that
 *  handles the IPN request from Paypal.
 *
 *  I tried to comment this file, aswell as the acutall class file, as well as
 *  I possibly could.  Please email me with questions, comments, and suggestions.
 *  See the header of paypal.class.php for additional resources and information.
*/
//echo "<pre>".print_r($_REQUEST);exit;
$cid = $_SESSION['frontuserid'];
$pupilid = $_REQUEST['pupilid'];
$clbs = $_REQUEST['clbs'];
$clbs = rtrim($clbs,',');
$slot = $_REQUEST['slot'];
$amount = $_REQUEST['amount'];
$cdt = date("Y-m-d h:i:s");

$id = str_replace(" ","+",$_REQUEST['id']);
$id = decrypt($id,$encrypt); 

/*
$sel = "select _Total,_ID from ".$tbname."_paypalbooking where _ID='".$id."'";
$rstsel = mysql_query($sel);
$rowsel = mysql_fetch_assoc($rstsel);
$Total = $rowsel['_Total'];*/


// Setup class
require_once('paypal.class.php');  // include the class file
$p = new paypal_class;             // initiate an instance of the class
//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
            
// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
//$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$this_script = $mainhttpaddress.'orderthanks.php';  //Localhost url
//$this_script = 'http://s374326078.onlinehome.us/1impression/';  // S37 url
//$Encrypt = "mysecretkey";
//$Total = replaceSpecialCharBack(decrypt($_GET['total'],$encrypt));

$Charges = 0;
$Vat = 0;

$description = '';
if(isset($_GET['item_name']) && $_GET['item_name'] != '')
{
	$description = replaceSpecialCharBack($_GET['item_name']);
}
else
{
	$description = $clbs;
}

$Total = 0;
if(isset($_GET['amount']) && $_GET['amount'] != '')
{
	$Total = $_GET['amount'];
}
else
{
	$Total = $amount;
}

if(isset($_GET['action']) && $_GET['action'] != '')
{
	$_GET['action'] = decrypt($_GET['action'],$encrypt) ;
} 
$cur = 'GBP';

// if there is not action variable, set the default action of 'process'
if (empty($_GET['action'])) $_GET['action'] = 'process';  

switch ($_GET['action']) {
    
   case 'process':      

   $insstr = "INSERT INTO " . $tbname . "_paypalbooking (";
			 
	$insstr .= " _CID,_PID,_Tsids,_Amount,_DateTime) VALUES(";
 
	if ($cid != "")
		  $insstr = $insstr . "'" . $cid . "', ";
	  else
		  $insstr = $insstr . "null, ";

	  if ($pupilid != "")
		  $insstr = $insstr . "'" . $pupilid . "', ";
	  else
		  $insstr = $insstr . "null, ";

	  if ($slot != "")
		  $insstr = $insstr . "'" . $slot . "', ";
	  else
		  $insstr = $insstr . "null, ";

	  if ($amount != "")
  	  	  $insstr = $insstr . "'" . $amount . "', ";
      else
  	      $insstr = $insstr . "null, ";

	  if ($cdt != "")
  	  	  $insstr = $insstr . "'" . $cdt . "' ";
      else
  	      $insstr = $insstr . "null ";

	  $insstr = $insstr . ") ";
	  //echo $insstr."<br>";exit;
	  $ins_run = mysqli_query($con , $insstr);

	  $id = mysqli_insert_id($con);

   	  // Process and order...
      // There should be no output at this point.  To process the POST data,
      // the submit_paypal_post() function will output all the HTML tags which
      // contains a FORM which is submited instantaneously using the BODY onload
      // attribute.  In other words, don't echo or printf anything when you're
      // going to be calling the submit_paypal_post() function.
 
      // This is where you would have your form validation  and all that jazz.
      // You would take your POST vars and load them into the class like below,
      // only using the POST values instead of constant string expressions.

      // For example, after ensureing all the POST variables from your custom
      // order form are valid, you might have:
      //
      // $p->add_field('first_name', $_POST['first_name']);
      // $p->add_field('last_name', $_POST['last_name']);
      

      /*$p->add_field('business', 'rahul.per2@gmail.com');*/
      $p->add_field('business', 'rahul.bizz@gmail.com');

      $p->add_field('return', $mainhttpaddress.'paypal/paypalbooking.php?action='.encrypt('success',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('cancel_return', $mainhttpaddress.'paypal/paypalbooking.php?action='.encrypt('cancel',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('notify_url', $mainhttpaddress.'paypal/paypalbooking.php?action='.encrypt('ipn',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('item_name', $description);

      $p->add_field('amount', $Total);

      $p->add_field('currency_code', $cur);

      $p->add_field('shipping', $Charges);

      $p->add_field('tax', $Vat);

     /*echo "<pre>";
      print_r($p);
      exit;*/
      
	  echo "<span style='font-weight:bold;font-size:20px;'>Please Wait...</span>";
	  //echo "<img src='../images/loader.gif'>";
      $p->submit_paypal_post(); // submit the fields to paypal
      //$p->dump_fields();      // for debugging, output a table of all the fields
      break;


   case 'success':      

      $pay_status = ( $_REQUEST['payment_status'] && $_REQUEST['payment_status'] == "Pending" ) ? "0" : ( ($_REQUEST['payment_status'] == "Suceess") ? "1" : "0" ) ;

      $trans_id = ( $_REQUEST['txn_id'] ) ? $_REQUEST['txn_id'] : "";

      $chng = "update {$tbname}_paypalbooking set _PStatus = '$pay_status' , _TransID = '$trans_id' where _ID = '".$id."'";
      
      $rst = mysqli_query($con,$chng);

      header("Location:".$mainhttpaddress."thanks.php?id=".encrypt($id,$encrypt));


      //die;

      

      // You could also simply re-direct them to another page, or your own 

      // order status page which presents the user with the status of their

      // order based on a database (which can be modified with the IPN code 

      // below).

      

      break;

      

   case 'cancel':       // Order was canceled...

      // The order was canceled before being completed.

 

     /* echo "<html><head><title>Canceled</title></head><body><h3>The order was canceled.</h3>";

      echo "</body></html>";*/



            header("Location:".$mainhttpaddress."cancle.php?id=".encrypt($id,$encrypt));

      

      

     

      break;

      

   case 'ipn':          // Paypal is calling page for IPN validation...

   

      // It's important to remember that paypal calling this script.  There

      // is no output here.  This is where you validate the IPN data and if it's

      // valid, update your database to signify that the user has payed.  If

      // you try and use an echo or printf function here it's not going to do you

      // a bit of good.  This is on the "backend".  That is why, by default, the

      // class logs all IPN data to a text file.

      

      if ($p->validate_ipn()) {

          

         // Payment has been recieved and IPN is verified.  This is where you

         // update your database to activate or process the order, or setup

         // the database with the user's order details, email an administrator,

         // etc.  You can access a slew of information via the ipn_data() array.

  

         // Check the paypal documentation for specifics on what information

         // is available in the IPN POST variables.  Basically, all the POST vars

         // which paypal sends, which we send back for validation, are now stored

         // in the ipn_data() array.

  

         // For this example, we'll just email ourselves ALL the data.

         echo $subject = 'Instant Payment Notification - Recieved Payment';

      }

         

      break;

 }     



      

?>