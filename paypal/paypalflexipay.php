<?php //paypalflexipay.php
session_start();
include("../include/functions.php");
include("../admin/global.php");

//$adminbizacc="globalindiatechtest-facilitator-1@gmail.com"; 
//for dynamic merchant email address is comming from the global.php  file
//file name paypalvirtualsubscribe.php

$id = decrypt($_REQUEST['id'],$encrypt); 

$sel = "select _PackageMethod,_PackageType,_TotalRate,_ID from ".$tbname."_virtualofficeflexibooking where _ID='".$id."'";
$rstsel = mysql_query($sel);
$rowsel = mysql_fetch_assoc($rstsel);

$method = $rowsel['_PackageMethod'];  ///0 for one time and 1 for recurring
$type = $rowsel['_PackageType'];  ///////for monthly ,weekly,yearly etc.
$Total = $rowsel['_TotalRate'];
$description= replaceSpecialCharBack($_GET['item_name']);

// COde FOr flexipay Details

/*  PHP Paypal IPN Integration Class Demonstration File
 *  4.16.2005 - Micah Carrick, email@micahcarrick.com
 *
 *  This file demonstrates the usage of paypal.class.php, a class designed  
 *  to aid in the interfacing between your website, paypal, and the instant
 *  payment notification (IPN) interface.  This single file serves as 4 
 *  virtual pages depending on the "action" varialble passed in the URL. It's
 *  the processing page which processes form data being submitted to paypal, it
 *  is the page paypal returns a user to upon success, it's the page paypal
 *  returns a user to upon canceling an order, and finally, it's the page that
 *  handles the IPN request from Paypal.
 *
 *  I tried to comment this file, aswell as the acutall class file, as well as
 *  I possibly could.  Please email me with questions, comments, and suggestions.
 *  See the header of paypal.class.php for additional resources and information.
*/



// Setup class

require_once('paypal.class.php');  // include the class file
$p = new paypal_class;             // initiate an instance of the class
//$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
          
// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
//$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$this_script = $httpAddress.'orderthanks.php';  //Localhost url
//$this_script = 'http://s374326078.onlinehome.us/1impression/';  // S37 url
//$encrypt = "mysecretkey";
//$Total = 10;
$Charges = 0;
$Vat = 0;

if(isset($_GET['action']) && $_GET['action'] != '')
{
   $_GET['action'] = decrypt($_GET['action'],$encrypt);
} 
$cur = 'GBP';

///echo $_GET['action'];


// if there is not action variable, set the default action of 'process'

if (empty($_GET['action'])) $_GET['action'] = 'process';  



switch ($_GET['action']) {

    

   case 'process':      // Process and order...



      // There should be no output at this point.  To process the POST data,

      // the submit_paypal_post() function will output all the HTML tags which

      // contains a FORM which is submited instantaneously using the BODY onload

      // attribute.  In other words, don't echo or printf anything when you're

      // going to be calling the submit_paypal_post() function.

 

      // This is where you would have your form validation  and all that jazz.

      // You would take your POST vars and load them into the class like below,

      // only using the POST values instead of constant string expressions.

 

      // For example, after ensureing all the POST variables from your custom

      // order form are valid, you might have:

      //

      // $p->add_field('first_name', $_POST['first_name']);

      // $p->add_field('last_name', $_POST['last_name']);

      
if($type == "0"){

// FOr One Time Payment
	  $p->add_field('business', $adminbizacc);
        
      $p->add_field('return', $httpAddress.'paypal/paypalflexipay.php?action='.encrypt('success',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('cancel_return', $httpAddress.'paypal/paypalflexipay.php?action='.encrypt('cancel',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('notify_url', $httpAddress.'paypalflexipay.php?action='.encrypt('ipn',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('item_name', $description);

      $p->add_field('amount', $Total);

      $p->add_field('currency_code', $cur);

      $p->add_field('shipping', $Charges);

      $p->add_field('tax', $Vat);

}else if($type == "1"){

	// FOr Recurring Payment

	  $p->add_field('business', $adminbizacc);

      $p->add_field('return', $httpAddress.'paypal/paypalflexipay.php?action='.encrypt('success',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('cancel_return', $httpAddress.'paypal/paypalflexipay.php?action='.encrypt('cancel',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('notify_url', $httpAddress.'paypalflexipay.php?action='.encrypt('ipn',$encrypt)."&id=".encrypt($id,$encrypt));

      $p->add_field('item_name', $description);

      $p->add_field('amount', $Total);

      $p->add_field('currency_code', $cur);

      $p->add_field('shipping', $Charges);

      $p->add_field('tax', $Vat);

      // Parameter For Recuurnig Payment

      $p->add_field('custom', $_SESSION['orderid']);

      $p->add_field('cmd','_xclick-subscriptions');

      $p->add_field('txn_type', "recurring_payment");

      $p->add_field('src', "1");

      $p->add_field('sra', "1");

      $p->add_field('a3', "$Total");

	      if($method == "0"){
	      	// weekly
	      	$p->add_field('t3', "W");

	      	$p->add_field('p3', "1");

	      }else if($method == "1"){

	      	// monthly
	      	$p->add_field('t3', "M");

	      	$p->add_field('p3', "1");

	      }else if($method == "2"){
	      	// quaterly
	      	$p->add_field('t3', "M");

	      	$p->add_field('p3', "3");
	      	

	      }else if($method == "3"){

	      	// yearly

	      	$p->add_field('t3', "Y");

	      	$p->add_field('p3', "1");

	      }else{

	      	// yearly

	      	$p->add_field('t3', "Y");

	      	$p->add_field('p3', "1");
	      }
}

         echo "<span style='font-weight:bold;font-size:20px;'>Loading...</span>";

         //echo "<img src='../images/loader.gif'>";

      $p->submit_paypal_post(); // submit the fields to paypal

      //$p->dump_fields();      // for debugging, output a table of all the fields

      break;

   case 'success':      // Order was successful...


      // This is where you would probably want to thank the user for their order
      // or what have you.  The order information at this point is in POST 
      // variables.  However, you don't want to "process" the order until you

      // get validation from the IPN.  That's where you would have the code to

      // email an admin, update the database with payment status, activate a
      // membership, etc.  

     /* echo "<html><head><title>Success</title></head><body><h3>Thank you for your order.</h3>";

      foreach ($_POST as $key => $value) { echo "$key: $value<br>"; }

      echo "</body></html>";*/ 

      $chng = "update ".$tbname."_virtualofficeflexibooking set _Status = '0' where _ID = '".$id."'";
      $rst = mysql_query($chng);

      header("Location:".$httpAddress."thanksflexi.php?orderid=".encrypt($id,$encrypt));

      //die;

      // You could also simply re-direct them to another page, or your own 

      // order status page which presents the user with the status of their

      // order based on a database (which can be modified with the IPN code 
      // below).

      break;

   case 'cancel':       // Order was canceled...

      // The order was canceled before being completed.

     /* echo "<html><head><title>Canceled</title></head><body><h3>The order was canceled.</h3>";
      echo "</body></html>";*/

    //  echo "Payment successfully Not Done";

      header("Location:".$httpAddress."cancleflexi.php?orderid=".encrypt($id,$encrypt));

      break;

   case 'ipn':          // Paypal is calling page for IPN validation...


      // It's important to remember that paypal calling this script.  There

      // is no output here.  This is where you validate the IPN data and if it's

      // valid, update your database to signify that the user has payed.  If

      // you try and use an echo or printf function here it's not going to do you

      // a bit of good.  This is on the "backend".  That is why, by default, the

      // class logs all IPN data to a text file.


      if ($p->validate_ipn()) {


         // Payment has been recieved and IPN is verified.  This is where you

         // update your database to activate or process the order, or setup

         // the database with the user's order details, email an administrator,

         // etc.  You can access a slew of information via the ipn_data() array.

         // Check the paypal documentation for specifics on what information
         // is available in the IPN POST variables.  Basically, all the POST vars
         // which paypal sends, which we send back for validation, are now stored
         // in the ipn_data() array.

         // For this example, we'll just email ourselves ALL the data.

         $subject = 'Instant Payment Notification - Recieved Payment';

         $to = 'YOUR EMAIL ADDRESS HERE';    //  your email

         $body =  "An instant payment notification was successfully recieved\n";

         $body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
         $body .= " at ".date('g:i A')."\n\nDetails:\n";

         foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
         mail($to, $subject, $body);

      }

      break;

 }     

?>