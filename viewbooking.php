<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

$id = $_SESSION['frontuserid'];
/*echo $id;
exit;*/
$qry = "SELECT pb.*, concat(cm._Firstname,' ',cm._Lastname) as clientname, concat(pm._FirstName,' ',pm._LastName) as pupilname  FROM ".$tbname."_paypalbooking as pb left join ".$tbname."_clientmaster as cm on cm._ID=pb._CID left join ".$tbname."_pupilmaster as pm on pm._ID=pb._PID WHERE pb._CID = '".$id."' ";
//echo $qry;exit;
$rs = mysqli_query($con,$qry);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-2">
					<?php include 'user_menu.php'; ?>
				</div>
				<div class="col-md-10">
					<br>
					<table class="table">
						<thead>
							  <tr>
							  	<th>Sr no.</th>
								<th>Order ID</th>
								<th>Pupil Name</th>
								<th>Date-Time</th>
								<th>Amount</th>
								<th>Payment Status</th>
								<th>View Time Slots</th>
							  </tr>
						</thead>
						<tbody>


						<?php
						$cnt = 0;
						while($row = mysqli_fetch_assoc($rs))
						{
							$cnt++;

							$clientname = $row['clientname'];
							$pupilname = $row['pupilname'];
							$slot = $row['_Tsids'];
							//$slot = explode(",", $slot);
							$ps = $row['_PStatus'];
							$psnm = '';

							$orderid = $row['_ID'];

							if($ps == "1")
							{
								$psnm = "<font style='color: blue;''>UnPaid</font>";

							}
							else if($ps == "2")
							{
								$psnm = "<font style='color: green;''>Paid</font>";
							}
							else
							{
								$psnm = "<font style='color: red;''>Pending...</font>";
							}
							?>
							<tr>
								<td><?php echo $cnt; ?></td>
								<td><?php echo $orderid; ?></td>
								<td><?php echo $pupilname; ?></td>
								<td><?php echo date("d-M-Y H:i:s",strtotime($row['_DateTime']));?></td>
								<td>&#163;<?php echo number_format((float)$row['_Amount'], 2, '.', ''); ?></td>
								<td><?php echo $psnm; ?></td>
								<td><a href="viewbookingdt.php?sl=<?php echo encrypt($slot,$encrypt); ?>">View Details</a></td>
							</tr>
						<?php
						}
						?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>