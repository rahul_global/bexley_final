<?php
session_start();
if(isset($_SESSION['frontuserid']) && $_SESSION['frontuserid'] != '')
{
	header("location:dashboard.php");
	exit;
}
include_once('admin/db/dbopen.php');
$result = isset($_GET['result']) && $_GET['result'] != ''?decrypt($_GET['result'],$encrypt) : '';

if(isset($_GET['done']))
{
	$result = decrypt(str_replace("+"," ",$_GET['done']),$encrypt);
}
?>
<html>
<head>
<title>bexleysnap</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<?php 
include_once('topscript.php');
?>
	<style type="text/css">
		.log{
			background-color: #F8A848;
		}
		.error {color : #000;}
		.fntclr {color:#fff; font-size:24px;text-align:right;}
		.rowmrgn{margin-top : 10px;}
	</style>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
		<div class="container-fluid">
			<div class="row">
				<?php
				if(!isset($_SESSION['frontuserid']))
				{
				?>
            	<div  class="col-md-12 log">
            		<div class="form_wrapper animated-short" id="login_form">
		                <form name="frmlogin" id="frmlogin" method="post" action="frontloginaction.php" onsubmit="return validate()">
			                <div class="row">
		                		<div class="col-md-12 col-sm-12">
		                			<h2 style="color: #fff;text-align:center;">Family Login</h2>
		                		</div>
			                </div>
							<?php
							if($result == "00")
							{
								?>
								<div class="col-md-12 text-center" style="padding: 10px 0;">
									<div class="form-group">	
										<span class='error'>Register Successfully.</span>
									</div>
								</div>
								<?php
							}
							if($result == "01"){
							?>
							<div class="col-md-12 text-center" style="padding: 10px 0;">
								<div class="form-group">
									<span class='error'>Register Successfully. With out child insert.</span>
								</div>
							</div> 
							<?php	
							}
							if($result == "error"){
								?>
							<div class="col-md-12 text-center" style="padding: 10px 0;">
								<div class="form-group">
									<span class='error'>incorrect email or password</span>
								</div>
							</div>
								<?php
							}
							if($result == "notactive"){
								?>
							<div class="col-md-12 text-center" style="padding: 10px 0;">
									<div class="form-group">
										<span class='error'>User is not active, try after some time</span>
									</div>
							</div>
								<?php
							}
							?>
							<div class="form-group">
								<input type="hidden" name="usertype" value="<?php echo encrypt('1',$encrypt); ?>">
							</div>
			                <div class="col-md-12 rowmrgn">
								<div class="form-group">
									<label for="txtlname" class="col-sm-5 col-md-5 control-label fntclr">Email: </label>
									<div class="col-sm-7 col-md-7">
										<input type="email" class="form-control" name="email" id="email" value="" style="width:320px;" required>
									</div>
			                	</div>
			                </div>
		                	<div class="col-md-12 rowmrgn">
								<div class="form-group">
									<label for="txtlname" class="col-sm-5 col-ms-5 control-label fntclr">Password: </label>
									<div class="col-sm-7 col-md-7">
										<input type="password" class="form-control" name="password" id="password" value="" style="width:320px;" required>
									</div>
			                	</div>
			                </div>
							<div class="col-md-12 rowmrgn" style="padding-bottom:20px;">
								<div class="form-group">
									<div class="col-sm-5 col-ms-5"></div>
									<div class="col-sm-7 col-md-7">
										<input type="submit" class="btn btn-alt btn-sml btn-bounce" name="submit" id="submit" value="Login" style="width:145px">
										<input type="reset" class="btn btn-alt btn-sml btn-bounce" name="reset" id="reset" value="Reset" style="margin-left:5px;width:145px">
									</div>
			                	</div>
			                </div>
		                </form>
		            </div>
            	</div>
				<?php } ?>
			</div>
		</div>
<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script type="text/javascript">
	function validate(){
		var err = '';
		if(document.frmlogin.email.value == '')
		{
			err += 'Please Enter Email \n';
		}
		if(document.frmlogin.password.value == '')
		{
			err += 'Please Enter Password';
		}
		
		if(err != '')
		{
			alert(err);
			return false;
		}
		return true;
	}
</script>
<style type="text/css">
	.error{
		color: #000;
		font-weight: 600;
	}
</style>
</body>
</html>