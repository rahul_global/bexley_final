<?php
session_start();
if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}
include_once('admin/db/dbopen.php');
$result = 0;
if(isset($_GET['done']))
{
	$result = $_GET['done'];
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Dashboard</title>
		<?php include 'topscript.php'; ?>
		
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
		<div class="row">
		
			<div class="col-md-12">
				<div class="col-md-3">
					<?php include 'user_menu.php'; ?>
				</div>
				<div class="col-md-8">
					<h2>Dashboard</h2>
				</div>
			</div>
		</div>
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>