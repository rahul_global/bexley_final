<?php 
require_once('admin/db/dbopen.php');
$action 			= decrypt($_REQUEST['e_action'],$encrypt);
/*echo "<pre>";
print_r($_REQUEST);
exit;
*/
	//$txtcarer            = $_POST['carer'];
    $txtclname           = replaceSpecialChar($_POST['txtclname']);
	$txtcfname           = replaceSpecialChar($_POST['txtcfname']);
	$txtfsurname         = replaceSpecialChar($_POST['txtfsurname']);
	$dob                 = $_POST['txtdob'];
	$dobcon              = str_replace("/", "-", $dob);
	$txtdob              = date('Y-m-d',strtotime($dobcon));
	$txtethnicity        = $_POST['txtethnicity'];
	$txtgender           = replaceSpecialChar($_POST['txtgender']);
	$txtschool           = $_POST['txtschool'];
	$txtresidential      = replaceSpecialChar($_POST['txtresidential']);
	$txtparent           = $_POST['txtparent'];
	$txttitle            = replaceSpecialChar($_POST['txttitle']);
	$txtfname            = replaceSpecialChar($_POST['txtfname']);
	$txtlname            = replaceSpecialChar($_POST['txtlname']);
	//$txtfullname       = $txttitle.' '.$txtfname.' '.$txtlname;
	$txtphone            = replaceSpecialChar($_POST['txtphone']);
	$txtmobile           = replaceSpecialChar($_POST['txtmobile']);
	$email               = $_POST['txtemail'];
	$txtemergname        = replaceSpecialChar($_POST['txtemergname']);
	$txtemergnum         = replaceSpecialChar($_POST['txtemergnum']);
	$txtriscassess       = $_POST['txtriscassess'];
	//$txtraworkername   = replaceSpecialChar($_POST['txtraworkername']);
	//$txtrasocialnum    = replaceSpecialChar($_POST['txtrasocialnum']);
	$txtcservice         = $_POST['txtcservice'];
	$txtcsworkername     = replaceSpecialChar($_POST['txtcsworkername']);
	$txtcssocialnum      = replaceSpecialChar($_POST['txtcssocialnum']);
	$txtstatement        = $_POST['txtstatement'];
	$txtnumhr            = replaceSpecialChar($_POST['txtnumhr']);
	$txtborough          = $_POST['txtborough'];
	$txtnicname          = replaceSpecialChar($_POST['txtnicname']);
	$txtdiagnosis        = replaceSpecialChar($_POST['txtdiagnosis']);
	$txtcommunicateby    = replaceSpecialChar($_POST['txtcommunicateby']);
	$txtmobility         = replaceSpecialChar($_POST['txtmobility']);
	$txtmedicalneeds     = $_POST['txtmedicalneeds'];
	$txtmedicalindicate  = replaceSpecialChar($_POST['txtmedicalindicate']);
	$txtpersonalneeds    = $_POST['txtpersonalneeds'];
	$txtpersonalindicate = replaceSpecialChar($_POST['txtpersonalindicate']);
	$txtchildbehave      = replaceSpecialChar($_POST['txtchildbehave']);
	$txtchildanxious     = replaceSpecialChar($_POST['txtchildanxious']);
	$txtchildhappy       = replaceSpecialChar($_POST['txtchildhappy']);
	$txtchildactivity    = replaceSpecialChar($_POST['txtchildactivity']);
	$txtchilddislike     = replaceSpecialChar($_POST['txtchilddislike']);
	$txtchildcalm        = replaceSpecialChar($_POST['txtchildcalm']);
	$txtchildfood        = replaceSpecialChar($_POST['txtchildfood']);
	$txtchilddislikefood = replaceSpecialChar($_POST['txtchilddislikefood']);
	$txtchildallergic    = replaceSpecialChar($_POST['txtchildallergic']);
	$txtchildabout       = replaceSpecialChar($_POST['txtchildabout']);
	$txtchildgpcontact   = replaceSpecialChar($_POST['txtchildgpcontact']);
	$txtchildhospital    = replaceSpecialChar($_POST['txtchildhospital']);
	$txtchildpermition   = $_POST['txtchildpermition'];
	$txtchildpublicity   = $_POST['txtchildpublicity'];
	$txtchildtakepic     = $_POST['txtchildtakepic'];
	$txtchildsuncream    = $_POST['txtchildsuncream'];
	$txtchildescort      = $_POST['txtchildescort'];
	$txtchildaid         = $_POST['txtchildaid'];
	$txtchildswim        = $_POST['txtchildswim'];
	$txtchildgetswim     = $_POST['txtchildgetswim'];

	if(isset($_POST['txtservices']) && !empty($_POST['txtservices']))
	{
		$txtservices = implode(',',$_POST['txtservices']);
	}
	$extraservice     = replaceSpecialChar($_REQUEST['txtextraser']);
	$txtotherservice1 = replaceSpecialChar($_POST['txtotherservice1']);
	$txtotherservice2 = replaceSpecialChar($_POST['txtotherservice2']);
	$txtotherservice3 = replaceSpecialChar($_POST['txtotherservice3']);
	$txtbudget        = $_POST['txtbudget'];
	$txtallowance     = $_POST['txtallowance'];
	$txtsnap          = replaceSpecialChar($_POST['txtsnap']);
	$txtpositive      = replaceSpecialChar($_POST['txtpositive']);
	$txtinfo          = replaceSpecialChar($_POST['txtinfo']);
	$profile_id = $_REQUEST['profile_id'];
	
	//echo $action;exit;
	
	if($action == "edit")
	{
		$pupil_id 			= decrypt($_POST['pupil_id'],$encrypt);

		if($txtcfname != '' && $txtclname != '')
		{
			$check_qry = "SELECT * FROM ".$tbname."_pupilmaster WHERE _ID = $pupil_id";
			$run_check = mysqli_query($con, $check_qry);
			$num = mysqli_num_rows($run_check);
			if($num > 0)
			{
				$fetch_data = mysqli_fetch_assoc($run_check);
				
				$query 	= "UPDATE ".$tbname."_pupilmaster SET ";
				
				if(isset($txtcfname) && $txtcfname!= ''){
					$query .= "`_FirstName` = '".$txtcfname."' ,";
				}else{
					$query .= "`_FirstName` = null,";
				}

				if(isset($txtclname) && $txtclname!= ''){
					$query .= "`_LastName` = '".$txtclname."' ,";
				}else{
					$query .= "`_LastName` = null,";
				}
				/*if(isset($txtcarer) && $txtcarer!= '')
				{
					$query .= "`_ClientID` = '".$txtcarer."' ,";
				}
				else
				{
					$query .= "`_ClientID` = null,";
				}*/
				if(isset($txtfsurname) && $txtfsurname!= ''){

					$query .= "`_FamilySurname` = '".$txtfsurname."' ,";

				}else{

					$query .= "`_FamilySurname` = null,";

				}		
				if(isset($txtdob) && $txtdob!= ''){
					
					$query .= "`_DOB` = '".$txtdob."' ,";

				}else{
					
					$query .= "`_DOB` = null,";

				}
				if(isset($txtethnicity) && $txtethnicity!= ''){
					
					$query .= "`_Ethnicity` = '".$txtethnicity."' ,";

				}else{
					
					$query .= "`_Ethnicity` = null,";

				}
				if(isset($txtgender) && $txtgender!= ''){
					
					$query .= "`_Gender` = '".$txtgender."' ,";

				}else{
					
					$query .= "`_Gender` = null,";

				}
				if(isset($extraservice) && $extraservice!= ''){
					
					$query .= "_Extraservice = '".$extraservice."' ,";

				}else{
					
					$query .= "_Extraservice = null,";

				}

				if(isset($profile_id) && $profile_id!= ''){
					$query .= "_Profileid = '".$profile_id."' ,";
				}else{
					$query .= "_Profileid = null,";
				}

				if(isset($txtschool) && $txtschool!= ''){
					
					$query .= "`_School`= ".$txtschool." ,";

				}else{
					
					$query .= "`_School`=null,";

				}
				if(isset($txtresidential) && $txtresidential!= ''){
					
					$query .= "`_Residential` = '".$txtresidential."' ,";

				}else{
					
					$query .= "`_Residential` = null,";

				}
				if(isset($txtparent) && $txtparent!= ''){
					
					$query .= "`_Carer` = '".$txtparent."' ,";

				}else{
					
					$query .= "`_Carer` = null,";

				}
				
				if(isset($txttitle) && $txttitle!= ''){
					
					$query .= "`_CarerTitle` = '".$txttitle."' ,";

				}else{
					
					$query .= "`_CarerTitle` = null,";

				}
				
				if(isset($txtfname) && $txtfname!= ''){
					
					$query .= "`_CarerFirstName` = '".$txtfname."' ,";

				}else{
					
					$query .= "`_CarerFirstName` = null,";

				}
				
				if(isset($txtlname) && $txtlname!= ''){
					
					$query .= "`_CarerLastName` = '".$txtlname."' ,";

				}else{
					
					$query .= "`_CarerLastName` = null,";

				}
				
				if(isset($txtphone) && $txtphone!= ''){
					
					$query .= "`_CarerPhone` = '".$txtphone."' ,";

				}else{
					
					$query .= "`_CarerPhone` = null,";

				}
				
				if(isset($txtmobile) && $txtmobile!= ''){
					
					$query .= "`_CarerMobile` = '".$txtmobile."' ,";

				}else{
					
					$query .= "`_CarerMobile` = null,";

				}
				if(isset($email) && $email!= ''){
					
					$query .= "`_Email` = '".$email."' ,";

				}else{
					
					$query .= "`_Email` = null,";

				}
				if(isset($txtemergname) && $txtemergname!= ''){
					
					$query .= "`_EmerContactName` = '".$txtemergname."' ,";

				}else{
					
					$query .= "`_EmerContactName` = null,";

				}
				
				if(isset($txtemergnum) && $txtemergnum!= ''){
					
					$query .= "`_EmerContactNumber`='".$txtemergnum."' ,";

				}else{
					
					$query .= "`_EmerContactNumber` = null,";

				}
				if(isset($txtriscassess) && $txtriscassess!= ''){
					
					$query .= "`_RiscAssessment` = '".$txtriscassess."' ,";

				}else{
					
					$query .= "`_RiscAssessment` = null,";

				}
				/*if(isset($txtraworkername) && $txtraworkername!= ''){
					
					$query .= "`_RASocialName` = '".$txtraworkername."' ,";

				}else{
					*/
					$query .= "`_RASocialName` = null,";

				/*}
				if(isset($txtrasocialnum) && $txtrasocialnum!= ''){
					
					$query .= "`_RASocialContact` ='".$txtrasocialnum."' ,";

				}else{
					*/
					$query .= "`_RASocialContact` = null,";

				/* } */
				if(isset($txtcservice) && $txtcservice!= ''){
					
					$query .= "`_DisableService` = '".$txtcservice."' ,";

				}else{
					
					$query .= "`_DisableService` = null,";

				}
				if(isset($txtcsworkername) && $txtcsworkername!= ''){
					
					$query .= "`_DSSocialName` = '".$txtcsworkername."' ,";

				}else{
					
					$query .= "`_DSSocialName` = null,";

				}
				if(isset($txtcssocialnum) && $txtcssocialnum!= ''){
					
					$query .= "`_DSSocialContact` = '".$txtcssocialnum."' ,";

				}else{
					
					$query .= "`_DSSocialContact` = null,";

				}
				if(isset($txtstatement) && $txtstatement!= ''){
					
					$query .= "`_Statement` = '".$txtstatement."' ,";

				}else{
					
					$query .= "`_Statement` = null,";

				}
				if(isset($txtnumhr) && $txtnumhr!= ''){
					
					$query .= "`_RecHours` = '".$txtnumhr."' ,";

				}else{
					
					$query .= "`_RecHours` = null,";

				}
				if(isset($txtborough) && $txtborough!= ''){
					
					$query .= "`_ReceiveBudget` = '".$txtborough."' ,";

				}else{
					
					$query .= "`_ReceiveBudget` = null,";
				}
				if(isset($txtnicname) && $txtnicname!= ''){

					$query .= "`_NicName` = '".$txtnicname."' ,";

				}else{
					
					$query .= "`_NicName` = null,";

				}
				if(isset($txtdiagnosis) && $txtdiagnosis!= ''){
					
					$query .= "`_MyDiagnosis` = '".$txtdiagnosis."' ,";

				}else{
					
					$query .= "`_MyDiagnosis` = null,";

				}
				if(isset($txtcommunicateby) && $txtcommunicateby!= ''){
					
					$query .= "`_CommunicateBy` = '".$txtcommunicateby."' ,";

				}else{
					
					$query .= "`_CommunicateBy` = null,";

				}
				
				if(isset($txtmobility) && $txtmobility!= ''){
					
					$query .= "`_MyMobility` = '".$txtmobility."' ,";

				}else{
					
					$query .= "`_MyMobility` = null,";

				}
				
				if(isset($txtmedicalneeds) && $txtmedicalneeds!= ''){
					
					$query .= "`_MedicalNeeds` = '".$txtmedicalneeds."' ,";

				}else{
					
					$query .= "`_MedicalNeeds` = null,";

				}
				
				if(isset($txtmedicalindicate) && $txtmedicalindicate!= ''){
					
					$query .= "`_MedicationFor` = '".$txtmedicalindicate."' ,";

				}else{
					
					$query .= "`_MedicationFor` = null,";

				}
				
				if(isset($txtpersonalneeds) && $txtpersonalneeds!= ''){
					
					$query .= "`_PersonalCareNeed` = '".$txtpersonalneeds."' ,";

				}else{
					
					$query .= "`_PersonalCareNeed` = null,";

				}
				if(isset($txtpersonalindicate) && $txtpersonalindicate!= ''){
					
					$query .= "`_PersonalCareFor` = '".$txtpersonalindicate."' ,";

				}else{
					
					$query .= "`_PersonalCareFor` = null,";

				}
				if(isset($txtchildbehave) && $txtchildbehave!= ''){
					
					$query .= "`_Behaviour` = '".$txtchildbehave."' ,";

				}else{
					
					$query .= "`_Behaviour` = null,";

				}
				if(isset($txtchildanxious) && $txtchildanxious!= ''){
					
					$query .= "`_WhenAnxious` = '".$txtchildanxious."' ,";

				}else{
					
					$query .= "`_WhenAnxious` = null,";

				}
				if(isset($txtchildhappy) && $txtchildhappy!= ''){
					
					$query .= "`_WhenHappy` = '".$txtchildhappy."' ,";

				}else{
					
					$query .= "`_WhenHappy` = null,";

				}
				if(isset($txtchildactivity) && $txtchildactivity!= ''){
					
					$query .= "`_LikedActivities` = '".$txtchildactivity."' ,";

				}else{
					
					$query .= "`_LikedActivities` = null,";

				}
				if(isset($txtchilddislike) && $txtchilddislike!= ''){
					
					$query .= "`_DislikedActivities` = '".$txtchilddislike."' ,";

				}else{
					
					$query .= "`_DislikedActivities` = null,";

				}
				if(isset($txtchildcalm) && $txtchildcalm!= ''){
					
					$query .= "`_CalmBy` = '".$txtchildcalm."' ,";

				}else{
					
					$query .= "`_CalmBy` = null,";

				}
				if(isset($txtchildfood) && $txtchildfood!= ''){
					
					$query .= "`_LikedFood` = '".$txtchildfood."' ,";

				}else{
					
					$query .= "`_LikedFood` = null,";
				}
				if(isset($txtchilddislikefood) && $txtchilddislikefood!= ''){
					
					$query .= "`_DislikedFood` = '".$txtchilddislikefood."' ,";

				}else{
					
					$query .= "`_DislikedFood` = null,";

				}
				if(isset($txtchildallergic) && $txtchildallergic!= ''){
					
					$query .= "`_Allergic` = '".$txtchildallergic."' ,";

				}else{
					
					$query .= "`_Allergic` = null,";

				}
				if(isset($txtchildabout) && $txtchildabout!= ''){
					
					$query .= "`_AboutMe` = '".$txtchildabout."' ,";

				}else{
					
					$query .= "`_AboutMe` = null,";

				}
				if(isset($txtchildgpcontact) && $txtchildgpcontact!= ''){
					
					$query .= "`_GPContact` = '".$txtchildgpcontact."' ,";

				}else{
					
					$query .= "`_GPContact` = null,";

				}
				if(isset($txtchildhospital) && $txtchildhospital!= ''){
					
					$query .= "`_HospitalConsultant` = '".$txtchildhospital."' ,";

				}else{
					
					$query .= "`_HospitalConsultant` = null,";

				}
				if(isset($txtchildpermition) && $txtchildpermition!= ''){
					
					$query .= "`_OutingPermitted` = '".$txtchildpermition."' ,";

				}else{
					
					$query .= "`_OutingPermitted` = null,";

				}
				if(isset($txtchildpublicity) && $txtchildpublicity!= ''){
					
					$query .= "`_AdvertisePermitted` = '".$txtchildpublicity."' ,";

				}else{
					
					$query .= "`_AdvertisePermitted` = null,";

				}
				if(isset($txtchildtakepic) && $txtchildtakepic!= ''){
					
					$query .= "`_CompletedPassport` = '".$txtchildtakepic."' ,";

				}else{
					
					$query .= "`_CompletedPassport` = null,";

				}
				if(isset($txtchildsuncream) && $txtchildsuncream!= ''){

					$query .= "`_SunCream` = '".$txtchildsuncream."' ,";

				}else{

					$query .= "`_SunCream` = null,";

				}
				if(isset($txtchildescort) && $txtchildescort!= ''){

					$query .= "`_EmergencyEscort` = '".$txtchildescort."' ,";
				
				}else{

					$query .= "`_EmergencyEscort` = null,";
				
				}
				if(isset($txtchildaid) && $txtchildaid!= ''){

					$query .= "`_EmergencyAid` = '".$txtchildaid."' ,";
				
				}else{

					$query .= "`_EmergencyAid` = null,";
				
				}
				if(isset($txtchildswim) && $txtchildswim!= ''){

					$query .= "`_AbleSwim` = '".$txtchildswim."' ,";

				}else{

					$query .= "`_AbleSwim` = null,";

				}
				if(isset($txtchildgetswim) && $txtchildgetswim!= ''){

					$query .= "`_GetPool` = '".$txtchildgetswim."' ,";

				}else{

					$query .= "`_GetPool` = null,";
				}
				/*if(isset($txtservices) && $txtservices!= ''){

					$query .= "`_Services` = '".$txtservices."' ,";
				
				}else{
					*/
					$query .= "`_Services` = null,";
/*
				}
				if(isset($txtotherservice1) && $txtotherservice1!= ''){

					$query .= "`_OtherService1` = '".$txtotherservice1."' ,";

				}else{
*/
					$query .= "`_OtherService1` = null,";
/*
				}
				if(isset($txtotherservice2) && $txtotherservice2!= ''){

					$query .= "`_OtherService2` = '".$txtotherservice2."' ,";

				}else{
*/
					$query .= "`_OtherService2` = null,";
/*
				}
				if(isset($txtotherservice3) && $txtotherservice3!= ''){

					$query .= "`_OtherService3` = '".$txtotherservice3."' ,";

				}else{
*/
					$query .= "`_OtherService3` = null,";

				/* } */
				if(isset($txtbudget) && $txtbudget!= ''){

					$query .= "`_ReceiveBudgetLBB` = '".$txtbudget."' ,";

				}else{

					$query .= "`_ReceiveBudgetLBB` = null,";

				}
				if(isset($txtallowance) && $txtallowance!= ''){

					$query .= "`_DisabilityAllowance` = '".$txtallowance."' ,";

				}else{

					$query .= "`_DisabilityAllowance` = null,";

				}
				if(isset($txtsnap) && $txtsnap!= ''){

					$query .= "`_WhyAccessSNAP` = '".$txtsnap."' ,";

				}else{

					$query .= "`_WhyAccessSNAP` = null,";

				}
				if(isset($txtpositive) && $txtpositive!= ''){

					$query .= "`_WantAchieve` = '".$txtpositive."' ,";

				}else{

					$query .= "`_WantAchieve` = null,";

				}
				if(isset($txtinfo) && $txtinfo!= ''){

					$query .= "`_OtherInfo` = '".$txtinfo."'";

				}else{
					
					$query .= "`_OtherInfo` = null";

				}
			
				$query .= " WHERE _ID = ".$pupil_id;
				
				$run = mysqli_query($con , $query);

				if($run)
				{	
					$sel_rel = "Select * from ".$tbname."_clipup_rel where _PupilID = '".$pupil_id."'";
					$rstrel  = mysqli_query($con,$sel_rel);
					/*if($txtcarer != ''){

						if(mysqli_num_rows($rstrel) > 0){

							$updrel = "update ".$tbname."_clipup_rel set _ClientID = '".$txtcarer."' where _PupilID = '".$pupil_id."'";
							mysqli_query($con,$updrel);

						}else{

							$insstu = "insert into ".$tbname."_clipup_rel (_ClientID,_PupilID) values ('".$txtcarer."','".$pupil_id."')";
							mysqli_query($con,$insstu);

						}
					}else{
						if(mysqli_num_rows($rstrel) > 0){
							$delrel = "Delete from ".$tbname."_clipup_rel where _PupilID = '".$pupil_id."'";
							mysqli_query($con,$delrel);
						}
					}*/
					//log create start
						/*$pemail_query = "SELECT _Email FROM ".$tbname."_pupilmaster WHERE _ID = ".$pupil_id;
						$run_pemail = mysqli_query($con , $pemail_query);
						$fetch_pemail = mysqli_fetch_assoc($run_pemail);
						$pupilemail = $fetch_pemail['_Email'];*/
					//	$create_log = auditlog($msg = "Updated Children ".$email);
					//log create end
					
					header("location:view_children.php");
					exit;
				}
				else
				{
					header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&id=".encrypt($pupil_id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
					exit;
				}
			}
			else
			{
				header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&id=".encrypt($pupil_id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
				exit;
			}
		}
		else
		{
			header("location:edit-pupil.php?result=".encrypt('failed',$encrypt)."&id=".encrypt($pupil_id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
			exit;
		}
	}
?>