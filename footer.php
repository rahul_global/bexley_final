  <div class="container">
    <div class="row">
      <div class="col-securepayments">
        <h5>Secure payments</h5>
        <img alt="FRSB – Give with confidence" src="img/logo-frsb.png" /> </div>
      <div class="col-challengers"> <!-- <img class="img-responsive" alt="Bexley Snap" src="img/logo-challengers-tear.jpg" /> -->
        <p>Registered Charity Number: 1167151<br />
          Bexley Snap is a Charitable Incorporated Organisation</p>
      </div>
      <div class="col-newsletter">
        <h5>Keep up-to-date with our newsletter</h5>
        <div class="form-wrapper">
          <form id="formNewsletter" class="" action="http://disability-challengers.org/newsletter/" method="GET">
            <div class="form-group">
              <div class="input-group input-group-h"> <span class="input-group-addon" style="padding:0px;border:0px;">
                <label class="form-label form-label-alt form-label-sml" for="inputEmail">Email</label>
                </span> <span class="form-control-wrapper">
                <input id="inputEmail" class="form-control form-control-alt form-control-sml" name="email" type="email" placeholder="" />
                </span> </div>
            </div>
            <div class="form-group">
              <button class="btn btn-alt btn-sml btn-bounce" type="submit">Sign up</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-credit">
    <div class="container">
      <div class="menu-footer-menu-container">
        <ul id="menu-footer-menu" class="footer-nav">
          <li id="menu-item-2654" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2654"><a target="_blank" href="#">Accessibility</a></li>
          <li id="menu-item-506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-506"><a href="PDFs/privacy-notice-families.docx">Privacy policy</a></li>
          <li id="menu-item-505" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-505"><a href="#">Terms &#038; conditions</a></li>
          <li id="menu-item-510" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-510"><a href="#">Media centre</a></li>
          <li id="menu-item-2041" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2041"><a href="#">Feedback</a></li>
		  <li><a href="contact.php" title="Contact us">Contact us</a>
		  </li>
        </ul>
      </div>
      <div class="copyright"> Bexley Snap &copy; 2018 </div>
    </div>
  </div>