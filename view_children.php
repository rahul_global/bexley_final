<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

$id = $_SESSION['frontuserid'];

$pupil_qry = "select pm._ID as pid, pm._DOB, CONCAT(pm._Firstname,' ' ,pm._Lastname) as child_name,cl._ClientID, cl._PupilID, cm._Lastname as clientname  FROM ".$tbname."_pupilmaster as pm left join ".$tbname."_clipup_rel as cl on cl._PupilID = pm._ID left join ".$tbname."_clientmaster as cm on cm._ID = cl._ClientID WHERE cl._ClientID = $id GROUP BY pm._ID";
//echo $pupil_qry;exit;
$rs = mysqli_query($con,$pupil_qry);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-2">
					<?php include 'user_menu.php'; ?>
				</div>
				<div class="col-md-10">
					<br>
					<table class="table">
						<thead>
							  <tr>
							  	<!-- <th>Sr no.</th> -->
								<th>Child Name</th>
								<th>Family Name</th>
								<th>Date of Birth</th>
								<th>View</th>
								<th>Edit</th>
							  </tr>
						</thead>
						<tbody>


						<?php
						$cnt = 0;
						while($row = mysqli_fetch_assoc($rs))
						{
							$cnt++;
						?>
							<tr>
								<!-- <td><?php // echo $cnt; ?></td> -->
								<td><?php echo $row['child_name'] ? $row['child_name'] : ''; ?></td>
								<td><?php echo $row['clientname'] ? $row['clientname'] : ''; ?></td>
								<td><?php echo date("d-M-Y",strtotime($row['_DOB']));?></td>
								<td><a href="view_childrendetail.php?id=<?php echo encrypt($row['pid'],$encrypt);?>&e_action=<?php echo encrypt('view',$encrypt);?>">View Details</a></td>
								<td><a href="view_childrendetail.php?id=<?php echo encrypt($row['pid'],$encrypt);?>&e_action=<?php echo encrypt('edit',$encrypt);?>">Edit Details</a></td>
							</tr>
						<?php
						}
						?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>