<?php
session_start();
include_once('admin/db/dbopen.php');

$result = isset($_GET['result']) && $_GET['result'] != '' ? decrypt($_GET['result'] , $encrypt) : '';

if(!isset($_SESSION['frontuserid']) || $_SESSION['frontuserid'] == '')
{
	header('location:login.php');
	exit;
}

$id = $_SESSION['frontuserid'];

$query  = "SELECT * FROM ".$tbname."_clientmaster WHERE _ID = ".$id;
$run 	= mysqli_query($con , $query);
$fetch  = mysqli_fetch_assoc($run);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-2">
					<ul class="menu">
						<li ><a href="booking.php">Make a Booking</a></li>
						<li><a href="viewbooking.php">View Bookings</a></li>
						<li><a href="profile.php">Profile</a></li>
					</ul>

				</div>
				<div class="col-md-10">
					<?php
						$cnt = 0;
						$booking = "SELECT ww_booked_lessons.* , ww_pupilmaster._FirstName, ww_pupilmaster._LastName  FROM  `ww_booked_lessons` JOIN  `ww_pupilmaster` ON ww_booked_lessons._PupilID = ww_pupilmaster._ID WHERE ww_booked_lessons._ClubID != '0'";
						$getclubid = "SELECT * FROM ".$tbname."_booked_lessons WHERE _ClubID != '0'";
						$getclubidresult = mysqli_query($con,$getclubid);
						$clubids = array();
						$ids = array();
						$clubnameforclnt = array();
						$clubidforclnt = array();
						while ($getclubids = mysqli_fetch_assoc($getclubidresult)) {
							$clubids[] = explode(",",$getclubids['_ClubID']);
						}
						
						foreach ($clubids as $key ) {
							foreach ($key as $ke ) {
								$ids[] =  $ke;
							}
						}
						//echo '<pre>';
						$ids = array_unique($ids);
						$idsstr = implode(',',$ids);
						$idsstr = rtrim($idsstr,',');
						//print_r(implode(',',$ids));
						$clubname = "SELECT * FROM ".$tbname."_bookingtype WHERE _ID IN( $idsstr )";
						/*echo $clubname;
						exit;*/
						$clubnameresult = mysqli_query($con,$clubname);

						
						while($clubnames = mysqli_fetch_assoc($clubnameresult))
						{
							$clubidforclnt[] = $clubnames['_ID'];
							$clubnameforclnt[] = $clubnames['_Type'];
						}
						$cbids = implode(',',$clubidforclnt);
						/*echo "SELECT * FROM ".$tbname."_timeslot WHERE _Bookingtype IN ( $idsstr )";
						exit;*/
						/*echo $clubname;
						exit;*/
						$bookingdetail = mysqli_query($con,$booking);

					?>
					<table class="table">
						<thead>
							  <tr>
							  	<th>Sr no.</th>
								<th>Booking id</th>
								<th>Child</th>
								<th>Club</th>
								<th>Date</th>
								<th>From Time</th>
								<th>To Time</th>	
							  </tr>
						</thead>
						<tbody>
							<?php
								while($bookingresult = mysqli_fetch_assoc($bookingdetail))
								{
									$clubid = $bookingresult['_ClubID'];
									$getclubsname = mysqli_query($con , "SELECT * FROM ".$tbname."_bookingtype WHERE _ID = '".$clubid."'");
									$getclubsnameresult = mysqli_fetch_assoc($getclubsname);
									$cnt++;
									?>
									<tr>
										<td><?php echo $cnt; ?></td>
										<td><?php echo $bookingresult['_ID']; ?></td>
										<td><?php echo $bookingresult['_FirstName'].' '.$bookingresult['_LastName']; ?></td>
										<td><?php echo $getclubsnameresult['_Type']; ?></td>
										<td><?php echo date("d-m-Y",strtotime($bookingresult['_Date'])); ?></td>
										<td><?php echo $bookingresult['_Fromtime']; ?></td>
										<td><?php echo $bookingresult['_Totime']; ?></td>									
									</tr>
									<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>