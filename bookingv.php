<?php
session_start();
include_once('admin/db/dbopen.php');

/*echo "<pre>";print_r ($_REQUEST);exit;*/

$action       = decrypt($_REQUEST['e_action'],$encrypt);
$id = $_SESSION['frontuserid'];
$pupilid      = $_REQUEST['txtpupil'];
$slot   = !empty($_REQUEST['slot']) ? $_REQUEST['slot'] : '';
$gch = "SELECT _FirstName,_LastName FROM ".$tbname."_pupilmaster WHERE _ID = '".$pupilid."'";
$rch 	= mysqli_query($con , $gch);
$fetchch  = mysqli_fetch_assoc($rch);
$chname = $fetchch['_FirstName']." ".$fetchch['_LastName'];
$clbs = '';


$query  = "SELECT bt._ID,bt._Type,bt._Price FROM ".$tbname."_timeslot ts inner join ".$tbname."_bookingtype as bt on bt._ID = ts._Bookingtype WHERE ts._ID IN ('" . implode("','",$slot) . "') group by ts._Bookingtype order by bt._Type";
$run 	= mysqli_query($con , $query);
$nrun = mysqli_num_rows($run);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bexley Snap CRM: Booking</title>
		<?php include 'topscript.php'; ?>
		 <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		  <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
		  <style type="text/css">
			.error{color:#ff0000;font-weight:bold;font-size:14px;}
		  </style>
		  <script type="text/javascript">
		  	function validate()
		  	{
		  		if(document.getElementById("alternate").checked == false)
		  		{
		  			document.frmpay.action = "paypal/paypalbooking.php";
		  			return true;
		  		}
		  		else
		  		{
		  			document.frmpay.action = "bookingvaction.php";
		  			return true;
		  		}
		  	}
		  </script>
	</head>
	<body>
		<header id="header" role="banner">
			<?php include_once('header.php');?>
		</header>
		<?php include_once('menu.php');?>
		<div class="container">
			<div class="col-md-12">
				
				<table class="table">
					<tr align="center">
						<td><h2><?php echo $chname;?></h2></td>
					</tr>
				</table>

				<table class="table">
					<?php
					if($nrun > 0)
					{
						$prctot = 0;
						while($fetch = mysqli_fetch_assoc($run))
						{
							$prc = 0;
							$prc = $fetch['_Price'];

							$clbs .= $fetch['_Type'].',';
							?>
							<tr>	
								<td>&nbsp;</td>
							</tr>
							<tr>	
								<td><b><u><?php echo $fetch['_Type']; ?></u></b></td>
							</tr>
							<?php

							$qrsts = "SELECT * FROM ".$tbname."_timeslot WHERE _ID IN ('" . implode("','",$slot) . "') and _Bookingtype = '".$fetch['_ID']."' order by _Date";

							$rsts = mysqli_query($con,$qrsts);
							$nsts = mysqli_num_rows($rsts);

							if($nsts > 0)
							{
							?>
							<tr>
								<td>
									<table class="table">
										<thead>
											<tr>
											  	<th>Sr no.</th>
												<th>Date</th>
												<th>From Time</th>
												<th>To Time</th>
												<th>Price</th>
											</tr>
										</thead>
										<tbody>
										<?php
										while($fetchts = mysqli_fetch_assoc($rsts))
										{
											$cnt++;
											?>
											<tr>
												<td><?php echo $cnt; ?></td>
												<td><?php echo date("d-M-Y",strtotime($fetchts['_Date']));?></td>
												<td><?php echo $fetchts['_Fromtime']; ?></td>
												<td><?php echo $fetchts['_Totime']; ?></td>
												<td>&#163;<?php echo number_format((float)$prc, 2, '.', ''); ?></td>
											</tr>
											<?php
											$prctot += $prc;
										}?>

										</tbody>
									</table>
								</td>
							</tr>
							<?php	
							}
						}
						?>

						<tr>	
							<td>
								<table class="table">
									<thead>
										  <tr>
										  	<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th style="text-align: right;"><b><u>Total : &#163;<?php echo number_format((float)$prctot, 2, '.', '');?></u></b></th>
										  </tr>
									</thead>
								</table>	
							</td>
						</tr>
					<?php	
					}
					$slot = implode(',',$slot);
					
					?>
				</table>		
			</div>
			

			<form name="frmpay" method="post" action="#" onsubmit="return validate();">
				<input type="hidden" name="pupilid" id="pupilid" value="<?php echo $pupilid;?>">
				<input type="hidden" name="clbs" id="clbs" value="<?php echo $clbs;?>">
				<input type="hidden" name="slot" id="slot" value="<?php echo $slot;?>">
				<input type="hidden" name="amount" id="amount" value="<?php echo $prctot;?>">

				<div class="col-md-12">
					<div class="col-md-1">
						<input type="checkbox" class="form-control" style="opacity:1;-webkit-appearance: checkbox;height:23px !important;display:block;" value="1" name="alternate" id="alternate">
					</div>
					
					<div class="col-md-11">
						Alternate Payment (Cash)
					</div>
					
				</div>

				<div class="col-md-12">
					<input class="btn btn-primary btn2" style="margin-bottom:30px;margin-top:15px" type="submit" value="Pay Now" name="pay">
				</div>
			</form>
		</div>
		<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
	</body>
</html>