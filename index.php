<?php
session_start();
?>
<html>
<head>

<title>Bexley Snap | Support for families of disabled children </title>		
<meta name="keywords" content="" />		
<meta name="Description" content="Bexley Snap is a parent-led support agency for families of disabled children and young people in Bexley and provides a wide range of services to both children and their parents and carers." />
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<?php 
include_once('topscript.php');
?>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
<?php include('menu.php');?>
<div class="section section-masthead">
  <div id="owl-demo" class="owl-carousel">
    <div class="item" style="background-image: url('img/img-masthead-home.jpg'); background-position:center; height:625px; margin:0; padding:0"> </div>
    <div class="item" style="background-image: url('img/Special-Needs-AdobeStock_116987422-copy.jpg'); background-position:center; height:625px; margin:0; padding:0"></div>
    <div class="item" style="background-image: url('img/child-images-23.jpg'); background-position:center; height:625px; margin:0; padding:0"></div>
  </div>
  <div class="left-panel">
  <div class="container">
  	<div class="col-md-5 col-md-offset-7">
    <div class="section-masthead-content">
      <div class="content-position-outer">
        <div class="content-center-inner">
          <div class="wysiwyg wysiwyg-padded">
            <h5>
              <p>Bexley Snap develops and delivers inclusive play and leisure for disabled children and young people.</p>
            </h5>
            <blockquote>
              <p>&ldquo;Bexley Snap is a lifeline to my son and our whole family&rdquo;</p>
            </blockquote>
			
			
			
			<p class="left-btn"><a class="btn2 btn-alt btn-bounce" href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=raI=" title="What’s going on">What’s going on</a></p>
            <p class="left-btn"><a class="btn2 btn-alt btn-bounce" href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=qqA=" title="Calendar">Calendar</a></p>
            <p class="left-btn"><a class="btn2 btn-alt btn-bounce" href="http://techcitywebdesign.co.uk/Bexley/login.php" title="Book a place">Book a place</a></p>
            <!--<p class="left-btn"><a class="btn2 btn-alt btn-bounce" href="https://uk.virginmoneygiving.com/charity-web/charity/finalCharityHomepage.action?charityId=1001927" title="Donate Now" target="_blank">Donate Now</a></p>-->
            <p class="left-btn"><a class="btn2 btn-alt btn-bounce" href="http://techcitywebdesign.co.uk/Bexley/register.php" title="Register here">Register here</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

<div class="my-newz">
	
	<div class="alert alert-danger alert-dismissible" role="alert">
  <!--<button type="button" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
  <!--<strong><i class="fa fa-warning"></i> Danger!</strong>--> <marquee><i><p style="font-size: 18pt;font-style: italic;">Last minute place available for this weekend !</p></i></marquee>
</div>
	
</div>


<div id="js-main-content" class="main-content"> 
  <section class="section section-content">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="home-video-container">
            <div class="video-wrapper">
              <div class="video-placeholder" style="background-image: url('img/summer-young-people-playing-tag.png');"></div>
              <iframe id="video" width="100%" height="315"
				src="https://www.youtube.com/embed/QcR3a1bej0c"> </iframe>
              
              <!--<div class="embed-responsive embed-responsive-16by9">
              
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zMUxuIf11Ag"></iframe>
              </div>--> 
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="wysiwyg wysiwyg-padded">
            <h2 class="h3"><strong>We are</strong><br />
              Bexley Snap!</h2>
            <p>At Bexley Snap we provide exciting, challenging, and – most importantly – non-exclusive play and leisure activities for disabled children and young people.</p>
            <h6>We achieve this through:</h6>
            <ul>
              <li>Preschool groups</li>
              <li>Family Days</li>
              <li>Play schemes</li>
              <li>Youth schemes &amp; overnight activities</li>
              <li>Young Adult Schemes</li>
            </ul>
            <div class="row sml-gutter">
              <div class="col-sm-6"><a class="btn btn-alt btn-block btn-bounce" title="About us" href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=rKM=">About us</a></div>
              <div class="col-sm-6"><a class="btn btn-alt btn-block btn-bounce" title="What's going on" href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=raI=">What's going on</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-accordion">
  	<div class="container">
    	<div class="col-md-3">
        	<div class="event-sec">
            	<img src="img/collection-boxes.png" class="img-responsive"/>
                <div class="evnet-hover">
                	<h4>Donate</h4>
                    <p>Your donation means the world to us!</p>
                    <span class="btn btn-bounce"><a target="_blank" href="https://uk.virginmoneygiving.com/charity-web/charity/finalCharityHomepage.action?charityId=1001927">Read more</a></span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        	<div class="event-sec">
            	<img src="img/fundraising-asda-bag-packing-group.png" class="img-responsive"/>
                <div class="evnet-hover">
                	<h4>Volunteer</h4>
                    <p>Gain unique experiences helping us.</p>
                    <span class="btn btn-bounce"><a href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=q58=">Read more</a></span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        	<div class="event-sec">
            	<img src="img/fundraising-asda-group-2013.png" class="img-responsive"/>
                <div class="evnet-hover">
                	<h4>Fundraise</h4>
                    <p>Help us achieve our goals.</p>
                    <span class="btn btn-bounce"><a href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=q6A=">Read more</a></span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        	<div class="event-sec">
            	<img src="img/rose-robb-and-rebecca-mcLachlan-adidas-5k.png" class="img-responsive"/>
                <div class="evnet-hover">
                	<h4>Work here</h4>
                    <p>We offer brilliant training opportunities.</p>
                    <span class="btn btn-bounce"><a href="http://techcitywebdesign.co.uk/Bexley/inner.php?id=r50=">Read more</a></span>
                </div>
            </div>
        </div>
    </div>
  </section>
  <!--<section class="section section-orange">
    <div class="container">
      <h2 class="title-news-events h3"><strong>What's going on</strong> in Bexley Snap</h2>
      <ul class="list-news-events row">
        <li class="grid-item col-xs-12 col-md-6"> <a class="article article-featured" href="#" title="Read more" style="background-image: url('img/families-three-kids-in-play-area.png');">
          <div data-type="View event" class="article-inner">
            <div class="article-info"> <span class="icon icon-circle icon-calendar" aria-hidden="true"></span> 
              
              <h3>Kelly's Cycle Challenge - Sunday 14th May 2017</h3>
              <p>The Kelly's Cycle Challenge returns for 2017!</p>
              <p class="link">Read more</p>
            </div>
          </div>
          </a> </li>
        <li class="grid-item col-xs-12 col-sm-6 col-md-3"> <a class="article " href="#" title="Read more" style="background-image: url('img/fundraising-gifts-on-table.png');">
          <div data-type="View event" class="article-inner">
            <div class="article-info"> <span class="icon icon-circle icon-calendar" aria-hidden="true"></span> 
              
              <h3>Bexley Snap Motoring Event 2017 - Sunday 21st May 2017</h3>
            </div>
          </div>
          </a> </li>
        <li class="grid-item col-xs-12 col-sm-6 col-md-3"> <a class="article " href="#" title="Read more" style="background-image: url('img/holiday-scheme-two-young-people-in-masks.png');">
          <div data-type="View event" class="article-inner">
            <div class="article-info"> <span class="icon icon-circle icon-calendar" aria-hidden="true"></span> 
              
              <h3>Surrey 3 Peaks - Saturday 24th June 2017</h3>
            </div>
          </div>
          </a> </li>
        <li class="grid-item col-xs-12 col-sm-6 col-md-3"> <a class="article " href="#" title="Read more" style="background-image: url('img/holiday-scheme-young-man-petting-snake.png');">
          <div data-type="View event" class="article-inner">
            <div class="article-info"> <span class="icon icon-circle icon-calendar" aria-hidden="true"></span> 
              
              <h3>Join the 2017 Bexley Snap Prudential RideLondon Team</h3>
            </div>
          </div>
          </a> </li>
        <li class="grid-item col-xs-12 col-sm-6 col-md-3"> <a class="article " href="#" title="Read more" style="background-image: url('img/holiday-scheme-young-men-with-hedgehog.png');">
          <div data-type="View article" class="article-inner">
            <div class="article-info"> <span class="icon icon-circle icon-newspaper-o" aria-hidden="true"></span> 
             
              <h3>Simplyhealth go above and beyond for Bexley Snap</h3>
            </div>
          </div>
          </a> </li>
        <li class="clearfix"></li>
        <li class="col-md-4"> <a class="btn btn-alt btn-block btn-icon btn-bounce" href="#" title="Upcoming events"><span class="icon icon-calendar" aria-hidden="true"></span>Upcoming events</a> </li>
        <li class="col-md-4"> <a class="btn btn-alt btn-block btn-icon btn-bounce" href="#" title="Taking action"><span class="icon icon-bullhorn" aria-hidden="true"></span>Taking action</a> </li>
        <li class="col-md-4"> <a class="btn btn-alt btn-block btn-icon btn-bounce" href="#" title="Up to date news"><span class="icon icon-newspaper-o" aria-hidden="true"></span>Up to date news</a> </li>
      </ul>
    </div>
  </section>-->
  <!--<section class="section section-content section-last">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="wysiwyg wysiwyg-padded">
            <h2>Spring Term 2017</h2>
            <p>We have lots of exciting activities for all age groups this spring. <a href="#" target="_blank">Find out more</a>.</p>
            <p><a class="btn btn-alt btn-bounce" title="Book now" href="#" target="_blank">Book now</a></p>
          </div>
        </div>
        <div class="col-sm-6"> <img class="img-responsive" alt="" src="img/new-home-bottom.jpg" /> </div>
      </div>
    </div>
  </section>-->
</div>
<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script type='text/javascript' src='js/owl.carousel.js'></script>
<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>
<style>
    #owl-demo .item{
        display: block;
        cursor: pointer;
        background: #ffd800;
        padding: 30px 0px;
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
    }
    #owl-demo .item:hover{
      background: #F2CD00;
    }
    </style>
<script>
    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel({
        navigation : false,
        singleItem : true,
        transitionStyle : "fade",
		autoPlay : 5000,
		stopOnHover : false
      });

      //Select Transtion Type
      $("#transitionType").change(function(){
        var newValue = $(this).val();

        //TransitionTypes is owlCarousel inner method.
        owl.data("owlCarousel").transitionTypes(newValue);

        //After change type go to next slide
        owl.trigger("owl.next");
      });
    });
    </script> 
<script>
$(document).ready(function(){
    $(".list-accordion-home li").hover(function(){
		
        $(this).toggleClass("active");
    });
	
	 $("#ss").focusin(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','-90px');
    });
	 $("#ss").focusout(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','0px');
    });
	
	 $("#inputEmail").focusin(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','-110px');
    });
	 $("#inputEmail").focusout(function(){
		
        $(this).parents().siblings(".input-group-addon").children().css('margin-left','0px');
    });
	
	/*-------------dropdown navigation---------------*/
	
	$("#nav .primary-nav li").each(function(){
		$(this).click(function(){
			
			var cls = $(this).children("a").siblings("ul").hasClass("is-hidden");
			
			if(cls == true){
				hideall($(this));
				

			$(this).children("a").siblings("ul").removeClass("is-hidden");
			
			if($(this).children("a").children().hasClass("icon-chevron-down"))
			{
				$(this).children("a").children().removeClass("icon-chevron-down");
				$(this).children("a").children().addClass("icon-chevron-up");
			}
			else
			{
				$(this).children("a").children().removeClass("icon-chevron-up");
				$(this).children("a").children().addClass("icon-chevron-down");
			}
			}else{
				$(this).children("a").siblings("ul").addClass("is-hidden");
				$(this).children("a").children().removeClass("icon-chevron-up");
								$(this).children("a").children().addClass("icon-chevron-down");
			
			}
			
			
		});
		
	});
	
	
	
	function hideall(test){
		
		$("#nav .primary-nav li").each(function() {
			if($(this).children("a").children().hasClass("icon-chevron-up"))
			{
				$(this).children("a").children().removeClass("icon-chevron-up");
				$(this).children("a").children().addClass("icon-chevron-down");
			}
			
			$(this).children("ul").addClass("is-hidden");
		});
		
	}
	
	
	/*-------------dropdown navigation---------------*/
	$("#js-nav-toggle").click(function(){
		
        $(this).parents("#header").next().children().children().toggleClass("is-visible");
		
    });
	
$("#nav .primary-nav li a").click(function(){
		
        $(this).siblings("").toggleClass("is-hidden3");
		$(this).children().toggleClass("icon-chevron-up");
    });
	
	$("#nav .primary-nav li ul li:first-child").click(function(){
	$(this).parent().removeClass("is-hidden3");
	
    });
	
	$("#js-search-toggle").click(function(){
		
	$(this).parent().siblings("#js-form-search").slideToggle();
	
    });
	

  $('.video-placeholder').on('click', function(ev) {
	$(this).fadeOut('fast'); 
 
    $("#video")[0].src += "?autoplay=1";
    ev.preventDefault();
	
  
  });

	
});
</script>
</body>
</html>