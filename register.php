<?php
session_start();
if(isset($_SESSION['frontuserid']) && $_SESSION['frontuserid'] != '')
{
	header("location:dashboard.php");
	exit;
}
include_once('admin/db/dbopen.php');
$result = isset($_GET['result']) && $_GET['result'] != ''?decrypt($_GET['result'],$encrypt) : '';
if(isset($_GET['done']))
{
	$result = $_GET['done'];
}

$pupil = array();
/*$sel_pupil = "Select * FROM ".$tbname."_pupilmaster";
$run_pupil = mysqli_query($con , $sel_pupil);*/
?>
<html>
<head>
<title>bexleysnap</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<?php 
include_once('topscript.php');
?>
	<style type="text/css">
		.log{
			background-color: #F8A848;
		}
		.error {color : #ff0000;}
		.fntclr {color:#fff; font-size:24px;text-align:right;}
		.rowmrgn{margin-top : 10px;}
	</style>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
		<div class="container-fluid">
			<div class="row">
				<?php
				if(!isset($_SESSION['frontuserid']))
				{
				?>
            	<div  class="col-md-12 log">
            		<div class="form_wrapper animated-short" id="login_form">
		                <form name="frmlogin" id="frmlogin" method="post" action="registeraction.php" onsubmit="return validate()">

			                <div class="row">
		                		<div class="col-md-12 col-sm-12">
		                			<h2 style="color: #fff;text-align:center;">Registration Form</h2>
		                		</div>
			                </div>

			                <div class="row">
		                		<div class="col-md-12 col-sm-12">
		                			<h5 style="color: #fff;text-align:center;"><br>Download our <a style="color: green;" href="http://techcitywebdesign.co.uk/Bexley/PDFs/Referral%20Form.pdf" target="_blank">Self-referral form</a> to become a member of Bexley SNAP.<br>Once completed, you can post the form to the address below, or email to <a style="color: green;" href="mailto:admin@bexleysnap.org.uk">admin@bexleysnap.org.uk</a>.</h5>
		                		</div>
			                </div>
			                

							<?php
							if($result != '')
							{
								
								if(decrypt($result,$encrypt) == '5')
								{
								?>
								<div class="col-md-12">
									<div class="form-group">
										<label for="txtlname" class="col-sm-5 col-md-5 control-label fntclr"></label>
										<div class="col-sm-7 col-md-7">	
											<span class='error'>Email address already exist.</span>
										</div>
									</div>
								</div>
								<?php
								}
								else
								{
								?>

								<div class="col-md-12">
									<div class="form-group">
										<label for="txtlname" class="col-sm-5 col-md-5 control-label fntclr"></label>
										<div class="col-sm-7 col-md-7">	
											<span class='error'>incorrect email or password</span>
										</div>
									</div>
								</div>
								<?php
								}
								
							}else if($result == '02')
							{
								?>
								<div class="col-md-12">
									<div class="form-group">
										<label for="txtlname" class="col-sm-5 col-md-5 control-label fntclr"></label>
										<div class="col-sm-7 col-md-7">	
											<span class='error'>Error in adding child.</span>
										</div>
									</div>
								</div>
								<?php
							}

							?>
							<div class="form-group">
								<input type="hidden" name="usertype" value="<?php echo encrypt('1',$encrypt); ?>">
							</div>
							<h3 class="heading_a" style="text-align:center;"><span class="heading_text">Login Credentials</span></h3>
							<h5 class="heading_a" style="text-align:center;"><span class="heading_text">Please enter an email and a password here. You will use these details to login to our platform in the future.</span></h5>
			                <div class="form-group">
                                            <label for="txtemail" class="col-sm-5 col-md-5 control-label fntclr">Email <font style="color: red;">*</font></label>
                                            <div class="col-sm-7 col-md-7">
                                                <input type="email" class="form-control" id="txtemail" name="txtemail" value="<?php echo $email;?>"  style="width:320px;" required>
                                            </div>
											
                                        </div>
		                	  <div class="form-group">
                                            <label for="pro_new_password"  class="col-sm-5 col-md-5 control-label fntclr">Password <font style="color: red;">*</font></label>
                                            <div class="col-sm-7 col-md-7">
                                                <input type="password" class="form-control" id="txtpassword" name="txtpassword" value="<?php echo $password; ?>" style="width:320px;" required>
                                            </div>
                                           
                                        </div>

                                        <div class="form-group">&nbsp;</div>

			                			<h3 class="heading_a" style="text-align:center;"><span class="heading_text">Carers</span></h3>
                                        
                                        <div class="form-group">
                                            <label for="carer1"  class="col-sm-5 col-md-5 control-label fntclr">Carer 1 <font style="color: red;">*</font></label>
                                           <div class="col-sm-7 col-md-7">
                                                
                                                <input type="text" class="form-control" id="carer1" name="carer1"  value="<?php echo isset($row['_Carer1'])?$row['_Carer1']:''; ?>" style="width:320px;" required>
                                            </div>
										
										</div>


										 <div class="form-group">
                                            <label for="carer2"  class="col-sm-5 col-md-5 control-label fntclr">Carer 2 <font style="color: red;">*</font></label>
                                           <div class="col-sm-7 col-md-7">
                                                
                                                <input type="text" class="form-control" id="carer2" name="carer2"  value="<?php echo isset($row['_Carer2'])?$row['_Carer2']:''; ?>" style="width:320px;" required>
                                            </div>
										
										</div>

										<div class="form-group">&nbsp;</div>

                                        <h3 class="heading_a" style="text-align:center;"><span class="heading_text">General info</span></h3>
                                       
                                        <div class="form-group">
                                            <label for="txtlname" class="col-sm-5 col-md-5 control-label fntclr">Family Name <font style="color: red;">*</font></label>
                                           <div class="col-sm-7 col-md-7">
                                                <input type="text" class="form-control" id="txtlname" name="txtlname" value="<?php echo $lname;?>" style="width:320px;" required>
                                            </div>
											
                                        </div>
                                        
										
										 <div class="form-group">
                                            <label for="txtzip" class="col-sm-5 col-md-5 control-label fntclr">Parent / Carer</label>
                                              <div class="col-sm-7 col-md-7">
												<select class="form-control" style="width:320px;" name="txtguardian">
  													<option value="Parent">Parent</option>
  													<option value="Carer">Carer</option>
												</select>
                                            </div>
                                           
                                        </div>



	                               <!--          <div class="form-group">
	                                            <label for="txtzip" class="col-sm-5 col-md-5 control-label fntclr">Parent / Carer</label>
	                                            <div class="col-sm-9">
													<input type="radio" class="form-control" name="txtguardian" id="txtguardian" value="Parent" > Parent
													<input type="radio" class="form-control" name="txtguardian" id="txtguardian" value="Carer" > Carer
	                                            </div>
	                                        
	                                        </div> -->



										 
										
										
                                        <div class="form-group Children">
                                            <label for="txtzip" class="col-sm-5 col-md-5 control-label fntclr">Children</label>
                                             <div class="col-sm-7 col-md-7">
                                                 <input type="text" name="txtchildname[]" id="txtchildname0" class="form-control" style="width:320px;">
                                                 <button type="button" class="btn-primary" id="add_more_child" name="add_more_child">+</button>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label for="txtphone" class="col-sm-5 col-md-5 control-label fntclr">Phone</label>
                                            <div class="col-sm-7 col-md-7">
                                                <input type="" class="form-control" id="txtphone" name="txtphone" value="<?php echo $phone;?>" style="width:320px;">
                                            </div>
											
                                        </div>

                                        <div class="form-group">
                                            <label for="txtphone" class="col-sm-5 col-md-5 control-label fntclr">Mobile</label>
                                            <div class="col-sm-7 col-md-7">
                                                <input type="" class="form-control" id="txtphone" name="txtmobile" value="<?php echo $phone;?>" style="width:320px;">
                                            </div>
											
                                        </div>
                                         <div class="form-group">
                                            <label for="txtadd1" class="col-sm-5 col-md-5 control-label fntclr">Address 1</label>
                                             <div class="col-sm-7 col-md-7">
                                                 <input type="text" class="form-control" name="txtadd1" id="txtadd1" value = "<?php echo $address1; ?>" style="width:320px;">
                                            </div>
											
                                        </div>
                                        <div class="form-group">
                                            <label for="txtadd2" class="col-sm-5 col-md-5 control-label fntclr">Address 2</label>
                                             <div class="col-sm-7 col-md-7">
                                                 <input type="text" class="form-control" name="txtadd2" id="txtadd2" value="<?php echo $address2; ?>" style="width:320px;">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtstate" class="col-sm-5 col-md-5 control-label fntclr">Town</label>
                                             <div class="col-sm-7 col-md-7">
                                                 <input type="text" class="form-control" name="txtstate" id="txtstate" value="<?php echo $state; ?>" style="width:320px;">
                                            </div>
											
                                        </div>
                                        <div class="form-group">
                                            <label for="txtcountry" class="col-sm-5 col-md-5 control-label fntclr">County</label>
                                             <div class="col-sm-7 col-md-7">
                                                 <input type="text" class="form-control" cols="10" rows="4" name="txtcountry" id="txtcountry" value="<?php echo $country; ?>" style="width:320px;">
                                            </div>
										</div>
                                        <div class="form-group">
                                            <label for="txtzip" class="col-sm-5 col-md-5 control-label fntclr">Postcode</label>
                                             <div class="col-sm-7 col-md-7">
                                                 <input type="text" class="form-control" cols="10" rows="4" name="txtzip" id="txtzip" value="<?php echo $postcode; ?>" style="width:320px;">
                                            </div>
											
                                        </div>
										

							<div class="col-md-12 rowmrgn" style="padding-bottom:20px;">
								<div class="form-group">
									<div class="col-sm-5 col-ms-5"></div>
									<div class="col-sm-7 col-md-7">
										<input type="submit" class="btn btn-alt btn-sml btn-bounce" name="btnsubmit" id="btnsubmit" value="Register" style="width:145px">
										<input type="reset" class="btn btn-alt btn-sml btn-bounce" name="reset" id="reset" value="Reset" style="margin-left:5px;width:145px">
									</div>
			                	</div>
			                </div>
			                <input type="hidden" name="e_action" value="<?php echo encrypt('add',$encrypt); ?>" />
		                </form>
		            </div>
            	</div>
				<?php } ?>
			</div>
		</div>
<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script type="text/javascript">
	function validate(){
		var err = '';
		if(document.frmlogin.email.value == '')
		{
			err += 'Please Enter Email \n';
		}
		if(document.frmlogin.password.value == '')
		{
			err += 'Please Enter Password';
		}
		
		if(err != '')
		{
			alert(err);
			return false;
		}
		return true;
	}

	$(document).ready(function(){
		var cnt = 0;
		$('#add_more_child').click(function(){
				cnt++;	
				var childlength = $('#frmlogin').find("input[id^='txtchildname']").length;
				//var inputelement = '<input type="text" name="'txtchildname+ childlength +'">';
				$('#add_more_child').before('<input type="text" name="txtchildname[]'+cnt+'" class="form-control" style="width:320px;">' );
					
		});
		
	});
</script>
</body>
</html>