<?php
session_start();
include_once('admin/db/dbopen.php');

include_once('topscript.php');

$id = str_replace(" ","+",$_REQUEST['id']);
$id = decrypt($id,$encrypt);

$qry = "SELECT pb.*, concat(cm._Firstname,' ',cm._Lastname) as clientname, concat(pm._FirstName,' ',pm._LastName) as pupilname  FROM ".$tbname."_paypalbooking as pb left join ".$tbname."_clientmaster as cm on cm._ID=pb._CID left join ".$tbname."_pupilmaster as pm on pm._ID=pb._PID WHERE pb._ID = '".$id."' ";

$rs = mysqli_query($con,$qry);

$row = mysqli_fetch_assoc($rs);
$clientname = $row['clientname'];
$pupilname = $row['pupilname'];
$slot = $row['_Tsids'];

$slot = explode(",", $slot);

?>
<html>
<head>
<title>bexleysnap</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />

<style>
.list-teasers.row a:hover{
	color:#fff;
}
.list-teasersxx li a {
    color:#fff;
}
.list-teasers li a:hover {
    color:#fff;
}
.club_info
{
	text-align:left!important;
}

table.club_info { 
    border-spacing: 0;
    border-collapse: collapse;
}
.club_info table { 
    border-spacing: 0;
    border-collapse: collapse;
}

.club_info td
{
	text-align:left!important;
}
.club_info td:nth-child(1) {  
  /* your stuff here */
  font-weight:bold;
}


.club table{
	text-align:left!important;
	padding-top:10px;
}

.club table { 
    border-spacing: 0!important;
    border-collapse: collapse!important;
}

table.club
 { 
    border-spacing: 0;
    border-collapse: collapse;
}

.club tbody{
	padding:0;
	margin:0;
}

.club td{
	text-align:left!important;
}

.club td:nth-child(1) {  
  font-weight:bold;
  width:200px;
  padding-right:0;
}

.club td:nth-child(2) {  
  width:700px;
}

</style>
</head>
<body>
<header id="header" role="banner">
<?php 
include_once('header.php');
?>
</header>
<?php include_once('menu.php');
?>

<div id="js-main-content" class="main-content"> 
	<section class="section section-content">
	    <div class="container">
	    	
	    	<div class="row">
	    		<div class="col-md-12">
	    			<center><h2>Thanks For your order</h2></center>
	    		</div>
	    	</div>

	    	<div class="row">
	    		<div class="col-md-12">&nbsp;</div>
	    	</div>		

	    	<div class="row">
	    		<div class="col-md-12">
	    			<h5><center><?php echo $_SESSION['frontuserdata']['_Firstname'].' '.$_SESSION['frontuserdata']['_Lastname'];?> has done the transaction for <?php echo $pupilname;?></center></h5>
	    		</div>
	    	</div>

	    	<?php
	    	$query  = "SELECT bt._ID,bt._Type,bt._Price FROM ".$tbname."_timeslot ts inner join ".$tbname."_bookingtype as bt on bt._ID = ts._Bookingtype WHERE ts._ID IN ('" . implode("','",$slot) . "') group by ts._Bookingtype order by bt._Type";
	    	
			$run 	= mysqli_query($con , $query);
			$nrun = mysqli_num_rows($run);
			?>
	    	
    		<div class="col-md-12">
    			
    			<table class="table">
					<?php
					if($nrun > 0)
					{
						$prctot = 0;
						while($fetch = mysqli_fetch_assoc($run))
						{
							$prc = 0;
							$prc = $fetch['_Price'];
							?>
							<tr>	
								<td>&nbsp;</td>
							</tr>
							<tr>	
								<td><b><u><?php echo $fetch['_Type']; ?></u></b></td>
							</tr>
							<?php

							$qrsts = "SELECT * FROM ".$tbname."_timeslot WHERE _ID IN ('" . implode("','",$slot) . "') and _Bookingtype = '".$fetch['_ID']."' order by _Date";

							$rsts = mysqli_query($con,$qrsts);
							$nsts = mysqli_num_rows($rsts);

							if($nsts > 0)
							{
							?>
							<tr>
								<td>
									<table class="table">
										<thead>
											  <tr>
											  	<th>Sr no.</th>
												<th>Date</th>
												<th>From Time</th>
												<th>To Time</th>
												<th>Price</th>
											  </tr>
										</thead>
										<tbody>
										<?php
										while($fetchts = mysqli_fetch_assoc($rsts))
										{
											$cnt++;
											?>
											<tr>
												<td><?php echo $cnt; ?></td>
												<td><?php echo date("d-M-Y",strtotime($fetchts['_Date']));?></td>
												<td><?php echo $fetchts['_Fromtime']; ?></td>
												<td><?php echo $fetchts['_Totime']; ?></td>
												<td>&#163;<?php echo number_format((float)$prc, 2, '.', ''); ?></td>
											</tr>
											<?php
											$prctot += $prc;
										}?>

										</tbody>
									</table>
								</td>
							</tr>
							<?php	
							}
						}
						?>

						<tr>	
							<td>
								<table class="table">
									<thead>
										  <tr>
										  	<th>&nbsp;</th>
										  	<th>&nbsp;</th>
										  	<th>&nbsp;</th>
										  	<th>&nbsp;</th>
										  	<th style="text-align: right;padding-right:100px;"><b><u>Total : &#163;<?php echo number_format((float)$prctot, 2, '.', ''); ?></u></b></th>
										  </tr>
									</thead>
								</table>	
							</td>
						</tr>
					<?php	
					}
					?>
				</table>
    		</div>	

    		<div class="row">
	    		<div class="col-md-12">
	    			<center><input type="button" name="ok" value="ok" onclick="location.href = 'index.php';"></center>
	    		</div>
	    	</div>

	    </div>
	
	</section>

</div>
<footer id="footer" role="contentinfo"><?php include_once('footer.php');?></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script type='text/javascript' src='js/owl.carousel.js'></script>
<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>
<style>
    #owl-demo .item{
        display: block;
        cursor: pointer;
        background: #ffd800;
        padding: 30px 0px;
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
    }
    #owl-demo .item:hover{
      background: #F2CD00;
    }
    </style>

</body>

</html>