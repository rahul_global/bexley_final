  <div class="container">
    <h1 class="site-title"> <a href="index.php" rel="home" title="Bexley Snap"><img alt="bexleysnap" src="img/logo.png" /></a> </h1>
    <div class="header-utilities">
	
	<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
	
      
      <div class="header-utilities-mobile"> <a href="mailto:information@disability-challengers.org" title="Email us"><span class="sr-only">Email us</span><span class="icon icon-email" aria-hidden="true"></span></a> <a id="js-search-toggle" title="Search the site"><span class="sr-only">Show search</span><span class="icon icon-search" aria-hidden="true"></span></a> </div>
      
      <ul class="list-utilities-contact">
        <li>Telephone: <a href="tel:01322334192">01322 334 192</a></li>
        <li>Email: <a href="mailto:admin@bexleysnap.org.uk" title="Email us">admin@bexleysnap.org.uk</a></li>
		<?php
		if(isset($_SESSION['frontuserid']) && $_SESSION['frontuserid'] != ''){
		?>
        <li><a href="logout.php" title="Contact us">Logout</a></li>
		<?php } else { ?>
		<li><a href="login.php" title="Login" id="login">Login</a></li>
    <li><a href="register.php" title="Register" id="register">Register</a></li>
		<?php } ?>
      </ul>
      <form id="js-form-search" class="form-search" action="http://disability-challengers.org" method="get">
        <div class="input-group input-group-h"> <span class="input-group-addon" style="padding:0px;">
          <label class="form-label form-label-sml" style="color: #fff;margin-top:-1px;">Search</label>
          </span> <span class="form-control-wrapper">
          <input id="ss" class="form-control form-control-sml" name="s" type="text" placeholder=""  style="border-color: rgb(0, 84, 166);" />
          </span> <span class="input-group-btn">
          <button class="btn btn-sml" title="Go" type="submit" style="margin-top:-1px;"><span class="sr-only">Go</span><span class="icon icon-search" aria-hidden="true"></span></button>
          </span> </div>
      </form>
    <!--  <a class="btn btn-sml btn-bounce" href="#" title="Donate now">Donate now</a> <a class="btn btn-alt btn-sml btn-bounce" href="#" title="Join us">Join us</a>-->
    <ul class="list-social-icons">
        <li><a href="#" target="_blank" title="Follow us on Facebook"><span class="icon icon-facebook" aria-hidden="true"></span></a></li>
        <li><a href="#" target="_blank" title="Tweet us on Twitter"><span class="icon icon-twitter" aria-hidden="true"></span></a></li>
      </ul>
     </div>
  </div>